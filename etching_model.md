# Etching - diamond_lattice kMC

## Setting of per NN etch rates (`initialise.f90`)

```fortran
! NN=0 not used as not physical
do NN=0,4
	etch_rates(NN) = calc_etch_rate(NN, Aetch, Eetch, Ts, dGG) * (Hconc/NA)
end do
```

```fortran
! Return NN dependent etch rate
real(dp) function calc_etch_rate(NN,A,E,T,scaling)

	rate_out = (A * EXP(-E/(Rgas*T)))/(scaling**(real(NN)))
	
end function calc_etch_rate
```

```fortran
if (etch_model == "N_N") then
	etch_rates(4) = 0.0_dp
	etch_norm = rDimerInsertCHX(1) * etch_factor
	etch_iso = etch_norm * isoEtch_factor
else if (etch_model == "bat") then
	etch_rates(2:4) = 0.0_dp
else if (etch_model == "T_N") then
	etch_rates(3:4) = 0.0_dp
endif
```

## Rate Setting (CHx)

```fortran
subroutine set_etch_rate(atom, rate_list)
	if (atom_is_linked) return
	call count_nearest_atoms(atom, NN)
	
	if (etch_model == 'N_N') then
		if (part_of_dimer) NN = NN + 1
		if (NN < NN_bulk-1) then !!NN=[0-2]
			!Check local non-bonding env
			if (is_isolated) then
				rate_value = etch_iso	!etch_norm * isoEtchFactor
            else
            	rate_value = etch_norm	!10% of dimer insertion rate
			end if
		end if
	else	! etch_model == ['bat', 'T_N']
		if (part_of_dimer) return
		if (NN /= 2) return	!Must be bonded as bridging (2 C-C bonds)
		if (not_unbonded_above) return	!N1,N4 must be empty
		if (neigh_2_3bonds .and. neigh_3_3bonds) then
			if (etch_model == 'bat') then
				rate_value = etch_rates(0) ! Not NN dependent - rapid etching 
            else !etch_model == 'T_N'
            	!T_N dependent on local env
            	rate_value = etch_rates(TN)
			end if
		end if
	end if

end subroutine set_etch_rate	
```

### T_N (TN scaling [0-2])

0. 1E+4
1. 1E+3
2. 1E+2

scaling could be more aggressive

### N_N (No scaling, `etch_norm`, `etch_iso`)

### Battaile1999 ('bat') [no scaling, binary]

possible: 1E+4
else: 0E+0

### Comments

N_N is now defunct, it used etch_norm and etch_iso which are empirically set to the dimer insertion rate, independent of hydrogen concentration.  

Might be a bug with bat where NN=2 is also being set to 0.0 - this is irrelevant as only etch_rates(0) is accessed by this model.

## Plan

## Battaile

Preferential etching model: highly aggressive etching of `isolated' CH2 surface species, no etching of atoms with more than 2 C-C bonds or those which are part of a group of dimers but not directly bonded.

### Battaile-Modified

Prevent etching of atoms which do not allow reconstruction of the dimer below once the atom etches. Reduces overall etching but also increases preference for atoms along a dimer row as only these can etch now. Take into account local environment to SCALE the etch rate not prevent it.

### Nearest Neighbour

A model where etch rates scales with local environment without any cut offs. Expect atoms within the bulk of a terrace to etch extremely slowly, step-edges to etch preferentially along different directions (according to literature) and isolated to etch relatively quickly, although migration of multiple steps should occur if possible.

