# Diamond Lattice Data Plotting / Scripting
## Data
The kmc-cvd (known as diamond lattice) simulation code produces a number of main output files:
- diamond.out
- diamond.json
- kmc_viz.xyz
- final_surface.xyz
in addition to per time/per iteration surface statistics files (when switched on):
- growth_rate.csv
- height.csv
- etc.

diamond.out represents a human readable outputfile for a single simulation while diamond.json is a computer readable version and contains additional data on the contents of the rate registries etc.

### Diamond Collection
When a sample of diamond_lattice simulations have been carried out, these diamond.json files can be aggregated into a single <all_data>.csv using the python script: python_scripts/read_diamond_json.py
read_all_outputs.py without arguments will find all diamond.json files which also containing a simulation.input file and read the diamond.json file into a row into the overall .csv simulation file.

These overall simulation files can then be used by plotting tools to obtain statistical analysis on the overall simulation samples.

### Plotting
You are free to create to use your own means of turning .csv files of simulations into analysis and figures, if you wish to reuse or rerun plotting and analysis tools which you might have in your possession, you will need to obtain a copy of python3 (Anaconda python is ideal for this purpose) as well as the python module jupyter lab (or notebook). Analysis and plotting uses jupyter notebooks (.ipynb)

#### Windows Instructions
- Open cmd and navigate to where the plotting scripts are located (e.g downloads\max_plotting)
- Run the command "python3 -m jupyter lab"
	- you will need to know where your copy of python is stored, for windows it is usually in program files\anaconda\python3.exe
- A webpage should open in your browser, which will allow you to navigate to a specific .ipynb file and open it interactively

### Plotting scripts/notebooks specifics
The top of each notebook contains module imports but a means of locating where all the helper modules I have created sit (inside the diamond lattice/kmc-cvd codebase) and the location of the data to be plotted

#### Plotting modules/data location
```
DL_DATA = os.environ.get('DL_DATA')
PYTHON_SCRIPTS = os.environ.get('PYTHON_SCRIPTS')
```
The top of the data directory structure and python script structure is set using Environment variables. For these scripts to work, you will need to set for your system set-up. For reference, mine were:
- `C:\\Users\\Max Work\\OneDrive - University of Bristol\\Documents\\diamond_lattice\\data`
- `'C:\\Users\\Max Work\\local\\dev\\diamond_lattice\\python_scripts`

## Other useful scripts
On your computing cluster the environment variables $DL_SCRIPTS must be set to point to the scripts directory in the code repository.
- create_numbered_directories.sh: for an input directory name (no trailing /) create N number of copies labelled directory_name_rN - useful for creating simulation samples
- python_scripts/submit_job.py: find all .sh files below and submit all to the grendel queue for -W walltime (-h gives help)
- scripts/clean_below.sh: removal all simulation outputs in directories below, ready for a new simulation run (a number of similar scripts also in this directory)

