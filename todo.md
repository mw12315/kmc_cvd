# Todo Diamond Lattice

## Dev
- "ASSERTS (outside of tests) which error stop on fails should be added, but only run when "USE_ASSERTS" is present
    - Well a pure function is being used, such asserts should exist directly following their use to prevent any IO
- Remove ilat attribute in rate_type type - not used
- Error/Warning logging mechanism (centralised/consistent) 
- Create error handling subroutine
