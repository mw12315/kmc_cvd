# diamond_lattice a.k.a. kMC v2
Kinetic Monte Carlo model of CVD diamond growth, allowing step-by-step analysis and visualisation both during and after simulation.

## Pre-requisites 
- <a href="https://github.com/jacobwilliams/json-fortran">json-fortran</a>

### Building
Requirements
cmake > 3.12.4
5.4 < GCC <= 7.3
#### Cygwin/MSYS2
```bash
$ mkdir -p path/to/build_dir
$ cd /path/to/build_dir
$ cmake -G "Unix Makefiles" \
-DCMAKE_Fortran_COMPILER=path/to/gfortran \  
-DCMAKE_SYSTEM_NAME=Generic \
-DCMAKE_BUILD_TYPE=Release/Debug \
-DPROFILE=ON/OFF \
-DJSONFORTRAN_PATH=path/to/json-fortran/build_dir \
path/to/CMakeLists.txt\
$ make -j
```
#### Unix
```bash
$ mkdir -p path/to/build_dir
$ cd /path/to/build_dir
$ cmake -DCMAKE_Fortran_COMPILER=path/to/gfortran \  
-DCMAKE_BUILD_TYPE=Release/Debug \
-DPROFILE=ON/OFF \
-DJSONFORTRAN_PATH=path/to/json-fortran/build_dir \
path/to/CMakeLists.txt\
$ make -j
```

#### Execution ####
1) Ensure all input files present in directory
2) $ /path/to/binary

### Input Files ###
Required input files: INPUT_FILES/\* + INPUT_FILES/0_B/<growth_condition>/\* 

- energy.input - energetics (other than etching)
- output.input - outputs and frequency
- reactions.input - switch reactions/processes on and off
- simulation.input - control length of simulations
- conditions.input - growth condition input
- etching.input - etching energetics
- bookmarking.input - surface atoms, lattice initialisation information for simulation continuation
- bookmarking.geom - surface to initialise from if resuming from bookmarking

## Steps For Running a Series of Simulations on Grendel
1. Copy input files into a target directory and edit accordingly
2. Create copies of this directory (using create_numbered_dirs.sh if useful) to create required number of simulations in the sample
2. Copy submit script (scripts/submission/go_diamond.sh) into directories and rename accordingly. scripts/submission_dirs.py might be useful here (python3 submission_dirs.py -h for help)
3. Submit each directory into the queue using qsub (python_scripts/submit_job.py -w 10:00:00)
4. Once all jobs have completed python_scripts/read_diamond_json.py can be used to collect all diamond.json into a directoryname_all.csv file for analysis and plotting