#!/bin/bash
set -e

#----------------------------------------
# EDIT ME
#----------------------------------------
# SOURCECODE SAVE LOCATION $HOME/local/src
SRC_LOCATION=""
# INSTALL LOCATION (binary) $HOME/local/bin
KMC_INSTALL_LOCATION=""
#----------------------------------------

# cmake and gfortran are required for building
eval "cmake --version"
eval "gfortran --version"

if [[ -z $SRC_LOCATION ]]; then
  echo "\$SRC_LOCATION must be set (suggested: $HOME/local/src)"
  FAIL="\$SRC_LOCATION"
fi

if [[ -z $KMC_INSTALL_LOCATION ]]; then
  echo "\$KMC_INSTALL_LOCATION  must be set (suggested: $HOME/local/bin)"
  FAIL="$FAIL \$KMC_INSTALL_LOCATION"
fi

if [[ -n $FAIL ]]; then
  echo 
  echo "Set $FAIL in install script to continue!"
  echo 
  exit 1
fi

# BUILD LOCATION
JSON_FORTRAN_BUILD_LOCATION=${SRC_LOCATION}/json-fortran/Build
echo "KMC source code location: ${SRC_LOCATION}"
echo "json-fortran source code location: ${SRC_LOCATION}"
echo "kmc binary location: ${KMC_INSTALL_LOCATION}"
echo "json-fortran build location: ${JSON_FORTRAN_BUILD_LOCATION}"

# Make target directories
echo "Create sourcecode and install directories"
mkdir -p ${SRC_LOCATION}
mkdir -p ${KMC_INSTALL_LOCATION}
mkdir -p ${SRC_LOCATION}

# Clone json-fortran v7.1
cd ${SRC_LOCATION}
git clone --depth 1 --branch master https://github.com/jacobwilliams/json-fortran.git 
JSON_FORTRAN_DIR="${PWD}/json-fortran"
cd $JSON_FORTRAN_DIR
git checkout 7.1.0
mkdir -p ${JSON_FORTRAN_BUILD_LOCATION}

# Build json-fortran library
echo "Building json-fortran"
cd ${JSON_FORTRAN_BUILD_LOCATION}

FC=/share/apps/gcc/7.3.0/bin/gfortran cmake $JSON_FORTRAN_DIR
make -j 4

# Build KMC binary (diamond_lattice)
echo "Building kmc"
cd ${KMC_DIR}
mkdir -p Release
cd Release
cmake -DCMAKE_Fortran_COMPILER=/share/apps/gcc/7.3.0/bin/gfortran -DCMAKE_BUILD_TYPE=Release -DPROFILE=OFF -DJSONFORTRAN_PATH=${JSON_FORTRAN_BUILD_LOCATION} -DCMAKE_RUNTIME_OUTPUT_DIRECTORY=${KMC_INSTALL_LOCATION} ${KMC_DIR}

make diamond -j 4

echo "Built diamond lattice to binary 'diamond' to ${KMC_INSTALL_LOCATION}"

echo "Building and running tests"
echo "--------------------------"
make test_diamond -j 4 &> /dev/null

cd $KMC_DIR/test_diamond
$KMC_INSTALL_LOCATION/test_diamond

echo "PASSED!"
