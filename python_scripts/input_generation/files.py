import re
from abc import abstractmethod


class Species(object):
    def __init__(self, conc_in_mols_per_cm3, s, g):
        self.conc_in_mols_per_cm3 = conc_in_mols_per_cm3
        self.s = s
        self.g = g

    def __str__(self):
        return "{:.2e}, {:.3f}, {:.3f}".format(self.conc_in_mols_per_cm3, self.s, self.g)


class Reaction(object):
    def __init__(self, A, E):
        self.A = A
        self.E = E

    def __str__(self):
        return "{:.3e}, {:.3e}".format(self.A, self.E)


# todo add __str__ override
class SpeciesReactions(object):
    def __init__(self, CH3: bool = True, CH2: bool = True, CH: bool = True, C: bool = True,
                 C2a: bool = True, C2X: bool = True, C2H2: bool = False):
        self.CH3 = CH3
        self.CH2 = CH2
        self.CH = CH
        self.C = C
        self.C2a = C2a
        self.C2X = C2X
        self.C2H2 = C2H2

    def as_iter(self):
        return self.CH3, self.CH2, self.CH, self.C, self.C2a, self.C2X, self.C2H2


# todo add __str__ override
class MigrationReactions(object):
    def __init__(self, critical_nucleus: int = 1,
                 dimer: bool = True, to_gap: bool = True, from_gap: bool = True, down: bool = True):
        self.down = down
        self.from_gap = from_gap
        self.to_gap = to_gap
        self.dimer = dimer
        self.critical_nucleus = critical_nucleus


# todo add __str__ override
class C2H2Reactions(object):
    def __init__(self, M1: bool = False, M2: bool = False, M3a: bool = False, M3b: bool = False,
                 migrate: bool = False, etchCH3: bool = False, etchC2H2: bool = False):
        self.M1 = M1
        self.M2 = M2
        self.M3a = M3a
        self.M3b = M3b
        self.migrate = migrate
        self.etchCH3 = etchCH3
        self.etchC2H2 = etchC2H2

# todo add __str__ override
class DimerAlign(object):
    def __init__(self, align_on: bool = False, align_range: int = 0, realign_on: bool = False):
        self.align_on = align_on
        self.align_range = align_range
        self.realign_on = realign_on


class ChxInsertion(object):
    def __init__(self, dimer: bool = True, trough: bool = True):
        self.dimer = dimer
        self.trough = trough


class Etching(object):
    def __init__(self, active: bool = True, etch_factor: int = 10,
                 isolated_NN_threshold: int = 0, isolated_etch_factor: int = 10):
        self.active = active
        self.etch_factor = etch_factor
        self.isolated_NN_threshold = isolated_NN_threshold
        self.isolated_etch_factor = isolated_etch_factor


class DLatInputFile(object):
    def update_value(self, param, new_value):
        instance, instance_field = self.parse_param(param)
        if instance is None and instance_field is None:
            return None
        elif instance is None:  # Ts or Tns
            setattr(self, instance_field, new_value)
        else:  # Species object
            species_field_ref = InstanceRef(instance, instance_field)
            species_field_ref.set(new_value)

    @abstractmethod
    def parse_param(self, param):
        raise NotImplementedError


def default(condition_id: str = 'mcd_hf_0_B'):
    return ConditionsInputFile.from_cond_id(condition_id)


class ConditionsInputFile(DLatInputFile):
    avail_conditions = ['scd_mw_0_5', 'scd_mw_0_A', 'scd_mw_0_B',
                        'mcd_hf_0_5', 'mcd_hf_0_A', 'mcd_hf_0_B',
                        'ncd_hf_0_5', 'ncd_hf_0_A', 'ncd_hf_0_B',
                        'uncd_hf_0_5', 'uncd_hf_0_A', 'uncd_hf_0_B',
                        'uncd_mw_0_5', 'uncd_mw_0_A', 'uncd_mw_0_B']

    def __init__(self, input_dict):
        self.cond_id = input_dict['cond_id']
        self.Ts = input_dict['Ts']
        self.Tns = input_dict['Tns']
        self.H: Species = input_dict['H']
        self.H2: Species = input_dict['H2']
        self.CH3: Species = input_dict['CH3']
        self.CH2: Species = input_dict['CH2_sing']
        self.CH: Species = input_dict['CH']
        self.C: Species = input_dict['C']
        self.C2_a: Species = input_dict['C2_a']
        self.C2_X: Species = input_dict['C2_X']
        self.C2H2: Species = input_dict['C2H2']

        self.file_name = "conditions.input"

    @staticmethod
    def from_cond_id(cond_id):
        if cond_id not in ConditionsInputFile.avail_conditions:
            print("No such condition: {}".format(cond_id))
            raise NotImplementedError

        file_dict = {'cond_id': cond_id}
        if cond_id in ['scd_mw_0_5', 'scd_mw_0_A', 'scd_mw_0_B']:
            file_dict['Ts'] = 973
            file_dict['Tns'] = 1736 if "0_5" in cond_id else file_dict['Ts']
            file_dict['H'] = Species(3.38E16, 0.0, 0.0)
            file_dict['H2'] = Species(9.33E17, 0.0, 0.0)
            if cond_id == 'scd_mw_0_5':
                file_dict['CH3'] = Species(3.24E13, 1.0, 1.0)
                file_dict['CH2_sing'] = Species(5.97E10, 1.0, 1.0)
                file_dict['CH'] = Species(1.6E11, 1.0, 1.0)
                file_dict['C'] = Species(1.41E12, 1.0, 1.0)
                file_dict['C2_a'] = Species(4.19E10, 0.0, 0.0)
                file_dict['C2_X'] = Species(1.12E10, 0.0, 0.0)
                file_dict['C2H2'] = Species(2.96E16, 1.0, 0.0)
            elif cond_id == 'scd_mw_0_A':
                file_dict['CH3'] = Species(1.0E13, 1.0, 0.25)
                file_dict['CH2_sing'] = Species(1.0E8, 1.0, 0.25)
                file_dict['CH'] = Species(5E8, 1.0, 0.25)
                file_dict['C'] = Species(1E10, 1.0, 0.25)
                file_dict['C2_a'] = Species(1E6, 0.0, 0.0)
                file_dict['C2_X'] = Species(1E5, 0.0, 0.0)
                file_dict['C2H2'] = Species(4E17, 1.0, 0.0)
            else:
                file_dict['CH3'] = Species(2.86E13, 1.0, 0.25)
                file_dict['CH2_sing'] = Species(6.38E5, 1.0, 0.25)
                file_dict['CH'] = Species(2.36E3, 1.0, 0.25)
                file_dict['C'] = Species(1.29E4, 1.0, 0.25)
                file_dict['C2_a'] = Species(2.66E0, 0.0, 0.0)
                file_dict['C2_X'] = Species(9.42E-1, 0.0, 0.0)
                file_dict['C2H2'] = Species(1.41E16, 1.0, 0.0)
        elif cond_id in ['mcd_hf_0_5', 'mcd_hf_0_A', 'mcd_hf_0_B']:
            file_dict['Ts'] = 1173
            file_dict['Tns'] = 1267 if "0_5" in cond_id else file_dict['Ts']
            file_dict['H'] = Species(1.85E14, 0.0, 0.0)
            file_dict['H2'] = Species(1.52E17, 0.0, 0.0)
            if cond_id == 'mcd_hf_0_5':
                file_dict['CH3'] = Species(8E12, 1.0, 0.25)
                file_dict['CH2_sing'] = Species(1E6, 1.0, 0.25)
                file_dict['CH'] = Species(1E6, 1.0, 0.25)
                file_dict['C'] = Species(6E8, 1.0, 0.25)
                file_dict['C2_a'] = Species(1E5, 0.0, 0.0)
                file_dict['C2_X'] = Species(4E4, 0.0, 0.0)
                file_dict['C2H2'] = Species(2.8E11, 1.0, 0.0)
            elif cond_id == 'mcd_hf_0_A':
                file_dict['CH3'] = Species(1.46E13, 1.0, 0.25)
                file_dict['CH2_sing'] = Species(3.66E8, 1.0, 0.25)
                file_dict['CH'] = Species(2.74E8, 1.0, 0.25)
                file_dict['C'] = Species(3.37E9, 1.0, 0.25)
                file_dict['C2_a'] = Species(6.57E4, 0.0, 0.0)
                file_dict['C2_X'] = Species(1.41E4, 0.0, 0.0)
                file_dict['C2H2'] = Species(2.49E11, 1.0, 0.0)
            else:
                file_dict['CH3'] = Species(2.18E13, 1.0, 0.25)
                file_dict['CH2_sing'] = Species(6.9E8, 1.0, 0.25)
                file_dict['CH'] = Species(1.1E9, 1.0, 0.25)
                file_dict['C'] = Species(1.95E10, 1.0, 0.25)
                file_dict['C2_a'] = Species(2.25E5, 0.0, 0.0)
                file_dict['C2_X'] = Species(1.50E5, 0.0, 0.0)
                file_dict['C2H2'] = Species(1.8E11, 1.0, 0.0)
        elif cond_id in ['ncd_hf_0_5', 'ncd_hf_0_A', 'ncd_hf_0_B']:
            file_dict['Ts'] = 1173
            file_dict['Tns'] = 1267 if "0_5" in cond_id else file_dict['Ts']
            file_dict['H'] = Species(1.52E14, 0.0, 0.0)
            file_dict['H2'] = Species(1.63E17, 0.0, 0.0)
            if cond_id == 'ncd_hf_0_5':
                file_dict['CH3'] = Species(5.68E13, 1.0, 0.25)
                file_dict['CH2_sing'] = Species(1.14E9, 1.0, 0.25)
                file_dict['CH'] = Species(6.53E8, 1.0, 0.25)
                file_dict['C'] = Species(3.37E9, 1.0, 0.25)
                file_dict['C2_a'] = Species(1.74E5, 0.0, 0.0)
                file_dict['C2_X'] = Species(5.40E4, 0.0, 0.0)
                file_dict['C2H2'] = Species(2.97E12, 1.0, 0.0)
            elif cond_id == 'ncd_hf_0_A':
                file_dict['CH3'] = Species(6.0E13, 1.0, 0.25)
                file_dict['CH2_sing'] = Species(5.0E6, 1.0, 0.25)
                file_dict['CH'] = Species(1E6, 1.0, 0.25)
                file_dict['C'] = Species(2.0E9, 1.0, 0.25)
                file_dict['C2_a'] = Species(2E5, 0.0, 0.0)
                file_dict['C2_X'] = Species(6E4, 0.0, 0.0)
                file_dict['C2H2'] = Species(3.7E12, 1.0, 0.0)
            else:
                file_dict['CH3'] = Species(7.3E13, 1.0, 0.25)
                file_dict['CH2_sing'] = Species(1E9, 1.0, 0.25)
                file_dict['CH'] = Species(1.4E9, 1.0, 0.25)
                file_dict['C'] = Species(1.24E10, 1.0, 0.25)
                file_dict['C2_a'] = Species(1.76E5, 0.0, 0.0)
                file_dict['C2_X'] = Species(1.33E5, 0.0, 0.0)
                file_dict['C2H2'] = Species(1.81E12, 1.0, 0.0)
        elif cond_id in ['uncd_hf_0_5', 'uncd_hf_0_A', 'uncd_hf_0_B']:
            file_dict['Ts'] = 1173
            file_dict['Tns'] = 1145 if "0_5" in cond_id else file_dict['Ts']
            file_dict['H'] = Species(3.00E13, 0.0, 0.0)
            file_dict['H2'] = Species(2.25E17, 0.0, 0.0)
            if cond_id == 'uncd_hf_0_5':
                file_dict['CH3'] = Species(3.82E13, 1.0, 0.25)
                file_dict['CH2_sing'] = Species(5.62E8, 1.0, 0.25)
                file_dict['CH'] = Species(5.2E7, 1.0, 0.25)
                file_dict['C'] = Species(1.05E7, 1.0, 0.25)
                file_dict['C2_a'] = Species(2.49E5, 0.0, 0.0)
                file_dict['C2_X'] = Species(1.49E4, 0.0, 0.0)
                file_dict['C2H2'] = Species(2.97E13, 1.0, 0.0)
            if cond_id == 'uncd_hf_0_A':
                file_dict['CH3'] = Species(8E12, 1.0, 0.25)
                file_dict['CH2_sing'] = Species(1E5, 1.0, 0.25)
                file_dict['CH'] = Species(1E4, 1.0, 0.25)
                file_dict['C'] = Species(1E4, 1.0, 0.25)
                file_dict['C2_a'] = Species(1E3, 0.0, 0.0)
                file_dict['C2_X'] = Species(1E0, 0.0, 0.0)
                file_dict['C2H2'] = Species(2.95E13, 1.0, 0.0)
            else:
                file_dict['CH3'] = Species(2.49E13, 1.0, 0.25)
                file_dict['CH2_sing'] = Species(6.97E7, 1.0, 0.25)
                file_dict['CH'] = Species(7.61E6, 1.0, 0.25)
                file_dict['C'] = Species(9.34E5, 1.0, 0.25)
                file_dict['C2_a'] = Species(1.11E2, 0.0, 0.0)
                file_dict['C2_X'] = Species(1.11E1, 0.0, 0.0)
                file_dict['C2H2'] = Species(1.13E13, 1.0, 0.0)
        elif cond_id in ['uncd_mw_0_5', 'uncd_mw_0_A', 'uncd_mw_0_B']:
            file_dict['Ts'] = 873
            file_dict['Tns'] = 1263 if "0_5" in cond_id else file_dict['Ts']
            file_dict['H'] = Species(4.31E14, 0.0, 0.0)
            file_dict['H2'] = Species(2.06E16, 0.0, 0.0)
            if cond_id == 'uncd_mw_0_5':
                file_dict['CH3'] = Species(8.48E12, 1.0, 0.25)
                file_dict['CH2_sing'] = Species(9.46E7, 1.0, 0.25)
                file_dict['CH'] = Species(1.10E9, 1.0, 0.25)
                file_dict['C'] = Species(8.14E9, 1.0, 0.25)
                file_dict['C2_a'] = Species(1.16E11, 0.0, 0.0)
                file_dict['C2_X'] = Species(4.93E10, 0.0, 0.0)
                file_dict['C2H2'] = Species(2.35E15, 1.0, 0.0)
            elif cond_id == 'uncd_mw_0_A':
                file_dict['CH3'] = Species(1E12, 1.0, 0.25)
                file_dict['CH2_sing'] = Species(1E6, 1.0, 0.25)
                file_dict['CH'] = Species(1E7, 1.0, 0.25)
                file_dict['C'] = Species(5E9, 1.0, 0.25)
                file_dict['C2_a'] = Species(1.1E10, 0.0, 0.0)
                file_dict['C2_X'] = Species(5E9, 0.0, 0.0)
                file_dict['C2H2'] = Species(2.7E15, 1.0, 0.0)
            else:
                file_dict['CH3'] = Species(8.48E12, 1.0, 0.25)
                file_dict['CH2_sing'] = Species(9.46E7, 1.0, 0.25)
                file_dict['CH'] = Species(1.1E9, 1.0, 0.25)
                file_dict['C'] = Species(8.14E9, 1.0, 0.25)
                file_dict['C2_a'] = Species(6.58E6, 0.0, 0.0)
                file_dict['C2_X'] = Species(3.4E6, 0.0, 0.0)
                file_dict['C2H2'] = Species(3.42E15, 1.0, 0.0)

        return ConditionsInputFile(file_dict)

    def update_value(self, param, new_value):
        species, species_field = self.parse_param(param)
        if species is None and species_field is None:
            return None
        elif species is None:  # Ts or Tns
            setattr(self, species_field, new_value)
        else:  # Species object
            if species_field == 'conc':
                species_field = 'conc_in_mols_per_cm3'
            species_field_ref = InstanceRef(species, species_field)
            species_field_ref.set(new_value)

    def parse_param(self, param):
        if param in ['Ts', 'Tns']:
            return None, param

        pat = re.compile("([\w_]*)_([\w.]*)")
        specie = pat.search(param).group(1)
        specie_property = pat.search(param).group(2)
        if specie is None or specie_property is None:
            print("invalid param: {}".format(param))
            return None
        else:
            target_species = getattr(self, specie)
            if specie_property in ['conc', 's', 'g']:
                return target_species, specie_property
            else:
                print("invalid property {}".format(specie_property))
                return None, None

    def write_output(self, f):
        # Create directory if it doesn't already exist
        # Write file
        f.write("CONDITIONS INPUT FILE FOR DIAMOND LATTICE\n{}\n".format(self.cond_id))
        f.write("Substrate Temperature /K, Temperature near surface /K\n")
        f.write("{:d}, {:d}\n".format(int(self.Ts), int(self.Tns)))
        f.write("[Gas] at Surface (cm^-3),s,g factors\n")
        f.write("[H],{}\n".format(self.H))
        f.write("[H2],{}\n".format(self.H2))
        f.write("[CH3],{}\n".format(self.CH3))
        f.write("[CH2(s)],{}\n".format(self.CH2))
        f.write("[CH],{}\n".format(self.CH))
        f.write("[C],{}\n".format(self.C))
        f.write("[C2(a)],{}\n".format(self.C2_a))
        f.write("[C2(x)],{}\n".format(self.C2_X))
        f.write("[C2H2],{}\n".format(self.C2H2))


class EnergyInputFile(DLatInputFile):
    def __init__(self, input_dict):
        self.H_abs: Reaction = input_dict['H abs']
        self.H2_add: Reaction = input_dict['H2 add']
        self.H_add: Reaction = input_dict['H add']
        self.CHx_etch: Reaction = input_dict['CHx etch']
        self.ddG = input_dict['ddG']
        self.CHx_flex_Iso: Reaction = input_dict['CHx flex Iso']
        self.CHx_flex_S_A: Reaction = input_dict['CHx flex S_A']
        self.CHx_flex_S_B: Reaction = input_dict['CHx flex S_B']
        self.CHx_flex_SAB: Reaction = input_dict['CHx flex SAB']
        self.CHx_flex_Ter: Reaction = input_dict['CHx flex Ter']
        self.thirdBondFactor = input_dict['CHx flex 3rdBond']
        self.CHx_dimer_migrate: Reaction = input_dict['dimer migrate']
        self.CHx_gap_migrate_1f: Reaction = input_dict['gap migrate 1f']
        self.CHx_gap_migrate_1r: Reaction = input_dict['gap migrate 1r']
        self.CHx_gap_migrate_2f: Reaction = input_dict['gap migrate 2f']
        self.CHx_gap_migrate_2r: Reaction = input_dict['gap migrate 2r']
        self.CHx_down_migrate: Reaction = input_dict['migrate down']
        self.beta_scission: Reaction = input_dict['beta scission']
        self.C2H2_add_M1: Reaction = input_dict['C2H2 M1']
        self.C2H2_add_M2: Reaction = input_dict['C2H2 M2']
        self.C2H2_add_M3a: Reaction = input_dict['C2H2 M3a']
        self.C2H2_add_M3b: Reaction = input_dict['C2H2 M3b']
        self.C2H2_etch_CH3: Reaction = input_dict['C2H2 etch ch3']
        self.C2H2_etch: Reaction = input_dict['C2H2 etch']
        self.file_name = "energy.input"

    def update_value(self, param, new_value):
        instance, instance_field = self.parse_param(param)
        if instance is None and instance_field is None:
            return None
        elif instance is None:  # ddg, 3rdBond
            setattr(self, instance_field, new_value)
        else:  # Species object
            species_field_ref = InstanceRef(instance, instance_field)
            species_field_ref.set(new_value)

    @staticmethod
    def default():
        return EnergyInputFile(
            {'H abs': Reaction(3.2E-12, 3430),
             'H2 add': Reaction(3.2E-13, 7850),
             'H add': Reaction(9.6E-13, 0.0),
             'CHx etch': Reaction(7.0E14, 2.926E4),
             'ddG': 10.0,
             'CHx flex Iso': Reaction(7.0E14, 35.0E3),
             'CHx flex S_A': Reaction(0.0, 39.3E3),
             'CHx flex S_B': Reaction(7.0E14, 35.0E3),
             'CHx flex SAB': Reaction(0.0, 39.3E3),
             'CHx flex Ter': Reaction(0.0, 241.0E3),
             'CHx flex 3rdBond': 0.0,
             'dimer migrate': Reaction(6.13E13, 1.284E5),
             'gap migrate 1f': Reaction(6E13, 150E3),
             'gap migrate 1r': Reaction(1E12, 50E3),
             'gap migrate 2f': Reaction(1E12, 15E3),
             'gap migrate 2r': Reaction(6E13, 130E3),
             'migrate down': Reaction(6.13E13, 1.284E5),
             'beta scission': Reaction(0.0, 1.8E5),
             'C2H2 M1': Reaction(1E14, 35E3),
             'C2H2 M2': Reaction(1E14, 35E3),
             'C2H2 M3a': Reaction(1E14, 83E3),
             'C2H2 M3b': Reaction(1E14, 108E3),
             'C2H2 etch ch3': Reaction(1E14, 346E3),
             'C2H2 etch': Reaction(1E14, 80E3)})

    def parse_param(self, param):
        """
        Parse a param string into a instance (reference) of a reaction and a
        string corresponding to the field to be accessed of that instance
        """
        if param in ['ddG', 'thirdBondFactor']:
            return param, None

        pat = re.compile("([\w_-]*)_([AE])")
        instance = pat.match(param).group(1)
        instance_property = pat.match(param).group(2)
        if instance is None:
            print("invalid param: {}".format(param))
            return None
        else:
            instance_ref = getattr(self, instance)
            if instance_property in ['E', 'A']:
                return instance_ref, instance_property
            else:
                print("invalid property {}".format(instance_property))
                return None, None

    def write_file(self, f):
        f.write(
            """ENERGETICS INPUT FILE FOR DIAMOND LATTICE
-----------------------------------------
H Abstraction (k1), A (/s), E (K)
""")
        f.write("{}\n".format(self.H_abs))
        f.write("H2 addition (km1), A (s), E (K)\n")
        f.write("{}\n".format(self.H2_add))
        f.write("H addition (k2), A (s)\n")
        f.write("{}\n".format(self.H_add))
        f.write("Etching, A (s), E (Jmol-1), ddG (wall-etchrate parameter)\n")
        f.write("{}, {:.2f}\n".format(self.CHx_etch, self.ddG))
        f.write("FLEXIBLE ETCHING (flx) A(s), E(Jmol-1)\n")
        f.write("ISO| {}\n".format(self.CHx_flex_Iso))
        f.write("S_A| {}\n".format(self.CHx_flex_S_A))
        f.write("S_B| {}\n".format(self.CHx_flex_S_B))
        f.write("S_AB| {}\n".format(self.CHx_flex_SAB))
        f.write("Ter| {}\n".format(self.CHx_flex_Ter))
        f.write("3rdBondFactor: {:.2f}\n".format(self.thirdBondFactor))
        f.write(
            """---MIGRATION---
A / s^-1  E / Jmol^-1
DIMER\n""")
        f.write("{}\n".format(self.CHx_dimer_migrate))
        f.write("-GAP-\n")
        f.write("1f| {}\n".format(self.CHx_gap_migrate_1f))
        f.write("1r| {}\n".format(self.CHx_gap_migrate_1r))
        f.write("2f| {}\n".format(self.CHx_gap_migrate_2f))
        f.write("2r| {}\n".format(self.CHx_gap_migrate_2r))
        f.write("-DOWN-\n")
        f.write("{}\n".format(self.CHx_down_migrate))
        f.write("Beta Scission E (Jmol-1)\n")
        f.write("{}\n".format(self.beta_scission))
        f.write(
            """---------
ACETYLENE
---------\n""")
        f.write("MECHANISM 1: A, E\n")
        f.write("{}\n".format(self.C2H2_add_M1))
        f.write("MECHANISM 2: A, E\n")
        f.write("{}\n".format(self.C2H2_add_M2))
        f.write("MECHANISM 3a: A, E\n")
        f.write("{}\n".format(self.C2H2_add_M3a))
        f.write("MECHANISM 3b: A, E\n")
        f.write("{}\n".format(self.C2H2_add_M3b))
        f.write("ETCH_CH3: A, E\n")
        f.write("{}\n".format(self.C2H2_etch_CH3))
        f.write("ETCH_C2H2: A, E\n")
        f.write("{}\n".format(self.C2H2_etch))


class ReactionsInputFile(DLatInputFile):
    def __init__(self, default_values):
        self.file_name = "reactions.input"
        self.species: SpeciesReactions = default_values['species']
        self.activate_surface = default_values['activate_surface']
        self.trough_insertion = default_values['chx_insertion'].trough
        self.dimer_insertion = default_values['chx_insertion'].dimer
        self.etching: Etching = default_values['etching']
        self.etch_model = default_values['etch_model']
        self.fmr_model = default_values['fmr_model']
        self.defect_model = default_values['defect_model']
        self.migration: MigrationReactions = default_values['migration']
        self.bat_etch_factor = default_values['bat_etch_factor']
        self.align_dimers: DimerAlign = default_values['align_dimers']
        self.c2h2: C2H2Reactions = default_values['c2h2']

    # todo implement parse_param method
    @staticmethod
    def default():
        return ReactionsInputFile({'species': SpeciesReactions(),
                                   'activate_surface': True,
                                   'chx_insertion': ChxInsertion(),
                                   'etching': Etching(),
                                   'etch_model': 'prf',
                                   'fmr_model': 'yuri',
                                   'defect_model': 'yuri',
                                   'migration': MigrationReactions(),
                                   'bat_etch_factor': 10,
                                   'align_dimers': DimerAlign(),
                                   'c2h2': C2H2Reactions()})

    def write_file(self, f):
        f.write("""REACTION INPUT FILE FOR DIAMOND LATTICE
---------------------------------------
-------
SPECIES
-------
CH3: .{}.
CH2: .{}.
CH: .{}.
C: .{}.
C2(a): .{}.
C2(X): .{}.
C2H2: .{}.
------
MODELS
------
etching model [prf]
{:s}
radical site density model [yuri, butler]
{:s}
defect density model [yuri, butler]
{:s}
---------
REACTIONS
---------
ACTIVATE_SURFACE: .{}.
INSERTION
TROUGH: .{}.
DIMER: .{}.
ETCHING: .{}.
isolated_TN_threshold: {:d}
MIGRATION
DIMER:    .{}.
TO_GAP:   .{}.
FROM_GAP: .{}.
DOWN:    .{}.
MIGRATION: CRIT. NUCLEUS [1-4]
{:d}
Align Dimers? Align range
.{}., {:d}
Re-align dimers?
.{}.
--------------
C2H2 REACTIONS
--------------
MECHANISM1: .{}.
MECHANISM2: .{}.
MECHANISM3a: .{}.
MECHANISM3b: .{}.
MIGRATE_BIRAD: .{}.
ETCHING
ETCH CH3 from C2H2: .{}.
ETCH C2H2: .{}.
""".format(*self.species.as_iter(),
           self.etch_model, self.fmr_model, self.defect_model,
           self.activate_surface,
           self.trough_insertion,
           self.dimer_insertion,
           self.etching.active, self.etching.isolated_NN_threshold,
           self.migration.dimer, self.migration.to_gap, self.migration.from_gap, self.migration.down,
           self.migration.critical_nucleus,
           self.align_dimers.align_on, self.align_dimers.align_range, self.align_dimers.realign_on,
           self.c2h2.M1, self.c2h2.M2, self.c2h2.M3a, self.c2h2.M3b, self.c2h2.migrate, self.c2h2.etchCH3, self.c2h2.etchC2H2))


class InstanceRef(object):
    def __init__(self, instance, field_name):
        self.instance = instance
        self.field_name = field_name

    @staticmethod
    def from_param(input_file_instance: DLatInputFile, param):
        instance, field = input_file_instance.parse_param(param)
        return InstanceRef(instance, field)

    def get(self):
        return getattr(self.instance, self.field_name)

    def set(self, new_val):
        return setattr(self.instance, self.field_name, new_val)


param_type = {'Ts': int,
              'Tns': int,
              'H2_conc': float,
              'H_conc': float,
              'CH3_conc': float,
              'CH3_s': float,
              'CH3_g': float,
              'CH2_conc': float,
              'CH2_s': float,
              'CH2_g': float,
              'CH_conc': float,
              'CH_s': float,
              'CH_g': float,
              'C_conc': float,
              'C_s': float,
              'C_g': float,
              'C2_a_conc': float,
              'C2_X_conc': float,
              'C2H2_conc': float,
              'CHx_flex_Iso_A': float, 'CHx_flex_Iso_E': float,
              'CHx_flex_S_A_A': float, 'CHx_flex_S_A_E': float,
              'CHx_flex_S_B_A': float, 'CHx_flex_S_B_E': float,
              'CHx_flex_SAB_A': float, 'CHx_flex_SAB_E': float,
              'CHx_flex_Ter_A': float, 'CHx_flex_Ter_E': float,
              'thirdBondFactor': float,
              'ddg': float,
              'CHx_dimer_migrate_A': float, 'CHx_dimer_migrate_E': float,
              'CHx_gap_migrate_1f_A': float, 'CHx_gap_migrate_1f_E': float,
              'CHx_gap_migrate_1r_A': float, 'CHx_gap_migrate_1r_E': float,
              'CHx_gap_migrate_2f_A': float, 'CHx_gap_migrate_2f_E': float,
              'CHx_gap_migrate_2r_A': float, 'CHx_gap_migrate_2r_E': float,
              'CHx_down_migrate_A': float, 'CHx_down_migrate_E': float,
              'beta_scission_A': float, 'beta_scission_E': float,
              'C2H2_add_M1_A': float, 'C2H2_add_M1_E': float,
              'C2H2_add_M2_A': float, 'C2H2_add_M2_E': float,
              'C2H2_add_M3a_A': float, 'C2H2_add_M3a_E': float,
              'C2H2_add_M3b_A': float, 'C2H2_add_M3b_E': float,
              'C2H2_etch_CH3_A': float, 'C2H2_etch_CH3_E': float,
              'C2H2_etch_A': float, 'C2H2_etch_E': float,
              }
param_family = {'Ts': 'conditions',
                'Tns': 'conditions',
                'H2_conc': 'conditions', 'H_conc': 'conditions', 'CH3_conc': 'conditions',
                'CH3_s': 'conditions', 'CH3_g': 'conditions', 'CH2_conc': 'conditions',
                'CH2_s': 'conditions', 'CH2_g': 'conditions', 'CH_conc': 'conditions',
                'CH_s': 'conditions', 'CH_g': 'conditions',
                'C_conc': 'conditions', 'C_s': 'conditions', 'C_g': 'conditions',
                'C2_a_conc': 'conditions',
                'C2_X_conc': 'conditions',
                'C2H2_conc': 'conditions',
                'CHx_flex_Iso_A': 'energy', 'CHx_flex_Iso_E': 'energy',
                'CHx_flex_S_A_A': 'energy', 'CHx_flex_S_A_E': 'energy',
                'CHx_flex_S_B_A': 'energy', 'CHx_flex_S_B_E': 'energy',
                'CHx_flex_SAB_A': 'energy', 'CHx_flex_SAB_E': 'energy',
                'CHx_flex_Ter_A': 'energy', 'CHx_flex_Ter_E': 'energy',
                'thirdBondFactor': 'energy',
                'ddg': 'energy',
                'CHx_dimer_migrate_A': 'energy', 'CHx_dimer_migrate_E': 'energy',
                'CHx_gap_migrate_1f_A': 'energy', 'CHx_gap_migrate_1f_E': 'energy',
                'CHx_gap_migrate_1r_A': 'energy', 'CHx_gap_migrate_1r_E': 'energy',
                'CHx_gap_migrate_2f_A': 'energy', 'CHx_gap_migrate_2f_E': 'energy',
                'CHx_gap_migrate_2r_A': 'energy', 'CHx_gap_migrate_2r_E': 'energy',
                'CHx_down_migrate_A': 'energy', 'CHx_down_migrate_E': 'energy',
                'beta_scission_A': 'energy', 'beta_scission_E': 'energy',
                'C2H2_add_M1_A': 'energy', 'C2H2_add_M1_E': 'energy',
                'C2H2_add_M2_A': 'energy', 'C2H2_add_M2_E': 'energy',
                'C2H2_add_M3a_A': 'energy', 'C2H2_add_M3a_E': 'energy',
                'C2H2_add_M3b_A': 'energy', 'C2H2_add_M3b_E': 'energy',
                'C2H2_etch_CH3_A': 'energy', 'C2H2_etch_CH3_E': 'energy',
                'C2H2_etch_A': 'energy', 'C2H2_etch_E': 'energy',
                }

if __name__ == '__main__':
    energy = EnergyInputFile.default()
    reactions = ReactionsInputFile.default()
    conditions = default()

    with open('conditions.input', 'w') as c:
        with open('reactions.input', 'w') as r:
            with open('energy.input', 'w') as e:
                conditions.write_output(c)
                reactions.write_file(r)
                energy.write_file(e)
