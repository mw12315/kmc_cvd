"""
Handle interaction when creating and manipulating the InputFile Objects
"""
import input_generation.files
from input_generation import files
from input_generation.files import param_type


def get_values(space_sep_vals, expected_type: type) -> list:
    try:
        cast_vals = list(map(expected_type, space_sep_vals.split()))
        return cast_vals
    except ValueError:
        print("Unable to cast inputs to expected type {}".format(expected_type.__name__))
        return None


def choose_conditions():
    while True:
        condition = input("[{}]\nBase condition: ".format(", ".join(
            input_generation.files.ConditionsInputFile.avail_conditions)))
        if condition in input_generation.files.ConditionsInputFile.avail_conditions:
            return condition
        else:
            print("{} not implemented".format(condition))


def write_gen_log_line(count, value, param, f):
    f.write("{:10d}".format(count))
    v_fstring = "{:10d}\n" if type(value) is int else "{:10.3f}\n"
    f.write(v_fstring.format(value))


def read_parameter_to_vary():
    """
    prompt user for a parameter to vary and return the key on valid submission
    :return: the key for the parameter to be varied
    """
    while True:
        param = input("[{}]\nParameter to vary: ".format(", ".join(param_type.keys())))
        if param in param_type.keys():
            return param
        else:
            print("{} not implemented".format(param))


def read_parameter_values(param_to_vary):
    while True:
        input_vals = get_values(input("space separated list of values: "), param_type[param_to_vary])
        if input_vals is not None:
            return input_vals


def condition_energy_fobj():
    chosen_condition = choose_conditions()
    return files.ConditionsInputFile.from_cond_id(chosen_condition), files.EnergyInputFile.default()


linked_params = ['CHx_flex_Iso_A', 'CHx_flex_Iso_E',
                 'CHx_flex_S_A_A', 'CHx_flex_S_A_E',
                 'CHx_flex_S_B_A', 'CHx_flex_S_B_E',
                 'CHx_flex_SAB_A', 'CHx_flex_SAB_E',
                 'CHx_flex_Ter_A', 'CHx_flex_Ter_E']