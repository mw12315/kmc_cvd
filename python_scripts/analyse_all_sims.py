import pandas as pd
import numpy as np
import sys
from matplotlib import pyplot as plt
sys.path.append("C:\\local\\src\\diamond_lattice\\python_scripts")
from data_analysis.pdtools import plot_bar,plot_event_bar,stat_sims
# files to be read in are submitted as arguments
# files = sys.argv[1:-1]

file = '.csv'
sort_by = 'Location'

# for file in files:
DF = pd.read_csv(file,delimiter=",", header=0, skipinitialspace=True)


sorted_DF = DF.sort_values(by=sort_by)
sort_by_labs = sorted_DF[sort_by].tolist()
# sort_by_labs = [round(x,-1) for x in sort_by_labs]


print(sorted_DF.columns)


# Add in extra calculation columns
sorted_DF = sorted_DF.assign(tot_incorporation=sorted_DF['Dimer Insert %'] + sorted_DF['Trough Insert %'])
sorted_DF = sorted_DF.assign(net_incorporation=sorted_DF['Dimer Insert %'] + sorted_DF['Trough Insert %'] - sorted_DF['Etch %'])

# Standard deviation columns

# ax_dimer_ins = sorted_DF.plot(kind="bar",y='Dimer Insert %',color='blue'

# Isolated Etch Factor Labels
# iso_labels = sorted_DF['Iso Rate Fac.']
# iso_labels = iso_labels.tolist()

# Sorting by a Column
# sorted_DF = sorted_DF.sort_values(by='Iso Rate Fac.')

# ------------------------------
# Create DataFrames and plotting
# ------------------------------
# Roughness
plt.figure(1)
roughness_sdev = np.std(sorted_DF['Roughness /m'])
plot_bar(
    sorted_DF, 'Roughness /m', roughness_sdev, 'RMS Roughness', 'RMS Roughness /nm', 'x axis', sort_by_labs, 1E-9)

# Growth Rat
plt.figure(2)
grate_sdev = np.std(sorted_DF['Growth Rate m/h'])
grate = plot_bar(
    sorted_DF, 'Growth Rate m/h', grate_sdev, 'Final Growth Rate', 'Growth Rate / µm/h', 'x axis', sort_by_labs, 1E-6)

# Events
plt.figure(3)
stacked_events = plot_event_bar(sorted_DF, ['Dimer Insert %','Trough Insert %','Etch %','Migrate %'], 'Percentage of Simulation Events', sort_by_labs)

# Output Statistical Information on Simulations within input .csv(s)
ave_rness = np.average(sorted_DF['Roughness /m'])
ave_grate = np.average(sorted_DF['Growth Rate m/h'])
ave_simtime = np.average(sorted_DF['Sim Time \s'])
std_simtime = np.std(sorted_DF['Sim Time \s'])
ave_cpu_time = np.average(sorted_DF['CPU Time /h'])
std_cpu_time = np.std(sorted_DF['CPU Time /h'])

stat_sims(ave_rness, roughness_sdev, ave_grate, grate_sdev, len(sorted_DF), ave_simtime, std_simtime, ave_cpu_time, std_cpu_time)

plt.show()


