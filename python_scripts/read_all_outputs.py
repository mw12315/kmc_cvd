#!/users/mw12315/local/usr/bin/python3.6
import os
import re
from collections import namedtuple
from sys import argv
import glob


class Simulation:
    def __init__(self, location, init_surface):
        self.location = location
        nx, ny, nz, nb = tuple(init_surface.split())
        self.initial_surface = {'Nx': int(nx),
                                'Ny': int(ny),
                                'Nz': int(nz),
                                'Nb': int(nb)}

        self.end_conditions = None
        self.induction_time = 0.0
        self.induction_height = 0.0
        self.clock_seed = 0
        self.iterations = 0
        self.events = 0
        self.simulated_time = 0.0
        self.total_time = 0.0
        self.temperatures = {}
        self.gas_concentrations = {}
        self.steric_factors = {}
        self.geometric_factors = {}
        self.reaction_prefactors = {}
        self.reaction_Ebarriers = {}
        self.scaling_factors = {}
        self.reaction_rates = {}
        self.radical_site_fractions = RadicalSiteFractions
        self.total_rates = 0
        self.reaction_counts = {}
        self.isolated_etch = 0.0
        self.migration_counts = {}
        self.ch3_birad_trough_ins = 0.0
        self.growth_stats = FinalGrowthStats
        self.surface_stats = FinalSurfaceStats
        self.cpu_time = 0.0
        self.average_time_step = 0.0

    def set_end_conditions(self, single_line):
        self.end_conditions = EndConditions._make(single_line.split())

    def set_induction_height(self, single_line):
        self.induction_height = float(single_line.split()[-1])

    def set_induction_time(self, single_line):
        self.induction_time = float(single_line.split()[-1])

    def set_temperatures(self, single_line):
        single_line = single_line.split()
        self.temperatures = {'Ts': int(single_line[-2]), 'Tns': int(single_line[-1])}

    def set_gas_concentrations(self, single_line):
        single_line = single_line.split()
        # No Geometric or Steric Factors
        if len(single_line) == 2:
            self.gas_concentrations[single_line[0]] = float(single_line[1])
        elif len(single_line) == 4:
            self.gas_concentrations[single_line[0]] = float(single_line[1])
            self.steric_factors[single_line[0]] = float(single_line[2])
            self.geometric_factors[single_line[0]] = float(single_line[3])
        else:
            print("Invalid String Read in (splits into {})".format(len(single_line)))

    def set_reaction_energies(self, single_line):
        single_line = single_line.split()
        self.reaction_prefactors[single_line[0]] = float(single_line[1])
        if len(single_line) > 2:
            self.reaction_Ebarriers[single_line[0]] = float(single_line[2])

    def set_scaling_factors(self, single_line):
        pat = re.compile("^\s*([a-zA-z.]*\s*[a-zA-z.()]*\s*[a-zA-z.()]*)\s*([0-9.E+-]*)")
        self.scaling_factors[pat.search(single_line).group(1).strip()] = pat.search(single_line).group(2)

    def set_reaction_rates(self, single_line):
        single_line = single_line.split()
        self.reaction_rates[" ".join(single_line[:-1])] = float(single_line[-1])

    def set_seeding_clock_value(self, single_line):
        self.clock_seed = int(single_line.split()[-1])

    def set_sim_it_time(self, l1, l2, l3, l4):
        self.iterations = int(l1.split()[-1])
        self.events = int(l2.split()[-3])
        self.simulated_time = float(l3.split()[-1])
        self.total_time = float(l4.split()[-1])

    def set_reaction_counts(self, single_line):
        single_line = single_line.split()
        self.reaction_counts[single_line[0]] = ReactionCounts(single_line[2],
                                                              single_line[4],
                                                              single_line[6],
                                                              single_line[9])

        if single_line[0] == "TOTAL":
            self.isolated_etch = single_line[7]

    def set_migration_counts(self, single_line):
        single_line = single_line.split()
        self.migration_counts[single_line[0]] = MigrationRateCounts(single_line[2],
                                                                    single_line[4],
                                                                    single_line[6],
                                                                    single_line[8],
                                                                    single_line[10])

    def set_biradical_trough_insertions(self, single_line):
        single_line = single_line.split()
        self.ch3_birad_trough_ins = float(single_line[-1])

    def set_surface_stats(self, lines):
        self.growth_stats = FinalGrowthStats(lines[0][-1], lines[1][-1], lines[8][-1], lines[9][-1])
        self.surface_stats = FinalSurfaceStats(lines[2][-1], lines[3][4], lines[4][-1], lines[5][-1], lines[6][-1], lines[7][-1])

    def return_output_str(self):
        output = [self.location, self.initial_surface['Nx'], self.initial_surface['Ny'],
                  self.temperatures['Tns'], self.temperatures['Ts'],
                  self.gas_concentrations['H'], self.gas_concentrations['H2'], self.gas_concentrations['CH3'],
                  self.gas_concentrations['C2H2'],
                  self.geometric_factors['CH3'], self.steric_factors['CH3'], self.steric_factors['C2H2'],
                  self.total_rates, self.scaling_factors['Trough Insertion'], self.scaling_factors['Trough Birad. Ins.'],
                  self.scaling_factors['Mig. Crit. Nuc.'],
                  self.scaling_factors['Isolation(TN) Thres.'], self.radical_site_fractions.mono_radical,
                  self.radical_site_fractions.bi_radical,
                  self.scaling_factors['Scaled Etching Factor'],
                  self.induction_height, self.induction_time,
                  self.reaction_rates['CH3 Dimer Insertion'], self.reaction_rates['CH2 Dimer Insertion'],
                  self.reaction_rates['CH Dimer Insertion'], self.reaction_rates['C Dimer Insertion'],
                  self.reaction_rates['C2H2 M1a Mechanism'], self.reaction_rates['C2H2 M1b Mechanism'],
                  self.reaction_rates['C2H2 M2 Mechanism'],
                  self.reaction_rates['C2H2 M3a Mechanism'], self.reaction_rates['C2H2 M3b Mechanism'],
                  self.reaction_rates['CH2 Dimer Migration'],
                  self.reaction_rates['CH2 Etch 0'],
                  self.events, self.total_time,
                  self.reaction_counts['TOTAL'].dimer_insertion, self.reaction_counts['TOTAL'].trough_insertion,
                  self.ch3_birad_trough_ins,
                  self.reaction_counts['TOTAL'].etch, self.isolated_etch,
                  self.reaction_counts['TOTAL'].migration, self.migration_counts['CH2'].dimer,
                  self.migration_counts['CH2'].gap, self.migration_counts['CH2'].down, self.migration_counts['CH2'].c2h2_birad,
                  self.migration_counts['CH2'].c2h2,
                  float(self.reaction_counts['CH3'].dimer_insertion) + float(self.reaction_counts['CH3'].trough_insertion),
                  float(self.reaction_counts['CH2'].dimer_insertion) + float(self.reaction_counts['CH2'].trough_insertion),
                  float(self.reaction_counts['CH'].dimer_insertion) + float(self.reaction_counts['CH'].trough_insertion),
                  float(self.reaction_counts['C'].dimer_insertion) + float(self.reaction_counts['C'].trough_insertion),
                  self.growth_stats.simulation_growth, self.growth_stats.simulation_growth_rate,
                  self.surface_stats.rms, self.surface_stats.rms_ma, self.surface_stats.N2, self.surface_stats.N4,
                  self.average_time_step, self.cpu_time]

        return ",".join(map(str, output)) + "\n"


def count_file_lines(file):
    import subprocess
    out = subprocess.run(['wc', '-l', file], stdout=subprocess.PIPE)
    lines = out.stdout.split()[0]
    return int(lines.decode())


# Keep track of size of input file
gas_concentrations_total = 9
reaction_energies_total = 10
scaling_factors_total = 5
reaction_rates_total = 23
reaction_counts_total = 5
migration_counts_total = 1
surface_info_total = 10

EndConditions = namedtuple('EndConditions', ['t', 'iterations', 'growth_height'])
RadicalSiteFractions = namedtuple('RadicalSiteFractions', ['mono_radical', 'bi_radical'])
ReactionCounts = namedtuple('ReactionCounts', ['dimer_insertion', 'trough_insertion', 'etch', 'migration'])
MigrationRateCounts = namedtuple('MigrationRateCounts', ['dimer', 'gap', 'down', 'c2h2_birad', 'c2h2'])
FinalGrowthStats = namedtuple('FinalGrowthStats',
                              ['simulation_growth', 'simulation_growth_rate', 'total_growth', 'total_growth_rate'])
FinalSurfaceStats = namedtuple('FinalSurfaceStats', ['rms', 'rms_ma', 'rms_deriv', 'N2', 'N3', 'N4'])

cwd = os.getcwd()  # Current working directory
cf = os.path.basename(cwd)  # Current folder
print(cf, cwd)
f_name = "{}_all.csv".format(cf)
print("Output to {}\n".format(f_name))

columns = ['Location', 'Nx', 'Ny', 'Tns', 'Ts', '[H]', '[H2]', '[CH3]', '[C2H2]', 'g_ch3', 's_ch3',
           'rates', 'T. Ins. Fact.', 'T Ins. Birad. Fact.', 'Mig. Crit. Nuc.', 'Iso. Threshold', "Fmr", "Bmr",
           'Etch Scaling', 'Induction Height', 'Induction Time',
           'CH3 D. Ins. rt.', 'CH2 D. Ins. rt.', 'CH D. Ins. rt.', 'C D. Ins. rt.', 'C2H2 M1a', 'C2H2 M1b',
           'C2H2 M2', 'C2H2 M3a', 'C2H2 M3b',
           'D. Mig. rt', 'Etch_0 rt',
           'events', 'time', '% Dimer Ins.', '% Trough Ins.', '<- % Birad',
           '% Etch', '% Isolated', '% Migration', '% Dimer Mig.', '% Gap Mig', '% Down Mig', '% C2H2_birad', '% C2H2 Mig',
           'CH3', 'CH2', 'CH', 'C',
           'Growth /m', 'Growth Rate / m/h', 'Roughness /m', 'MA Roughness /m', '2N RMS', '4N RMS', 'Ave. time step', 'CPU']

# Detect submission file deletion or output_length modification
if len(argv) > 1:
    delete_submit = True
else:
    delete_submit = False

unfinished = []
simulations = []
with open(f_name, 'w') as all_sim_file:
    all_sim_file.write(",".join(columns) + "\n")
    for f_tuple in os.walk(cwd):
        if "diamond.out" in f_tuple[2]:
            output_length = 119
            height_end = False
            sim_dir = os.path.join(f_tuple[0])
            output_file = os.path.join(sim_dir, "diamond.out")
            if os.stat(output_file).st_size == 0: continue
            sim_file_lines = count_file_lines(output_file)
            if sim_file_lines > output_length:
                output_length = sim_file_lines
                height_end = True
            if sim_file_lines != output_length:
                print("{} not of correct length ({}), simulation not finished? ({} lines)".format(output_file,
                                                                                                  output_length,
                                                                                                  sim_file_lines))
                continue
            print("{}".format(f_tuple[0]))
            with open(output_file, 'r') as f:
                for x in range(7):
                    f.readline()

            with open(output_file, 'r') as f:
                for x in range(7):
                    f.readline()

                sim = Simulation(sim_dir, f.readline())  # Initial Surface Nx,Ny,Nz,Nb
                f.readline()
                sim.set_end_conditions(f.readline())  # End Conditions (Time, Iteration, Growth)
                for x in range(3):
                    f.readline()

                sim.set_temperatures(f.readline())  # Temperatures (Ts, Tns)
                f.readline()  # Gas Concentrations
                f.readline()
                f.readline()
                for x in range(gas_concentrations_total):
                    sim.set_gas_concentrations(f.readline())

                f.readline()
                f.readline()  # Reaction Energies
                f.readline()
                for x in range(reaction_energies_total):
                    sim.set_reaction_energies(f.readline())

                f.readline()  # Scaling Factors
                for x in range(scaling_factors_total):
                    sim.set_scaling_factors(f.readline())

                f.readline()  # Reaction Rates
                f.readline()
                f.readline()
                for x in range(reaction_rates_total):
                    sim.set_reaction_rates(f.readline())

                f.readline()
                # Radical Site Fraction (Mono)
                line = f.readline().split()
                sim.radical_site_fractions = RadicalSiteFractions(line[-1], f.readline().split()[-1])
                # Active Rates
                sim.total_rates = int(f.readline().split()[-1])

                f.readline()

                # Seeding Clock Value
                sim.set_seeding_clock_value(f.readline())
                if height_end:
                    # Allow for added 'Reached Growth Height Limit (A):' message
                    f.readline()

                for x in range(4):
                    f.readline()

                # todo need to allow for height limit ending which prints an additional line into the input file

                # Iterations
                sim.set_sim_it_time(f.readline(), f.readline(), f.readline(), f.readline())
                sim.set_induction_height(f.readline())
                sim.set_induction_time(f.readline())

                for x in range(4):
                    f.readline()

                # Reaction Counts (Species Specific)
                for x in range(reaction_counts_total):
                    sim.set_reaction_counts(f.readline())

                f.readline()
                # Reaction Counts (TOTAL)
                sim.set_reaction_counts(f.readline())

                for x in range(3):
                    f.readline()

                # Migration
                for x in range(migration_counts_total):
                    sim.set_migration_counts(f.readline())

                f.readline()
                sim.set_biradical_trough_insertions(f.readline())
                f.readline()

                f.readline()
                # Surface/Growth Information
                s_lin = []
                for x in range(surface_info_total):
                    s_lin.append(f.readline().split())

                sim.set_surface_stats(s_lin)

                f.readline()
                # Average Time Step
                sim.average_time_step = f.readline().split()[-1]
                # Total CPU Time
                sim.cpu_time = f.readline().split()[-1]

                simulations.append(sim)

                # Delete submission file now that output file is read (if an argument is entered)
                if delete_submit:
                    for sub_file in glob.glob(os.path.join(sim_dir, "*.sh")):
                        unfinished.append(sim_dir)
                        os.remove(sub_file)

    # Write to file
    for simulation in simulations:
        all_sim_file.write(simulation.return_output_str())

    print("Found and processed {} simulation output files".format(len(simulations)))

    print("Unfinished: ")
    print(" ".join(unfinished))
