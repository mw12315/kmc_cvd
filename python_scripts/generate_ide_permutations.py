import numpy as np

tf = [True, False]
combinations = []

for i in tf:
    for j in tf:
        for k in tf:
            for m in tf:
                combinations.append([i, j, k, m])

print("[", end=" ")
for i,c in enumerate(combinations):
    for j, s in enumerate(c):
        value = ".true." if s else ".false."
        if i + 1 == len(combinations) and j+1 == len(c):
            print(value, end=" ")
        else:
            print(value, end=", ")
    if i+1 != len(combinations): print("&")
print("]")