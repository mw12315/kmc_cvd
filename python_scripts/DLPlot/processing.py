import os
import numpy as np
import pandas as pd
from pandas.core.frame import DataFrame
from pandas.core.groupby import GroupBy
from pandas.core.series import Series
from pathlib import Path
from datetime import datetime
from data_analysis import all_sims_stats as ass


def most_average(df: DataFrame, column: str) -> Series:
    average = df[column].mean()
    values = df[column].to_list()
    indices = list(df.index)

    deviations = [abs(element - average) for element in df[column]]
    i_min, iloc_min, min_dev = int(np.argmin(deviations)), indices[int(np.argmin(deviations))], np.min(deviations)
    print("Min. Deviation: {} ({})".format(df.loc[iloc_min].location, min_dev))
    return df.loc[iloc_min]


def most_average_over_series(df: DataFrame, column_list) -> (DataFrame, float):
    ilocs, abs_deviations = average_deviation_over_series(df, column_list)

    imin = int(np.argmin(abs_deviations))
    return df.loc[ilocs[imin]], np.min(abs_deviations)


def average_deviation_over_series(df: DataFrame, column_list) -> ([int], [float]):
    """
    Get the average absolute deviation of rows of a dataframe from the average of a column
    Args:
        df: Dataframe of data
        column_list: list of columns to compile average deviation from

    Returns: index, average deviation for each row of the df over the course of the simulation
    """
    ilocs = []
    average_deviations = []

    for iloc in df.index:
        ilocs.append(iloc)
        deviations = [_deviation(df[c], iloc) for c in column_list]
        average_deviations.append(np.nanmean(deviations))

    return ilocs, average_deviations


def _deviation(s: Series, iloc: int) -> float:
    mean = s.mean()
    val = s.loc[iloc]
    if np.isnan(mean) or np.isnan(val):
        return np.NaN
    return abs(mean - val)


def fetch_zmean_data(df: pd.core.groupby.generic.DataFrameGroupBy,
                     col: str,
                     g0_mesh: np.ndarray,
                     g1_mesh: np.ndarray):
    z = np.zeros((g0_mesh.shape[0], g1_mesh.shape[1]))
    z_err = np.zeros((g0_mesh.shape[0], g1_mesh.shape[1]))
    for i in range(len(g0_mesh[0, :])):
        for j in range(len(g1_mesh[:, 0])):
            z[j, i] = df.get_group((g0_mesh[j, i], g1_mesh[j, i]))[col].mean()
            z_err[j, i] = df.get_group((g0_mesh[j, i], g1_mesh[j, i]))[col].std()

    return z, z_err


def fetch_zmean_series_data(df: pd.core.groupby.generic.DataFrameGroupBy,
                            col: str,
                            g0_mesh: np.ndarray,
                            g1_mesh: np.ndarray, nvals: int):
    z = np.zeros((g0_mesh.shape[0], g1_mesh.shape[1]))
    z_err = np.zeros((g0_mesh.shape[0], g1_mesh.shape[1]))
    for i in range(len(g0_mesh[0, :])):
        for j in range(len(g1_mesh[:, 0])):
            df_sub = df.get_group((g0_mesh[j, i], g1_mesh[j, i]))
            series_mean, series_std = mean_std_x(df_sub, col, nvals)
            z[j, i], z_err[j, i] = series_mean, series_std

    return z, z_err


"""
Convert data to an xyz file to read into an OVITO file
"""
def data2xyz(data, scaling=1, fmt='f', units="", f_path=None, ndummy_vals=0):
    # blank initial frames (frame0, nucleation0)
    if f_path != None:
        f = open(f_path, 'w')
        print("Output to {}".format(f_path))

    str_out = "1\n--\nH 0.0 0.0 0.0\n"
    for i in range(ndummy_vals):
        if f_path is None:
            print(str_out, end="")
        else:
            f.write(str_out)
    for i in list(data):
        str_out = "1\n{0:{1}} {2}\nH 0.0 0.0 0.0\n".format(i / scaling, fmt, units)
        if f_path is None:
            print(str_out, end="")
        else:
            f.write(str_out)

    f.close()


def xvals_zero2NaN(df, fill_with_last_non_zero=False, nan_xreg=False,
                   add_final_vals=True):
    """
    If a value within an x<quantity> is zero due to not being reached,
    then the value is set to NaN to ensure that it is not plotted.
    """
    xreg_base = "xreg_{}_{}"
    xcols = ['xgrowth_rate_{}', 'xrms_{}', 'xinstgr_{}',
             'xrms2N_{}', 'xrms3N_{}', 'xrms4N_{}']
    fcol_val = ['average growth rate m/h', 'rms roughness',
                None, '2N rms roughness', '3N rms roughness','4N rms roughness']
    fcol_scale = [1E-9, 1] + [None] + [1] * 3
    n_xvals = 0
    iloc_empty = np.zeros(len(df), dtype=np.int_)
    while True:
        try:
            df['xgrowth_rate_{}'.format(n_xvals)]  # Check key is valid
            for i in df.index:
                if df.loc[i, 'xgrowth_rate_{}'.format(n_xvals)] != 0.0:
                    iloc_empty[i] = n_xvals
            n_xvals += 1

        except KeyError:
            break

    print("Detected {} xvals".format(n_xvals))

    if not fill_with_last_non_zero:
        for c in xcols:
            try:
                [df[c.format(x)].replace(0.0, np.NaN, inplace=True) for x in range(n_xvals)]
            except KeyError:
                pass

    if (fill_with_last_non_zero or nan_xreg) and len(iloc_empty) != 0:
        for i in df.index:
            # Replace 0.0 with last value before final value
            if fill_with_last_non_zero or add_final_vals:
                df.loc[i, 'xtimes_{}'.format(iloc_empty[i] + 1)] = df.loc[i, 'sim_time']
                for fval_col, fscale, c in zip(fcol_val, fcol_scale, xcols):
                    try:
                        if add_final_vals:
                            if fval_col:
                                df.loc[i, c.format(iloc_empty[i] + 1)] = df.loc[i, fval_col] / fscale
                        elif fill_with_last_non_zero:
                            final_value = df.loc[i, c.format(iloc_empty[i])]
                            for x in range(n_xvals):
                                df.loc[i, c.format(x)] = final_value if df.loc[i, c.format(x)] == 0.0 else df.loc[
                                    i, c.format(x)]
                    except KeyError:
                        pass

            # Process xRegistry in required
            if nan_xreg:
                for irate in range(n_xvals):
                    for x in range(iloc_empty[i], n_xvals):
                        df.loc[i, xreg_base.format(irate, x)] = np.NaN
    print("\nProcessed")
    return n_xvals


def average_gr_from_instant_gr(df, last_n):
    n_xvals = xvals_zero2NaN(df)
    if last_n > n_xvals:
        last_n = n_xvals

    while True:
        columns = ['xinstgr_{}'.format(i) for i in range(n_xvals - last_n, n_xvals)]
        instant_growth_rates = df[columns]
        mean = instant_growth_rates.mean(axis=1)
        mean.replace(0.0, value=np.NaN, inplace=True)

        if np.NaN in mean:
            n_xvals -= 1
            print(n_xvals)
        else:
            return mean


def add_condition_column(df: DataFrame, i_condition=0):
    """
    Split df.location by '/' in order to isolate the condition in the path
    and creating a new column which holds this data. Operates in place
    Args:
        df: Pandas DataFrame from diamond_lattice output file
        i_condition (int): index in split that holds the condition
    """
    import re
    conditions = df['location'].str.split('/')
    conditions = list(map(lambda x: x[i_condition], conditions))
    conditions = list(map(lambda x: re.match("([a-z]*(?:_hf)?(?:_mw)?)[_r]?[0-9]*", x).group(1), conditions))
    df.insert(0, 'condition', conditions)

    return None


def add_pc_migration_col(df: DataFrame):
    """
    add a pc_migration column to a diamond_lattice dataframe
    returns the name of the new column
    """
    pc_migration = df['pc_mig_dimer'] + df['pc_mig_gap'] + df['pc_mig_down']
    df.insert(len(df.columns), 'pc_migration', pc_migration)
    return 'pc_migration'


def add_pc_adsorption_col(df: DataFrame):
    """
    add a pc_migration column to a diamond_lattice dataframe
    returns the name of the new column
    """
    pc_adsorption = df['pc_d_ins'] + df['pc_t_ins']
    df.insert(len(df.columns), 'pc_ads', pc_adsorption)
    return 'pc_ads'


def add_xheight(df: pd.DataFrame):
    """
    Add xheight_X columns by combining xtimes_X and xgrowth_rate_X columns
    Resulting columns represent average surface height in nm
    """
    x = 0
    while True:
        try:
            df['xheight_{}'.format(x)] = df['xgrowth_rate_{}'.format(x)] * df['xtimes_{}'.format(x)] / 3600
            x += 1
        except KeyError:
            break


def mean_std_x(df: pd.DataFrame, column, n_values):
    """
    Return the mean, std for xvalue (times, rms, growth_rate) from 0 to n_values
    :param column: string with single interpolation entry
    :param n_values: number of values to process [0:n_values]
    :param df: dataframe containing xcolumn required
    :return: float
    """
    # Check that n_values are available
    for i in range(n_values):
        if column.format(i) not in df.columns:
            print('Only {} n_values available!'.format(i))
            return -1, -1

    try:
        mean = [df[column.format(i)].mean() for i in range(n_values)]
    except KeyError:
        print("Invalid column: {} passed".format(column))
        return -1, -1

    return np.nanmean(mean), np.nanstd(mean)


def load_df(load_from_pickle=True, PICKLE_PATH=None, csv_path=None,
            last_non_zero=False, nan_xreg=True, final_vals=True, cond_index=0):
    """
    :param bool load_from_pickle: load from pickle if possible
    :param Path PICKLE_PATH: path to pickle file (to save to if loading from csv)
    :param Path csv_path: csv datafile to load from (if not pickle)
    :param bool last_non_zero: Repeat the last xval non-zero value
    :param bool nan_xreg: Turn zeroes in xregistry to NaN
    :param bool final_vals: Put final xvalue at final xtime
    :param int cond_index: split index in path to capture condition
    :return: DataFrame
    """
    # Import all_data.csv
    if PICKLE_PATH is None or not os.path.exists(PICKLE_PATH):
        load_from_pickle = False
    if not load_from_pickle:
        print("Choosing to import csv and process")
        if csv_path is None:
            print("Must provide csv to load if not loading from pickle")
            exit(1)
        df = ass.csv2dataframe(csv_path)
        c_pcads = add_pc_adsorption_col(df)
        print(f"pc adsorption: {c_pcads}")
        c_pcmig = add_pc_migration_col(df)
        print(f"pc migration: {c_pcmig}")
        nvals = xvals_zero2NaN(df, fill_with_last_non_zero=last_non_zero, nan_xreg=nan_xreg, add_final_vals=final_vals)
        add_xheight(df)
        add_condition_column(df, cond_index)
        print("Imported data from csv")
        if PICKLE_PATH is not None:
            df.to_pickle(PICKLE_PATH, protocol=4)
            print("processed and saved/updated pickle")
    else:
        df = pd.read_pickle(PICKLE_PATH)
        epoch_modified = os.path.getmtime(PICKLE_PATH)
        print("Loaded processed data from pickle (age: {:.0f} days)".format(
            (datetime.now() - datetime.fromtimestamp(epoch_modified)).total_seconds() / (3600 * 24)))

    return df


def event_labels():
    """
    :return: list of labels linking event type index with its name
    """
    return [
        "CH3 dimer",
        "CH2 dimer",
        "CH dimer",
        "C dimer",
        "CH3 trough",
        "CH2 trough",
        "CH trough",
        "C trough",
        "C2H2 M1a",
        "C2H2 M1b",
        "C2H2 M2",
        "C2H2 M3",
        "Dimer migration row",
        "Dimer migration chain",
        "Gap migration A",
        "Gap migration B",
        "Gap migration C",
        "Gap migration D",
        "Gap migration C2H2 1",
        "Gap migration C2H2 2",
        "Migrate down atomic step",
        "Migrate C2H2 (biradical)",
        "Migrate C2H2",
        "Preferential etch",
        "etch C2H2",
        "etch CH3 from C2H2",
        "ide-1",
        "ide-2",
        "ide-3",
        "ide-4",
        "ide-5",
        "Single carbon etch (k11)",
        "C2H2 pendant to dimer"
    ]


def sum_raw_counts(df: DataFrame, indices: [int], as_percentage=False):
    """
    Return sum (and its error) for the list of event indices
    by accessing raw_counts_i within the passed dataframe
    """
    total, error = 0, 0
    for i in indices:
        total += df["raw_counts_{}".format(i)].mean()
        error += df["raw_counts_{}".format(i)].std()

    if as_percentage:
        iterations = df["iterations"].mean()
        return 100.0 * total / iterations, 100.0 * error / iterations
    else:
        return total, error


def sum_xevents(df: DataFrame, indices: [int], ix_max, ix_min=0):
    """
    return an array of summed xevents for a given list of event indices
    :param df:
    :param indices:
    :param ix_max: maximum xtime index to sum to
    :param ix_min: initial xtime index (default 0)
    :return: np.array
    """
    cum_total = np.zeros(ix_max - ix_min)
    cum_error = np.zeros(cum_total.shape)
    for i, ix in enumerate(range(ix_min, ix_max)):
        columns = [f"xevents_{ievent}_{ix}" for ievent in indices]
        cum_total[i] = np.sum([df[c ].mean() for c in columns])
        cum_error[i] = np.sum([df[c].std() for c in columns])

    return cum_total, cum_error


def atomic_layers_from_m(length_in_m):
    m_per_layer = 3.5698E-10 / 4
    return length_in_m / m_per_layer


def m_from_atom_layers(layers):
    m_per_layer = 3.5698E-10 / 4
    return layers * m_per_layer


def condid2cond(cond_id, show_reactor=False):
    """
    :param cond_id: condition id (from condition file)
    :param show_reactor: show reactor type
    :return: growth condition string
    """
    if show_reactor:
        return {"MCD_HF_0_B": "mcd (HF)",
                "UNCD_HF_0_B": "uncd (HF)",
                "UNCD_MW_0_B": "uncd (MW)",
                "SCD_MW_0_B": "scd (MW)",
                "NCD_HF_0_B": "ncd (HF)"}.get(cond_id, "unknown condition")
    else:
        return {"MCD_HF_0_B": "mcd",
                "UNCD_HF_0_B": "uncd",
                "UNCD_MW_0_B": "uncd",
                "SCD_MW_0_B": "scd",
                "NCD_HF_0_B": "ncd"}.get(cond_id, "unknown condition")


def ch3_concs(condition_id):
    return {"MCD_HF_0_B": 2.18E13,
                "UNCD_HF_0_B": 2.49E13,
                "UNCD_MW_0_B": 8.48E12,
                "SCD_MW_0_B": 2.86E13,
                "NCD_HF_0_B": 7.30E13}.get(condition_id)


def formatted_print_list(lst, fmt='.2f'):
    return ', '.join(map(lambda x: '{0:{1:s}}'.format(x, fmt), lst))


if __name__ == '__main__':

    DL_DATA = os.environ.get('DL_DATA') or \
              r'C:\Users\Max Work\OneDrive - University of Bristol\Documents\diamond_lattice\data'
    PYTHON_SCRIPTS = os.environ.get('PYTHON_SCRIPTS') or \
                     r'C:\Users\Max Work\local\src\diamond_lattice\python_scripts'
    import sys

    sys.path.append(PYTHON_SCRIPTS)
    from data_analysis import all_sims_stats as ass
    import numpy as np
    import pandas as pd

    base_path = os.path.join(DL_DATA, "thesis", "chapter1", "param_1d")

    d_all = ass.csv2dataframe(os.path.join(base_path, 'Ts_0_B_all.csv'))
    xvals_zero2NaN(d_all, True, False, True)
