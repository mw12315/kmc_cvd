from DLPlot import plotting
import matplotlib.pyplot as plt

if __name__ == '__main__':
    import pathlib

    event_path = pathlib.Path(r'F:\xyz\thesis_c1\mcd_migration_off\most_average\10s\4N_RMS\10s\mcd_DTE_DGDo_mcn_r10_10s\events.csv')

    times, grids = plotting.spatial_events_per_timestep(event_path, list(range(1,9)), 1.0, (9.0, 10.0))
    grid = grids[-1]
    time = times[-1]

    # for j in range(30):
    #     for i in range(30):
    #         print("{:3d}".format(grid[i, j]), end=" ")
    #     print()

    ax = plotting.plot_event_grid(time, grid, xticklabels=False, yticklabels=False)

    plt.show()
