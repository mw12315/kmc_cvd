"""
Generate a number of input files where etch barrier (isolated+S_B) and CH3 s/g factor are combined
"""
from collections import namedtuple
from itertools import product
from os import path, mkdir, getcwd, chdir
import input_generation
from input_generation.files import EnergyInputFile, ConditionsInputFile, generate_condition_dictonary, \
    generate_energy_dictionary
from input_generation.input import Reaction, Species

def replace_reaction_E_value(t_in: Reaction, new_E)-> Reaction:
    return Reaction(t_in.A, new_E)

def replace_species_s_value(t_in: Species, new_s)-> Species:
    return Species(t_in.conc_in_mols_per_cm3, new_s, t_in.g)


e_vals = input('Enter etch barrier (isolated/S_B edge) values in J/mol\n')
e_vals = map(float, e_vals.split())

s_vals = input('Enter CH3 s values\n')
s_vals = map(float, s_vals.split())

base_condition_dict = generate_condition_dictonary(input('Enter conditions you wish to generate for [mcd_hf, ncd_hf, '
                                                         'scd_mw]'))
base_energy_dict = generate_energy_dictionary()

# Create starting file objects
e_file = EnergyInputFile(base_energy_dict)
c_file = ConditionsInputFile(base_condition_dict)

counter = 0
base_folder = "ebarr_ch3_s"
with open('generate.log', 'w') as glog:
    glog.write("{:5s}  {:10s}  {:10s}\n".format('#', 'E(J/mol)', 'CH3 s'))
    for energy, s_val in product(e_vals, s_vals):
        counter += 1
        e_file.input_dict['CHx flex Iso'] = replace_reaction_E_value(e_file.input_dict['CHx flex Iso'], energy)
        e_file.input_dict['CHx flex S_B'] = replace_reaction_E_value(e_file.input_dict['CHx flex S_B'], energy)
        c_file.CH3 = replace_species_s_value(c_file.CH3, s_val)
        glog.write("{:5d} {:10.3e}  {:10.3f}\n".format(counter, e_file.input_dict['CHx flex Iso'].E, c_file.CH3.s))
        # Create directory
        dir_path = path.join(getcwd(), "{}_{}".format(base_folder, counter))
        try:
            mkdir(dir_path)
        except FileExistsError:
            pass
        # Open files and pass to file object write methods
        with open(path.join(dir_path, c_file.file_name), "w") as f_cond:
            with open(path.join(dir_path, e_file.file_name), "w") as f_eng:
                c_file.write_output(f_cond)
                e_file.write_file(f_eng)





