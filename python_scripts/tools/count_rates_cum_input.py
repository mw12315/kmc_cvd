#!/usr/bin/python
import numpy as np

file = input('file location: ')


event_file = np.genfromtxt(file+"\events.csv",delimiter=',',skip_header=1)
event_counter = open(file+"\cumulative_event_counts.csv", "w")
print("Data read in: Counting...")
count_ads = 0
count_ins = 0
count_etch = 0
count_migration = 0

event_counter.write("step,adsch3,insch3,etch,mig\n")
# enumeration
ads_ch3 = 1
insert_ch3 = 2
etch = 3
migrateN = 4
migrateE = 5
migrateS = 6
migrateW = 7


for index in range(0,len(event_file[:,1])):
    if int(event_file[index,2]) == ads_ch3:
        count_ads = count_ads + 1
    elif event_file[index,2] == insert_ch3:
        count_ins = count_ins + 1
    elif event_file[index,2] == etch:
        count_etch = count_etch + 1
    elif migrateN <= event_file[index, 2] <= migrateW:
        count_migration = count_migration + 1

    event_counter.write(str(event_file[index,0])+ " , " + str(event_file[index,1]) + " , " + str(count_ads)+" , "+str(count_ins)+" , "+str(count_etch)+" , "+str(count_migration)+"\n")


event_counter.close()
