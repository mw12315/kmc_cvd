#!/users/mw12315/local/usr/bin/python3
"""
Create an moving average from a multicolumn input file
"""
import numpy as np
from sys import argv
import re

def moving_average(y, window):
  """
  :param array: 1D list/numpy array
  :param window: size of moving average window
  :return: 1D array of moving average data
  """
  # Ensure that window is an even integer
  if window % 2 != 0: window += 1

# Set up returned y list
  y_out = []
  for i in range(window//2,len(y)-window//2):
    ave = []
    for j in range(-window//2,1+window//2):
      ave.append(y[i+j])
    y_out.append(np.average(ave))

  return y_out

# Input of file, x column, columns to be processed
if len(argv) < 5:
  print("Not all required arguments given!\nfile\nwindow_size\nx column index\n[columns]")
  exit()

file = argv[1]
pattern = re.compile("([A-Za-z_]*).[a-z]+")
file_name = pattern.match(file).group(1)
window = int(argv[2])
x_col = int(argv[3])
cols = list(map(int,argv[4:]))

print("Importing data file...",end="")
data = np.genfromtxt(file,delimiter=",",skip_header=1)
print("done")

with open(file,'r') as f:
  header = f.readline().strip().split(',')


cols_out = []
data_x = data[window//2:len(data[:,0])-window//2,x_col]
header_out = []
header_out.append(header[x_col])
for col in cols:
  print("Column {}".format(col))
  header_out.append(header[col])

  column_in = data[:,col]

  windowed_data = moving_average(column_in,window)

  cols_out.append(windowed_data)
# Output to file
stacked_columns = data_x[::window//2]
for col in cols_out:
  stacked_columns = np.column_stack( (stacked_columns,col[::window//2]) )
f_name = file_name+"_w_{}_{}".format(window,"_".join(map(str,cols)))

np.savetxt(f_name+'.csv', stacked_columns, delimiter=',',header=",".join(header_out))
print("Finished")
