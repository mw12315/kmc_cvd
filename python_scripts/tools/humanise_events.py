#!/usr/bin/python
import numpy as np
from numpy import genfromtxt

eventData = genfromtxt('events.csv',delimiter=',',skip_header=1,usecols=[0,2,3,4,5,6],defaultfmt=['%i','%c','%i','%i','%i','%i'])
humanData = open('events_human.csv',"w")
type = ""

for line in range(len(eventData[:,0])):
    if eventData[line,1] == 1:
        type = "Insert Trough"
    elif eventData[line,1] == 2:
        type = "Insert Dimer "
    elif eventData[line,1] == 3:
        type = "Etch Carbon  "
    elif eventData[line,1] >= 4:
        type = "Migrate      "

    humanData.write("{:2d} , {} , {} , {} , {} , {} , {}\n".format(int(eventData[line,0]),int(eventData[line,1]),type,int(eventData[line,2]),int(eventData[line,3]),int(eventData[line,4]),int(eventData[line,5])))


