import argparse
import json
import pathlib
import sys

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('s', help='snippet of json (valid json)')
    parser.add_argument('files',
                        help='files to append to', nargs='*')

    args = parser.parse_args()

    if len(args.files) == 0:
        print('Require at least 1 file')
        sys.exit()

    with open(args.s, 'r') as s:
        json_snippet = json.load(s)

    for file in args.files:
        print(pathlib.Path(file).parent)
        with open(file, 'r') as f:
            json_base = json.load(f)

        json_new = {**json_base, **json_snippet}

        with open(file, 'w') as f:
            json.dump(json_new, f)
