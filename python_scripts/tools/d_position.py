#!/usr/bin/python3
"""Take input of indices and output the real space position of that atom"""

i = int(input("i:     "))
j = int(input("j:     "))
ibasis = int(input("basis: "))
k = int(input("k:     "))

iplus = 0.0
jplus = 0.0
kplus = 0.0

if ibasis == 2:
  iplus = 0.5
  jplus = 0.5
elif ibasis == 3:
  iplus = 0.25
  jplus = 0.25
  kplus = 0.25
elif ibasis == 4:
  iplus = 0.75
  jplus = 0.75
  kplus = 0.25
elif ibasis == 5:
  iplus = 0.5
  kplus = 0.5
elif ibasis == 6:
  jplus = 0.5
  kplus = 0.5
elif ibasis == 7:
  iplus = 0.75
  jplus = 0.25
  kplus = 0.75
elif ibasis == 8:
  iplus = 0.25
  jplus = 0.75
  kplus = 0.75

x_pos = 3.5698 * (i + iplus)
y_pos = 3.5698 * (j + jplus)
z_pos = 3.5698 * (k + kplus)

print("({},{},{})".format(x_pos,y_pos,z_pos))
