#!/usr/bin/python3
"""Takes input of 3 index's and returns the direction of reconstruction"""
import sys

# read in indices from script arguments
i = int(sys.argv[1])
j = int(sys.argv[2])
ibasis = int(sys.argv[3])

multiplier = 1

if ibasis in [1,2,7,8]:
    index_sum = i+j+ibasis
    if ibasis in [7, 8]:
        multiplier = 3
else:
    index_sum = i + j
    if ibasis in [3,4]:
        multiplier = 2

if index_sum % 2 == 0:
    if ibasis in [1,2,5,6]:
        print("(+x,+y)")
    elif ibasis in [3,4]:
        print("(+x,-y)")
    elif ibasis in [7,8]:
        print("(-x,+y)")
else:
    if ibasis in [1,2,5,6]:
        print("(-x,-y)")
    elif ibasis in [3,4]:
        print("(-x,+y)")
    elif ibasis in [7,8]:
        print("(+x,-y)")



