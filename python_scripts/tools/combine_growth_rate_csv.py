"""
Take a input folder wildcard and create an time by time dataframe from their growth_rate.csv files
"""
from glob import glob
from sys import argv
from os import path
import numpy as np
import pandas as pd
import argparse


class IncrementError(Exception):
    pass


# Dataframe format:
# Index | time 1 | time 2 | time 3 |
# 0     | 500    | 300    |


def file_names(pattern):
    return glob(path.join(pattern, 'growth_rate.csv'), recursive=True)


def m_to_microns(v):
    return float(v) / 1E-6


def import_growth_rate(f_path):
    """
    return columnar data of time, average growth rqte and instant growth rate from the path
    """
    return np.genfromtxt(f_path, skip_header=1, usecols=(1, 2, 3), dtype=np.float, delimiter=',', autostrip=True, converters={2: m_to_microns, 3: m_to_microns})


def get_times(f_path):
    """
    Return 1d array of times
    """
    return np.loadtxt(f_path, delimiter=',', skiprows=1, usecols=1)


def analyse_times(file_paths):
    max_times, t_increment = np.zeros(len(files)), np.zeros(len(files))
    for i, p in enumerate(file_paths):
        f = get_times(p)
        max_times[i] = f[-1]
        t_increment[i] = f[-1] - f[-2]

    if np.all(t_increment == t_increment[0]):
        return np.max(max_times), t_increment[0]
    else:
        raise IncrementError("Increments are not equal")


def create_df(file_paths, t_incr, max_time, nmax_t_vals: int):
    for i, fp in enumerate(file_paths):
        f = import_growth_rate(fp)
        times = np.arange(t_incr, max_time+t_incr, t_incr)
        grs = np.full(shape=times.shape, fill_value=np.nan)
        igrs = np.full(shape=times.shape, fill_value=np.nan)
        times[:len(f[:, 0])] = np.round(f[:, 0], 2)
        grs[:len(f[:, 0])] = f[:, 1]
        igrs[:len(f[:, 0])] = f[:, 2]
        data_gr, data_igr = {}, {}
        for itime in range(len(times)):
            data_gr[times[itime]] = grs[itime]
            data_igr[times[itime]] = igrs[itime]
        if i == 0:
            df_gr, df_igr = pd.DataFrame(data_gr, index=[0]), pd.DataFrame(data_igr, index=[0])
        else:
            try:
                df_gr = df_gr.append(data_gr, ignore_index=True)
                df_igr = df_igr.append(data_igr, ignore_index=True)
            except ValueError:
                pass

    return df_gr, df_igr


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("p", help="directory path wildcard")
    parser.add_argument("-f", help="output filename base", dest="f")

    args = parser.parse_args()
    files = file_names(args.p)
    print("Found {} matching directories".format(len(files)))
    if len(files) == 0:
        exit(0)

    try:
        t_max, increment = analyse_times(files)
        max_tvals = int(t_max/increment)
        print("{:d} values between {:.2f} and {:.2f}".format(max_tvals, increment, t_max))
    except IncrementError as e:
        print(e.args[0])
        print("Ensure that input time increments are consistent")
        exit(1)

    gr, igr = create_df(files, increment, t_max, max_tvals)

    print(gr.head(len(files)))

    if args.f:
        base_fname = args.f if args.f[-1] == "_" else args.f + "_"
    else:
        base_fname = ""
    gr.to_pickle(path.join('.', base_fname + 'gr.pickle'))
    igr.to_pickle(path.join('.', base_fname + 'igr.pickle'))
