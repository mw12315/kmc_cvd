"""Output the iterations where an input rate type appears in the events.csv output and a final count of total events found"""

import numpy as np

events = np.genfromtxt('events.csv',delimiter=",",skip_header=1,usecols=(0,2))

chosen_rate = int(input("Rate to find: "))
output = open("find_event_"+str(chosen_rate)+".txt","w")
output.write("Iterations where rate {} occurs\n".format(str(chosen_rate)))

rate_counter = 0

for line in range(len(events[:,0])):
	if events[line][1] == chosen_rate:
		output.write("{:.0f}\n".format(events[line][0]))
		rate_counter +=1

output.write("{:.0f} total 'event {:.0f}s' in ./events.csv".format(rate_counter,chosen_rate))
print("{:.0f} total 'event {:.0f}s' in events.csv".format(rate_counter,chosen_rate))

output.close()
