"""Read in a frame xyz file, shifting all atoms by a input number of cells and output a new shifted frame file"""
import sys
import time

a0 = 3.5698  # lattice parameter in angstrom

f_name = sys.argv[1]
# Check file exists
try:
    f = open(f_name, "r")
    f.close()
except IOError:
    print('File not found')
    sys.exit(1)

# Input shift values
try:
    shiftx = int(input("x shift (unit cells): "))
    shifty = int(input("y shift (unit cells): "))
except ValueError:
    print('Invalid shift value (must be int)')
    sys.exit(1)

# Timing
start_time = time.time()

# New shifted output file open
new_f_name = "shift_"+str(shiftx)+str(shifty)+"_"+f_name
f_new = open(new_f_name, "w")

total_atoms = 0
total_frames = 0
with open(f_name, "r") as f:
    for line in f:
        split = line.split()
        if len(split) == 1 or len(split) > 9:
            if len(split) == 1:
                total_frames += 1
            f_new.write(line)
            continue
        else:  # Coordinate line (len=6)
            total_atoms += 1
            split[1] = round(float(split[1]) + shiftx * a0, 5)  # shift x coordinate by x shift
            split[1] = str(split[1])
            split[2] = round(float(split[2]) + shifty * a0, 5)  # shift y coordinate by x shift
            split[2] = str(split[2])
            new_line = "{:3s} {:15s} {:15s} {:15s} {:10s} {:5s} {:5s} {:5s} {:5s} ".format(split[0], split[1], split[2], split[3], split[4], split[5], split[6], split[7], split[8])
            f_new.write(new_line+"\n")

end_time = time.time()
total_time = round(end_time - start_time, 1)
print("Shifted {} frames by ({},{}) [{} atoms] in {} s".format(total_frames, shiftx, shifty, total_atoms, total_time))
print("Written to {}".format(new_f_name))
