import math
from scipy.constants import physical_constants
from numpy import nan, infty

kb = physical_constants['Boltzmann constant'][0]
N_A = physical_constants['Avogadro constant'][0]
r_gas = physical_constants['molar gas constant'][0]


def arrhenius_rate(pre_expon, energy_barrier, temperature_in_K, ebarrier_unit='kJmol'):
    if ebarrier_unit in ['kJ/mol', 'K']:
        if ebarrier_unit == 'K':
            return pre_expon * math.exp(-energy_barrier / temperature_in_K)
        elif ebarrier_unit == 'kJ/mol':
            return pre_expon * math.exp(-energy_barrier / (kb * temperature_in_K))
    else:
        print("Invalid energy unit")
        return None


def subreaction_equilibrium_constants(k_vals) -> list:
    reactions = {'f': {}, 'r': {}}
    n_reactions = 0
    for k, v in k_vals.items():
        n = int(k[0])
        if n > n_reactions:
            n_reactions = n
        direction = k[1]
        reactions[direction][n] = v

    equib_constants = [0.0] * n_reactions
    for i_n in range(n_reactions):
        if i_n+1 in reactions['f'] and i_n+1 in reactions['r']:
            if reactions['f'][i_n+1] == 0.0:
                equib_constants[i_n] = -infty
            elif reactions['r'][i_n+1] == 0.0:
                equib_constants[i_n] = infty
            else:
                equib_constants[i_n] = reactions['f'][i_n+1] / reactions['r'][i_n+1]
        else:
            equib_constants[i_n] = infty

    return equib_constants


def formatted_rate_print(value_list):
    for i, v in enumerate(value_list):
        if v == infty or v == infty:
            print("{:3d} {:5f}".format(i + 1, v))
        else:
            print("{:3d} {:5.3e}".format(i + 1, v))


def create_12_1_ae(temperature, hconc_in_per_cm3) -> (dict, dict):
    return {
               '1f': 3.3E13,
               '1r': 1.1E12,
               '2f': 2.8E11,
               '2r': 1.0E13,
               '3f': 2.0E4,
               '3r': 4.5E6 * (temperature ** 2) * (hconc_in_per_cm3 / N_A),
               '4f': 1.0E7,
               '4r': 0.0E0,
               '5f': 1.39E12,
               '5r': 4.44E11,
               '6f': 2.6E13,
               '6r': 1.32E12,
               '7f': 8.13E10,
               '7r': 9.4E13,
           }, {
               '1f': 26.925E3,
               '1r': 14.595E3,
               '2f': 14.847E3,
               '2r': 35.128E3,
               '3f': 0.0E3,
               '3r': 5E3,
               '4f': 0.0E0,
               '4r': 0.0E0,
               '5f': 10.216E3,
               '5r': 11.525E3,
               '6f': 4.428E3,
               '6r': 1.409E3,
               '7f': 3.774E3,
               '7r': 20.181E3,
           }


def create_12_2_ae(temperature, hconc_in_per_cm3):
    return {
               '1f': 4.1E8 * (temperature ** 1.5) * (hconc_in_per_cm3 / N_A),
               '1r': 1.6E9 * (temperature ** 1.2) * math.exp(-18.722E3 / temperature) + 1.8E12 * (
                       hconc_in_per_cm3 / N_A),
               '2f': 4.5E13 * (hconc_in_per_cm3 / N_A),
               '2r': 2.8E12 * (temperature ** 3.5) * (hconc_in_per_cm3 / N_A),
               '3f': 3.5E25 * (temperature ** -2.6) * (hconc_in_per_cm3 / N_A),
           }, {
               '1f': 0.503E3,
               '1r': 0.0E0,
               '2f': 0.0E3,
               '2r': 2.617E3,
               '3f': -45.496E3,
           }


def create_12_3_ae(temperature, hconc_in_per_cm3):
    return {
               '1f': 4.1E8 * (temperature ** 1.5) * (hconc_in_per_cm3 / N_A),
               '1r': 1.6E9 * (temperature ** 1.2) * math.exp(-18.722E3 / temperature) + 1.8E12 * (
                       hconc_in_per_cm3 / N_A),
               '2f': 2.1E13,
               '2r': 2.1E12,
               '3f': 5.2E13 * (hconc_in_per_cm3 / N_A),  # k1
               '3r': 4.5E6 * (temperature ** 2) * (hconc_in_per_cm3 / N_A),  # k9
               '4f': 4.1E8 * (temperature ** 1.5) * (hconc_in_per_cm3 / N_A),  # 1f
               '4r': 1.6E9 * (temperature ** 1.2) * math.exp(-18.722E3 / temperature) + 1.8E12 * (
                       hconc_in_per_cm3 / N_A),  # 1r
               '5f': 1.2E13,
           }, {
               '1f': 0.503E3,
               '1r': 0.0E0,
               '2f': 0.0E3,
               '2r': 13.394E3,
               '3f': 3.360E3,
               '3r': 2.516E3,
               '4f': 0.503E3,
               '4r': 0.0E0,
               '5f': 15.249E3,
           }


def create_12_4_ae(temperature, hconc_in_per_cm3):
    return {
               '1f': 5.2E13 * (hconc_in_per_cm3/N_A) * math.exp(-3360/temperature),
               '1r': 2.0E13 * (hconc_in_per_cm3/N_A),
               '2f': 9.4E13,
               '2r': 8.13E10,
               '3f': 1.32E12,
               '3r': 2.6E13,
               '4f': 4.8E11,
               '4r': 1.4E12,
               '5f': 3.8E11,
               '5r': 2.6E11,
               '6f': 1.3E13,
           }, {
               '1f': 0.503E3,
               '1r': 0.0E0,
               '2f': 20.181E3,
               '2r': 3.774E3,
               '3f': 1.409E3,
               '3r': 4.428E3,
               '4f': 18.269E3,
               '4r': 16.809E3,
               '5f': 9.663E3,
               '5r': 10.267E3,
               '6f': 16.809E3,
           }


def create_12_5_ae(temperature, hconc_in_per_cm3):
    return {
               '1f': 2.8E5 * temperature ** 2.5 * (hconc_in_per_cm3 / N_A),
               '1r': 5.4E14 * (hconc_in_per_cm3 / N_A),
               '2f': 4.3E13,
               '2r': 3.6E12,
               '3f': 5.2E13 * (hconc_in_per_cm3 / N_A),  # k1
               '3r': 4.5E6 * (temperature ** 2) * (hconc_in_per_cm3 / N_A),  # k9
               '4f': 4.5E13 * (hconc_in_per_cm3 / N_A),
               '4r': 1E14 * math.exp(-20.03E3 / temperature) + 9.6E13 * (hconc_in_per_cm3 / N_A),
               '5f': 1.3E13  # 4-6f
           }, {
               '1f': 6.14E3,
               '1r': 0.503E3,
               '2f': 19.678E3,
               '2r': 0.302E3,
               '3f': 3.360E3,  # k1
               '3r': 2.516E3,  # k9
               '4f': 1.208E3,
               '4r': 0.0,
               '5f': 16.809E3,
           }


def rate_coefficients(a_vals: dict, e_vals: dict, temperature_in_kelvin, e_unit) -> dict:
    rate_constants = {}
    for key in a_vals.keys():
        rate_constants[key] = arrhenius_rate(a_vals[key], e_vals[key], temperature_in_kelvin, ebarrier_unit=e_unit)

    return rate_constants


def create_k12_1_all(k_vals: dict) -> float:
    top = k_vals['1f'] * k_vals['2f'] * k_vals['3f'] * k_vals['4f']
    bottom = (k_vals['1r'] + k_vals['2f']) * k_vals['3f'] * k_vals['4f'] + (k_vals['3r'] + k_vals['4f']) * k_vals[
        '1r'] * k_vals['2r']
    return top / bottom


def create_k12_2_all(k_vals: dict) -> float:
    top = k_vals['1f'] * k_vals['2f'] * k_vals['3f']
    bottom = k_vals['1r'] * k_vals['2r'] + k_vals['1r'] * k_vals['3f'] + k_vals['3f'] * k_vals['2f']
    return top / bottom


def create_k12_3_all(kvals: dict) -> float:
    top = kvals['1f'] * kvals['2f'] * kvals['3f'] * kvals['4f'] * kvals['5f']
    bottom = (kvals['4r'] + kvals['5f']) * kvals['1r'] * kvals['2r'] * kvals['3r'] + (
            kvals['1r'] * kvals['2r'] + kvals['2f'] * kvals['3f']) * kvals['4f'] * kvals['5f']
    return top / bottom


def create_k12_4_all(kvals: dict) -> float:
    top = kvals['1f'] * kvals['2f'] * kvals['3f'] * kvals['4f'] * kvals['5f'] * kvals['6f']
    bottom = (kvals['5r'] + kvals['6f']) * kvals['2r'] * kvals['3r'] * kvals['4r'] + (
            kvals['1r'] * kvals['2r'] * kvals['4f'] + kvals['1r'] * kvals['2r'] * kvals['4f'] + kvals['1r'] * kvals[
        '3f'] * kvals['4f'] + kvals['2f'] * kvals['3f'] * kvals['4f']) * kvals['5f'] * kvals['6f']
    return top / bottom


def create_k12_5_all(kvals: dict) -> float:
    top = kvals['1f'] * kvals['2f'] * kvals['3f'] * kvals['4f'] * kvals['5f']
    bottom = (kvals['1r'] + kvals['2f']) * (
            kvals['2r'] * (kvals['3r'] * kvals['4r'] + kvals['3r'] * kvals['5f'] + kvals['4f'] * kvals['5f'])
            + kvals['3f'] * kvals['4f'] * kvals['5f']) + kvals['2f'] * kvals['2r'] * kvals['4f'] * kvals['4r']
    return top / bottom


def _k12_1_k_12_i(k12_1, k12_i):
    """
    Return the overall rate coefficient for sequential reactions
    :param k12_1: rate coefficient of k12-1
    :param k12_i: rate coefficient of k12-i
    :return: overall rate coefficient
    """
    return (k12_1 ** -1 + k12_i ** -1) ** -1


def k12i_1_overall_rate(coefficients, condition: str, i: int):
    """
    Return the overall rate coefficient for sequential reactions
    :param coefficients: dict of lists of rate coefficients (0-4)
    :param condition: the condition key
    :param i: 2nd rate in chain
    :return: result of combining input coefficient values
    """
    k12_1 = coefficients[condition][0]
    k12_i = coefficients[condition][i-1]
    return _k12_1_k_12_i(k12_1, k12_i)


if __name__ == '__main__':
    from input_generation.files import ConditionsInputFile

    conditions = ['scd_mw_0_B', 'mcd_hf_0_B', 'ncd_hf_0_B', 'uncd_hf_0_B', 'uncd_mw_0_B']

    k12_i_sub_all = {}
    K_12_i_sub_all = {}
    rate_coefficients_fr_all = {}
    for c in conditions:
        print(c)
        base_conditions: ConditionsInputFile = ConditionsInputFile.from_cond_id(c)
        temp_in_kelvin = base_conditions.Ts
        h_concentration_cm3 = base_conditions.H.conc_in_mols_per_cm3
        k12_1_sub = rate_coefficients(*create_12_1_ae(temperature=temp_in_kelvin, hconc_in_per_cm3=h_concentration_cm3),
                                      temperature_in_kelvin=temp_in_kelvin, e_unit='K')
        k12_2_sub = rate_coefficients(*create_12_2_ae(temperature=temp_in_kelvin, hconc_in_per_cm3=h_concentration_cm3),
                                      temperature_in_kelvin=temp_in_kelvin, e_unit='K')
        k12_3_sub = rate_coefficients(*create_12_3_ae(temperature=temp_in_kelvin, hconc_in_per_cm3=h_concentration_cm3),
                                      temperature_in_kelvin=temp_in_kelvin, e_unit='K')
        k12_4_sub = rate_coefficients(*create_12_4_ae(temperature=temp_in_kelvin, hconc_in_per_cm3=h_concentration_cm3),
                                      temperature_in_kelvin=temp_in_kelvin, e_unit='K')
        k12_5_sub = rate_coefficients(*create_12_5_ae(temperature=temp_in_kelvin, hconc_in_per_cm3=h_concentration_cm3),
                                      temperature_in_kelvin=temp_in_kelvin, e_unit='K')
        rate_coefficients_fr_all[c] = [k12_1_sub, k12_2_sub, k12_3_sub, k12_4_sub, k12_5_sub]

        K_12_i_sub_all[c] = [subreaction_equilibrium_constants(k12_i) for k12_i in
                             [k12_1_sub, k12_2_sub, k12_3_sub, k12_4_sub, k12_5_sub]]
        k12_i_sub_all[c] = [create_k12_1_all(k12_1_sub),
                            create_k12_2_all(k12_2_sub),
                            create_k12_3_all(k12_3_sub),
                            create_k12_4_all(k12_4_sub),
                            create_k12_5_all(k12_5_sub)]

    for c in conditions:
        print(c, "rate coefficients")
        formatted_rate_print(k12_i_sub_all[c])
    for c in conditions:
        print(c, "subreaction equilbrium constants")
        [formatted_rate_print(K_n_sub) for K_n_sub in K_12_i_sub_all[c]]

    for c in conditions:
        print(c, "f/r subreaction coefficients")
        for i in range(5):
            for j in rate_coefficients_fr_all[c][i].keys():
                print("12-{}-{}: {:5.3e}".format(i+1, j, rate_coefficients_fr_all[c][i][j]))

    print('\nk12-1 + k12-i')
    for c in conditions:
        print(c)
        for i in range(2, 6):
            print("12-1 + 12-{}: {:.2e}".format(i, k12i_1_overall_rate(k12_i_sub_all, c, i)))


    # Generate input file values (initial)
    for c in conditions:
        base_conditions: ConditionsInputFile = ConditionsInputFile.from_cond_id(c)
        temp_in_kelvin = base_conditions.Ts
        h_concentration_cm3 = base_conditions.H.conc_in_mols_per_cm3
        i_max = [7, 3, 5, 6, 5]
        print("\n{}\n".format(c))
        print("12, {:s}, {:s}, {:8s}, {:8s}, {:8s}, {:8s}".format("i", "j", "fA", "fE", "rA", "rE"))
        ae_12_1_5 = [create_12_1_ae(temp_in_kelvin,h_concentration_cm3),
                     create_12_2_ae(temp_in_kelvin, h_concentration_cm3),
                     create_12_3_ae(temp_in_kelvin, h_concentration_cm3),
                     create_12_4_ae(temp_in_kelvin, h_concentration_cm3),
                     create_12_5_ae(temp_in_kelvin, h_concentration_cm3)]

        for i in range(5):
            a, e = [], []
            for j in range(i_max[i]):
                fr_a, fr_e = [], []
                for d in ['f', 'r']:
                    k = str(j+1)+d
                    if k in ae_12_1_5[i][0].keys():
                        fr_a.append(ae_12_1_5[i][0][k])
                        fr_e.append(ae_12_1_5[i][1][k])
                    else:
                        fr_a.append(0.0)
                        fr_e.append(0.0)
                a.append(fr_a)
                e.append(fr_e)
            for j in range(len(a)):
                print("12, {:d}, {:d}, {:6.2e}, {:6.2e}, {:6.2e}, {:6.2e}".format(
                            i+1, j+1, a[j][0], e[j][0], a[j][1], e[j][1]))


