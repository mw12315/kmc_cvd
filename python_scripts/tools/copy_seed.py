"""copy seed of an output file into the input file within the same directory"""
import os
from sys import argv

if len(argv) == 1:
    p = '.'
else:
    p = argv[1]

seed = 0

#Get Seed
with open(os.path.join(p, "diamond.out"), "r") as f:
    for line in f.readlines():
      if "SEEDING CLOCK VALUE:" in line:
            split_line = line.split()
            seed = int(split_line[-1].strip())
            break

#Put seed into input file
with open(os.path.join(p, "simulation.input"), "r+") as f, open(os.path.join(p, "temp_simulation.input"), 'w') as output:
    for line in f.readlines():
        if "SEED:" in line:
            output.write("SEED: " + str(seed)+"\n")
        else:
            output.write(line)


# Delete original and rename temp to original
os.remove(os.path.join(p, "simulation.input"))
os.rename(os.path.join(p, "temp_simulation.input"), os.path.join(p, "simulation.input"))

print("{}:\t{}".format(os.path.basename(p), seed))

