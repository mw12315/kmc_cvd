"""
Read in a .xyz file and replace 'last_event' colour values
"""
import re
import argparse
import os

n_reactions = 27

# Parser
parser = argparse.ArgumentParser(f'Re-color last event atom colouring: expected event range: ({0}, {n_reactions})')
parser.add_argument('--file', '-f', type=argparse.FileType('r'), action='store', required=True, help='extended xyz '
                                                                                                    'file to process')
parser.add_argument('--event', '-e', action='append', required=True, type=int, help='event type to process')
parser.add_argument('--color', '-c', action='append', required=True, type=int, nargs=3, help='new R G B color (0-255)')
parser.add_argument('--only', '-o', action='store_true', default=False,
                    help='only specified atoms are colored, the rest are reset to carbon grey')

# args attributes: file, event, color
args = parser.parse_args()

# Process events and colors into a event: new color dictionary
colors = {}
for e, (i, c) in zip(args.event, enumerate(args.color)):
    rgb_color = [x / 255.0 for x in c]
    colors[e] = "{:3.2f} {:3.2f} {:3.2f}".format(*rgb_color)

f = args.file
name, extension = os.path.splitext(f.name)
event_str = "-".join(list(map(lambda x: "{:d}".format(x), args.event)))
f_new = open(name + f'_rc_{event_str}' + extension, 'w')

# Replace lines
line_pat = re.compile("([\d]*)\s*([\d.]{4}\s*[\d.]{4}\s*[\d.]{4})\s*[TF]")  # Capture event and RGB color
for line in f:
    search = line_pat.search(line)
    if search:
        event, c_string = int(search.group(1)), search.group(2)
        if event in colors.keys():
            f_new.write(line.replace(search.group(2), colors[event]))
        else:
            if args.only:  # Set selection flag to false (prevent custom coloring)
                f_new.write(line.replace('T', 'F'))
            else:
                f_new.write(line)
    else:
        f_new.write(line)

f.close()
f_new.close()
