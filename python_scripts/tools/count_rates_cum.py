"""
Create Cumulative data from an events.csv file
"""
import numpy as np
import sys
import os

if len(sys.argv) != 3:
    print("Usage: {} path/to/events.csv path/to/output")
    sys.exit(1)
else:
    f_path = sys.argv[1]
    b_path = sys.argv[2]


event_file = np.genfromtxt(f_path, delimiter=',', skip_header=1)
event_counter = open(os.path.join(b_path), 'w')

cum = np.zeros(24, dtype=np.int)
col_names = list(range(1, 25))
t = "{:6d},"*24 + "\n"
event_counter.write("{:10s},{:10s}".format("Step", "Time") + t.format(*col_names))
t2 = "{:10d}," + "{:10.5e}" + ",{:6d}"*24 + "\n"
for index in range(len(event_file[:, 1])):

    event = int(event_file[index, 2])
    time = event_file[index, 1]

    cum[event-1] += 1

    event_counter.write(t2.format(index+1, time, *cum[:]) )

