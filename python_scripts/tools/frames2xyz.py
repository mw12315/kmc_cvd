#!/usr/bin/python
import sys

input_file_name = sys.argv[1]
input_file = open(input_file_name, "r")
output_file_name = input_file_name[0:-4]
output_file = open(output_file_name + ".xyz", "w")
# log_file = open("log_file.txt","w")

# input_file = open("position_frames.txt","r")
# output_file = open("frames.xyz","w")

# read in entire file:
all_lines = input_file.readlines()
total_file_lines = len(all_lines)

dimerList = [-1, 1, -2, 2, -3, 3]
displace = 0.35

print("Total Lines in File: " + str(total_file_lines))

double_frame_counter = 0
for line in range(0, total_file_lines):
    current_line = all_lines[line]

    split_current_line = current_line.split()
    # Write frame number and atom number immediately to file
    if len(split_current_line) < 5:
        output_file.write(current_line)
        double_frame_counter = double_frame_counter + 1
    else:
        dimer = int(split_current_line[-1])
        if dimer in dimerList:
            if dimer == 1:
                x_pos = float(split_current_line[1])
                y_pos = float(split_current_line[2])
                x_pos = x_pos + displace
                y_pos = y_pos + displace
                split_current_line[1] = x_pos
                split_current_line[2] = y_pos
            elif dimer == 2:
                x_pos = float(split_current_line[1])
                y_pos = float(split_current_line[2])
                x_pos = x_pos + displace
                y_pos = y_pos - displace
                split_current_line[1] = x_pos
                split_current_line[2] = y_pos
            elif dimer == 3:
                x_pos = float(split_current_line[1])
                y_pos = float(split_current_line[2])
                x_pos = x_pos - displace
                y_pos = y_pos + displace
                split_current_line[1] = x_pos
                split_current_line[2] = y_pos

            elif dimer == -1:
                x_pos = float(split_current_line[1])
                y_pos = float(split_current_line[2])
                x_pos = x_pos - displace
                y_pos = y_pos - displace
                split_current_line[1] = x_pos
                split_current_line[2] = y_pos
            elif dimer == -2:
                x_pos = float(split_current_line[1])
                y_pos = float(split_current_line[2])
                x_pos = x_pos - displace
                y_pos = y_pos + displace
                split_current_line[1] = x_pos
                split_current_line[2] = y_pos
            elif dimer == -3:
                x_pos = float(split_current_line[1])
                y_pos = float(split_current_line[2])
                x_pos = x_pos + displace
                y_pos = y_pos - displace
                split_current_line[1] = x_pos
                split_current_line[2] = y_pos

        rejoined = "	".join(map(str, split_current_line))
        output_file.write(rejoined + "\n")

print("total frames output: " + str((double_frame_counter+1)/2))
