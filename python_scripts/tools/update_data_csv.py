"""
Take an older style data_all.csv file (diamond lattice output file collection) and convert to the new format
by removing spaces and making changes to a couple of column names (e.g. average growth rate m/h -> average_growth_rate)
"""
import pandas as pd
from sys import argv


def replace_column_spaces(csv_path):
    try:
        df = pd.read_csv(csv_path, delimiter=",", header=0, skipinitialspace=True)
    except FileNotFoundError:
        print("File doesn't exist!")
        return False

    counter = 0
    columns = list(df.columns)
    for i, c in enumerate(columns):
        if c == 'average growth rate m/h':
            c = 'average_growth_rate'
        else:
            if ' ' in c:
                c = c.replace(' ', '_')
            if '(' in c:
                c = c.replace('(', '_')
            if ')' in c:
                c = c.replace(')', '')

        columns[i] = c
        counter += 1

    df.columns = columns
    df.to_csv(csv_path, index=False)

    print("Replaced [' ', '(', ')'] from {} columns".format(counter))
    return True


if __name__ == "__main__":
    if len(argv) != 2:
        print("Usage: {} path/to/data_all.csv".format(argv[0]))
        exit(1)

    replace_column_spaces(argv[1])
