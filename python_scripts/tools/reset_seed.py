"""reset seed of an input file"""
import os

seed = 0

# Put seed into input file
with open("simulation.input","r+") as f, open("temp_simulation.input", 'w') as output:
    for line in f.readlines():
        if "SEED:" in line:
            output.write("SEED: " + "-1" +"\n")
        else:
            output.write(line)


# Delete original and rename temp to original
os.remove("./simulation.input")
os.rename("temp_simulation.input", "simulation.input")

