import argparse
import pandas as pd
import collections


def join_csv(files, output_file=None, kwargs={}):
    open_csv = []
    col_match = True
    for i, f in enumerate(files):
        open_csv.append(pd.read_csv(f, **kwargs))
        cols0 = set(open_csv[0].columns)
        colsi = set(open_csv[i].columns)
        if cols0 != colsi:
            print(f"Ensure column matching of {f}")
            diff = cols0-colsi if len(cols0) > len(colsi) else colsi - cols0
            print(diff)
            return 1

    result: pd.DataFrame = pd.concat(open_csv, ignore_index=True)

    if output_file is None:
        result.to_csv(files[0], mode="w", index=False)
    else:
        result.to_csv(output_file, mode="w", index=False)

    return 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", nargs="+", required=True, help="Files: [append target, file(s) to append]", dest="files")
    parser.add_argument("-o", required=False, help="Output for concatinated csv", default=None, dest="output")
    args = parser.parse_args()
    if len(args.files) > 1:
        exit(join_csv(args.files, args.output))
    else:
        print("Require more than a single file")
        exit(2)
