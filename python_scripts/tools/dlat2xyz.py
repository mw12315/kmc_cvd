"""script to convert an output of diamond lattice points in atom number | x position | y_position | z_position | into xyz format a.k.a replacing atom number with atom type. Takes argument of the name of the program minus the extension, input file should be of .txt MIME type."""
import sys
input_file_name = sys.argv[1]
input_file = open(input_file_name+".txt","r")
output_file = open(input_file_name+".xyz","w")

all_lines = input_file.readlines()
for index,line in enumerate(all_lines):
    if index == 0:
        output_file.write(str(len(all_lines)-1)+"\n")
        output_file.write(line)
    else:
        split_line = line.split()
        split_line[0] = "C"
        joined_line = " ".join(split_line)
        output_file.write(joined_line+"\n")

print("done")
input_file.close()
output_file.close()
