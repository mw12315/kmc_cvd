# CompareLogic.py
import numpy

def outputDimerDirection(i1,j1,b1,i2,j2):

    diff_i = i1 - i2
    diff_j = j1 - j2
    ind_i = 0
    ind_j = 0

    print("{} {}".format(diff_i,diff_j))
    if (abs(diff_i) > 1): diff_i = -(diff_i/abs(diff_i))
    if (abs(diff_j) > 1): diff_j = -(diff_j / abs(diff_j))
    print("{} {}".format(diff_i, diff_j))

    base_value = 1
    if (b1 == 1):
        if (diff_i > 0): base_value = -1
    elif (b1 == 2):
        if (diff_i == 0): base_value = -1
    elif (b1 == 3):
        if (diff_i > 0): base_value = -1
    elif (b1 == 4):
        if (diff_i == 0): base_value = -1
    elif (b1 == 5):
        if (diff_i == 0): base_value = -1
    elif (b1 == 6):
        if (diff_i > 0):
            base_value = -1
    elif (b1 == 7):
        if (diff_i < 0): base_value = -1
    elif (b1 == 8):
        if (diff_i == 0): base_value = -1

    if (diff_i == -1): ind_i = 0
    elif (diff_i == 0): ind_i = 1
    elif (diff_i == 1): ind_i = 2

    if (diff_j == -1): ind_j = 0
    elif (diff_j == 0): ind_j = 1
    elif (diff_j == 1): ind_j = 2


    print("{} {} ({}{}) = {} | ".format(diff_i,diff_j,ind_i,ind_j,base_value),end="")
    output = (ind_i,ind_j,log_diff)
    return output

logicArray = numpy.zeros((3,3))
demoArray = numpy.zeros((3,3))
index = 0
for j in range(0, 3):
    for i in range(0, 3):
        logicArray[i][j] = -5

tup_out = (0,0,0)

# for basis in range(1,9):
#     print("Basis {}".format(basis))
#     tup_out = compareLogic(2 , 2, basis, 3, 3)
#     logicArray[tup_out[1]][tup_out[0]] = tup_out[2]
#
#     tup_out = compareLogic(3, 2, basis, 3, 3)
#     logicArray[tup_out[1]][tup_out[0]] = tup_out[2]
#
#     tup_out = compareLogic(3, 2, basis, 2, 3)
#     logicArray[tup_out[1]][tup_out[0]] = tup_out[2]
#
#     tup_out = compareLogic(2, 2, basis, 3, 2)
#     logicArray[tup_out[1]][tup_out[0]] = tup_out[2]
#
#     tup_out = compareLogic(3, 2, basis, 3, 2)
#     logicArray[tup_out[1]][tup_out[0]] = tup_out[2]
#
#     tup_out = compareLogic(3, 2, basis, 2, 2)
#     logicArray[tup_out[1]][tup_out[0]] = tup_out[2]
#
#     tup_out = compareLogic(2, 3, basis, 3, 2)
#     logicArray[tup_out[1]][tup_out[0]] = tup_out[2]
#
#     tup_out = compareLogic(3, 3, basis, 3, 2)
#     logicArray[tup_out[1]][tup_out[0]] = tup_out[2]
#
#     tup_out = compareLogic(3, 3, basis, 2, 2)
#     logicArray[tup_out[1]][tup_out[0]] = tup_out[2]
#
#     for i in range(0,3):
#         print("\n")
#         for j in range(0,3):
#             print("{:2.0f}".format(logicArray[i][j]),end=" ")
#     print("\n")
