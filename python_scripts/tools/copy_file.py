import os
import subprocess as sp
import re
import shutil

cwd = os.getcwd()
print(cwd)
f = input("File name: ")
file_ext = input("File extension: ")
file_pattern = re.compile(f+"[\w]*")
for root, dirs, files in os.walk(cwd):
    for file in files:
        if re.match(file_pattern,file):
            location = os.path.join(root,file)

            shutil.copyfile(location,"{}_{}.{}".format(os.path.join(cwd,os.path.basename(root)), f, file_ext))
            print("{}{}_{}".format(cwd,os.path.basename(root),file))
            break
