#!/usr/bin/python3
"""script to calculate rate constants from temperature, activation energies and pre-factors"""

from math import *

# --------- #
# Constants #
# --------- #
# Tsurf = 1173
Tsurf = 1200
# Tns = 1267
Tns = 1300
R = 8.314
NA = 6.02E23

# --------- #
#   Gases   #
# --------- #
concH = 1.85E14
concH2 = 1.52E17
concCH3 = 1.46E13

# --------------- #
# CH3 Impact Rate #
#    per site     #
# --------------- #
sCH3 = 0.5
gCH3 = 0.15
sg = sCH3 * gCH3
mass_CH3 = 15E-3 # kg mol-1
v_CH3 = sqrt((8 * R * Tns)/ (pi * mass_CH3))  # m/s
p_carbon = 1.56E15  # atoms per cm2

CH3Impact = (sg * concCH3 * (v_CH3 * 100))/ (p_carbon * 4)
print(sg)
print(concCH3)
print(v_CH3)
print(p_carbon)
print((sg * concCH3 * v_CH3 * 100)/(4 * p_carbon))

# -------------- #
# Rate Variables #
# -------------- #

# H Abstraction (CH + H -> C* + H2) k1
k0_Habs = 3.2E-12  # cm3 s-1
E_Habs = 3430  # K

# Hydrogen Addition (C* + H2 -> CH + H) km1
k0_H2add = 3.2E-13  # cm3 s-1
E_H2add = 7850  # K

# H Addition (C* + H -> CH) k2
k0_Hadd = 9.6E-12  # cm3 s-1
E_Hadd = 0.0  # K

# H Desorb (CH -> C* + H)
A_Hdes = 1E13   # s-1
E_Hdes = 413E3  # kJ/mol

# CH3 Pendant Adsorption
k0_CH3Pen = 2.4E-13
E_CH3Pen = 0.0

# CH3 Dimer Insertion (Frenkel)
k_CH3Dinsert = 1E13 / NA

# CH2 Migration
A_mig = 6.13E13
E_mig = 128.4E3

# Etching
A_etch = 2.5E13
E_etch = 200E3

#####################
Ea_DI = 53.1E3
A_DI = 1E13
k_DI = A_DI * exp(-Ea_DI/(R*Tsurf))
r_DI = k_DI * CH3Impact

# -------------------------- #
# Rate Constant Calculations #
# -------------------------- #
k_Habs = k0_Habs * Tns**0.5 * exp(-E_Habs/Tsurf)

k_H2add = k0_H2add * Tns**0.5 * exp(-E_H2add/Tsurf)

k_Hadd = k0_Hadd * Tns**0.5 * exp(-E_Hadd/Tsurf)

k_Hdes = A_Hdes * exp(-E_Hdes/R * Tsurf)

k_CH3Pen = k0_CH3Pen * Tns**0.5 * exp(-E_CH3Pen/Tsurf)


k_mig = A_mig * exp(-E_mig/(R*Tsurf))

k_etch = A_etch * exp(-E_etch/(R*Tsurf))


monorad_yuri = 1/(1 + 0.3*exp(3430/Tsurf) + 0.1*exp(-4420/Tsurf)*(concH2/concH))
monorad_butler = k_Habs/(k_Habs + k_Hadd)

# -------------------------- #
#      Rate Calculations     #
# -------------------------- #
r_Habs = k_Habs * concH
r_H2add = k_H2add * concH2
r_Hadd = k_Hadd * concH

r_CH3Pen = k_CH3Pen * concCH3
r_CH3Dinsert = k_CH3Dinsert * concCH3
r_mig = k_mig
r_etch = k_etch

# ------ #
# Output #
# ------ #

print("--- Concentrations ---\n")
print("H  : {:5.2E} cm3".format(concH))
print("H2 : {:5.2E} cm3".format(concH2))
print("CH3: {:5.2E} cm3".format(concCH3))
print("CH3 Velocity: {:5.2E} m/s".format(v_CH3))
print("CH3 Site Impact Rate: {:5.2E} per second per site\n".format(CH3Impact))

print("--- Rates ---\n")
print("H Abstraction (k1): {:10.3f} ".format(r_Habs))
print("H2 Addition (km1) : {:10.3f} ".format(r_H2add))
print("H Addition (k2)   : {:10.3f}\n".format(r_Hadd))

print("CH3 Pendant Adsorption to Dimer: {:10.3f} ".format(r_CH3Pen))
print("CH3 Dimer Insertion            : {} ".format(r_CH3Dinsert))
print("CH2 Migration                  : {} ".format(r_mig))
print("Carbon Etch (Isolated)         : {} \n".format(r_etch))

print("Mono(bi)-Radical Site %:")
print("Yuri  :  {:5.3f} ({:5.3f}) ".format(monorad_yuri*100,(monorad_yuri**2)*100))
print("Butler:  {:5.3f} ({:5.3f}) ".format(monorad_butler*100,(monorad_butler**2)*100))

print("--- Extra Rates ---")
print("CH3 Dimer Insertion:    {:5.2E}".format(r_DI))



