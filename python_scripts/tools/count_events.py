"""Count occurrences of each rate within an events.csv file"""
from sys import argv, exit

num_rates = 19

if len(argv) < 2:
    print("Usage: {} events.csv".format(argv[0]))
    exit(1)

file_path = argv[1]

counts = [0] * num_rates

with open(file_path, 'r') as f:
    for line in f:
        if len(line.split()) == 4: continue

        event = int(line.split(',')[2]) - 1
        counts[event] += 1

print("{:10s} {:10s}".format("rate index", "count"))
for i in range(len(counts)):
    print("{:10} {:10}".format(i+1,counts[i]))
