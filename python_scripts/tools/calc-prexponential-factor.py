import argparse
from scipy.constants import gas_constant
import math

parser = argparse.ArgumentParser()
parser.add_argument('k', type=float, help='rate of reaction (/s)')
parser.add_argument('E', type=float, help='energy barrier in kJ/mol')
parser.add_argument('T', type=float, help='temperature in kelvin')
args = parser.parse_args()

# k = A * exp(-E/RT)
# A = k/exp(-E/RT)
A = args.k / (math.exp(-args.E/(gas_constant * args.T)))
print("Arrhenius pre-exponential factor: {:5.2e} s\u207B\u00B9".format(A))
