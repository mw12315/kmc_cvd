import json
import os
import sys
import flatten_json
import csv
import glob
import argparse


class Simulation(object):
    def __init__(self, output_file):
        with open(output_file) as f:
            self.data: dict = json.load(f)

        self.row = flatten_json.flatten_json(self.data)
        self.location = {'location': os.path.split(os.path.abspath(output_file))[0]}
        self.row = {**self.location, **self.row}


if len(sys.argv) != 1:
    delete = True
else:
    delete = False


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--delete", dest="delete", action="store_true", default=False, help="delete submission scripts if complete")
    parser.add_argument("-p", "--path", dest="path", help="limit search to a path wildcard [glob] (** for recursive mode)", default="**")
    parser.add_argument("-o", "--output", dest="output_fname", help="output file path",
                        default="{}_all.csv".format(os.path.basename(os.getcwd())))

    args = parser.parse_args()
    if args.delete: print("removing submission scripts for completed simulations")

    files_found = 0
    files_read = 0
    print("Output to {}\n".format(args.output_fname))

    files = glob.glob(os.path.join(args.path, "diamond.json"), recursive=True)
    if len(files) == 0:
        print("Read 0 output files")
        exit(0)

    files_found = len(files)

    with open(args.output_fname, 'w') as f_all:
        sims = []
        unfinished = []
        errors = []
        for f_djson in files:
            containing_dir = os.path.split(f_djson)[0]
            if os.path.getsize(f_djson) > 0:
                print(containing_dir)
                files_read += 1
                if files_read == 1:  # Ensure this only runs for the first file
                    with open(f_djson) as f:
                        # Get fields of input files
                        ex_fields = list(flatten_json.flatten_json(json.load(f)).keys())
                        writer = csv.DictWriter(f_all, fieldnames=['location'] + ex_fields)
                        writer.writeheader()

                try:
                    sims.append(Simulation(f_djson))
                except json.decoder.JSONDecodeError:
                    errors.append(f_djson)

                if delete:
                    sh_files = glob.glob(os.path.join(os.path.split(f_djson)[0], '*.sh'))
                    for sh_file in sh_files:
                        os.remove(sh_file)
            else:
                unfinished.append(os.path.abspath(containing_dir))

        for sim in sims:
            try:
                writer.writerow(sim.row)
            except ValueError as e:
                print(sim.location, 'invalid .json fields - old .json file?', e)

    try:
        print("Read {} of {} files ({:3.1f}%)".format(files_read, files_found, 100*(files_read/files_found)))
    except ZeroDivisionError:
        print("Read {} of {} files ({:3.1f}%)".format(files_read, files_found, 0.0))

    if len(unfinished):
        print("Unfinished: ")
        print(" ".join(unfinished))

    if len(errors) > 0:
        print("\nJSON ERRORS")
        print(" ".join(errors))
