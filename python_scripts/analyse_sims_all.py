import pandas as pd
import matplotlib.pyplot as plt
from sys import argv, exit
from os import path

# Open CSV containing simulation data
data_file = ""
if len(argv) == 2:
    data_file = argv[1]
else:
    print("Usage: {} path/to/all_sims_file.csv".format(argv[0]))
    exit(1)

file_without_ext = path.splitext(path.split(data_file)[1])[0]

DF = pd.read_csv(filepath_or_buffer=data_file, delimiter=",", header=0, skipinitialspace=True)

#!!!!!!!!!!!!!!!!!!
# Summarise Results
#!!!!!!!!!!!!!!!!!!
print(DF.columns)
group_col = input("Group Column: ")

summary = DF.groupby(by=group_col).agg({'Growth Rate / m/h' : ['mean', 'std'],
                              'Roughness /m' : ['mean', 'std'],
                              '2N RMS' : ['mean', 'std'],
                              '4N RMS' : ['mean', 'std'],
                              '% Dimer Ins.' : ['mean', 'std'],
                              '% Trough Ins.' : ['mean', 'std'],
                              '% Etch' : ['mean', 'std'],
                              '% Migration' : ['mean', 'std'],
                              'CPU' : ['mean'],
                              })

summary = summary.reset_index()

# Rename columns within data series
summary.columns = [group_col, 'G. Rate mean', 'G. Rate std',
                   'RMS Roughness mean', 'RMS Roughness std',
                   '2N RMS mean', '2N RMS std',
                   '4N RMS mean', '4N RMS std',
                   '% Dimer Ins. mean', '% Dimer Ins. std',
                   '% Trough Ins. mean', '% Trough Ins. std',
                   '% Etch mean', '% Etch std',
                   '% Migration mean', '% Migration std',
                   'CPU mean'
                   ]

summary_file = '{}_summary.csv'.format(file_without_ext)
with open(summary_file, 'w') as f:
    summary.to_csv(summary_file)

# Plot some of the summary
# Growth Rates
print(summary.head())

# summary.plot(x=summary[group_col],y=summary['G. Rate mean'])
