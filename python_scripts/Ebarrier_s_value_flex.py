"""
Generate a number of input files where etch barrier (isolated) and s/g factor are combined
"""
from collections import namedtuple
from itertools import product
from os import path, mkdir, getcwd, chdir
from input_generation.files import EnergyInputFile, ConditionsInputFile


# def replace_reaction_E_value(t_in: Reaction, new_E) -> Reaction:
#     return Reaction(t_in.A, new_E)
#
#
# def replace_species_s_value(t_in: Species, new_s) -> Species:
#     return Species(t_in.conc_in_mols_per_cm3, new_s, t_in.g)
#
#
# def replace_species_g_value(t_in: Species, new_g) -> Species:
#     return Species(t_in.conc_in_mols_per_cm3, t_in.s, new_g)


e_vals = input('Enter etch barrier (Battaile isolated) values in J/mol\n')
e_vals = map(float, e_vals.split())

s_vals = input('Enter CH3 s values\n')
s_vals = map(float, s_vals.split())

c_file = ConditionsInputFile.from_cond_id(input('Growth Condition: '))
e_file = EnergyInputFile.default()

counter = 0
base_folder = "sg_etch"
with open('generate.log', 'w') as glog:
    glog.write("{:5}  {:10}  {:10}\n".format('#', 'Eiso(J/mol)', 'CH3 s'))
    for energy, s_val in product(e_vals, s_vals):
        counter += 1
        e_file.CHx_flex_Iso.E = energy
        e_file.CHx_flex_S_B.E = energy
        c_file.CH3.s = s_val
        glog.write("{:5d} {:10.2e}  {:10.2f}\n".format(counter, e_file.CHx_flex_Iso.E, c_file.CH3.s))
        # Create directory
        dir_path = path.join(getcwd(), "{}_{}".format(base_folder, counter))
        try:
            mkdir(dir_path)
        except FileExistsError:
            pass
        # Open files and pass to file object write methods
        with open(path.join(dir_path, c_file.file_name), "w") as f_cond:
            with open(path.join(dir_path, e_file.file_name), "w") as f_eng:
                c_file.write_output(f_cond)
                e_file.write_file(f_eng)
