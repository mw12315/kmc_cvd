"""
Generate output files for traversing a parameter space for a single variable
"""
from os import path, mkdir, getcwd
from input_generation import files
from input_generation.files import param_family
from input_generation.input import write_gen_log_line, read_parameter_to_vary, \
    read_parameter_values, condition_energy_fobj

if __name__ == '__main__':
    from sys import argv

    starting_dir_num = int(argv[1]) if len(argv) == 2 else 0

    parameter_to_vary = read_parameter_to_vary()
    param_values = read_parameter_values(parameter_to_vary)

    condition_file, energy_file = condition_energy_fobj()
    base_name = parameter_to_vary

log = open('generate_{}.log'.format(parameter_to_vary), 'w')
log.write("{:10s}{:10s}\n".format("Counter", parameter_to_vary))

current_dir_label = starting_dir_num
total_dirs = 0
for v in param_values:
    current_dir_label += 1
    total_dirs += 1
    if param_family[parameter_to_vary] == 'conditions':
        condition_file.update_value(parameter_to_vary, v)
        ref = files.InstanceRef.from_param(condition_file, parameter_to_vary)
    elif param_family[parameter_to_vary] == 'energy':
        energy_file.update_value(parameter_to_vary, v)
        ref = files.InstanceRef.from_param(energy_file, parameter_to_vary)

    write_gen_log_line(current_dir_label, v, parameter_to_vary, log)
    dir_path = path.join(getcwd(), "{}_{}".format(base_name, current_dir_label))
    try:
        mkdir(dir_path)
    except FileExistsError:
        pass

    with open(path.join(dir_path, condition_file.file_name), "w") as f_cond:
        with open(path.join(dir_path, energy_file.file_name), "w") as f_eng:
            condition_file.write_output(f_cond)
            energy_file.write_file(f_eng)

log.close()
print("generated {} input file directories".format(total_dirs))
