import subprocess
from sys import argv
import os
import glob

DIAMOND_LATTICE = os.environ.get('DLAT_PATH') or "/mnt/storage/home/mw12315/local/bin/diamond"

if len(argv) == 1:
    print("Expecting NP as argument")
    exit(2)
np = int(argv[1])

directories = glob.glob("*/")
if len(directories) > np:
    print("""More directories than jobs
    only first {} directories will be processed:""".format(np))
    directories = directories[:np]
    [print(d) for d in directories]

completed = [subprocess.run(DIAMOND_LATTICE,
                            stdout=open(os.path.join(d, 'o.out'), 'w'),
                            stderr=open(os.path.join(d, 'e.out'), 'w'),
                            cwd=d) for d in directories]

print("Jobs terminated with non-zero exit status:")
failed = 0
for i, c in enumerate(completed):
    if c.returncode != 0:
        failed += 1
        print(directories[i], end=" ")

print("{:d}/{:d} ({:2.1f}%)".format(failed, len(directories), 100*(failed/len(directories))))


