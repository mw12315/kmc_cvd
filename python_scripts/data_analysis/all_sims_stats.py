import pandas as pd
import sys
import os
import collections
import argparse

non_numerical_columns = ['location',
                         'name',
                         'etch model',
                         'align_dimers',
                         'realign_dimers']


def csv2dataframe(path_to_all_sims_csv):
    return _add_percent_columns(pd.read_csv(path_to_all_sims_csv, delimiter=",", header=0, skipinitialspace=True))


def summarise_all_sims(d: pd.DataFrame):
    """
    Catalog grouped data and return a DataFrame
    """

    MeanStdT = collections.namedtuple('MeanStdT', 'mean std')
    sim_summary: pd.DataFrame = d.agg({'average_growth_rate': MeanStdT('mean', 'std'),
                                       'rms_roughness': MeanStdT('mean', 'std'),
                                       '2N_rms_roughness': MeanStdT('mean', 'std'),
                                       '3N_rms_roughness': MeanStdT('mean', 'std'),
                                       '4N_rms_roughness': MeanStdT('mean', 'std'),
                                       'iterations': MeanStdT('mean', 'std'),
                                       'sim_time': MeanStdT('mean', 'std'),
                                       'nucleation_time': MeanStdT('mean', 'std'),
                                       'migrations_per_atom_0': MeanStdT('mean', 'std'),
                                       'average_Fmr': MeanStdT('mean', 'std'),
                                       'CPU_time': MeanStdT('mean', 'std'),
                                       })

    return sim_summary.transpose()


def _add_percent_columns(d: pd.DataFrame):
    """
    Generate percentage columns for each type of reaction
    Should be refactored to use the 'raw_counts' array instead
    :param d: dataframe imported from csv
    :return: dataframe augmented with new columns
    """
    try:
        d = d.assign(pc_d_ins=100 * (d['count_d_ins_CH3']
                                     + d['count_d_ins_CH2']
                                     + d['count_d_ins_CH']
                                     + d['count_d_ins_C']) / d['iterations'])
        d = d.assign(pc_t_ins=100 * (d['count_t_ins_CH3']
                                     + d['count_t_ins_CH2']
                                     + d['count_t_ins_CH']
                                     + d['count_t_ins_C']) / d['iterations'])
        d = d.assign(pc_C2H2_Mx=100 * (d['count_C2H2_M1_0'] +
                                       d['count_C2H2_M1_1'] +
                                       d['count_C2H2_M2'] +
                                       d['count_C2H2_M3']) / d['iterations'])
        d = d.assign(pc_mig_dimer=100 * ((d['count_mig_dimer_0'] + d['count_mig_dimer_1']) / d['iterations']))
        d = d.assign(pc_mig_gap=100 * (
                (d['count_mig_gap_0'] + d['count_mig_gap_1'] +
                 d['count_mig_gap_2'] + d['count_mig_gap_3']) / d['iterations']))
        d = d.assign(pc_mig_down=100 * (d['count_mig_down'] / d['iterations']))
        d = d.assign(pc_mig_C2H2=100 * ((d['count_mig_C2H2_biradical'] + d['count_mig_C2H2']) / d['iterations']))
        d = d.assign(pc_pref_etch_chx=100 * (d['count_pref_etch_CHx_0']) / d['iterations'])
        d = d.assign(pc_iso_etch_chx=100 * (d['count_pref_etch_CHx_1']) / d['count_pref_etch_CHx_0'])
        d = d.assign(pc_etch_C2H2=100 * ((d['count_pref_etch_C2H2_CH3_0'] + d['count_pref_etch_C2H2_1']) / d['iterations']))
        d = d.assign(pc_ide_etch=100 * (d['count_ide_etch_ide_12_2'] +
                                        d['count_ide_etch_ide_12_3'] +
                                        d['count_ide_etch_ide_12_4'] +
                                        d['count_ide_etch_ide_12_5']) / d['iterations'])
        d = d.assign(pc_c2h2p_mig=100 * (d['count_mig_C2H2p_gap_0'] + d['count_mig_C2H2p_gap_1']) / d['iterations'])
        d = d.assign(pc_c2h2p_dimer=100 * d['count_mig_C2H2p_dimer'] / d['iterations'])
        d = d.assign(pc_k11_etch=100 * d['count_k11_etch_CHx'] / d['iterations'])
        d = d.assign(pc_etch_all=100 * (d['count_k11_etch_CHx'] +
                                        d['count_ide_etch_ide_12_2'] +
                                        d['count_ide_etch_ide_12_3'] +
                                        d['count_ide_etch_ide_12_4'] +
                                        d['count_ide_etch_ide_12_5'] +
                                        d['count_pref_etch_CHx_0']) / d['iterations'])

    except KeyError:
        pass

    return d


def subtract_sim_data(d1: pd.DataFrame, d2: pd.DataFrame):
    return remove_non_numerical_columns(d2) - remove_non_numerical_columns(d1)


def remove_non_numerical_columns(d: pd.DataFrame):
    for column in non_numerical_columns:
        if column in list(d.columns):
            d = d.drop(axis=1, labels=[column])

    return d


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("files", nargs="*", help="*_all.csv filepaths")
    args = parser.parse_args()

    print(50 * "-")
    for f in args.files:
        print(f"{((50-len(f))//2)*'-'}{f}{((50-len(f))//2)*'-'}")
        df = csv2dataframe(f)
        print(summarise_all_sims(df).head(30))
        print(50 * "-")
