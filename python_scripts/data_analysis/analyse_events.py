# AnalyseEvents
import numpy as np
from matplotlib import pyplot as plt
import sys
import os
from mpl_toolkits.mplot3d import axes3d, Axes3D
"""Read events.csv and analyse/plot the activities of each grid"""
if len(sys.argv) != 2:
    print("Usage: {} path/to/events.csv".format(sys.argv[0]))
    event_path = os.path.join(os.getcwd(), 'events.csv')
else:
    event_path = sys.argv[1]


def plotheatmap(array,number,time,ave_events):
    # Plot general array
    fig_general_array = plt.figure('ALL EVENTS')
    # axes_general_array = Axes3D(fig_general_array)
    axes_general_array = fig_general_array.add_subplot(1, 1, 1)
    axes_general_array.set_xlim(1,grid_x)
    axes_general_array.set_ylim(1,grid_y)
    axes_general_array.set_xticks(list(range(1, grid_x + 1)))
    axes_general_array.set_yticks(list(range(1, grid_y + 1)))

    axes_general_array.set_title("(Events per Cell - Average) @ {:5.2f} s".format(time))

    X, Y = np.meshgrid(list(range(1, grid_x + 1)), list(range(1, grid_y + 1)))

    Z = array

    # axes_general_array.plot_surface(X,Y,Z,cmap='hot')
    cf = axes_general_array.pcolormesh(X, Y, Z - ave_events, cmap='hot')
    fig_general_array.colorbar(cf, ax=axes_general_array, spacing='proportional', extend='neither', extendfrac='auto')
    number = round(number,2)
    file_name = str(number) + '.png'
    fig_general_array.savefig(file_name)

    plt.close()

    return


events_file = np.genfromtxt(event_path, delimiter=",", skip_header=1, usecols=(0, 1, 3, 4))

# Get grid dimension from first line in  events.csv
with open(event_path, 'r') as in_file:
    first_line = in_file.readline().split()
    grid_x = int(first_line[0])
    grid_y = int(first_line[1])

print(grid_x, grid_y)
time_range = input("Time Range (start finish): ").split()
time_range = [float(i) for i in time_range]
print(time_range)
output_sep = float(input('seconds increment: '))

general_array = np.zeros((grid_x, grid_y))
number = time_range[0]
next_frame_time = time_range[0]
# Increment arrays according to position (general array) AND type of rate (array of arrays)
for line in range(0, np.size(events_file, 0)):
    event = int(events_file[line, 0])
    time = float(events_file[line, 1])
    i = int(events_file[line, 2])-1
    j = int(events_file[line, 3])-1
    general_array[i][j] += 1
    if time_range[0] <= time <= time_range[1] + (output_sep * 0.5):
        if time >= next_frame_time:
            print(time, event)
            ave_events_per_site = (line+1)/(grid_x * grid_y)
            next_frame_time = next_frame_time + output_sep
            plotheatmap(general_array, number, time, ave_events_per_site)
            number += output_sep








