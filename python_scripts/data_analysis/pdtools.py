"""Contains important definitions (and classes) for repeated using during analysis"""
import numpy as np

# Output Statistical Information on Simulations within input .csv(s)
def stat_sims(ave_rness, std_rness, ave_grate, std_grate, n_sims, ave_sim_time, std_sim_time, ave_cpu, std_cpu):

    print("\nStatistical information for {} simulations:".format(n_sims))

    print("Average RMS Roughness: {:6.2f} nm   +/-{:6.2f}nm   ({:4.2f}%)".format(
        ave_rness/1E-9, std_rness/1E-9, 100*std_rness/ave_rness))

    print("Average Growth Rate:   {:6.2f} µm/h +/-{:6.2f}µm/h ({:4.2f}%)".format(
        ave_grate/1E-6, std_grate/1E-6, 100*std_grate/ave_grate))

    print("Average Simulated Time:{:6.1f} s    +/-{:6.2f} s".format(
        ave_sim_time, std_sim_time))

    print("Average CPU Time:      {:6.3f} m    +/-{:6.3f} m".format(
        ave_cpu*60, std_cpu*60))

# Accepts a list of error bar values or a single value
def plot_bar(df, column, yerror, title, ylab, xlab, xlab_tic,units):
    if isinstance(yerror,list):
        yerror = [x/units for x in yerror]
    else:
        yerror = yerror/units
    if not isinstance(yerror,list) and yerror == 0.0:
        plot = (df[column] / units).plot.bar(title=title)
    else:
        plot = (df[column]/units).plot.bar(yerr=yerror, capsize=5, title=title)

    plot.set_ylabel(ylab)
    plot.set_xlabel(xlab)
    if len(xlab_tic) != 0:
        plot.set_xticklabels(xlab_tic, rotation='horizontal')

    return plot


def plot_event_bar(df,columns,title,xlab_tic):
    plot = df[columns].plot.bar(title=title,stacked=True)
    plot.set_ylabel('Percentage of Simulation Events')
    if len(xlab_tic) != 0:
        plot.set_xticklabels(xlab_tic, rotation='horizontal')

    return plot


def plot_scatter(df, columnx, columny, yerror, title, xlab, ylab, yunits):
    if isinstance(yerror,list):
        yerror = [x/yunits for x in yerror]
    else:
        yerror = yerror/yunits
    if not isinstance(yerror,list) and yerror == 0.0:
        plot = df.plot.scatter(x=columnx, y=columny, title=title)
    else:
        plot = df.plot.scatter(x=columnx, y=columny, yerr=yerror, capsize=5, title=title)

    plot.set_ylabel(ylab)
    plot.set_xlabel(xlab)
    # if len(xlab_tic) != 0:
    #     plot.set_xticklabels(xlab_tic, rotation='horizontal')

    return plot

