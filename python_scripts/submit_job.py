import os
import sys
import re
import subprocess as sp
from time import sleep
import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-w", "--walltime", default="05:00:00", dest="wt", help="walltime of the jobs")
    parser.add_argument("-m", "--mem4g", default=False, help="use 4G/cpu", dest="mem4g", action="store_true")
    parser.add_argument("-s", "--skip-sleep", default=False, action="store_true",
                        help="skip sleep between submission", dest="skip_sleep")
    args = parser.parse_args()

walltime = args.wt.strip()

if args.mem4g:
    options = "-l walltime=" + walltime + ",nodes=1:mem4G"
else:
    options = "-l walltime=" + walltime + ",nodes=1:ppn=1"

cwd = os.getcwd()
print("Submitting " + cwd + "\n\n")

# pattern for submit file matching
pattern = re.compile("[\w\-]*.sh")

total_submitted = 0
for root, dirs, files in os.walk(cwd):
    for file in files:
        if pattern.fullmatch(file):
            total_submitted = total_submitted + 1
            os.chdir(root)
            sp.run('qsub < '+ file + " " + options + " -N " + file, shell=True)
            print(os.path.join(root, file))
            if not args.skip_sleep: sleep(1.1)


print("Submitted {} jobs to queue".format(total_submitted))
