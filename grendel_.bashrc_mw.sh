# .bashrc
module load Intel-compilers
module load gcc/7.3.0

export MODULEPATH=/users/pb4941/share/modulefiles:$MODULEPATH
module add entos/0.7.1
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# User specific aliases and functions
alias python="python3.6"
export DL_SCRIPTS=$HOME/local/src/diamond_lattice/scripts

export PATH="/users/mw12315/local/bin/:/users/mw12315/local/src/diamond_lattice/plotting_scripts/:/users/mw12315/local/src/diamond_lattice/python_scripts/:$PATH"
export PATH="/opt/python/bin/:$PATH"
export PATH="$HOME/local/usr/bin/:$PATH"
export PATH="$DL_SCRIPTS/:$PATH"
export PATH="$DL_SCRIPTS/submit/:$PATH"
export PYTHONPATH="$HOME/local/src/diamond_lattice/python_scripts:$PYTHONPATH"

export MAKEFLAGS="-g"
alias read_diamond_out="nice -n 19 python3.6 /users/mw12315/local/src/diamond_lattice/python_scripts/read_diamond_json.py"
alias sim_stats="python3 $HOME/local/src/diamond_lattice/python_scripts/data_analysis/all_sims_stats.py"
alias copy_file="python3 ~/local/src/diamond_lattice/python_scripts/tools/copy_file.py"
alias cp_submit="python3 /users/mw12315/local/src/diamond_lattice/scripts/submission_dirs.py"

alias d_make="cd ~/local/src/diamond_lattice/Release/;  make diamond -j; cd - > /dev/null"
alias d_pull="cd ~/local/src/diamond_lattice/Release/; git pull; cd - > /dev/null"

# Job Queue
alias submit_all="python3.6 ~/local/src/diamond_lattice/python_scripts/submit_job.py"
alias qq="qstat -u mw12315 | grep ' Q ' | wc -l"
alias qr="qstat -u mw12315 | grep ' R ' | wc -l"
alias qqr="qstat -u mw12315 | wc -l"