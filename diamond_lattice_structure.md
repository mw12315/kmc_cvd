# diamond_lattice proposed structure

## Top Level Routines
- kmc_loop
- rate setting
- rate execution calling

## Mid level Routines
- outputs/printing
- individual execution of rates

## low level
- type initialisation

- input file reading

- variable setting

- site getting

- atom setting/getting

  

## Current Subroutines

low level
*medium level*
**high level**
***API level***

*`adsorb_atom_set_chx_insert_rate(site,rate_list)`*
*`adsorb_atom_set_c2h2_M1_rate(atom, rate_list)`*
*`adsorb_atom_set_c2h2_M2_rate(atom, atoms, rate_list)`*
*`adsorb_atom_set_c2h2_M3_rate(atom, atoms, rate_list)`*
*`adsorb_atom_execute_dimer_insertion(atom, event_type, lattice)`*
*`adsorb_atom_excute_trough_insertion(atom, event_type, lattice)`*
*`adsorb_atom_execute_C2H2_adsorption(site1, site2, event_type, lattice)`*
`adsorb_atom_get_c2h2_aM1_sites(atom,C1_fsite,C2_fsite,C1_act_sites,C2_act_sites,C2_fsites2,C2_act_sites2)`
`adsorb_atom_get_c2h2_aM2_sites(atom1, C1_fsite, C2_fsite, C1_act_sites)`
`adsorb_atom_get_c2h2_aM3_sites(atom, C1_fsite, C2_fsite, C1_act_sites)`
`adsorb_atom_biradical_site(site, atoms)`
`adsorb_atom_fully_supported(atom)`

`basis_position(atom,k_scale,atom_coords`
`basis_hydogenate_atom(atom,k_min,pair_index,site,h_coord)`

*`dimer_call_dimer_checking(site, atoms)`*
*`dimer_check_create_dimer(atom, atoms)`*
`dimer_create_dimer(atom1, atom2, atoms)`
*`dimer_check_remove_dimer(atom, atoms)`*
`dimer_remove_dimer(atom1, atom2, atoms)`
`dimer_return_bridging_atom(atom1,atom2,bridge_atom)`
*`dimer_align_dimer_with(atom,pair)`*
`dimer_get_dim_dir(basis,dirs)`
`dimer_pair_basis(basis)`
`dimer_dimer_state_value(atom1,atom2)`
`dimer_return_layer(basis)`
`dimer_return_pair_atom(atom)`

*`etching_set_etch_rate(atom, rate_list)`*
*`etching_set_etch_c2H2_rate(atom,rate_list)`*
`etching_execute_etch(atom, lattice)`

**`execute_rates_execute_event(atom,site2,event,atoms,write_to_events_file)`**
*`execute_rates_call_migration_execution(event,atom,end_site,lattice,success)`*
`execute_rates_increment_rate_counter(event,NN,TN,biradical)`

`fileIO_file_exists(filename)`

**`initialise_read_input_files()`**
`intialise_zero_global_counters()`
`initialise_initialise_rate_type_arrays()`
`initialise_initialise_atom_colours`
`initialise_set_rate_indexes()`
`*initialise_read_in_events()`
`initialise_initialise_lattice()`
`initialise_initialise_rates(rsv)` Depreciated
`initialise_calc_etch_rate`
`initialise_generate_rates()`

**`input_read_sim_inputs()`**
*`input_read_outputs()`*
*`input_read_bookmark_file(start_step,time_start,init_atoms,k_surf_min,k_surf_max,atoms,index_list,base_hight)`*
*`input_read_resimulate_input_file()`*

*`json_json_output_input_values(sim_info,seeding_clock)`*
*`json_sim_end_output(sim_info,kmc_info,surface_info,surf_bmark_info,cpu_time)`*

***`kmc_kmc_loops(atoms,sim_info,surface_info,timings,nucleation)`***
`kmc_create_rsv(rate_list,rsv)`
`kmc_count_rates(rate_list,kmc_info`
`kmc_get_event(chosen,rate_list,atoms,atom,site2,event)`
`kmc_add_to_rms_moving_average(current_roughness)`

***`main_run_resim_analysis(frame,time,surface)`***
`main_tidy_up_err_files()`
`main_get_site_from_path()`

*`migration_set_dimer_migration_rate(atom,rate_list)`*
*`migration_set_gap_mig_rate(atom,rate_list)`*
*`migration_set_mig_down_rate(atom,rate_list)`*
*`migration_set_C2H2_mig_biradical_rate(atom,atoms,rate_list)`*
`migration_execute_migration(event,atom,final_site,lattice)`
`migration_get_dimer_mig_sites(atom,f_sites,a_sites,blocking_sites)`
`migration_get_gap_mig_sites(atom,f_sites,a_sites)`
`migration_get_mig_down_sites(atom,f_sites,a_sites,blocking_sites,sup_atoms)`
`migration_get_C2H2_mig_birad_sites(atom,atoms,final_sites,a_sites)`
`migration_return_gap_env(atom,site)`
`migration_check_critical_nuc(atom,final_site)`
*`migration_visualise_dimer_migration(atom)`*
*`migration_visualise_gap_migration(atom)`*
*`migration_visualise_migration_down(atom)`*

`output_output_input_values(sim_info,seeding_clock)`
*`output_output_surface(lattice,k_min,k_madx,step,total_time,write_file)`*
`output_carbonyl_position(carbonyl_atom, height_scale)`
`output_get_atom_code(atom_type,act_state,atom_code)`
**`output_output_final(sim_info,kmc_info,surface_info,lattice,cpu_time)`**
*`output_surface_stats(lattice,start_height,surface_info)`*
`output_output_xyz(file_unit,atoms,comment)`
*`output_get_average_height(atoms)`*
*`output_get_growth_height(atoms,start_height)`*
`output_calc_growth_height(surface_height,start_height)`
`output_look_below(atom,atom_list)`
*`output_roughness_234NN(atom,sqr_diff_2NN,sqr_diff_3NN,sqr_diff_4NN)`*
`output_ret_height(atom)`
*`output_write_bookmark(lattice,simulation,step,time,k_surf_min,k_surf_max)`*
`output_get_output_string(curr_step,curr_time,k_max,k_scale_mode,selection_on,last_frame,scale_val,ave_height,text_out)`
`output_return_colour_text(atom,text)`

`periodic_init_neighbour_type()`
`periodic_ret_neighbour(atom,num)`
`periodic_get_neighbour_atoms(site_in,N1,N2,N3,N4)` refactor to use `periodic_neighbours(ibasis))`
`periodic_get_periodic_neighbour(i,j,direction,ni,nj)`
*`periodic_count_nearest_atoms(atom,NN)`*
`periodic_num_bonds()`
`periodic_return_periodic_atom(i,j,ibasis,k,direction, atom)`
`periodic_pbs_i(index)`
`periodic_pbs_j(index)`
`periodic_ret_TN(atom,compass)`
`periodic_count_TN(atom)`
*`get_site_from_N_path(isite,path)`*

`random_initi_random_seed()`
`random_seed_random_seed(clock_seed)`

**`randomise_activate_surface(lattice,next_output_time)`**
*`randomise_count_connectivity(lattice,coordPercent)`*
`randomise_increment_coord(NN,TN, count)`

`rates_locate(rsv,rnum,k)`
`rates_zero_rate(rate_list)`
**`rates_call_rate_setting(lattice,rate_list)`**
*`rates_visualise_init_rates(atom)`*
`periodic_ret_TN(atom,compass)`
`periodic_count_TN(atom)`
*`get_site_from_N_path(isite,path)`*

`random_initi_random_seed()`
`random_seed_random_seed(clock_seed)`

**`randomise_activate_surface(lattice,next_output_time)`**
`count_connectivity(lattice,coordPercent)
