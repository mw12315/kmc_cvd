!Routines associated with pendant atoms, which require non-standard storage of atoms, due to non-standard bonding with the surface
module pendant

    use kinds, only : dp

    implicit none

contains

    !-------------------------------------------------------------------------
    !Return the atom position that the pendant atom of a site should be stored
    !Will depend on whether the default location is already filled
    !-------------------------------------------------------------------------
    function patom2_store(atom, atoms) result(patom2)
        !Modules
        use types, only : atom_type, atom_type_ext
        use variables, only : read_atom
        use enumerate, only : types_of_atom, file_units
        use periodic, only : ret_neighbour
        use output, only : dump_surrounding_lattice
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        !Variables
        type(atom_type) :: patom2
        integer :: funit
        !Method

        patom2 = ret_neighbour(atom, 4)
        if (read_atom(patom2, atoms) == types_of_atom%empty_site) then
            return
        end if
        patom2 = ret_neighbour(atom, 1)
        if (read_atom(patom2, atoms) == types_of_atom%empty_site) then
            return
        else
            write(file_units%error, *) "No valid storage places for atom 2 of pendant", atom
            !Dump atoms to xyz and then abort
            open(newunit=funit, file='dump.xyz')
            call dump_surrounding_lattice(atom, atoms, 'comment', funit)
            stop 1
        end if

    end function patom2_store


    !---------------------------------------
    !Is the site adjacent to a pendant C2H2?
    !        H2
    !        C
    !       ||
    !   X   C
    !  / \ / \
    ! C   C   C
    !---------------------------------------
    logical function adjacant_to_pendant(site, atoms, ignore_atom) result(has_adjacent)
        !Modules
        use types, only : atom_type, atom_type_ext, same_atom
        use variables, only : read_ilink, read_atom, read_linked_atom
        use enumerate, only : types_of_atom
        use periodic, only : get_site_from_N_path
        !Arguments
        type(atom_type), intent(in) :: site
        type(atom_type), optional, intent(in) :: ignore_atom !Atom to be ignored if considered
        type(atom_type_ext), intent(in) :: atoms(:)
        !Variables
        integer :: paths(2,2) = reshape([2,1, 3,4], shape=[2,2], order=[2,1])
        integer :: ipath
        type(atom_type) :: adjacent_site !A site adjacent to the passed site
        has_adjacent = .false.

        do ipath=1,size(paths,1)
            adjacent_site = get_site_from_N_path(site, paths(ipath,:))
            if (present(ignore_atom)) then
                if (same_atom(adjacent_site, ignore_atom)) cycle
            end if
            if (read_ilink(adjacent_site, atoms) /= 0) then
                if (read_linked_atom(adjacent_site, atoms) == types_of_atom%acetyl_c2) then
                    has_adjacent = .true.
                    return
                end if
            end if
        end do

    end function adjacant_to_pendant


    !-----------------------------------------------------
    !Is the target atom a pendant C2H2
    !Stored as a surface carbon linked to a acetyl_c2 atom
    !Returns .true. for either atom of the linkage
    !-----------------------------------------------------
    logical function is_pendant(atom, atoms)
        !Modules
        use types, only : atom_type, atom_type_ext
        use variables, only : read_atom, read_linked_atom
        use enumerate, only : types_of_atom
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        !Variables
        integer :: atom1_state, atom2_state

        atom1_state = read_atom(atom, atoms)
        atom2_state = read_linked_atom(atom, atoms)
        is_pendant = ( &
                ((atom1_state == types_of_atom%surf_carb) &
                .and. &
                (atom2_state == types_of_atom%acetyl_c2)) &
                .or. &
                ((atom2_state == types_of_atom%surf_carb) &
                .and. &
                (atom1_state == types_of_atom%acetyl_c2)) &
                      )

    end function is_pendant


    logical function is_linked(atom, atoms)
        !Modules
        use types, only : atom_type, atom_type_ext
        use variables, only : read_ilink
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        !Variables
        if (read_ilink(atom, atoms) /= 0) then
            is_linked = .true.
        else
            is_linked = .false.
        end if

    end function is_linked

end module pendant