module activate

    use kinds, only : dp

    implicit none

contains
    !------------------------------------------------------------------
    !Activate target atom by incrementing (if possible) %act_state by 1
    !------------------------------------------------------------------
    subroutine activate_atom(atom, NN, atoms, activation_count, activated)
        !Modules
        use types, only : atom_type,atom_type_ext
        use variables, only : read_act, set_act_state
        !Arguments
        type(atom_type), intent(in) :: atom
        integer, intent(in) :: NN   !Number of C-C bonds to atom
        type(atom_type_ext), intent(inout) :: atoms(:)
        integer, optional, intent(inout) :: activation_count
        logical, optional, intent(out) :: activated
        !Variables
        integer :: act_state
        !Main Method
        act_state =  read_act(atom, atoms)

        if (act_state < max_act_level(NN)) then
            call set_act_state(atom, atoms, act_state + 1)
            if (present(activation_count)) activation_count = activation_count + 1
            if (present(activated)) activated = .true.
            return
        end if

        if (present(activated)) activated = .false.
    end subroutine activate_atom

    !------------------------------------------------------------------
    !Deactivate target atom by decrementing (if possible) %act_state by 1
    !------------------------------------------------------------------
    subroutine deactivate_atom(atom, atoms, total_deactivated)
        !Modules
        use types, only : atom_type,atom_type_ext
        use variables, only : set_act_state, read_act
        use enumerate, only : not_activated
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), dimension(:), intent(inout) :: atoms
        integer, optional, intent(inout) :: total_deactivated
        !Variables
        integer :: act_state
        !-----------
        !Main Method
        !-----------
        act_state =  read_act(atom, atoms)

        if (act_state > not_activated) then
            call set_act_state(atom, atoms, act_state - 1)
            if (present(total_deactivated)) total_deactivated = total_deactivated + 1
        end if

    end subroutine deactivate_atom


    !------------------------------------------------------
    !Return the maximum activation level of an atom, taking
    !the number of C-C bonds of an atom
    !------------------------------------------------------
    integer function max_act_level(NN) result(max_level)
        !Modules
        use variables, only : NN_bulk
        integer, intent(in) :: NN

        max_level = NN_bulk - NN
        return

    end function max_act_level

end module activate