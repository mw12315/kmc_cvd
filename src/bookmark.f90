module bookmark

    use kinds, only : dp
    use json_module, only : json_core, json_file, json_value

contains

    subroutine init_from_bookmark(kmc_info, sim_info, surf_info, outputs, counters, initial_atoms, bookmarked_atoms, index_list)
        !Modules
        use types, only : kmc_type, sim_type, surface_type, counter_type, atom_type_ext, outputs_ftype
        use enumerate, only : file_units
        !Arguments
        type(kmc_type), intent(inout) :: kmc_info
        type(sim_type), intent(inout) :: sim_info
        type(surface_type), intent(inout) :: surf_info
        type(outputs_ftype), intent(inout) :: outputs
        type(counter_type), intent(inout) :: counters
        integer :: initial_atoms, i                              !Number of initial atoms stored within bookmark.geom
        type(atom_type_ext), allocatable :: bookmarked_atoms(:)  !Array of atoms stored within bookmark.geom
        integer, allocatable :: index_list(:)

        !Read in atoms and system size from bookmark.geom
        call read_bookmark_geom(sim_info, initial_atoms, surf_info, bookmarked_atoms, index_list)
        !Read in bookmark.json file for counters etc
        call read_bookmark_json(sim_info, kmc_info, surf_info, outputs, counters)

        !Set the current xtime index
        call determine_xtime(outputs, kmc_info)

        write(file_units%bookmark_log,'(a,i10,a,ES10.2,a,2(i10))') "Read bookmark file: step: ", &
                kmc_info%step, " start time: ",kmc_info%time, &
                " surface bounds (k): ",surf_info%k_min%val, surf_info%k_max%val
        !Extend end_time to match continuation time (if not fininished a previously unfinished simulation)
        if (.not. sim_info%continue_unfinished) then

            sim_info%end%time = sim_info%end%time + kmc_info%time
            sim_info%end%iteration = sim_info%end%iteration + kmc_info%step
            if (sim_info%end%growth_height > 0.0_dp) then
                sim_info%end%growth_height = sim_info%end%growth_height + surf_info%ave_growth_height
            end if
            write(file_units%bookmark_log,'(a, i10, ES10.2, f10.2)') &
                    "new end iteration, time, growth height: ", sim_info%end%iteration, sim_info%end%time, sim_info%end%growth_height

        else

            write(file_units%bookmark_log,'(a, i10, ES10.2, f10.2)') &
                    "finishing at original end points: ", sim_info%end%iteration, sim_info%end%time, sim_info%end%growth_height
        end if

        write(file_units%log,'(a,f10.2,a,i10)') "Resuming from 'bookmark.geom': ", kmc_info%time, " s at step ", kmc_info%step
        write(file_units%log,'(i10,a,i6,a,i6)') size(bookmarked_atoms), " atoms at a height range of ", surf_info%k_min%val, " : ", surf_info%k_max%val
        write(file_units%bookmark_log, '(a, i3, a, f5.2, a)') &
                "xtime start index is:", kmc_info%icurr_xtime, " corresponding to ", outputs%values_at_x_s%times(kmc_info%icurr_xtime), 's'
        write(file_units%bookmark_log, '(a)', advance='NO') "therefore xtime output is "
        if (kmc_info%xtime_finished) then
            write(file_units%bookmark_log, '(a)') "finished"
        else
            write(file_units%bookmark_log, '(a)') "unfinished"
        end if

        !Reset counters if not continuing an unfinished simulation
        if (.not. sim_info%continue_unfinished) then
            write(file_units%bookmark_log, '(a)') &
                "reset event counters and averages for *new* continuation simulation"
            !Reset Counters
            counters%events(:,:) = 0
            kmc_info%total_events = 0
            counters%d_align(:) = 0
            counters%d_break = 0
            !Reset average values and counters
            call surf_info%ave_rms%reset()
            call surf_info%ave_2Nrms%reset()
            call surf_info%ave_3Nrms%reset()
            call surf_info%ave_4Nrms%reset()
            call kmc_info%ave_rsum%reset()
            call kmc_info%Fmr%reset()
            call surf_info%ave_active_layers%reset()
            call surf_info%ave_dimer_deviation%reset()
            call surf_info%ave_dimers_prop%reset()
            do i=0,4
                call surf_info%ave_envs(i)%reset()
            end do

        end if


        write(file_units%m_output, *) "---------- Resuming from Bookmark File ----------"
        write(file_units%m_output,'(a,f10.2,a,i10)') "Continuation of simulation starting at ",kmc_info%time," s and step ",kmc_info%step

    end subroutine init_from_bookmark

    !Read bookmark geom file (atom list reading and basic surface basics
    subroutine read_bookmark_geom(sim_info, init_atoms, surf_info, atom_list, index_list)
        !Modules
        use types, only : sim_type, surface_type, kmc_type, atom_type_ext
        use enumerate,  only : file_units
        !Arguments
        type(sim_type), intent(in) :: sim_info
        type(surface_type), intent(inout) :: surf_info
        integer, intent(out) :: init_atoms
        type(atom_type_ext), intent(out), allocatable :: atom_list(:)
        !Variables
        integer :: iatom
        integer :: Nx, Ny
        integer, intent(out), allocatable :: index_list(:)
        open(unit=file_units%bookmark,file="bookmark.geom",position='rewind',action="read")

        !Read 1st line
        read(file_units%bookmark,*) Nx,Ny,init_atoms,surf_info%k_min%val,surf_info%k_max%val, surf_info%init_height

        !Check Nx,Ny with input file
        if (Nx /= sim_info%Nx) write(file_units%bookmark_log,*) "bookmark.in dimension Nx does not match diamond.input Nx value"
        if (Ny /= sim_info%Ny) write(file_units%bookmark_log,*) "bookmark.in dimension Ny does not match diamond.input Ny value"

        allocate(atom_list(init_atoms))
        allocate(index_list(init_atoms))
        !Read in atoms
        do iatom=1,init_atoms
            read(file_units%bookmark,*) index_list(iatom),atom_list(iatom)%atom%i,atom_list(iatom)%atom%j,atom_list(iatom)%atom%basis,atom_list(iatom)%atom%k, &
                    & atom_list(iatom)%state,atom_list(iatom)%dimer,atom_list(iatom)%ipair,atom_list(iatom)%surface, atom_list(iatom)%iLink
        end do

        close(file_units%bookmark)


    end subroutine read_bookmark_geom


    !Output a bookmark file (overwrite) detailing the current surface, time and step for continuation
    subroutine write_bookmark_geom(atoms, simulation, surf_info)
        !Modules
        use types,      only : atom_type_ext, sim_type, surface_type, kmc_type
        use enumerate,  only : file_units
        use variables,  only : return_ilattice_limits
        !Arguments
        type(atom_type_ext), dimension(:), intent(in) :: atoms
        type(sim_type), intent(in) :: simulation
        type(surface_type), intent(in) :: surf_info
        !Variables
        integer :: iatom
        integer :: limits(2)
        logical :: is_open
        type(atom_type_ext) :: atom_ext
        !Main Method
        !Open benchmarch file
        inquire(unit=file_units%bookmark,opened=is_open)
        if (is_open) close(file_units%bookmark)
        open(unit=file_units%bookmark,file='bookmark.geom',status='replace')

        call return_ilattice_limits(surf_info%k_min%val, surf_info%k_max%val, limits)

        !Output conditions
        write(file_units%bookmark,'(2(i3,a1),i8,a1,2(i6,a1),f10.3)') &
                simulation%Nx,",",simulation%Ny,",", &
                limits(2)-limits(1)+1,",",surf_info%k_min%val,",",surf_info%k_max%val,",", &
                surf_info%init_height


        do iatom=limits(1),limits(2)
            atom_ext = atoms(iatom)
            write(file_units%bookmark,'(i10,a1,4(i4,a1),i2,a1,L1,a1,i10,a1,L1,a1,i10,a1,i4)') &
                    & iatom,",",atom_ext%atom%i,",",atom_ext%atom%j,",",atom_ext%atom%basis,",",atom_ext%atom%k,",", &
                    & atom_ext%state,",",atom_ext%dimer,",",atom_ext%ipair,",",atom_ext%surface,",",atom_ext%iLink,",",atom_ext%mig
        end do

        close(file_units%bookmark)

    end subroutine write_bookmark_geom


    !---------------------------------------------------------------------------------------------------
    !Read in the required information from bookmark.json in order to continue from a simulation bookmark
    !---------------------------------------------------------------------------------------------------
    subroutine read_bookmark_json(sim_info, kmc_info, surf_info, outputs, counters)
        !Modules
        use types, only : sim_type, kmc_type, surface_type, counter_type, outputs_ftype
        use enumerate, only : tRates
        !Arguments
        type(sim_type), intent(inout) :: sim_info
        type(kmc_type), intent(inout) :: kmc_info
        type(surface_type), intent(inout) :: surf_info
        type(outputs_ftype), intent(inout) :: outputs
        type(counter_type), intent(inout) :: counters
        !Variables
        type(json_core) :: json
        type(json_file) :: j_file  !bookmark.json
        logical :: found(34), found_events(2*tRates%nrates)
        integer :: i
        character(12) :: e0_txt, e1_txt
        character(2) :: tmp_txt
        !Initialisation
        call json%initialize()

        call j_file%load_file(filename='./bookmark.json')
        if (j_file%failed()) then
            call j_file%print_error_message(6)
            stop
        end if

        !Reading in all values relating to growth information for perfect continuation
        !Outputs, reactions, and other inputs are left to the files within root rather than the previous run
        found(:) = .true.
        found_events(:) = .true.

        call j_file%get('iterations', kmc_info%step, found(1))
        if (.not. sim_info%continue_unfinished) kmc_info%start_step = kmc_info%step
        call j_file%get('events', kmc_info%total_events, found(2))
        call j_file%get('sim_time', kmc_info%time, found(3))
        kmc_info%start_time = kmc_info%time

        call j_file%get('average_growth', surf_info%ave_growth_height, found(4))
        call j_file%get('average_growth_rate', surf_info%ave_growth_rate, found(5))
        call j_file%get('n_xvals', outputs%values_at_x_s%num_values, found(6))
        if (allocated(outputs%values_at_x_s%times)) then
            deallocate(outputs%values_at_x_s%times)
        end if
        call j_file%get('xtimes', outputs%values_at_x_s%times, found(7))
        if (allocated(surf_info%grate_at_x)) deallocate(surf_info%grate_at_x)
        call j_file%get('xgrowth_rate', surf_info%grate_at_x, found(8))
        if (allocated(surf_info%inst_grate_x)) deallocate(surf_info%inst_grate_x)
        call j_file%get('xinstgr', surf_info%inst_grate_x, found(9))
        if (allocated(surf_info%rms_at_x)) deallocate(surf_info%rms_at_x)
        call j_file%get('xrms', surf_info%rms_at_x, found(10))
        if (allocated(surf_info%rms2N_at_x)) deallocate(surf_info%rms2N_at_x)
        call j_file%get('xrms2N', surf_info%rms2N_at_x, found(11))
        if (allocated(surf_info%rms3N_at_x)) deallocate(surf_info%rms3N_at_x)
        call j_file%get('xrms3N', surf_info%rms3N_at_x, found(12))
        if (allocated(surf_info%rms4N_at_x)) deallocate(surf_info%rms4N_at_x)
        call j_file%get('xrms4N', surf_info%rms4N_at_x, found(13))
        call j_file%get('rms_roughness', surf_info%rms_roughness, found(14))
        call j_file%get('ave_rms(1)', surf_info%ave_rms%count, found(15))
        call j_file%get('ave_rms(2)', surf_info%ave_rms%sum, found(16))
        call j_file%get('2N_rms', surf_info%rms_2NN, found(17))
        call j_file%get('ave_2N_rms(1)', surf_info%ave_rms%count, found(18))
        call j_file%get('ave_2N_rms(2)', surf_info%ave_rms%sum, found(19))
        call j_file%get('3N_rms', surf_info%rms_3NN, found(20))
        call j_file%get('ave_3N_rms(1)', surf_info%ave_rms%count, found(21))
        call j_file%get('ave_3N_rms(2)', surf_info%ave_rms%sum, found(22))
        call j_file%get('4N_rms', surf_info%rms_4NN, found(23))
        call j_file%get('ave_4N_rms(1)', surf_info%ave_rms%count, found(24))
        call j_file%get('ave_4N_rms(2)', surf_info%ave_rms%sum, found(25))

        do i=1, tRates%nrates
            write(tmp_txt, '(i2)') i
            write(e0_txt, '(3(a))') 'events_0(',trim(tmp_txt),')'
            write(e1_txt, '(3(a))') 'events_1(',trim(tmp_txt),')'
            call j_file%get(trim(e0_txt), counters%events(i,1), found_events(i))
            call j_file%get(trim(e1_txt), counters%events(i,2), found_events(tRates%nrates + i))
        end do

        call j_file%get('d_align(1)', counters%d_align(1), found(26))
        call j_file%get('d_align(2)', counters%d_align(2), found(27))
        call j_file%get('d_break', counters%d_break, found(28))

        call j_file%get('dimer_deviation', surf_info%dimer_deviation, found(29))
        call j_file%get('ave_dimer_deviation(1)', surf_info%ave_dimer_deviation%count, found(30))
        call j_file%get('ave_dimer_deviation(2)', surf_info%ave_dimer_deviation%sum, found(31))
        call j_file%get('dimer_prop', surf_info%dimer_prop, found(32))
        call j_file%get('ave_dimer_prop(1)', surf_info%ave_dimers_prop%count, found(33))
        call j_file%get('ave_dimer_prop(2)', surf_info%ave_dimers_prop%sum, found(34))

        call j_file%destroy()

        if (any(found .eqv. .false.) .or. any(found_events .eqv. .false.)) then
            write(*,*) "Error in bookmark.json"
            ERROR STOP
        end if

    end subroutine read_bookmark_json

    subroutine write_bookmark_json(kmc_info, surface_info, outputs, counters)
        !Modules
        use types, only : sim_type, kmc_type, surface_type, counter_type, outputs_ftype
        use enumerate, only : file_units, tRates
        !Arguments
        type(kmc_type), intent(in) :: kmc_info
        type(surface_type), intent(in) :: surface_info
        type(outputs_ftype), intent(in) :: outputs
        type(counter_type), intent(in) :: counters
        !Variables
        type(json_core) :: json
        type(json_value), pointer :: bmark_root !JSON root for bookmark.json
        type(json_value), pointer :: reaction_counts, dimer_insertion, trough_insertion, pref_etch, ide_etch, C2H2, migration

        call json%initialize()

        call json%create_object(bmark_root, 'simulation') !Create root
        !Simulation Info (time, iterations)
        call json%add(bmark_root, 'iterations', kmc_info%step)
        call json%add(bmark_root, 'events', kmc_info%total_events)
        call json%add(bmark_root, 'sim_time', kmc_info%time)

        !Reaction Counts

        !Surface/Growth Information
        call json%add(bmark_root, 'average_growth', surface_info%ave_growth_height)
        call json%add(bmark_root, 'average_growth_rate', surface_info%ave_growth_rate)
        call json%add(bmark_root, 'rms_roughness', surface_info%rms_roughness)
        call json%add(bmark_root, 'ave_rms', [real(surface_info%ave_rms%count,dp), surface_info%ave_rms%sum])
        call json%add(bmark_root, '2N_rms', surface_info%rms_2NN)
        call json%add(bmark_root, 'ave_2N_rms', [real(surface_info%ave_2Nrms%count,dp), surface_info%ave_2Nrms%sum])
        call json%add(bmark_root, '3N_rms', surface_info%rms_3NN)
        call json%add(bmark_root, 'ave_3N_rms', [real(surface_info%ave_3Nrms%count,dp), surface_info%ave_3Nrms%sum])
        call json%add(bmark_root, '4N_rms', surface_info%rms_4NN)
        call json%add(bmark_root, 'ave_4N_rms', [real(surface_info%ave_4Nrms%count,dp), surface_info%ave_4Nrms%sum])
        call json%add(bmark_root, 'dimer_prop', real(surface_info%dimer_prop, dp))
        call json%add(bmark_root, 'ave_dimer_prop', [real(surface_info%ave_dimers_prop%count, dp), surface_info%ave_dimers_prop%sum])
        call json%add(bmark_root, 'dimer_deviation', real(surface_info%dimer_deviation, dp))
        call json%add(bmark_root, 'ave_dimer_deviation', [real(surface_info%ave_dimer_deviation%count, dp), surface_info%ave_dimer_deviation%sum])

        !Output @ x
        call json%add(bmark_root, 'n_xvals', outputs%values_at_x_s%num_values)
        call json%add(bmark_root, 'xtimes', [outputs%values_at_x_s%times(1:size(outputs%values_at_x_s%times))])
        call json%add(bmark_root, 'xgrowth_rate', [surface_info%grate_at_x])    !Ang/s
        call json%add(bmark_root, 'xinstgr', [surface_info%inst_grate_x])       !Ang/s
        call json%add(bmark_root, 'xrms', [surface_info%rms_at_x])              !A
        call json%add(bmark_root, 'xrms2N', [surface_info%rms2N_at_x])          !A
        call json%add(bmark_root, 'xrms3N', [surface_info%rms3N_at_x])          !A
        call json%add(bmark_root, 'xrms4N', [surface_info%rms4N_at_x])          !A

        call json%add(bmark_root, 'events_0', counters%events(:,1))
        call json%add(bmark_root, 'events_1', counters%events(:,2))
        call json%add(bmark_root, 'd_align', counters%d_align(:))
        call json%add(bmark_root, 'd_break', counters%d_break)

        open(unit=file_units%json_bookmark, file="bookmark.json", status='REPLACE')
        call json%print(bmark_root, file_units%json_bookmark)
        flush(file_units%json_bookmark)
        close(file_units%json_bookmark)

        nullify(bmark_root)
        call json%destroy()

    end subroutine write_bookmark_json


    !Wrapper for writing both geom and json bookmark files
    subroutine write_bookmark(atoms, sim_info, surf_info, kmc_info, outputs, counters)
        !Modules
        use types, only : atom_type_ext, sim_type, kmc_type, surface_type, counter_type, outputs_ftype
        !Arguments
        type(atom_type_ext), dimension(:), intent(in) :: atoms
        type(sim_type), intent(in) :: sim_info
        type(surface_type), intent(in) :: surf_info
        type(kmc_type), intent(in) :: kmc_info
        type(outputs_ftype), intent(in) :: outputs
        type(counter_type), intent(in) :: counters

        call write_bookmark_geom(atoms, sim_info, surf_info)
        call write_bookmark_json(kmc_info, surf_info, outputs, counters)

    end subroutine write_bookmark

    !--------------------------------------------------------------------
    !Set the value of the current xtime index after resuming a simulation
    !--------------------------------------------------------------------
    subroutine determine_xtime(outputs, kmc_info)
        !Variables
        use types, only : outputs_ftype, kmc_type
        !Arguments
        type(outputs_ftype), intent(in) :: outputs
        type(kmc_type), intent(inout) :: kmc_info
        !Variables
        integer :: xtime_index

        do xtime_index=1, size(outputs%values_at_x_s%times)
            if (outputs%values_at_x_s%times(xtime_index) > kmc_info%time) then
                kmc_info%icurr_xtime = xtime_index  !The next time to trigger on
                exit
            end if
        end do

        if (xtime_index == size(outputs%values_at_x_s%times)) then
            kmc_info%xtime_finished = .true.
        end if

    end subroutine determine_xtime

end module bookmark
