!Routines associated with finalisation of the system: deallocatation, flushing(?), closing files etc
module finalise

    implicit none

contains

    !-------------------------------------------------------
    !Deallocate allocatables nested in the surface_type type
    !-------------------------------------------------------
    subroutine deallocate_surface_instance(s)
        !Modules
        use types, only : surface_type
        !Arguments
        type(surface_type), intent(inout) :: s

        !Method
        if (allocated(s%grate_at_x)) deallocate(s%grate_at_x)
        if (allocated(s%inst_grate_x)) deallocate(s%inst_grate_x)
        if (allocated(s%rms_at_x)) deallocate(s%rms_at_x)
        if (allocated(s%rms2N_at_x)) deallocate(s%rms2N_at_x)
        if (allocated(s%rms3N_at_x)) deallocate(s%rms3N_at_x)
        if (allocated(s%rms4N_at_x)) deallocate(s%rms4N_at_x)
        if (allocated(s%reg_at_x)) deallocate(s%reg_at_x)
        if (allocated(s%events_at_x)) deallocate(s%events_at_x)
        if (allocated(s%layers)) deallocate(s%layers)

    end subroutine deallocate_surface_instance

end module finalise