module types

    use kinds, only : dp

    implicit none

    !Storage of a sum and a count for averaging
    type average_type
       real(dp) :: sum = 0.0_dp
        integer :: count = 0
    contains
        procedure :: reset => average_reset
    end type average_type

    type average_rates_set_type
        real(dp), allocatable, dimension(:) :: sum
        integer :: count = 0
    contains
        procedure :: reset => average_reset_rates_set
    end type average_rates_set_type

    !Min/Max Atom k indices on the surface
    type minmax_k
        integer :: val
        integer :: loc_i
        integer :: loc_j
    end type minmax_k

    !Atom Colours (rgb)
    type rgb_type
        real(dp) :: R
        real(dp) :: G
        real(dp) :: B
    end type rgb_type

    type ahhr_type
        real(dp) :: E
        real(dp) :: A
    end type ahhr_type

    !Hold arrhenius A, E values for reversible reactions
    type ahhr_fr_type
        type(ahhr_type) :: f,r
    end type ahhr_fr_type

    type fr_k_type
        real(dp) :: f,r
    end type fr_k_type

    type acetylene_rate_type
        type(ahhr_type) :: M1
        type(ahhr_type) :: M2
        type(ahhr_type) :: M3a
        type(ahhr_type) :: M3b
        type(ahhr_type) :: etchCH3
        type(ahhr_type) :: etchC2H2
    end type acetylene_rate_type

    type log_species_type
        logical :: CH3
        logical :: CH2
        logical :: CH
        logical :: C
        logical :: C2a
        logical :: C2X
        logical :: C2H2
    end type log_species_type

    type acetylene_reactions_type
        logical :: M1
        logical :: M2
        logical :: M3a
        logical :: M3b
        logical :: migC2H2_birad
        logical :: etchCH3
        logical :: etchC2H2
    end type acetylene_reactions_type
    
    !--------------------------------
    !Store all rate switches together
    !--------------------------------
    type rate_switch_type
        logical :: surf_activation_on
        logical :: InsertTroughCHX_on
        logical :: InsertDimerCHX_on
        logical :: insert_pendant
        logical :: pref_etch_C_on
        logical :: ide_etch_on(5)
        logical :: dimer_migration_on
        logical :: gap_mig_on
        logical :: gap_mig_ind(4)
        logical :: gap_mig_down_on
        logical :: C2H2p_mig(2)
        logical :: C2H2p_dimer
        logical :: etch_CHx !Netto
        type(acetylene_reactions_type) :: acetylene
    end type rate_switch_type

    !Store end conditions for the simulation
    type sim_end_type
        real(dp) :: time
        real(dp) :: growth_height
        integer  :: iteration
    end type sim_end_type

    type output_real_range_type
        real(dp) :: start
        real(dp) :: end
        real(dp) :: dt
        logical  :: minus_1
    end type output_real_range_type

    type output_switches_type
        logical :: event_csv
        logical :: kmc_vis
        logical :: growth_rate
        logical :: act_fraction
        logical :: rate_reg
        logical :: dimer_count
        logical :: check_dimer
        logical :: all_rates
        logical :: activation
        logical :: surf_list
        logical :: surf_connect
        logical :: dt
        logical :: atom_envs
        logical :: layers
        logical :: events_time
        !Resimulation
        logical :: roughness
        logical :: roughness_NN
        logical :: height
    end type output_switches_type

    !Output at specific times, keeping track of current time
    type specific_output_type
        integer :: num_values
        real(dp), dimension(:), allocatable :: times
    end type specific_output_type

    !Kinetic information
    type kmc_type
        real(dp) :: start_time = 0.0_dp !Time at start of this simulation
        integer :: start_step = 0       !Step at start of this simulation
        real(dp) :: time = 0.0_dp       !Current time
        integer  :: step = 0            !Current step
        integer :: icurr_xtime = 1  !Current index of output x time array
        logical :: xtime_finished = .false. !Has the final xtime value been reached?
        real(dp) :: nucleation_height, nucleation_time
        integer :: total_events = 0
        type(average_type) :: migs  !Number,Count of migrations
        type(average_type) :: Fmr
        type(average_rates_set_type) :: ave_rates_set, ave_rate_set_since_last
        type(average_type) :: ave_rsum
    end type kmc_type

    !Growth rate at an instant (since last query)
    type instant_grate_type
        real(dp) :: rate
        real(dp) :: last_height,last_time    !The height (ave) of the surface and time measured during the last call
        real(dp) :: height_diff, time_diff
    end type instant_grate_type

    type surface_type
        real(dp) :: start_height                !Height at start of simulation
        real(dp) :: init_height                 !Initialised Height of lattice (simulation independent)
        integer  :: total_atoms = 0             !Total surface atoms
        integer  :: total_dimers = 0            !Total dimers on the surface
        integer  :: total_isolated_dimers = 0   !Isolated dimers on the surface
        type(minmax_k) :: k_min,k_max           !Min/Max k value of surface atoms
        real(dp) :: average_height              !Current average height of the surface A
        real(dp) :: ave_growth_height           !Growth for this simulation, A total including bookmarked reaction (not updated during simulations)
        real(dp) :: gr_height           !Height to be used for growth rate calculation [start_height, init_height] (new sim, continuation, etc)
        type(instant_grate_type) :: instant_growth_rate
        real(dp) :: ave_growth_rate !Angstrom/second
        real(dp) :: rms_roughness
        real(dp) :: d_rms_roughness             !1st derivative for rms roughness (wrt to cells)
        real(dp) :: rms_2NN,rms_3NN,rms_4NN     !Roughness on the lengthscales of 2NN,3NN,4NN
        real(dp) :: width                       !Width of the surface in Angstrom
        real(dp), allocatable, dimension(:) :: layers   !Array of % atoms in each layer of the lattice
        integer :: min_layer,max_layer  !Index of lowest/highest layer index containing a surface atom
        integer :: active_layers    !Total number of layers with surface atoms
        type(average_type) :: ave_active_layers
        type(average_type) :: ave_rms, ave_2Nrms, ave_3Nrms, ave_4Nrms
        real(dp), allocatable, dimension(:) :: top_occupancy  !Top occupancy of x layers
        integer,dimension(0:4) :: envs  !Surface Environments
        type(average_type),dimension(0:4) :: ave_envs   !Surface Environments: average
        real(dp) :: dimer_deviation                     !Current dimer alignment of the surace
        real(dp) :: dimer_prop                          !Current proporption of the surface that is dimers
        type(average_type) :: ave_dimer_deviation       !Average alignment of surface dimers
        type(average_type) :: ave_dimers_prop           !Average proportion of the surface that is dimers
        !Specific Time output values
        real(dp), dimension(:), allocatable :: grate_at_x, inst_grate_x, &
                rms_at_x, rms2N_at_x, rms3N_at_x, rms4N_at_x
        real(dp), dimension(:,:), allocatable :: reg_at_x, events_at_x
    end type surface_type

    !-----------------------------------------------------
    !Input file derived types acting as passing namespaces
    !-----------------------------------------------------
    type sim_type
        character(20) :: name
        integer :: seeding_clock                        !Seeding Clock value of the simulation
        integer :: Nx, Ny, Nz !Surface Dimensions
        integer :: Nbasis = 8 !Number of basis values within a unit cell (x,y,z)
        integer :: fill_to_basis !Number of basis values to fill up to when initialising the surface
        logical :: use_segments
        integer :: xsegments, ysegments !Number of x, y segements
        logical :: use_bookmark, bookmark_mode_active
        integer :: max_height !Height/iteration exit conditions
        integer :: rates_per_site
        integer :: unique_rates
        type(sim_end_type) :: end   !Simulation End conditions (time, iteration, growth height)
        logical :: nucleation_height_on
        real(dp) :: nucleation_height
        logical :: continue_unfinished = .false.
    end type sim_type

    type reactions_ftype
        type(log_species_type) :: active_species
        type(rate_switch_type) :: active
        real(dp) :: dimer_insertion_scale, trough_insertion_scale
        integer :: mig_crit_nuc
        integer :: isolated_TN_threshold
        logical :: align_dimers                 !Align dimers when possible during dimer formation
        logical :: realign_dimers               !Realignment of dimers when possible during activation of the surface
        integer :: d_align_range                !Number of dimer units to look up/down when aligning dimer units
        integer :: dimer_row_isolation_range
        logical :: single_c_prevent_iso         !a single carbon can prevent isolation detection
        character(10) :: RadSiteDensity, DefectDensity
        character(3) :: etch_model
        logical :: ide_individual_active        !IDE etching occurs via single rates (12-1, 12-2 etc)
                                                !Else: 12-2 = 12-1 + 12-2
    end type reactions_ftype

    type energy_ftype
        real(dp) :: Ak1,Ek1,Akm1,Ekm1
        real(dp) :: Ak2
        real(dp) :: AH2add,EH2add
        real(dp) :: AHadd
        real(dp) :: A_dmig,E_dmig
        real(dp) :: A_gmig1f,E_gmig1f
        real(dp) :: A_gmig1r,E_gmig1r
        real(dp) :: A_gmig2f,E_gmig2f
        real(dp) :: A_gmig2r,E_gmig2r
        real(dp) :: A_down_mig,E_down_mig
        real(dp) :: EbetaScission
        real(dp) :: Abat_etch,Ebat_etch
        real(dp) :: etch_factor
        integer  :: C2H2p_nreactions
        type(ahhr_fr_type), allocatable :: C2H2p_mig(:)
        type(ahhr_type) :: c2h2p_dimer
        type(acetylene_rate_type) :: acetylene_ads

    end type energy_ftype

    !Store variables representing values read in from etching.input
    type etching_ftype
        !Constant Values
        character(13) :: input_fname = "etching.input"
        !Preferential Etching (prf)
        character(9) :: prf_rate_labels(0:4) =(/ "CHx F:ISO", "CHx F:S_A", "CHx F:S_B", "CHx F:SAB", "CHx F:Ter" /)
        type(ahhr_type), dimension(0:4) :: flex_etch
        real(dp) :: flex_etch_3rdbond_factor
        !Isolated Dimer Etching (ide)
        integer :: n_reactions = 5
        integer, dimension(5) :: n_ide_subreactions = (/ 7, 3, 5, 6, 5 /)
        type(ahhr_fr_type) :: ide_ahhr(5,7)
        logical :: explicit_rate_input
        real(dp) :: explicit_rates(5)
    end type etching_ftype

    type conditions_ftype
        character(20) :: condition_id
        integer :: Ts, Tns
        real(dp) :: Hconc,H2conc
        character(3) :: CHx_labels(4) = (/ 'CH3', 'CH2', 'CH ', 'C  ' /)
        real(dp),dimension(4) :: CHxconc, CHx_s, CHx_g
        real(dp) :: C2a_conc
        real(dp) :: C2X_conc
        real(dp) :: C2H2_conc,C2H2_s
    end type conditions_ftype

    type outputs_ftype
        integer :: height_scale_mode !0, 1, 2
        logical :: highlight_last_frame
        logical :: selection_on
        type(output_real_range_type) :: kmc_out
        type(output_switches_type) :: L
        real(dp) :: timestep
        real(dp) :: next_timestep
        real(dp) :: bmark_timestep
        type(specific_output_type) :: values_at_x_s
        integer :: nlookdown
    end type outputs_ftype

    type dimer_ftype
        character(11) :: fname = "dimer.input"
        integer :: nb_form_thres = 3 !Threshold number of bonds to form a dimer
        logical :: incorp_CHx_sat_dimer = .false.
        real(dp) :: sat_dimer_incorp_scale = 1.0_dp
        real(dp) :: hind_trough_deact_factor = 1.0_dp
        logical :: dihyride_hindered = .false.
        logical :: gap_via_dimer = .false. !A dimer is just a bi-radical which can rapidly break
        logical :: iso_dimer_can_break      !Can a dimer of ISO type break?
        logical :: random_adj_dimer_breaking !Is the random breaking of dimers possible?
        real(dp) :: prob_adj_dimer_break    !Probability of a adjacent dimer being broken
        character(6) :: dimer_break_model   !Follow rate scaling of dimer breaking of Skokov (1995) or Netto (2005)
    end type dimer_ftype

    type timing_type
        integer :: count
        real(dp) :: time
    end type timing_type

    type kmc_timings
        real(dp) :: set_rates
        real(dp) :: find_rate
        real(dp) :: act_surf
        real(dp) :: ex_rate
        real(dp) :: create_rsv
        type(timing_type) :: surf_stats
    end type kmc_timings

    !Basic positioning atom type
    type atom_type
        integer :: i = 0
        integer :: j = 0
        integer :: basis = 0
        integer :: k = 0
    end type atom_type

    type rate_type
        type(atom_type) :: site = atom_type(0,0,0,0) !Primary atom/atom site
        real(dp) :: rate_val = 0.0_dp
        integer :: event = 0
        !Optional, additional site information (eg migration final site, acetylene C2 site)
        type(atom_type) :: site2 = atom_type(0,0,0,0)
        integer :: ilat = 0 !Reference to atom 'object' in lattice (list)
    end type rate_type

    !Atom extended, represents the lattice
    !todo dimer information could go into a specific dimer type if performance isn't affected
    type atom_type_ext
        type(atom_type) :: atom
        integer  :: state = 0
        integer  :: act_state = 0  !0 CHX 1 | CHX-1 | 2 CHX-2 | 3 CHX-3
        logical  :: dimer = .false.
        integer  :: ipair = 0
        logical  :: surface = .false.
        logical  :: last_frame = .false.
        integer  :: last_event = 0
        integer  :: iLink = 0      !Lattice index of linked carbon (eg C1 - C2 carbons for adsorbed C2H2)
        integer  :: mig  = 0        !Number of migrations carried out by the atom

        contains
            procedure :: reset => reset_atom_ext
    end type atom_type_ext

    type event_vis_type
        type(atom_type) :: atom,f_site
        integer         :: event
    end type event_vis_type

    !Migration site and direction arrays
    type mig_site_type
        !(basis, direction)
        integer, dimension(2)   :: N
        integer, dimension(2)   :: E
        integer, dimension(2)   :: S
        integer, dimension(2)   :: W
    end type mig_site_type

    !Store types of atom
    type types_of_atom_type
        integer :: empty_site            = 0        !Empty atomic site
        integer :: surf_carb             = 1        !Carbon (C,CH,CH2 depending on activation)
        integer :: acetyl_c2             = 2        !2nd carbon above a 2 coordinated carbon atom (>C=C)
    end type types_of_atom_type

    !Store directions and basis and k relative to atom
    type neighbours_type
        integer, dimension(4) :: dir
        integer, dimension(4) :: basis
        integer, dimension(4) :: rel_k
    end type neighbours_type

    type NN_dist_type
        real(dp) :: NN2 = 3.0_dp
        real(dp) :: NN3 = 8.0_dp
        real(dp) :: NN4 = 12.0_dp
    end type NN_dist_type

    !----------------
    !Store file units
    !----------------
    type file_units_type
        integer             :: input            = 8                 !Input file for the program
        integer             :: events           = 10                !Events log input
        integer             :: log              = 11                !Log file - alternative to output to standard out
        integer             :: bookmark         = 12                !bookmark.geom
        integer             :: json_bookmark    = 13                !bookmark.geom
        integer             :: nucleation       = 14                !Surface at point of nucleation
        integer             :: kmc_vis          = 15                !Surface output during KMC simulation
        integer             :: pos_final        = 16                !Output of final atom positions
        integer             :: vis_rates        = 18                !Visualise Rates
        integer             :: act_frames       = 19                !surface atoms only frame output
        integer             :: surf_conn        = 23                !Outputs information on surface nearest neighbours
        integer             :: error            = 90                !Error file
        integer             :: warning          = 92                !Warning file
        integer             :: dimer_count      = 96                !Counting numbers of dimers per timestep
        integer             :: dimer_stat       = 97                !Dimer alignment/dimer proportion
        integer             :: activation_log   = 98                !Randomise log file
        integer             :: act_fract        = 99                !Activation surface fraction output file
        integer             :: rate_list_count  = 100               !Output file for breakdown of registry rates
        integer             :: rate_list_sum    = 103               !Output sum of rates within categories
        integer             :: dt               = 101               !DT output
        integer             :: non_zero_rates   = 111               !List of non_zero rates after each rate setting step
        integer             :: check_dimer      = 444               !Debug for the check_dimer() subroutine
        integer             :: surf_list        = 500               !List of the surface list generated each iteration
        integer             :: event_out        = 600               !Output of each event for each iteration
        integer             :: bookmark_log     = 666
        integer             :: json_m_output    = 999               !Main output in json format
        integer             :: m_output         = 1000              !Main output file for the program
        integer             :: height           = 1001              !Surface height output
        integer             :: roughness        = 1002              !Surface Roughness output
        integer             :: growth           = 1003              !Growth rate output
        integer             :: temp_events      = 1004              !Temporary easy to read event output
        integer             :: atom_envs        = 1005              !Atoms atom environments
        integer             :: layers           = 1006              !Layer-wise occupacy by the surface
        integer             :: events_time      = 1007              !Events over time
    end type file_units_type


    type counter_type
        integer, allocatable :: events(:,:)          !(rate_count, special counter)
        integer :: d_align(2)            !Number of dimers (aligned,realigned)
        integer :: d_break
        integer, allocatable :: rates_set_per_it(:)
        integer :: failed_dimer_attempts = 0 !Number times a dimer formation has failed
    end type counter_type


    !----------------
    !Store Rate Types
    !----------------
    type tRate_type
        integer :: nrates = 33
        integer :: DinsertCH3       = 1
        integer :: DinsertCH2       = 2
        integer :: DinsertCH        = 3
        integer :: DinsertC         = 4
        integer :: TinsertCH3       = 5
        integer :: TinsertCH2       = 6
        integer :: TinsertCH        = 7
        integer :: TinsertC         = 8
        integer :: C2H2_M1a         = 9
        integer :: C2H2_M1b         = 10
        integer :: C2H2_M2          = 11
        integer :: C2H2_M3          = 12
        integer :: dimerMigrate_r   = 13
        integer :: dimerMigrate_c   = 14
        integer :: gapMigrateA      = 15
        integer :: gapMigrateB      = 16
        integer :: gapMigrateC      = 17
        integer :: gapMigrateD      = 18
        integer :: gapMigrateC2H2_0 = 19
        integer :: gapMigrateC2H2_1 = 20
        integer :: migDown          = 21
        integer :: migC2H2_birad    = 22    !Migration of acetyl carbon into neighbouring biradical site
        integer :: migrateC2H2      = 23
        integer :: pref_etch_CHx    = 24
        integer :: etchC2H2         = 25
        integer :: etchCH3_C2H2     = 26
        integer :: ide_12_1         = 27
        integer :: ide_12_2         = 28
        integer :: ide_12_3         = 29
        integer :: ide_12_4         = 30
        integer :: ide_12_5         = 31
        integer :: etch_chx         = 32 !Netto2005
        integer :: c2H2p_dimer      = 33
    end type tRate_type

    type coord_type
        real(dp) :: x
        real(dp) :: y
        real(dp) :: z
    end type coord_type

    type rate_vis_switch_type
        logical :: dimer_migrate = .false.
        logical :: gap_migration = .false.
        logical :: migrate_down = .false.
    end type rate_vis_switch_type

    !Edge Environment enumerator
    type edge_state_type
        integer :: isolated = 0 !Isolated on the surface (TN=0)
        integer :: S_A_edge = 1 !Part of a S_A edge (dimer row direction || to edge)
        integer :: S_B_edge = 2 !Part of a S_B edge (dimer row direction _|_ to edge)
        integer :: S_AB_edge = 3!Corner where S_A and S_B edges meet
        integer :: terrace = 4  !Not part of an edge but within a terrace (TN=4)
    end type edge_state_type

    type species_type
        integer :: CH4 = 4
        integer :: CH3 = 3
        integer :: CH2 = 2
        integer :: CH = 1
        integer :: C = 0
    end type species_type

    type acetylene_rates_type
        real(dp) :: M1a
        real(dp) :: M1b
        real(dp) :: M2
        real(dp) :: M3a
        real(dp) :: M3b
        real(dp) :: etchCH3,etchC2H2
    end type acetylene_rates_type


    !Group upper and lower bounds
    type upper_lower_int_type
        integer :: lower !Inclusive
        integer :: upper !Non-inclusive
    end type upper_lower_int_type

    type xy_segment_type
        type(upper_lower_int_type) :: x
        type(upper_lower_int_type) :: y
    end type xy_segment_type

    type event_colour_type
        type(rgb_type) :: colour
        logical :: colour_event
    end type event_colour_type

    type dimer_env_enum
        integer :: NONE = 0
        integer :: ISO = 1
        integer :: ADJ = 2
    end type dimer_env_enum

contains

    !----------------------------
    !Are 2 atom objects the same?
    !----------------------------
    logical function same_atom (atom1,atom2) result(same)
        !Arguments
        type(atom_type), intent(in) :: atom1,atom2
        !Method
        if (atom1%i == atom2%i .and. atom1%j == atom2%j .and. atom1%basis == atom2%basis .and. atom1%k == atom2%k) then
            same = .true.
        else
            same = .false.
        end if
    end function same_atom

    !Check an array of atoms for uninitialised atoms
    logical function uninit_atom_list(atoms, sim_info) result(pass)
        !Arguments
        type(atom_type), intent(in), dimension(:) :: atoms
        type(sim_type), intent(in) :: sim_info
        !Variables
        integer :: iatom
        !Main Method
        pass = .true.
        do iatom=1,size(atoms)
            if ((atoms(iatom)%i < 1 .or. atoms(iatom)%i > sim_info%Nx) .or. &
            & (atoms(iatom)%j < 1 .or. atoms(iatom)%j > sim_info%Ny) .or. &
            & (atoms(iatom)%basis < 1 .or. atoms(iatom)%basis > sim_info%Nbasis)     .or. &
            & (atoms(iatom)%k < 1 .or. atoms(iatom)%k > sim_info%max_height)) then
                pass = .false.
            end if
        end do

    end function uninit_atom_list

    function average(this) result(ave)
        !Arguments
        type(average_type),intent(in) :: this
        real(dp) :: ave
        !Method

        if (this%count == 0) then
            ave = 0.0_dp
            return
        end if

        ave = this%sum/this%count

    end function average

    subroutine average_add(this, amount)
        !Arguments
        type(average_type),intent(inout) :: this
        real(dp),intent(in) :: amount
        !Method

        this%sum = this%sum + amount
        this%count = this%count + 1

    end subroutine average_add

    subroutine average_reset(this)
        !Arguments
        class(average_type), intent(inout) :: this
        !Method
        this%sum = 0.0_dp
        this%count = 0

    end subroutine average_reset

    function average_rates_set(this) result(ave)
        !Arguments
        type(average_rates_set_type),intent(in) :: this
        real(dp), dimension(:), allocatable :: ave
        integer :: i
        !Method
        allocate(ave(size(this%sum)))
        if (this%count == 0) then
            ave(:) = 0.0_dp
            return
        end if

        do i=1,size(this%sum)
            ave(i) = this%sum(i)/this%count
        end do

    end function average_rates_set

    subroutine average_add_rates_set(this, amount)
        !Arguments
        type(average_rates_set_type),intent(inout) :: this
        integer, dimension(:), intent(in) :: amount
        !Method

        if (size(this%sum) /= size(amount)) then
            write(6,*) "Invalid amount vector passed in order to add to sum vector"
        end if
        this%sum(:) = this%sum(:) + amount(:)
        this%count = this%count + 1

    end subroutine average_add_rates_set

    subroutine average_reset_rates_set(this)
        !Arguments
        class(average_rates_set_type), intent(inout) :: this
        !Method
        this%sum(:) = 0.0_dp
        this%count = 0

    end subroutine average_reset_rates_set

    function XYSegment(xl, xu, yl, yu) result(seg)
        !Arguments
        integer, intent(in) :: xl, xu, yl, yu
        type(xy_segment_type)  :: seg
        !Method
        seg%x%lower = xl
        seg%x%upper = xu
        seg%y%lower = yl
        seg%y%upper = yu

    end function XYSegment

    subroutine reset_atom_ext(self)
        !Arguments
        class(atom_type_ext), intent(inout) :: self
        !Method
        self%act_state = 0
        self%dimer = .false.
        self%ipair = 0
        self%last_frame = .false.
        self%last_event = 0
        self%ilink = 0
        self%mig = 0
    end subroutine reset_atom_ext

end module types
