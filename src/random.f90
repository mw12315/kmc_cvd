module random

implicit none

contains

!Seed the random number generator with either an uninitialised seed (-1)
!or an input clock seed
!If unititialised, seeding_clock is set from the system clock before the
!seeding process takes place
subroutine init_random_seed(clock_seed)
    !Modules
    !Arguments
    integer, intent(inout) :: clock_seed
    !Variables
    integer :: i, n
    integer, dimension(:), allocatable :: seed
  
    call random_seed(size = n)
    allocate(seed(n))

    if (clock_seed == -1) then
        call system_clock(count=clock_seed)
    end if


  seed(:) = clock_seed + 37 * (/ (i - 1, i = 1, n) /)
  call random_seed(put = seed)

  deallocate(seed)

end subroutine init_random_seed

end module random

