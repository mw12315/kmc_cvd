module enumerate

    use kinds,                  only : dp
    use types,                  only : file_units_type,types_of_atom_type,tRate_type,species_type, &
                                     & edge_state_type,rgb_type,dimer_env_enum

    implicit none

    !----------
    !Atom Types
    !----------
    type(types_of_atom_type)  :: types_of_atom
    type(species_type)        :: species

    !----------
    !Rate Types
    !----------
    type(tRate_type) :: tRates
    integer,dimension(4) :: irate_dimer_ins
    integer,dimension(4) :: irate_trough_ins

    !----------------------
    !Atom Environment Types
    !----------------------
    integer, parameter  :: bulk_site            = 0                 !Bulk
    integer, parameter  :: dimer_site           = 1                 !Directly above a dimer bond
    integer, parameter  :: trough_site_2        = 2                 !Trough Site between 2 dimers
    integer, parameter  :: trough_site_1        = 3                 !Trough Site between 1 dimer
    integer, parameter  :: trough_site_0        = 4                 !Trough Site between 0 dimers

    !----------------
    !Edge Environment
    !----------------
    type(edge_state_type) :: edge_state

    !---------------------
    !Dimer Env/ Dimer Type
    !---------------------
    type(dimer_env_enum) :: dimer_env !NONE, ISO, ADJ

    !----------------
    !Activation State
    !----------------
    integer, parameter :: actstate_max = 3
    integer, parameter :: not_activated = 0
    integer, parameter :: actstate_min = 1

    !------
    !Dimers
    !------
    integer, parameter  :: no_dimer = 0

    !------------------------------
    !File Units
    !Values stored within types.f90
    !------------------------------
    type(file_units_type),save  :: file_units


end module enumerate
