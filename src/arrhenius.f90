! Created by Max Williams on 02/02/2021.

module arrhenius

    use kinds, only : dp

    contains

    !Calculate the rate coffeficient of a reaction given its pre-exponential factor, A
    !energy barrier, E and temperature
    real(dp) function k_arrhenius(A, E, T)
        !Modules
        use variables, only : Rgas
        !Arguments
        real(dp), intent(in) :: A !Units of /s
        real(dp), intent(in) :: E !Units of J/mol
        integer, intent(in) :: T !Units of K

        k_arrhenius = A * exp(-E/(Rgas * T))

    end function k_arrhenius

end module arrhenius