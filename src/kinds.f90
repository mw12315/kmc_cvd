module kinds

  implicit none

  integer, parameter :: dp=selected_real_kind(15,307) !double precision
  integer, parameter :: sp=selected_real_kind(6,37) !single precision


end module kinds
