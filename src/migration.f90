module migration

    use kinds, only : dp

    implicit none

    !Dimer Migration NN Paths
    integer, parameter, dimension(2,4) :: d_mig_chain = reshape((/ 2,1,2,1, 3,4,3,4 /), shape = (/ 2, 4 /), order = (/ 2,1 /))
    integer, parameter, dimension(2,2) :: d_mig_row = reshape((/ 1,2, 4,3 /), shape = (/ 2,2 /), order = (/ 2,1 /))

    public set_dimer_migration_rate, set_gap_mig_rate, set_mig_down_rate, set_C2H2_mig_biradical_rate, &
           execute_migration, visualise_dimer_migration, visualise_gap_migration, visualise_migration_down
    private get_dimer_mig_sites, get_mig_down_sites, get_C2H2_mig_birad_sites, &
            return_gap_env, check_crit_nuc, passed_basic_migration_reqs, incr_atom_mig_counter


contains

    !===================================
    !----------- RATE SETTING ----------
    !===================================
!   todo Should a dimer prevent migration? Inhibit but not prevent? critical nucleus of 1 does the same. Should CN inhibit but not prevent?
    !------------------------------------------------------------------------------------
    !Set DIMER migration rates (all directions) for a target atom
    !A dimer migration follows the information from Netto and Frenklach's
    !2005 kMC model:
    !Netto, A., & Frenklach, M. (2005). Diamond and Related Materials, 14(10), 1630–1646.
    !------------------------------------------------------------------------------------
    subroutine set_dimer_migration_rate(atom, atoms, rate_list,ilr)
        !Modules
        use types, only : atom_type, atom_type_ext, rate_type
        use variables, only : read_atom, read_dimer, set_rate, is_act, read_ilink
        use periodic, only : count_nearest_atoms, ret_neighbour
        use derived_values, only : k_dimer_mig
        use enumerate, only : types_of_atom, tRates, dimer_site
        use surface_m, only : detect_site_env, C2H2_pendant
        !Arguments
        type(atom_type), intent(inout) :: atom
        type(atom_type_ext), dimension(:), intent(in) :: atoms
        type(rate_type), dimension(:), intent(inout) :: rate_list
        integer, intent(inout) :: ilr
        !Variables
        type(atom_type), dimension(4) :: f_sites           !4 final site positions
        type(atom_type), dimension(4,4) :: a_sites         !Atoms required to be activated (2 per site)
        type(atom_type), dimension(4) :: b_sites           !Atom sites which could block a migration
        integer :: iSite
        integer :: NN, NN2, NN3         !N nearest neighbours, NN of a neighbours 2 and 3
        logical :: dimer_can_reform     !Dimer can reform under site left by migrating atom
        integer,dimension(4) :: row_or_chain ! Store the rate type of row and chain at iSite
        !----------
        !Initialise
        !----------
        !todo Does the ability of a dimer to reform in a a dimer migration effect energy barrier
        dimer_can_reform = .false.
        row_or_chain(1:2) = tRates%dimerMigrate_c
        row_or_chain(3:4) = tRates%dimerMigrate_r
        !Main Method
        !Acetyle_c2 or C2H2 atoms cannot migrate via this method
        if (read_atom(atom, atoms) == types_of_atom%acetyl_c2 .or. read_ilink(atom, atoms) /= 0) return

        !Get sites associated with chain and row migrations
        call get_dimer_mig_sites(atom, atoms, f_sites, a_sites, b_sites)
        !Collect site/local environment information
        call count_nearest_atoms(atom, atoms,  NN)
        if (.not. passed_basic_migration_reqs(read_dimer(atom, atoms), NN)) return
        !Dimer can reform below original site?
        call count_nearest_atoms(ret_neighbour(atom,2), atoms, NN2)
        call count_nearest_atoms(ret_neighbour(atom,3), atoms, NN3)
        if (NN2 == 3 .and. NN3 == 3) dimer_can_reform = .true.

        !Set Migration Rates for all 4 possible final sites
        do iSite=1,size(f_sites)
            !Requirements to have rate set:
            !TN < Critical Nucleus or TN increased by migration
            !Final Site is empty and is above a dimer reconstruction
            !Both required atoms are activated
            !Potential blocking species are empty
            !A dimer can reform behind the migration species
            !No C2H2 atom hindering the final migration site
            if (check_crit_nuc(atom, atoms, f_sites(iSite))) then
                if (read_atom(f_sites(iSite), atoms) == types_of_atom%empty_site) then
                    if (detect_site_env(f_sites(iSite), atoms) == dimer_site) then
                        !Activation
                        if (is_act(a_sites(iSite,1), atoms) .and. is_act(a_sites(iSite,2), atoms)) then
                            !Blocking Species
                            if (read_atom(b_sites(iSite), atoms) == types_of_atom%empty_site) then
                                if (dimer_can_reform) then
                                    !Check that C2H2 pendant not hindering final site
                                    if (.not. C2H2_pendant(f_sites(iSite), atoms)) then
                                        call set_rate(atom, &
                                                      row_or_chain(iSite), & !Rate Type
                                                      k_dimer_mig, & !Rate
                                                      rate_list, &
                                                      ilr, &
                                                      f_sites(iSite))
                                    end if
                                end if
                            end if
                        end if
                    end if
                end if
            end if

        end do

    end subroutine set_dimer_migration_rate

    !------------------------------------------------------------------------------------
    !Set Migration into gap rates for an input atom
    !If an edge is detected, migration down can be called
    !A gap migration follows the information from Netto and Frenklach's
    !2005 kMC model:
    !Netto, A., & Frenklach, M. (2005). Diamond and Related Materials, 14(10), 1630–1646.
    !------------------------------------------------------------------------------------
    subroutine set_gap_mig_rate(atom, atoms, rate_list, ilr)
        !Modules
        use types, only : atom_type_ext, atom_type, rate_type
        use variables, only : read_atom, read_dimer, set_rate, is_act, read_ilink
        use periodic, only : num_bonds
        use derived_values, only : kgmig_all_rates
        use enumerate, only : types_of_atom, tRates
        use input, only : reactions, dimerf
        use surface_m, only : C2H2_pendant
        use dimer, only : dimer_can_break
        !Variables
        type(atom_type), intent(inout) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        type(rate_type), intent(inout) :: rate_list(:)
        integer, intent(inout) :: ilr
        !Variables
        integer :: iSite
        integer :: migration_env            !Environment of final/initial sites [1,4]
        type(atom_type) :: f_sites(2)       !Final Sites(:)
        type(atom_type) :: act_sites(2,2)     !Activation Sites(:,:) linked with f_sites(:)
        logical :: rate_set(2)
        rate_set = .true.
        !Main Method
        !==============================================
        !Conditions for a gap migration
        !- Atom forming <= 2 bonds/not part of dimer
        !- not an part of a C2H2 unit
        !- Final site empty
        !- activated atoms around final site
        !- Critcal nucleus increased or maintained
        !     O   M   F   O
        !    / \ / \ . . / \
        !   C   C   C   C   C
        !- M - Migration atom
        !- F - Final Site
        !- O - Optional atoms present
        !- . - Radical sites
        !==============================================
        !Return if migrating atom over-bonded
        if (num_bonds(atom, atoms) > 2) then
            return
        end if

        !Acetyle_c2 or C2H2 atoms cannot migrate via this (specific) method
        if (read_atom(atom, atoms) == types_of_atom%acetyl_c2 .or. read_ilink(atom, atoms) /= 0) return

        !Get final_sites + associated sites required to be activated
        call get_gap_mig_sites(atom, f_sites, act_sites)
        !Cycle through final sites and set rate if migration possible
        do iSite=1,size(f_sites)
            !Empty final site
            if (read_atom(f_sites(iSite), atoms) /= types_of_atom%empty_site) then
                rate_set(iSite) = .false.
                cycle
            end if
            !Activated atoms (or a dimer which could break)
            if (.not. is_act(act_sites(iSite,1), atoms) .or. (.not. is_act(act_sites(iSite,2), atoms))) then
                if (dimerf%gap_via_dimer) then
                    if (.not. dimer_can_break(act_sites(iSite,1), act_sites(iSite,2), atoms)) then
                        rate_set(iSite) = .false.
                        cycle
                    end if
                else
                    rate_set(iSite) = .false.
                    cycle
                end if
            end if
            !Critcal Nucleus
            if (.not. check_crit_nuc(atom, atoms, f_sites(iSite))) then
                rate_set(iSite) = .false.
                cycle
            end if
            !C2H2 pendant next to final site
            if (C2H2_pendant(f_sites(iSite), atoms)) then
                rate_set(iSite) = .false.
                cycle
            end if
        end do

        do iSite=1,size(f_sites)
            if (rate_set(iSite)) then
                migration_env = return_gap_env(atom, atoms, f_sites(iSite))
                if (reactions%active%gap_mig_ind(migration_env)) then
                    call set_rate(atom, &
                            tRates%gapMigrateA+migration_env-1, &
                            kgmig_all_rates(migration_env), &
                            rate_list, &
                            ilr, &
                            f_sites(iSite))
                end if
            end if
        end do

    end subroutine set_gap_mig_rate

    !-----------------------------------------------------------------------------------------
    !Set Migration down a single step rates (if supporting atoms for normal migration missing)
    !-----------------------------------------------------------------------------------------
    subroutine set_mig_down_rate(atom, atoms, rate_list, ilr)
        !Module
        use types, only : atom_type_ext, atom_type,rate_type
        use periodic, only : count_nearest_atoms
        use variables, only : read_atom, read_dimer, set_rate, is_act, read_ilink
        use enumerate, only : types_of_atom, tRates
        use derived_values, only : k_down_mig
        use surface_m, only : C2H2_pendant
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), dimension(:), intent(in) :: atoms
        type(rate_type), dimension(:), intent(inout) :: rate_list
        integer, intent(inout) :: ilr
        !Variables
        integer :: iSite
        integer :: NN
        type(atom_type), dimension(2) :: f_sites        !Final Sites
        type(atom_type), dimension(2,3) :: act_sites    !Activation sites (1 .and. (2 .or. 3)
        type(atom_type), dimension(2) :: b_sites        !Sites blocking migration
        type(atom_type), dimension(2,2) :: s_atoms      !Supporting atoms of f_sites
        !-----------
        !Main Method
        !-----------
        call get_mig_down_sites(atom, atoms, f_sites, act_sites, b_sites, s_atoms)
        !----------------------------------------
        !Requirements
        !One of the supporting atoms is activated
        !Empty site
        !N2 + N3 filled
        !----------------------------------------
        !Atom has no more than 3 bonds/not part of a dimer
        call count_nearest_atoms(atom, atoms, NN)
        if (NN > 2 .or. read_dimer(atom, atoms)) return

        !Acetyle_c2 or C2H2 atoms cannot migrate via this method
        if (read_atom(atom, atoms) == types_of_atom%acetyl_c2 .or. read_ilink(atom, atoms) /= 0) return

        do iSite=1,size(f_sites)
            if (check_crit_nuc(atom, atoms, f_sites(iSite))) then
                if (read_atom(f_sites(iSite), atoms) == types_of_atom%empty_site) then
                    !Activation (act_sites(i,1) .and. (act_sites(i,2) .or. act_sites(i,3))
                    if (is_act(act_sites(iSite,1), atoms) .and. &
                    & (is_act(act_sites(iSite,2), atoms) .or. is_act(act_sites(iSite,3), atoms))) then
                        !Atoms below site both present
                        if (read_atom(s_atoms(iSite,1), atoms) /= types_of_atom%empty_site .and. &
                                read_atom(s_atoms(iSite,2), atoms) /= types_of_atom%empty_site) then
                            !Blocking atoms (the original gap site)
                            if (read_atom(b_sites(iSite), atoms) == types_of_atom%empty_site) then
                                !Check no trailing C=C next to final site
                                if (.not. C2H2_pendant(f_sites(iSite), atoms)) then
                                    !Rate setting
                                    call set_rate(atom, &
                                                  tRates%migDown, &
                                                  k_down_mig, &
                                                  rate_list, &
                                                  ilr, &
                                                  f_sites(iSite))
                                end if
                            end if
                        end if
                    end if
                end if
            end if
        end do

    end subroutine set_mig_down_rate


    !---------------------------------------------------------------------------
    !Set rate for adsorption of a C2H2 molecule followed by immediate adsorption
    !As a dimer reconstruction due to the presence of a biradical site
    !---------------------------------------------------------------------------
    subroutine set_C2H2_mig_biradical_rate(atom, atoms, rate_list, ilr)
        !Modules
        use types, only : atom_type, atom_type_ext, rate_type
        use variables, only : read_atom, set_rate, is_act
        use enumerate, only : tRates, types_of_atom
        use adsorb_atom, only : biradical_site
        use derived_values, only : k_dimer_mig
        use surface_m, only : C2H2_pendant
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        type(rate_type), dimension(:), intent(inout) :: rate_list
        integer, intent(inout) :: ilr
        !Variables
        type(atom_type), dimension(2) :: f_sites        !Final migration sites
        type(atom_type), dimension(2,2) :: act_sites    !Sites required to be activated (for each site)
        integer :: iSite
        !Main Method
        !Site getting
        call get_C2H2_mig_birad_sites(atom, atoms, f_sites, act_sites)

        !Analyse potential for rate setting
        !Atom is of correct type (acetyle_c2)
        !Empty final site
        !Atom is activated (for now)
        !Biradical site below
        if (read_atom(atom, atoms) == types_of_atom%acetyl_c2) then
            do iSite=1, size(f_sites)

                if (read_atom(f_sites(iSite), atoms) == types_of_atom%empty_site) then
                    if (.not. C2H2_pendant(f_sites(iSite), atoms)) then
                      if (biradical_site(f_sites(iSite), atoms)) then
                          if (is_act(atom, atoms)) then
                              call set_rate(atom, &
                                      tRates%migC2H2_birad, &
                                      k_dimer_mig, &
                                      rate_list, &
                                      ilr, &
                                      f_sites(iSite))
                          end if
                      end if
                    end if
                end if

            end do
        end if

    end subroutine set_C2H2_mig_biradical_rate

    !===================================
    !--------- EVENT EXECUTION ---------
    !===================================
    !Execute a migration reaction
    !Operations
    !> Add the species to the new site (site2)
    !   - In the case of C2H2 migration, a pendant atom should also be created
    !> Remove migration species from the original location (site)
    !   - C2H2 will require the linked atom to also be removed
    !> Reform any dimers if required
    !> Add new atoms to the surface
    !> Remove the old atoms from the surface
    subroutine execute_migration(rate_to_execute, kmc_info, atoms)
        !Modules
        use types, only : rate_type, kmc_type, atom_type_ext, atom_type, same_atom
        use variables, only : read_atom, read_act, read_ilink, linked_atom, &
                set_act_state, set_last_frame, link_atoms, ilattice
        use enumerate, only : types_of_atom, tRates, not_activated
        use periodic, only : count_nearest_atoms, ret_neighbour
        use dimer, only : create_dimer, dimer_assessment
        use surface_m, only : remove_surface_atom, add_surface_atom
        use pendant, only : patom2_store
        !Arguments
        type(rate_type), intent(in) :: rate_to_execute
        type(kmc_type), intent(inout) :: kmc_info
        type(atom_type_ext),dimension(:), intent(inout) :: atoms
        !Variables
        integer :: act_level
        integer :: dont_activate(3) !Don't inherit the activation state for these reactions
        integer :: c2h2p_migrations(2) !Indices which are C2H2 pendant migrations
        integer :: reform_dimers(5) !Events where dimer reformed below initial site
        !Reduce derived type access
        integer :: event
        type(atom_type) :: initial_site, initial_site_c2, final_site, final_site_c2
        event = rate_to_execute%event
        initial_site = rate_to_execute%site
        final_site = rate_to_execute%site2
        !Initialise event type arrays
        dont_activate = [tRates%migC2H2_birad, &
                         tRates%gapMigrateC2H2_0, &
                         tRates%gapMigrateC2H2_1]
        c2h2p_migrations = [tRates%gapMigrateC2H2_0, tRates%gapMigrateC2H2_1]
        reform_dimers = [tRates%dimerMigrate_c, tRates%dimerMigrate_r, &
                         tRates%gapMigrateC2H2_0, tRates%gapMigrateC2H2_1, &
                         tRates%migDown]

        !Main Method
        !Inherit the activation stay between migrations?
        if (any(event == dont_activate)) then
            act_level = not_activated
        else
            act_level = read_act(initial_site, atoms)
        end if

        !----------------
        !Final Site Atoms
        !----------------
        call add_surface_atom(final_site, types_of_atom%surf_carb, atoms)
        call dimer_assessment(final_site, atoms)
        call set_act_state(final_site, atoms, act_level)
        if (any(event == c2h2p_migrations)) then
            final_site_c2 = patom2_store(final_site, atoms)
            call add_surface_atom(final_site_c2, types_of_atom%acetyl_c2, atoms)
            call link_atoms(final_site, final_site_c2, atoms)
        end if

        !Increment migration counter of migrating atom
        call incr_atom_mig_counter(initial_site, final_site, atoms)

        !Remove initial site(s)
        !From surface
        if (any(event == c2h2p_migrations)) initial_site_c2 = linked_atom(initial_site, atoms)
        call remove_surface_atom(initial_site, kmc_info, atoms, is_migration=.true.)
        if (any(event == c2h2p_migrations)) then
            call remove_surface_atom(initial_site_c2, kmc_info, atoms)
        end if


        call set_last_frame(final_site, atoms ,.true., event)
        if (any(event == c2h2p_migrations)) call set_last_frame(final_site_c2, atoms ,.true., event)
        !Reform dimer below initial site if required by mechanism
        if (any(event == reform_dimers)) then
            call create_dimer(ret_neighbour(initial_site, 2), ret_neighbour(initial_site, 3), atoms)
        end if
!        call dimer_assessment(initial_site, atoms)
!        call dimer_assessment(final_site, atoms)

    end subroutine execute_migration

    !===================================
    !----------- SITE GETTING ----------
    !===================================
    !------------------------------------------------------------------------------------------
    !Return final migration sites, required activation and potential blocking atoms
    !for dimer migration [A. Netto and M. Frenklach, Diam. Relat. Mater., 2005, 14, 1630–1646.]
    !------------------------------------------------------------------------------------------
    subroutine get_dimer_mig_sites(atom, atoms, final_sites, act_sites, blocking_sites)
        !-------
        !Modules
        !-------
        use types,  only : atom_type, atom_type_ext
        use periodic,   only : get_site_from_N_path, ret_neighbour
        use variables,  only : is_act
        !---------
        !Arguments
        !---------
        type(atom_type), intent(in) :: atom                             !Starting atom position
        type(atom_type_ext), dimension(:),intent(in) :: atoms
        type(atom_type), dimension(4), intent(out) :: final_sites       !4 final site positions
        type(atom_type), dimension(4,4), intent(out) :: act_sites       !Atoms required to be activated (2 per site)
        type(atom_type), dimension(4), intent(out) :: blocking_sites    !Atom sites which could block a migration
        !---------
        !Variables
        !---------
        integer :: iSite
        integer :: row_act_dir      !Activated neighbour of initial site [2,3]
        integer :: nChain           !Number of Chain sites (for index modification)
        !-----------
        !Main method
        !-----------
        nChain = size(d_mig_chain,1) !Number chain directions take from array containing said directions
        !Chain sites (2) and associated sites
        do iSite=1,size(d_mig_chain,1)
            final_sites(iSite) = get_site_from_N_path(atom, d_mig_chain(iSite,:)) !Follow full path to end
            blocking_sites(iSite) = get_site_from_N_path(atom,d_mig_chain(iSite,1:2)) !Follow half way to end
            act_sites(iSite,1) = get_site_from_N_path(atom, d_mig_chain(iSite,1:1))
            act_sites(iSite,2) = get_site_from_N_path(atom, d_mig_chain(iSite,1:3))
        end do

        !Row sites (2) and associated sites
        do iSite=1,size(d_mig_row,1)
            !iSite modified to access row part of arrays (+nChain)
            final_sites(iSite+nChain) = get_site_from_N_path(atom, d_mig_row(iSite,1:2))
            !No Blocking atom exists for row migration, -1,-1,-1,-1 will always be empty
            blocking_sites(iSite+nChain) = atom_type(-1,-1,-1,-1)
            !Choice of activated atoms (paired) depends on initial activation
            if (is_act(ret_neighbour(atom,2), atoms)) then
                row_act_dir = 2
            else if (is_act(ret_neighbour(atom,3), atoms)) then
                row_act_dir = 3
            else
                row_act_dir = 2
            end if
            act_sites(iSite+nChain,1) = ret_neighbour(atom,row_act_dir)
            act_sites(iSite+nChain,2) = ret_neighbour(final_sites(iSite+nChain),row_act_dir)
        end do

    end subroutine get_dimer_mig_sites

    !-------------------------------------------------------
    !Return atom_type arrays of final_sites(2), act_sites(2)
    !associated with 'gap migration'
    !     O   M   F   O
    !    / \ / \ . . / \
    !   C   C   C   C   C
    !-------------------------------------------------------
    subroutine get_gap_mig_sites(atom, final_sites, act_sites)
        !Modules
        use types,      only : atom_type,uninit_atom_list
        use periodic,   only : ret_neighbour,get_site_from_N_path
        use variables,  only : simulation
        use enumerate,  only : file_units
        !Variables
        type(atom_type), intent(in) :: atom
        type(atom_type), dimension(2), intent(out) :: final_sites
        type(atom_type), dimension(2,2), intent(out) :: act_sites
        integer, dimension(2,2) :: f_site_paths
        integer  :: iSite
        !Init direction storage
        f_site_paths(1,:) = (/ 2,1 /)
        f_site_paths(2,:) = (/ 3,4 /)
        !Main Method
        !     O   M   F   O
        !    / \ / \ . . / \
        !   C   C   C   C   C
        !- M - Migration atom
        !- F - Final Site
        !- O - Optional atoms present
        !- . - Radical sites

        do iSite=1,size(final_sites)
            final_sites(iSite) = get_site_from_N_path(atom, f_site_paths(iSite,1:2))
            act_sites(iSite,1) = ret_neighbour(final_sites(iSite),2)
            act_sites(iSite,2) = ret_neighbour(final_sites(iSite),3)
        end do


    end subroutine get_gap_mig_sites

    !--------------------------------------------------------------------------
    !Return array(2) of final sites for migration down a step for gap migration
    !--------------------------------------------------------------------------
    subroutine get_mig_down_sites(atom, atoms, final_sites, act_sites, blocking_sites, supporting_atoms)
        !---------------------
        !Module use statements
        !---------------------
        use types,          only : atom_type_ext,atom_type,uninit_atom_list
        use periodic,       only : ret_neighbour
        use variables,      only : read_atom,simulation
        use enumerate,      only : types_of_atom,file_units
        !---------------------
        !Variable Declarations
        !---------------------
        type(atom_type), intent(in) :: atom
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        type(atom_type), dimension(2), intent(out) :: final_sites,blocking_sites
        type(atom_type), dimension(2,3), intent(out):: act_sites
        type(atom_type), dimension(2,2), intent(out):: supporting_atoms
        type(atom_type), dimension(2) :: gap_sites
        type(atom_type), dimension(2,2) :: gap_act_sites
        integer :: iSite
        !---------------------
        !Main Method
        !      A    X
        !     / \ /  \
        !    C   C    S
        !    |   |    |
        !    C   C    C
        !   / \ / \ /  \
        !  C   C   C    C
        ! A - atom
        ! S - empty site
        ! X - empty 'gap site'
        !---------------------
        call get_gap_mig_sites(atom, gap_sites, gap_act_sites)

        do iSite=1,size(gap_sites)
            !N2 of gap site empty
            if (read_atom(ret_neighbour(gap_sites(iSite),2), atoms) == types_of_atom%empty_site) then
                blocking_sites(iSite) = gap_sites(iSite)
                final_sites(iSite) = ret_neighbour(gap_sites(iSite),2)
                act_sites(iSite,1) = ret_neighbour(gap_sites(iSite),3)
                act_sites(iSite,2) = ret_neighbour(final_sites(iSite),2)
                act_sites(iSite,3) = ret_neighbour(final_sites(iSite),3)
            !N3 of gap site empty
            else
                blocking_sites(iSite) = gap_sites(iSite)
                final_sites(iSite) = ret_neighbour(gap_sites(iSite),3)
                act_sites(iSite,1) = ret_neighbour(gap_sites(iSite),2)
                act_sites(iSite,2) = ret_neighbour(final_sites(iSite),2)
                act_sites(iSite,3) = ret_neighbour(final_sites(iSite),3)
            end if
            !Support atoms (common between directions)
            supporting_atoms(iSite,1:2) = (/ ret_neighbour(final_sites(iSite),2),ret_neighbour(final_sites(iSite),3) /)
        end do

        !Check output atom lists for uninitialised values
        if (.not. uninit_atom_list(final_sites, simulation)) then
            write(file_units%warning,'(a)') "get_mig_down_sites()_final_sites: uninitialised"
        end if
        if (.not. uninit_atom_list(act_sites(1,:), simulation)) then
            write(file_units%warning,'(a)') "get_mig_down_sites()_act_sites1: uninitialised"
        end if
        if (.not. uninit_atom_list(act_sites(2,:), simulation)) then
            write(file_units%warning,'(a)') "get_mig_down_sites()_act_sites2: uninitialised"
        end if
        if (.not. uninit_atom_list(supporting_atoms(1,:), simulation)) then
            write(file_units%warning,'(a)') "get_mig_down_sites()_s_sites1: uninitialised"
        end if
        if (.not. uninit_atom_list(supporting_atoms(2,:), simulation)) then
            write(file_units%warning,'(a)') "get_mig_down_sites()_s_sites2: uninitialised"
        end if
        if (.not. uninit_atom_list(blocking_sites, simulation)) then
            write(file_units%warning,'(a)') "get_mig_down_sites()_b_sites: uninitialised"
        end if

    end subroutine get_mig_down_sites

    subroutine get_C2H2_mig_birad_sites(atom, atoms, final_sites, activated_sites)
        !Modules
        use types, only : atom_type, atom_type_ext
        use variables, only : read_ilink,kmc_info
        use enumerate, only : file_units
        use periodic, only : get_site_from_N_path,ret_neighbour
        !Variables
        type(atom_type), intent(in) :: atom
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        type(atom_type), dimension(2), intent(out) :: final_sites
        type(atom_type), dimension(2,2), intent(out) :: activated_sites !Sites required to be activated

        type(atom_type) :: base_atom
        integer :: ilink,iSite
        integer, dimension(2,2) :: path_to_final_sites = reshape((/ 1,2, 4,3 /), shape = (/ 2, 2 /), order = (/ 2,1 /))
        !Main Method
        ilink = read_ilink(atom, atoms)
        !Find supporting atom (of carbonyl)
        if (ilink > 0) then
            base_atom = atoms(ilink)%atom
        else
            base_atom = atom_type(0,0,0,0)
            write(file_units%error,*) kmc_info%step, "C2H2 Migrate2birad sites: acetyle_c2 not linked with base atom"
            STOP
        end if

        !Get final sites
        do iSite=1, size(final_sites)
            final_sites(iSite) = get_site_from_N_path(base_atom, path_to_final_sites(iSite,:))
            activated_sites(iSite,1:2) = (/ ret_neighbour(final_sites(iSite),2), ret_neighbour(final_sites(iSite),3) /)
        end do

    end subroutine get_C2H2_mig_birad_sites

    !---------------------------------------------------------------
    !Return Pendant Migration Sites, starting from an activated site
    !---------------------------------------------------------------
!    subroutine get_mig_pen_sites(atom,down_sites,act_sites)
!        !-------
!        !Modules
!        !-------
!        use types,      only : atom_type,return_atom
!        use variables,  only : read_atom
!        use periodic,   only : ret_neighbour
!        use dimer,      only : return_layer
!        use enumerate,  only : types_of_atom
!        !---------
!        !Variables
!        !---------
!        type(atom_type),intent(in)              :: atom
!        type(atom_type),intent(out),dimension(4):: down_sites       !All sites down from starting position
!        type(atom_type),intent(out),dimension(4):: act_sites        !Sites required to be activated
!
!        integer                                 :: idx              !Array/list index
!        integer                                 :: layer
!
!        !          M
!        !         / \
!        !        C   A
!        !        |   |
!        !        C   C
!        !       / \ / \
!        !      C   X   X
!        !
!        !    Migrating atom (M) with A required to be activated and either X activated. Pendant would be attached to an activated X atom
!
!        do idx=1,size(act_sites)
!            call return_atom(0,0,0,0,   act_sites(idx))
!        end do
!        !-----------
!        !Main Method
!        !-----------
!        !Gets sites if required atoms are activated
!        call get_mig_down_sites(atom,down_sites(1:2),down_sites(3:4))
!
!        do idx=1,size(down_sites)
!            if (down_sites(idx)%i == 0) cycle
!            if (read_atom(down_sites(idx)) /= types_of_atom%empty_site) cycle
!
!
!            layer = return_layer(down_sites(idx)%basis)
!            if (idx <= 2) then      !Up Sites
!                if (layer == 1 .or. layer == 3) then
!                    act_sites(idx) = ret_neighbour(down_sites(idx),2)
!                else
!                    act_sites(idx)= ret_neighbour(down_sites(idx),3)
!                end if
!            else                    !Down Sites
!                if (layer == 1 .or. layer == 3) then
!                    act_sites(idx) = ret_neighbour(down_sites(idx),3)
!                else
!                    act_sites(idx) = ret_neighbour(down_sites(idx),2)
!                end if
!            end if
!        end do
!
!
!    end subroutine get_mig_pen_sites

    !===================================
    !------------- FUNCTIONS -----------
    !===================================
    !--------------------------------------------------------------------------------
    !Return environment (1-4) of a gap site depending on co-ordination of input sites
    !From Netto2005: 10.1016/j.diamond.2005.05.009
    !--------------------------------------------------------------------------------
    integer function return_gap_env(atom, atoms, site) result(env)
        !Modules
        use types,      only : atom_type_ext,atom_type
        use variables,  only : read_atom,kmc_info
        use periodic,   only : ret_neighbour,get_site_from_N_path
        use enumerate,  only : types_of_atom,file_units
        !Variables
        type(atom_type_ext), intent(in) :: atoms(:)
        type(atom_type), intent(in) :: atom, site

        type(atom_type)             :: adj_atom,adj_site!Store atoms adj to the atom and site
        integer,dimension(2,2)      :: env_dirs
        integer                     :: c_atom,c_site    !Adjacent carbons to atom and site
        integer                     :: idir             !direction index

        !Initialise Variables
        env = -1
        c_atom = 0
        c_site = 0
        env_dirs(1,:) = (/ 2,1 /)
        env_dirs(2,:) = (/ 3,4 /)
        !Main Method
        !Check that atom is present
        if (read_atom(atom, atoms) == types_of_atom%empty_site) then
            write(file_units%error,*) kmc_info%step, "return_gap_env(atom,site): atom site is empty"
        end if
        !Count atoms adjacent to atom and site
        do idir = 1,size(env_dirs,1)
            adj_atom = get_site_from_N_path(atom, env_dirs(idir,:))
            adj_site = get_site_from_N_path(site, env_dirs(idir,:))

            if (read_atom(adj_atom, atoms) /= types_of_atom%empty_site) c_atom = c_atom + 1
            if (read_atom(adj_site, atoms) /= types_of_atom%empty_site) c_site = c_site + 1
        end do


        !Determine environment
        if (c_site == 2) then
            if (c_atom == 0) then
                env = 1
            else if (c_atom == 1) then
                env = 2
            end if
        else if (c_site == 1) then
            if (c_atom == 0) then
                env = 3
            elseif (c_atom == 1) then
                env = 4
            end if
        else
            write(file_units%error,*) kmc_info%step,"Gap migration environment not found (atom,site,c_atom,s_site)",atom,"|",site,"|",c_atom,"|",c_site
            write(*,*) "Gap migration rate setting error: Stopping"
            STOP
        end if


    end function return_gap_env

    !---------------------------------------------------------------------------------------------------------------
    !Count Next Nearest Neighbours if initial and final sites, returning true if the number of critical atoms
    !to prevent migration hasn't been reached OR that the final site has equal to or more NNN than the initial site.
    !---------------------------------------------------------------------------------------------------------------
    logical function check_crit_nuc(atom,atoms,final_site) result(able)
        !-------
        !Modules
        !-------
        use types,      only : atom_type_ext,atom_type
        use input,      only : reactions
        use periodic,   only : count_TN
        !---------
        !Variables
        !---------
        type(atom_type), intent(in) :: atom,final_site
        type(atom_type_ext),dimension(:), intent(in) :: atoms

        integer                     :: atomTN               !Number of NNN for atom
        !-----------
        !Main Method
        !-----------
        able = .true.
        atomTN = sum(count_TN(atom, atoms))

        if (atomTN < reactions%mig_crit_nuc) then
            return
        else !Atom is next to enough atoms to prevent migration
            !Count final_sites NNN
            if (sum(count_TN(final_site, atoms)) > atomTN) then
                !Final site has the same or more NNN  than initial site - can go ahead
                return
            end if
        end if

        !Atom has too many NNN and final site doesn't increase this number - cannot migrate
        able = .false.
        return

    end function check_crit_nuc

    !Assess migration possibility using basic prerequisits of:
    !- Not part of a dimer
    !- No more than 2 C-C bonds
    !todo implement passed_basic_migration_reqs() into other migration rate setting subroutines
    logical function passed_basic_migration_reqs(part_of_dimer, neighbours) result(passed)
        logical, intent(in) :: part_of_dimer
        integer, intent(in) :: neighbours !nearest neighbours

        if (part_of_dimer .or. neighbours > 2) then
            passed = .false.
        else
            passed = .true.
        end if
    end function passed_basic_migration_reqs

    !======================
    !VISUALISATION OF RATES
    !======================
    !-----------------------------------------------------------------------------------------
    !Construct atom lists for atoms relevant to dimer migration and call output_xyz(atom_list)
    !-----------------------------------------------------------------------------------------
    subroutine visualise_dimer_migration(atom, atoms)
        !-------
        !Modules
        !-------
        use types,  only : atom_type,atom_type_ext
        use variables,  only : read_dimer,read_ipair, lattice
        use enumerate, only : types_of_atom,file_units
        use periodic, only : get_site_from_N_path,ret_neighbour
        use output, only : output_xyz
        !---------
        !Variables
        !---------
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in), dimension(:) :: atoms
        type(atom_type_ext), dimension(13) :: atom_list
        type(atom_type), dimension(4) :: final_sites       !4 final site positions
        type(atom_type), dimension(4,4) :: act_sites       !Atoms required to be activated (2 per site)
        type(atom_type), dimension(4) :: blocking_sites    !4 final site positions
        character(25) :: comment = 'dimer migration'
        integer :: iSite, ia_list
        !-----------
        !Main Method
        !-----------
        write(*,'(a,4(i4))') "VISUALISING DIMER MIGRATION: ",atom
        ia_list = 1
        call get_dimer_mig_sites(atom, lattice, final_sites, act_sites, blocking_sites)
        !Atom
        atom_list(ia_list) = atom_type_ext(atom, &
                types_of_atom%surf_carb, 2, &
                .false.,0,.true., &
                .false., 0, &
                0, 0)
        ia_list = ia_list + 1

        do iSite=1, size(final_sites)
            !Final Site
            atom_list(ia_list) = atom_type_ext(final_sites(iSite), &
                    types_of_atom%surf_carb, 1, &
                    read_dimer(final_sites(iSite), atoms), &
                    read_ipair(final_sites(iSite), atoms), .true., &
                    .false., 0, &
                    0, 0)
            ia_list = ia_list + 1
            !Activation Site 1
            atom_list(ia_list) = atom_type_ext(act_sites(iSite,1), &
                    types_of_atom%surf_carb, 1, &
                    read_dimer(act_sites(iSite,1), atoms), &
                    read_ipair(act_sites(iSite,1), atoms), &
                    .true., &
                    .false., 0, &
                    0, 0)
            ia_list = ia_list + 1
            !Activation Site 2
            atom_list(ia_list) = atom_type_ext(act_sites(iSite,2), &
                    types_of_atom%surf_carb, 1, &
                    read_dimer(act_sites(iSite,2), atoms), &
                    read_ipair(act_sites(iSite,2), atoms), .true., &
                    .false., 0, &
                    0, 0)
            ia_list = ia_list + 1
        end do

        call output_xyz(file_units%vis_rates, atom_list, comment)

    end subroutine visualise_dimer_migration

    subroutine visualise_gap_migration(atom, atoms)
        !Modules
        use types, only : atom_type,atom_type_ext
        use variables, only : read_atom, read_dimer, read_ipair
        use enumerate, only : types_of_atom, file_units
        use output, only : output_xyz
        !Variables
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in), dimension(:) :: atoms
        type(atom_type_ext), dimension(7) :: atom_list
        type(atom_type), dimension(2) :: final_sites       !4 final site positions
        type(atom_type), dimension(2,2) :: act_sites       !Atoms required to be activated (2 per site)
        character(25) :: comment = 'gap migration'
        integer :: iSite, ia_list
        !Main Method
        write(*,'(a,4(i4))') "VISUALISING GAP MIGRATION: ",atom
        ia_list = 1
        call get_gap_mig_sites(atom, final_sites, act_sites)

        !Atom
        atom_list(ia_list) = atom_type_ext(atom, &
                types_of_atom%surf_carb, 2, &
                .false.,0,.true., &
                .false., 0, &
                0, 0)
        ia_list = ia_list + 1

        do iSite=1, size(final_sites)
            !Final Site
            atom_list(ia_list) = atom_type_ext(final_sites(iSite), &
                    types_of_atom%surf_carb, 2, &
                    read_dimer(final_sites(iSite), atoms), &
                    read_ipair(final_sites(iSite), atoms),.true., &
                    .false., 0, &
                    0, 0)
            ia_list = ia_list + 1
            !Activation Site 1
            atom_list(ia_list) = atom_type_ext(act_sites(iSite,1), &
                    types_of_atom%surf_carb, 1, &
                    read_dimer(act_sites(iSite,1), atoms), &
                    read_ipair(act_sites(iSite,1), atoms), .true., &
                    .false., 0, &
                    0, 0)
            ia_list = ia_list + 1
            !Activation Site 2
            atom_list(ia_list) = atom_type_ext(act_sites(iSite,2), &
                    types_of_atom%surf_carb, 1, &
                    read_dimer(act_sites(iSite,2), atoms), &
                    read_ipair(act_sites(iSite,2), atoms), .true., &
                    .false., 0, &
                    0, 0)
            ia_list = ia_list + 1
        end do

        call output_xyz(file_units%vis_rates, atom_list, comment)

    end subroutine visualise_gap_migration

    subroutine visualise_migration_down(atom, atoms)
        !-------
        !Modules
        !-------
        use types, only : atom_type_ext,atom_type
        use variables, only : read_atom, read_dimer, read_ipair
        use enumerate, only : types_of_atom, file_units
        use output, only : output_xyz
        !---------
        !Variables
        !---------
        type(atom_type), intent(in) :: atom
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        type(atom_type_ext), dimension(9) :: atom_list
        type(atom_type), dimension(2) :: f_sites        !Final sites
        type(atom_type), dimension(2) :: b_sites        !Blocking sites
        type(atom_type), dimension(2,3) :: a_sites      !Activated sites (required)
        type(atom_type), dimension(2,2) :: s_atoms      !Supporting atoms (required)
        character(25) :: comment = 'migration down (gap)'
        integer :: iSite, ia_list
        !Main Method
        write(*,'(a,4(i4))') "VISUALISING DOWN MIGRATION: ",atom
        ia_list = 1
        call get_mig_down_sites(atom, atoms, f_sites, a_sites, b_sites, s_atoms)

        !Atom
        atom_list(ia_list) = atom_type_ext(atom, &
                types_of_atom%surf_carb, 2, &
                .false.,0, &
                .true., &
                .false., 0, &
                0, 0)
        ia_list = ia_list + 1

        do iSite=1, size(f_sites)
            !Final Site
            atom_list(ia_list) = atom_type_ext(f_sites(iSite), &
                    types_of_atom%surf_carb, 1, &
                    read_dimer(f_sites(iSite), atoms), &
                    0,.true., &
                    .false., 0, &
                    0, 0)
            ia_list = ia_list + 1
            !Activation Site 1
            atom_list(ia_list) = atom_type_ext(a_sites(iSite,1), &
                    types_of_atom%surf_carb, 1, &
                    read_dimer(a_sites(iSite,1), atoms), &
                    read_ipair(a_sites(iSite,1), atoms), .true., &
                    .false., 0, &
                    0, 0)
            ia_list = ia_list + 1
            !Activation Site 2
            atom_list(ia_list) = atom_type_ext(a_sites(iSite,2), &
                    types_of_atom%surf_carb, 1, &
                    read_dimer(a_sites(iSite,2), atoms), &
                    read_ipair(a_sites(iSite,2), atoms), .true., &
                    .false., 0, &
                    0, 0)
            ia_list = ia_list + 1
            !Activation Site 3
            atom_list(ia_list) = atom_type_ext(a_sites(iSite,3), &
                    types_of_atom%surf_carb, 1, &
                    read_dimer(a_sites(iSite,3), atoms), &
                    read_ipair(a_sites(iSite,3), atoms), .true., &
                    .false., 0, &
                    0, 0)
            ia_list = ia_list + 1
        end do

        call output_xyz(file_units%vis_rates, atom_list, comment)

    end subroutine visualise_migration_down

    subroutine incr_atom_mig_counter(i_site, f_site, atoms)
        !Modules
        use types, only : atom_type_ext,atom_type
        use variables, only : ilattice
        !Arguments
        type(atom_type),intent(in) :: i_site, f_site
        type(atom_type_ext),dimension(:),intent(inout) :: atoms
        !Variables
        integer :: ilat1,ilat2
        !Method
        ilat1 = ilattice(i_site)
        ilat2 = ilattice(f_site)

        !Copy initial site migrations to new site and increment by 1
        atoms(ilat2)%mig = atoms(ilat1)%mig + 1
        !Reset initial site migrations
        atoms(ilat1)%mig = 0

    end subroutine incr_atom_mig_counter

end module migration
