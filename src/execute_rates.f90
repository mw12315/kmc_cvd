module execute_rates

    use kinds,                 only : dp

    implicit none

contains

    !----------------------------------------------------------------------------------------------------------------
    !Determine the correct rate execution subroutine to be called from an input rate type, atom and final position
    !Incrementation of counters, activation/deactivation of atoms and formation/destruction of dimers is also handled
    !----------------------------------------------------------------------------------------------------------------
    subroutine execute_event(rate_to_execute, atoms, kmc_info, write_to_events_file)
        !Modules
        use types, only : atom_type_ext,atom_type,kmc_type,same_atom,rate_type
        use input, only : outputs
        use surface_m, only : add_surface_atom,remove_surface_atom
        use variables, only : read_atom,reached_max_height, reached_min_height, simulation
        use enumerate, only : tRates,file_units,types_of_atom
        use periodic, only : count_nearest_atoms,count_TN
        !Rate modules
        use etching, only : execute_etch, execute_ide_etch
        use adsorb_atom, only : execute_chx_insertion, execute_C2H2_adsorption, biradical_site
        use ide, only : execute_c2h2p_dimer_reformation
        !Arguments
        type(rate_type), intent(in) :: rate_to_execute
        type(atom_type_ext), dimension(:), intent(inout) :: atoms
        type(kmc_type),intent(inout) :: kmc_info
        logical, intent(in) :: write_to_events_file
        !Variables
        type(atom_type) :: atom
        type(atom_type) :: site2
        integer :: event
        integer :: base_rate                !Base (first) rate of an input rate
        integer :: NN,TN
        logical :: is_migration,success     !Event is migration, migration successful
        character(25) :: event_output = '(i12,a1,ES17.10,9(a1,i5))'
        logical :: biradical                !Is the event occurring within a biradical site environment
        is_migration = .false.
        biradical = .false.
        success = .true.
        atom = rate_to_execute%site
        site2 = rate_to_execute%site2
        event = rate_to_execute%event

        if (atom%k >= simulation%max_height) reached_max_height = .true.
        if (atom%k <= 1) reached_min_height = .true.
        !===================================================================================================
        ! Rate Types are indentifiable by their type (enumerate.mod)
        ! |  1-4  | Methyl Dimer Insertion
        ! |  5-8  | Methyl Trough Insertion
        ! |  9-12 | C2H2 adsorption M1-3
        ! |  13   | Migration along dimer row
        ! |  14   | Migration along dimer chain
        ! | 15-18 | Migration into Gap (A-D)
        ! | 19-20 | C2H2 pendant migration into gap
        ! |  21   | Migration Down a Single Step
        ! |  22   | Miration of C2H2 down into a biradical site
        ! |  23   | Miration of C2H2
        ! |  24   | CHx etch
        ! |  25   | C2H2 etch of CH3
        ! |  26   | C2H2 etch
        ! |  27   | Isolated Dimer etch 12_1 (formation of C2H2 pendant) (etch recursor)
        ! |  28   | Isolated Dimer etch 12_2 (CHx loss)
        ! |  29   | Isolated Dimer etch 12_3 (C2H2 loss)
        ! |  30   | Isolated Dimer etch 12_4 (C2H2 loss)
        ! |  31   | Isolated Dimer etch 12_5 (C2H2 loss)
        ! |  32   | Etch CHx (slow, netto2005)
        ! |  33   | C2H2 pendant to dimer
        !===================================================================================================

        !Output Event to File
        if (write_to_events_file) then
            if (outputs%L%event_csv) then
                write(file_units%event_out,event_output) kmc_info%total_events,",",kmc_info%time,",",event,",", &
                & atom%i,",",atom%j,",",atom%basis,",",atom%k,",", &
                & site2%i,",",site2%j,",",site2%basis,",",site2%k
            end if
        end if
        !Detect an atom error
        if (atom%i == 0 .or. atom%j == 0 .or. atom%basis == 0 .or. atom%k == 0) write(file_units%error,*) kmc_info%step,"execute_event: atom input error:",atom
        base_rate = base_reaction(event)

        !Rate Execution
        !=============
        !INCORPORATION
        !=============
        !-------------
        !CHX Insertion
        !-------------
        if (base_rate == tRates%TinsertCH3 .or. base_rate == tRates%DinsertCH3) then
            if (read_atom(atom, atoms) /= types_of_atom%empty_site) write(file_units%error,*) kmc_info%step,"CHX Insertion onto filled site",atom
            !Check for biradical site presence
            if (base_rate == tRates%TinsertCH3) biradical = biradical_site(atom, atoms)
            call execute_chx_insertion(rate_to_execute, kmc_info, atoms)
        !===============
        !C2H2 Adsorption
        !===============
        else if (base_rate == tRates%C2H2_M1a) then
            call execute_C2H2_adsorption(rate_to_execute, kmc_info, atoms)
        !============
        !ETCHING
        !============
        !PREFERENTIAL
        !------------
        else if (base_rate == tRates%pref_etch_CHx) then
            if (read_atom(atom, atoms) == types_of_atom%empty_site) then
                write(file_units%error,*) kmc_info%step,"Etch empty atom site",atom
                return
            endif

            call count_nearest_atoms(atom, atoms,  NN)
            TN = sum(count_TN(atom, atoms))

            call execute_etch(rate_to_execute, kmc_info, atoms)
        !------------------
        !Non-preferential
        !Netto2005 CHx etch
        !------------------
        else if (base_rate == tRates%etch_chx) then
            if (read_atom(atom, atoms) == types_of_atom%empty_site) then
                write(file_units%error,*) kmc_info%step,"Etch empty atom site",atom
                return
            endif

            call count_nearest_atoms(atom, atoms,  NN)
            TN = sum(count_TN(atom, atoms))

            call execute_etch(rate_to_execute, kmc_info, atoms)
        !--------------
        !ISOLATED DIMER
        !--------------
        else if (base_rate == tRates%ide_12_1) then
            if (read_atom(atom, atoms) == types_of_atom%empty_site .or. &
                    read_atom(site2, atoms) == types_of_atom%empty_site) then
                write(file_units%error,*) kmc_info%step,"Etch empty atom site", atom, site2, "event:", event
                return
            endif
        call execute_ide_etch(rate_to_execute, kmc_info, atoms)
        !----------------------------------
        !C2H2 pendant -> non-isolated dimer
        !----------------------------------
        else if (base_rate == tRates%c2H2p_dimer) then
            call execute_c2h2p_dimer_reformation(rate_to_execute, kmc_info, atoms)
        !---------
        !MIGRATION
        !---------
        elseif (event >= tRates%dimerMigrate_r .and. event <= tRates%migrateC2H2) then
                call call_migration_execution(rate_to_execute, kmc_info, atoms, success)
        else
            write(file_units%error,('(i12,a,a,i2,a1,4(i5),a1)')) kmc_info%step, "execute event", &
                & "rate type out of range:",event,"|",atom,"|"
            STOP 1
        end if


        call increment_rate_counter(event,TN,biradical)

    end subroutine execute_event

    !todo combine this with execute_migration, no reason to have this interface layer
    subroutine call_migration_execution(rate_to_execute, kmc_info, atoms, success)
        !Modules
        use types, only : rate_type, kmc_type, atom_type, atom_type_ext
        use variables, only : read_atom
        use migration, only : execute_migration
        use enumerate, only : file_units, types_of_atom
        !Arguments
        type(rate_type), intent(in) :: rate_to_execute
        type(kmc_type), intent(inout) :: kmc_info
        type(atom_type_ext), dimension(:), intent(inout) :: atoms
        logical, intent(out) :: success
        !Main Method
        !Error Checking before execution
        if (read_atom(rate_to_execute%site, atoms) == types_of_atom%empty_site) then
            write(file_units%error,*) "Migrating atom doesn't exist", rate_to_execute%site
            success = .false.
            return
        end if
        if (read_atom(rate_to_execute%site2, atoms) /= types_of_atom%empty_site) then
            write(file_units%error,*) "Migration site filled", rate_to_execute%site2
            success = .false.
            return
        end if

        !Call generic migration execution
        call execute_migration(rate_to_execute, kmc_info, atoms)
        success = .true.

    end subroutine call_migration_execution

    !Increment rate counter according to the event input
    !In the case of an etch the TN becomes important
    !otherwise 0
    subroutine increment_rate_counter(event,TN,biradical)
        !Modules
        use enumerate,      only : tRates,irate_trough_ins
        use variables,      only : counters
        use periodic,       only : count_nearest_atoms
        use input,          only : reactions
        !Variables
        integer, intent(in) :: event, TN
        logical, intent(in) :: biradical

        !Main Method
        counters%events(event,1) = counters%events(event,1) + 1

        !Biradical trough insertion
        if (ANY(irate_trough_ins == event .and.biradical)) then
            counters%events(event,2) = counters%events(event,2) + 1
        end if

        !Etching
        if (event >= tRates%pref_etch_CHx .and. event <= tRates%etchCH3_C2H2) then
            if (TN <= reactions%isolated_TN_threshold) then
                counters%events(event,2) = counters%events(event,2) + 1
            end if
        end if

    end subroutine increment_rate_counter

    !Get the base rate (T insertion, gap migration.. etc)
    !from an input rate
    function base_reaction(irate) result(base_trate)
        !Modules
        use enumerate, only : tRates
        !Arguments
        integer, intent(in) :: irate
        integer :: base_trate
        !Method
        if (irate >= tRates%DinsertCH3 .and. irate <= tRates%DinsertC) then
            base_trate = tRates%DinsertCH3
        else if (irate >= tRates%TinsertCH3 .and. irate <= tRates%TinsertC) then
            base_trate = tRates%TinsertCH3
        else if (irate >= tRates%C2H2_M1a .and. irate <= tRates%C2H2_M3) then
            base_trate = tRates%C2H2_M1a
        else if (irate >= tRates%dimerMigrate_r .and. irate <= tRates%dimerMigrate_c) then
            base_trate = tRates%dimerMigrate_r
        else if (irate >= tRates%gapMigrateA .and. irate <= tRates%gapMigrateD) then
            base_trate = tRates%gapMigrateA
        else if (irate == tRates%migDown) then
            base_trate = tRates%migDown
        else if (irate >= tRates%migC2H2_birad .and. irate <= tRates%migrateC2H2) then
            base_trate = tRates%migC2H2_birad
        else if (irate >= tRates%pref_etch_CHx .and. irate <= tRates%etchCH3_C2H2) then
            base_trate = tRates%pref_etch_CHx
        else if (irate >= tRates%ide_12_1 .and. irate <= tRates%ide_12_5) then
            base_trate = tRates%ide_12_1
        else if (irate >= tRates%gapMigrateC2H2_0 .and. irate <= tRates%gapMigrateC2H2_1) then
            base_trate = tRates%gapMigrateC2H2_0
        else if (irate >= tRates%c2H2p_dimer .and. irate <= tRates%c2H2p_dimer) then
            base_trate = tRates%c2H2p_dimer
        else if (irate == tRates%etch_chx) then
            base_trate = tRates%etch_chx
        else
            write(6,'(a,i2,a)') "Base reaction could not be applied to ", irate, " as it is out of range."
            STOP
        end if

    end function base_reaction

end module execute_rates
