module periodic

    use enumerate,  only : file_units
    use types,      only : atom_type,neighbours_type


    implicit none

    type(neighbours_type), dimension(8)  :: neighbours

    integer, parameter :: migdir_N = 1
    integer, parameter :: migdir_E = 2
    integer, parameter :: migdir_S = 3
    integer, parameter :: migdir_W = 4

contains

    !Initialise the neighbours look up array
    subroutine init_neighbour_type()

        neighbours(1)%dir(1:4)      = (/ 0, 5, 7, 6 /)
        neighbours(1)%basis(1:4)    = (/ 3, 8, 7, 4 /)
        neighbours(1)%rel_k(1:4)    = (/ 0,-1,-1, 0 /)

        neighbours(2)%dir(1:4)      = (/ 0, 0, 0, 0 /)
        neighbours(2)%basis(1:4)    = (/ 4, 7, 8, 3 /)
        neighbours(2)%rel_k(1:4)    = (/ 0,-1,-1, 0 /)

        neighbours(3)%dir(1:4)      = (/ 0, 0, 0, 0 /)
        neighbours(3)%basis(1:4)    = (/ 5, 2, 1, 6 /)
        neighbours(3)%rel_k(1:4)    = (/ 0, 0, 0, 0 /)

        neighbours(4)%dir(1:4)      = (/ 3, 2, 0, 1 /)
        neighbours(4)%basis(1:4)    = (/ 6, 1, 2, 5 /)
        neighbours(4)%rel_k(1:4)    = (/ 0, 0, 0, 0 /)

        neighbours(5)%dir(1:4)      = (/ 0, 5, 0, 5 /)
        neighbours(5)%basis(1:4)    = (/ 7, 4, 3, 8 /)
        neighbours(5)%rel_k(1:4)    = (/ 0, 0, 0, 0 /)

        neighbours(6)%dir(1:4)      = (/ 0, 0, 7, 7 /)
        neighbours(6)%basis(1:4)    = (/ 8, 3, 4, 7 /)
        neighbours(6)%rel_k(1:4)    = (/ 0, 0, 0, 0 /)

        neighbours(7)%dir(1:4)      = (/ 3, 3, 0, 0 /)
        neighbours(7)%basis(1:4)    = (/ 1, 6, 5, 2 /)
        neighbours(7)%rel_k(1:4)    = (/ 1, 0, 0, 1 /)

        neighbours(8)%dir(1:4)      = (/ 0, 1, 0, 1 /)
        neighbours(8)%basis(1:4)    = (/ 2, 5, 6, 1 /)
        neighbours(8)%rel_k(1:4)    = (/ 1, 0, 0, 1 /)

    end subroutine init_neighbour_type

    !------------------------------------------------------
    !Return the requested neighbour of an input atom
    !If the passed direction is 0, return the input atom
    !This allows N path be passed arrays of matched lengths
    !with padding 0s having no effect on the result
    !------------------------------------------------------
    type(atom_type) pure function ret_neighbour(atom,num) result(neighbour)
        !Modules
        use types,      only : atom_type
        !Variables
        type(atom_type), intent(in) :: atom
        integer,         intent(in) :: num      !Choice of neighbour(1-4)
        !Variables
        integer                     :: b
        integer :: ni,nj
        !Main Method
        b = atom%basis
        if (atom%i == 0 .and. atom%j == 0 .and. atom%basis == 0 .and. atom%k == 0) then
            neighbour = atom_type(0,0,0,0)
            return
        end if

        if (num == 0) then
            neighbour = atom
            return
        end if

        if (num > 4) then
            ERROR STOP
        end if

        call ij_cell_from_dir(atom%i,atom%j, neighbours(b)%dir(num), &
                                  ni, nj)
        neighbour = atom_type(ni, nj, neighbours(b)%basis(num), atom%k + neighbours(b)%rel_k(num))

    end function ret_neighbour

    !----------------------------------------------
    !Return type(atom) for all 4 nearest neighbours
    !Neighbours 1-4 are labelled in a clockwise
    !direction starting a 1pm
    !----------------------------------------------
    subroutine get_neighbour_atoms(site_in,   neigh1,neigh2,neigh3,neigh4)
        !Modules
        use types, only : atom_type
        !Argumenrts
        type(atom_type), intent(in)  :: site_in
        type(atom_type), intent(out) :: neigh1, neigh2, neigh3, neigh4
        !Variables
        integer :: ni,nj
        integer :: N1_dir,N2_dir,N3_dir,N4_dir                !Stores direction to look for nearest neighbour  N4       N1
        integer :: N1_bas,N2_bas,N3_bas,N4_bas                !Stores basis of nearest neighbour                    C
        integer :: N1_k,N2_k,N3_k,N4_k                        !Stops relative height of neighbour              N3       N2

        N1_k = 0; N2_k = 0; N3_k = 0; N4_k = 0;
        !Main Method

        !Set neighbour direction,basis and relative k index
        if (site_in%basis == 1) then
            N1_dir=0;N2_dir=5;N3_dir=7;N4_dir=6
            N1_bas=3;N2_bas=8;N3_bas=7;N4_bas=4
            N1_k=0;N2_k=-1;N3_k=-1;N4_k=0
        else if (site_in%basis == 2) then
            N1_dir=0;N2_dir=0;N3_dir=0;N4_dir=0
            N1_bas=4;N2_bas=7;N3_bas=8;N4_bas=3
            N1_k=0;N2_k=-1;N3_k=-1;N4_k=0
        else if (site_in%basis == 3) then
            N1_dir=0;N2_dir=0;N3_dir=0;N4_dir=0
            N1_bas=5;N2_bas=2;N3_bas=1;N4_bas=6
            N1_k=0;N2_k=0;N3_k=0;N4_k=0
        else if (site_in%basis == 4) then
            N1_dir=3;N2_dir=2;N3_dir=0;N4_dir=1
            N1_bas=6;N2_bas=1;N3_bas=2;N4_bas=5
            N1_k=0;N2_k=0;N3_k=0;N4_k=0
        else if (site_in%basis == 5) then
            N1_dir=0;N2_dir=5;N3_dir=0;N4_dir=5
            N1_bas=7;N2_bas=4;N3_bas=3;N4_bas=8
            N1_k=0;N2_k=0;N3_k=0;N4_k=0
        else if (site_in%basis == 6) then
            N1_dir=0;N2_dir=0;N3_dir=7;N4_dir=7
            N1_bas=8;N2_bas=3;N3_bas=4;N4_bas=7
            N1_k=0;N2_k=0;N3_k=0;N4_k=0
        else if (site_in%basis == 7) then
            N1_dir=3;N2_dir=3;N3_dir=0;N4_dir=0
            N1_bas=1;N2_bas=6;N3_bas=5;N4_bas=2
            N1_k=1;N2_k=0;N3_k=0;N4_k=1
        else if (site_in%basis == 8) then
            N1_dir=0;N2_dir=1;N3_dir=0;N4_dir=1
            N1_bas=2;N2_bas=5;N3_bas=6;N4_bas=1
            N1_k=1;N2_k=0;N3_k=0;N4_k=1
        else
            ERROR STOP
        end if

        !Assign to atom types
        !NEIGHBOUR1
        call get_periodic_neighbour(site_in%i,site_in%j,N1_dir, ni,nj)
        neigh1 = atom_type(ni,nj, N1_bas,site_in%k+N1_k)
        !NEIGHBOUR2
        call get_periodic_neighbour(site_in%i,site_in%j,N2_dir, ni,nj)
        neigh2 = atom_type(ni,nj, N2_bas,site_in%k+N2_k)
        !NEIGHBOUR3
        call get_periodic_neighbour(site_in%i,site_in%j,N3_dir, ni,nj)
        neigh3 = atom_type(ni,nj, N3_bas,site_in%k+N3_k)
        !NEIGHBOUR4
        call get_periodic_neighbour(site_in%i,site_in%j,N4_dir, ni,nj)
        neigh4 = atom_type(ni,nj, N4_bas,site_in%k+N4_k)

    end subroutine get_neighbour_atoms

    pure subroutine get_periodic_neighbour(i,j,direction,  ni,nj)
        !Modules
        use variables, only : simulation
        !Variables
        integer, intent(in) :: i,j,direction
        integer, intent(out) :: ni,nj
        integer :: i_in,j_in ! i,j input values
        !-----------
        !Main Method
        !-----------
        i_in = i-1
        j_in = j-1

        if (direction == 0) then
            ! Do nothing, base settings are in effect
        else if (direction == 1) then
            j_in = j
        else if (direction == 2) then
            i_in = i
            j_in = j
        else if (direction == 3) then
            i_in = i
        else if (direction == 4) then
            i_in = i
            j_in = j-2
        else if (direction == 5) then
            j_in = j-2
        else if (direction == 6) then
            i_in = i-2
            j_in = j-2
        else if (direction == 7) then
            i_in = i-2
        else if (direction == 8) then
            i_in = i-2
            j_in = j
        else
            ERROR STOP
        end if

        ni = mod(abs(simulation%Nx+i_in),simulation%Nx) + 1
        nj = mod(abs(simulation%Ny+j_in),simulation%Ny) + 1

    end subroutine get_periodic_neighbour

    !---------------
    !Count number of nearest neighbours of an atom (atom_type)
    !---------------
    subroutine count_nearest_atoms(atom, atoms, NN)
        !---------------
        !Module use statements
        !---------------
        use types,      only : atom_type_ext, atom_type
        use variables,  only : read_atom, are_linked
        use enumerate,  only : types_of_atom
        !---------------------
        !Variable declarations
        !---------------------
        type(atom_type), intent(in) :: atom
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        integer, intent(out) :: NN !Number of nearest neighbours
        type(atom_type) :: neighbour
        integer :: iN

        !---------------
        !Main Method
        !---------------
        NN = 0
        do iN=1,4
            neighbour = ret_neighbour(atom, iN)
            if (atom%k > 1) then
                if (read_atom(neighbour, atoms) == types_of_atom%surf_carb) then
                    NN = NN + 1
                else if (read_atom(neighbour, atoms) == types_of_atom%acetyl_c2) then
                    if (are_linked(atom, neighbour, atoms)) then
                        NN = NN + 1
                    end if
                end if
            else
                !Atoms at the bottom of the lattice are assumed to have 2 atoms below
                if (iN == 2 .or. iN == 3) then
                    NN = NN + 1
                end if
            end if
        end do

    end subroutine count_nearest_atoms

    !-----------------------------------------------------------
    !Count number of bonded neighbours with an atom
    !Includes C-C dimer bonding (not NN based)
    !An acetyl_c2 atom is counted via linkage not NN positioning
    !-----------------------------------------------------------
    integer function num_bonds(atom, atoms) result(NN)
        !------
        !Module
        !------
        use types,      only : atom_type_ext,atom_type
        use variables,  only : read_atom,read_dimer,read_ilink,atom_from_index
        use enumerate,  only : types_of_atom
        !---------
        !Variables
        !---------
        type(atom_type), intent(in) :: atom
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        type(atom_type) :: N1,N2,N3,N4
        type(atom_type), dimension(4) :: neighbours
        type(atom_type) :: paired_atom
        integer :: iN,iLink
        !-----------
        !Main Method
        !-----------
        iLink = read_ilink(atom, atoms)
        NN = 0
        if (read_atom(atom, atoms) == types_of_atom%acetyl_c2) then
            NN = 1 !Might be 2 depending on whether C=C and C-C are to be non-ambiguous
        else
            call get_neighbour_atoms(atom, N1,N2,N3,N4)
            neighbours = (/ N1, N2, N3, N4 /)
            !Increment NN for each none empty neighbour
            do iN=1,size(neighbours)
                if (neighbours(iN)%k > 1) then
                    if (read_atom(neighbours(iN), atoms) /= types_of_atom%empty_site) then
                        if (read_atom(neighbours(iN), atoms) /= types_of_atom%acetyl_c2) then
                            NN = NN + 1
                        end if
                    end if
                else
                    !Atoms at bottom of lattice are assumed to have 2 atoms below
                    if (iN == 2 .or. iN == 3) NN = NN + 1
                end if
            end do

        end if

        !Dimer C-C bonding (not conventional bonding neighbours)
        if (read_dimer(atom, atoms)) NN = NN + 1
        !C-C acetyl bonding
        if (iLink /= 0) then
            paired_atom = atom_from_index(iLink, atoms)
            if (read_atom(paired_atom, atoms) == types_of_atom%acetyl_c2) then
                NN = NN + 1
            end if
        end if

    end function num_bonds
    !---------------------------------------------------------------------------------
    !Return atom with ibasis and k with i and j corresponding to direction of movement
    !---------------------------------------------------------------------------------
    pure subroutine ij_cell_from_dir(i, j, direction, ni, nj)
        !Modules
        use variables,  only : simulation
        !Arguments
        integer, intent(in)  :: i, j, direction
        integer, intent(out) :: ni, nj
        !Variables
        integer :: i_in, j_in

        !Method

        select case(direction)
        case(0)
            i_in = i-1
            j_in = j-1
        case(1)
            i_in = i-1
            j_in = j
        case(2)
            i_in = i
            j_in = j
        case(3)
            i_in = i
            j_in = j-1
        case(4)
            i_in = i
            j_in = j-2
        case(5)
            i_in = i-1
            j_in = j-2
        case(6)
            i_in = i-2
            j_in = j-2
        case(7)
            i_in = i-2
            j_in = j-1
        case(8)
            i_in = i-2
            j_in = j
        case default
            ERROR STOP
        end select

        ni = mod(abs(simulation%Nx+i_in),simulation%Nx) + 1
        nj = mod(abs(simulation%Ny+j_in),simulation%Ny) + 1

    end subroutine ij_cell_from_dir

    !-------------------------------------------------------
    ! Returning a atom's next nearest neighbour (TN)
    ! from input of an atom and compass direction (NESW,1-4)
    !-------------------------------------------------------
    type(atom_type) function ret_TN(atom,compass) result(TN)
        !-------
        !Modules
        !-------
        use types,      only : atom_type
        !---------
        !Variables
        !---------
        type(atom_type), intent(in) :: atom
        integer,         intent(in) :: compass      !1-4
        integer, dimension(2)       :: NNfind       !Contains the order of nearest neighbours to search
        !-----------
        !Main Method
        !-----------
        !Set order of nearest neighbours to find
        if (atom%basis == 1 .or. atom%basis == 2 .or. atom%basis == 5 .or. atom%basis == 6) then
            select case(compass)
                case(migdir_N)
                    NNfind(1:2) = (/ 3,4 /)
                case(migdir_S)
                    NNfind(1:2) = (/ 2,1 /)
                case(migdir_E)
                    NNfind(1:2) = (/ 1,2 /)
                case(migdir_W)
                    NNfind(1:2) = (/ 4,3 /)
            end select
        else
            select case(compass)
            case(migdir_N)
                NNfind(1:2) = (/ 4,3 /)
            case(migdir_S)
                NNfind(1:2) = (/ 1,2 /)
            case(migdir_E)
                NNfind(1:2) = (/ 2,1 /)
            case(migdir_W)
                NNfind(1:2) = (/ 3,4 /)
            end select
        end if

        !Find requested Next Nearest Neighbour (TN)
        TN = ret_neighbour(ret_neighbour(atom,NNfind(1)),NNfind(2))
        return

    end function ret_TN
    !-----------------------------------------------------------------------------------------------------
    !Returns the surrounding next nearest neighbours of an atom in the form of a 4 element array (N,E,S,W)
    !-----------------------------------------------------------------------------------------------------
    function count_TN(atom, atoms) result(TN_all)
        !-------
        !Modules
        !-------
        use types,      only : atom_type_ext, atom_type
        use variables,  only : read_atom
        use enumerate,  only : types_of_atom
        !---------
        !Variables
        !---------
        type(atom_type), intent(in)         :: atom
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        integer, dimension(4)               :: TN_all
        integer                             :: direction

        TN_all = 0
        !-----------
        !Main Method
        !-----------
        do direction=migdir_N,migdir_W
            if (read_atom(ret_TN(atom,direction), atoms) /= types_of_atom%empty_site) TN_all(direction) = 1
        end do

    end function count_TN

    !----------------------------------------------------------------------
    !Follow a chain of nearest neighbours (array) and return the final site
    !----------------------------------------------------------------------
    type(atom_type) pure function get_site_from_N_path(i_site, path) result(f_site)
        !Arguments
        type(atom_type), intent(in) :: i_site   !Initial Site
        integer, dimension(:), intent(in) :: path
        !Variables
        integer :: iN   !Neighbour index

        f_site = i_site
        do iN=1,size(path)
            f_site = ret_neighbour(f_site,path(iN))
        end do

        return

    end function get_site_from_N_path

end module periodic
