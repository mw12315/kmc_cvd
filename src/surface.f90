module surface_m

    use kinds,      only : dp

    implicit none

    !Link layer type membership for each basis
    integer,dimension(8),parameter :: tlayer_from_basis = (/ 1,1, 2,2, 1,1, 2,2 /)


contains
    !----------------------------------------------------------------------------------------
    !Add an atom from the surface_list and check for additional atoms which should be removed
    !Atoms removed from the surface list are deactivated.
    !The added atom is then set
    !----------------------------------------------------------------------------------------
    subroutine add_surface_atom(atom, state, atoms)
        !Modules
        use types,      only : atom_type, atom_type_ext
        use variables,  only : surface, ilattice, surface, NN_bulk, set_atom, kmc_info, set_act_state, unsurface_atom, &
                undimer_atom, read_dimer
        use periodic,   only : get_neighbour_atoms, count_nearest_atoms
        use enumerate,  only : file_units
        !Variables
        type(atom_type), intent(in) :: atom
        integer, intent(in) :: state
        type(atom_type_ext),intent(inout) :: atoms(:)
        !Variables
        type(atom_type) :: Neighbours(4)
        integer :: NN
        integer :: index

        !Check that newly 'added' surface atom is actually a surface atom
        !Add the atom to atoms array
        call set_atom(atom, atoms, state)
        call count_nearest_atoms(atom, atoms,  NN)
        if (NN < NN_bulk) then
            surface%total_atoms = surface%total_atoms + 1
            atoms(ilattice(atom))%surface = .true.
        else
            write(file_units%error,*) kmc_info%step,"added atom to a non surface site:",atom,NN
            atoms(ilattice(atom))%surface = .false.
        end if
        
        !---------------------------
        !Update highest/lowest atoms
        !---------------------------
        !Update Highest Atom
        if (atom%k > surface%k_max%val) then
            surface%k_max%val = atom%k
            surface%k_max%loc_i = atom%i
            surface%k_max%loc_j = atom%j
        end if
        !----------------------------
        !Remove newly bulk neighbours
        !----------------------------
        call get_neighbour_atoms(atom,  Neighbours(1),Neighbours(2),Neighbours(3),Neighbours(4))
        !If atom now has NN = NN_bulk - remove from atom list (and deactivate) and decrement tot_surf_atoms

        do index=1,size(Neighbours)
            call count_nearest_atoms(Neighbours(index), atoms,  NN)
            if (NN == NN_bulk) then
                !Is it in the surface list?
                if (atoms(ilattice(Neighbours(index)))%surface) then
                    call unsurface_atom(Neighbours(index), atoms)
                    surface%total_atoms = surface%total_atoms - 1
                    if (read_dimer(atom, atoms)) then
                        write(file_units%warning,*) kmc_info%step, Neighbours(index), "part of dimer but not part of surface?"
!                        call undimer_atom(Neighbours(index), atoms)
                    end if
                end if
            end if
        end do

    end subroutine add_surface_atom


    !-----------------------------------------------------------------------------------------------
    !Remove an atom from the surface_list and check for additional atoms which are now surface atoms
    !-----------------------------------------------------------------------------------------------
    subroutine remove_surface_atom(atom, kmc_info, atoms, is_migration)
        !Modules
        use types,      only : atom_type,atom_type_ext,kmc_type,average_add
        use variables,  only : read_atom,read_dimer,set_atom,NN_bulk,surface,ilattice, &
                             & set_act_state,pop_mig
        use enumerate,  only : types_of_atom,not_activated
        use periodic,   only : get_neighbour_atoms,count_nearest_atoms
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(inout) :: atoms(:)
        type(kmc_type), intent(inout) :: kmc_info
        logical, optional, intent(in) :: is_migration
        !Variables
        type(atom_type) :: Neighbours(4)
        integer :: NN
        integer :: index
        integer :: ilat
        logical :: is_migration_

        if (present(is_migration)) then
            is_migration_ = is_migration
        else
            is_migration_ = .false.
        end if
        ilat = ilattice(atom)
        !If atom is already not on the surface and doesn't exist, do nothing
        if ((read_atom(atom, atoms) == types_of_atom%empty_site) .and. (atoms(ilat)%surface .eqv. .false.)) return
        !Collect migration count
        if (.not. is_migration_) then
            call average_add(kmc_info%migs, real(pop_mig(atom, atoms),dp))
        end if

        !Remove from the atomic lattice
        call set_atom(atom, atoms, types_of_atom%empty_site)

        atoms(ilat)%surface = .false.
        call count_nearest_atoms(atom, atoms,  NN)
        surface%total_atoms = surface%total_atoms - 1
        !---------------------------
        !Update highest/lowest atoms
        !---------------------------
        !Update Lowest Atom
        if (atom%k < surface%k_min%val) then
            surface%k_min%val = atom%k
            surface%k_min%loc_i = atom%i
            surface%k_min%loc_j = atom%j
        end if

        !--------------------------
        !Add new surface neighbours
        !--------------------------
        call get_neighbour_atoms(atom, Neighbours(1),Neighbours(2),Neighbours(3),Neighbours(4))
        !If atom now has NN < NN_bulk - add to atom list and increment tot_surf_atoms
        do index=1,size(Neighbours)
            call count_nearest_atoms(Neighbours(index), atoms,   NN)
            if (NN <  NN_bulk .and. NN /= 0) then
                if (read_atom(Neighbours(index), atoms) /= types_of_atom%empty_site) then
                    !Not already in surface list?
                    if (atoms(ilattice(Neighbours(index)))%surface .eqv. .false.) then
                        surface%total_atoms = surface%total_atoms + 1
                        !Set atoms surface status
                        atoms(ilattice(Neighbours(index)))%surface = .true.
                    end if
                end if
            end if
        end do

    end subroutine remove_surface_atom

    !-------------------------------------------------------------------
    ! Calculates
    ! average surface_height
    ! average growth rate
    ! atom environments (current)
    ! rms surface roughness
    ! average change in surface roughness (with respect to surface cell)
    !-------------------------------------------------------------------
    subroutine surface_stats(atoms, kmc_info, start_height, start_time, start_heights_seg, surface_info, segment_info, &
            opt_calc_instant_grate)
        !Modules
        use types,      only : atom_type,atom_type_ext,coord_type,surface_type,kmc_type, average_add
        use variables,  only : return_ilattice_limits, segment_bounds, simulation
        use enumerate,  only : types_of_atom,edge_state
        use basis,      only : position
        use segment,    only : iseg_from_cell
        use dimer,      only : alignment_of_dimer
        use input,      only : reactions
        !Arguments
        type(atom_type_ext),    intent(in)      :: atoms(:)
        real(dp),               intent(in)      :: start_height                         !Used to calculate growth height
        real(dp),               intent(in)      :: start_time                         !Used to calculate ave growth rate
        real(dp),               intent(in)      :: start_heights_seg(:)     !Calculate growth height on a per segment basis
        type(kmc_type),         intent(in)      :: kmc_info
        type(surface_type),     intent(inout)   :: surface_info
        type(surface_type),     intent(inout)   :: segment_info(:)
        logical, optional,      intent(in)      :: opt_calc_instant_grate
        !Variables
        logical :: calc_instant_grate
        type(atom_type) :: atom
        type(coord_type) :: coords
        integer :: index
        integer,dimension(2) :: ilattice_limits
        real(dp) :: height_sum
        !Instant Growth Rate
        real(dp) :: time_diff, height_diff           !Differences in time and height since last surface_stats call
        integer,dimension(edge_state%isolated:edge_state%terrace) :: atom_envs                        !Local copy of surface atom environments
        integer :: a_env                            !Environment of the atom
        integer :: total_sites
        !Layers
        integer, allocatable, dimension(:) :: a_layers  !Number of surface atoms in each layer
        integer :: ilayer
        !Dimer Alignment
        integer :: atoms_in_dimers      !Dimers on the surface
        real(dp) :: d_align_sum !Sum of dimer alignment on the surface
        real(dp) :: dimer_prop, overall_dimer_deviation !Surface dimer proportion, surface dimer alignment
        !Segments
        integer :: iseg !Segment of the current atom
        real(dp), dimension(size(segment_info)) :: z_sum_seg    !Segment height sum
        integer, dimension(size(segment_info),edge_state%isolated:edge_state%terrace) :: atom_envs_seg   !atom_envs_seg(iseg, env)
        integer, dimension(size(segment_info)) :: total_sites_seg
        real(dp), dimension(size(segment_info)) :: height_diff_seg
        total_sites = 0
        total_sites_seg(:) = 0
        height_sum = 0.0_dp
        z_sum_seg(:) = 0.0_dp
        atom_envs = 0
        atom_envs_seg(:,:) = 0
        atoms_in_dimers = 0
        d_align_sum = 0.0
        !Layers
        allocate(a_layers(size(surface_info%layers)+4))
        a_layers(:) = 0
        !Populate optional arguments
        if (present(opt_calc_instant_grate)) then
            calc_instant_grate = opt_calc_instant_grate
        else
            calc_instant_grate = .false.
        end if
        !-----------
        !Main Method
        !-----------
        call return_ilattice_limits(surface_info%k_min%val,surface_info%k_max%val,ilattice_limits)
        do index=ilattice_limits(1),ilattice_limits(2)
            if (atoms(index)%state /= types_of_atom%empty_site .and. atoms(index)%surface) then
                atom = atoms(index)%atom

                !Surface height
                total_sites = total_sites + 1
                call position(atom, atoms, coords)
                height_sum = height_sum + coords%z
                !Atom environments
                a_env = identify_edge_state(atom, atoms)
                atom_envs(a_env) = atom_envs(a_env) + 1

                !Dimers
                if (atoms(index)%dimer) then
                    if (index < atoms(index)%ipair) then
                        atoms_in_dimers = atoms_in_dimers + 1
                        d_align_sum = d_align_sum + alignment_of_dimer(atom, atoms, reactions%d_align_range)
                    end if
                end if

                !Segments
                if (simulation%use_segments) then
                    iseg = iseg_from_cell(atom, segment_bounds)
                    total_sites_seg(iseg) = total_sites_seg(iseg) + 1
                    z_sum_seg(iseg) = z_sum_seg(iseg) + coords%z
                    atom_envs_seg(iseg, a_env) = atom_envs_seg(iseg, a_env) + 1
                end if
                !Layers
                call incre_layers(a_layers, atom, atoms)
            end if
        end do

        surface_info%average_height = height_sum/total_sites
        surface_info%ave_growth_height = surface_info%average_height - start_height
        surface_info%envs = atom_envs

        !Update surface_info%layers (percentages)
        surface_info%min_layer = -1
        surface_info%max_layer = -1
        do ilayer=1, size(surface_info%layers)
            if (a_layers(ilayer) == 0) then
                surface_info%layers(ilayer) = 0.0_dp
            else
                if (surface_info%min_layer == -1) surface_info%min_layer = ilayer    !Detect lowest layer
                surface_info%max_layer = ilayer
                surface_info%layers(ilayer) = 100.0_dp * a_layers(ilayer)/total_sites
            end if
        end do
        surface_info%active_layers = surface_info%max_layer - surface_info%min_layer + 1
        call average_add(surface_info%ave_active_layers, real(surface_info%active_layers, dp))
        deallocate(a_layers)

        !Determine Top X layers of Occupancy
!        surface_info%top_occupancy = top_layer_occupancy(surface_info%layers, surface_info%min_layer, surface_info%max_layer)
        if (allocated(surface_info%top_occupancy)) deallocate(surface_info%top_occupancy)
        allocate(surface_info%top_occupancy(surface_info%active_layers))
        surface_info%top_occupancy(:) = surface_info%layers(surface_info%min_layer:surface_info%max_layer)

        !Growth Rate
        if (kmc_info%time == 0.0_dp) then
            surface_info%ave_growth_rate = 0.0_dp
        else
            surface_info%ave_growth_rate = surface_info%ave_growth_height / (kmc_info%time - start_time)
        end if

        !---------------------------
        !GROWTH RATE SINCE LAST CALL
        !---------------------------
        if (calc_instant_grate) then
            time_diff = kmc_info%time - surface_info%instant_growth_rate%last_time
            height_diff = surface_info%average_height - surface_info%instant_growth_rate%last_height

            if (time_diff == 0.0) then
                height_diff = 0.0
                time_diff = 1.0
            end if
            surface_info%instant_growth_rate%rate = height_diff/time_diff

            surface_info%instant_growth_rate%last_time = kmc_info%time
            surface_info%instant_growth_rate%last_height = surface_info%average_height
        end if

        !-----------------
        !Surface Roughness
        !-----------------
        call rms_roughness(atoms, surface_info, segment_info, &
                surface_info%rms_roughness, &
                surface_info%rms_2NN, &
                surface_info%rms_3NN, &
                surface_info%rms_4NN)

        call average_add(surface_info%ave_rms, real(surface_info%rms_roughness, dp))
        call average_add(surface_info%ave_2Nrms, real(surface_info%rms_2NN, dp))
        call average_add(surface_info%ave_3Nrms, real(surface_info%rms_3NN, dp))
        call average_add(surface_info%ave_4Nrms, real(surface_info%rms_4NN, dp))
        !==============================
        !Update individual segment data
        !==============================
        if (simulation%use_segments) then
            do iseg=1, size(segment_info)
                !Average height
                segment_info(iseg)%average_height = z_sum_seg(iseg)/total_sites_seg(iseg)
                !Growth height
                segment_info(iseg)%ave_growth_height = segment_info(iseg)%average_height - start_heights_seg(iseg)
                !Instant growth rate
                if (calc_instant_grate) then
                    height_diff_seg(iseg) = segment_info(iseg)%average_height - segment_info(iseg)%instant_growth_rate%last_height
                    segment_info(iseg)%instant_growth_rate%rate = height_diff_seg(iseg)/surface_info%instant_growth_rate%last_time
                    segment_info(iseg)%instant_growth_rate%last_height  = segment_info(iseg)%average_height
                end if
                !Atom Environements
                segment_info(iseg)%envs = atom_envs_seg(iseg,:)
                !Roughness is distruted during the rms_roughness() call
            end do
        end if

        !Dimers
        overall_dimer_deviation = d_align_sum / atoms_in_dimers
        dimer_prop = real(atoms_in_dimers, dp) / total_sites
        surface_info%dimer_deviation = overall_dimer_deviation
        surface_info%dimer_prop = dimer_prop
        call average_add(surface_info%ave_dimer_deviation, real(overall_dimer_deviation, dp))
        call average_add(surface_info%ave_dimers_prop, real(dimer_prop, dp))

    end subroutine surface_stats

    !Return global and length-scale based rms roughness for the current surface
    subroutine rms_roughness(atoms, s_info, segment_info, &
                             global, NN2, NN3, NN4)
        !Modules
        use input, only : outputs
        use types, only : atom_type_ext, atom_type, surface_type, coord_type
        use variables, only : simulation, return_ilattice_limits, segment_bounds
        use basis, only : get_atom_position
        use segment, only : iseg_from_cell
        !Arguments
        type(atom_type_ext), intent(in) :: atoms(:)
        type(surface_type), intent(in) :: s_info
        type(surface_type), intent(inout) :: segment_info(:)
        real(dp), intent(out) :: global, NN2, NN3, NN4
        !Variables
        integer :: index
        integer,dimension(2) :: ilattice_limits
        type(atom_type) :: atom
        real(dp) :: sum_global, sum_2NN, sum_3NN, sum_4NN
        real(dp) :: NNr2, NNr3, NNr4    !Store nearest neighbour deviations for 2NN, 3NN and 4NN (3-12 A)
        integer :: c_global             !Count of surface atoms
        type(coord_type) :: atom_coords
        !Segments
        integer :: iseg !Segment index of atom
        real(dp), dimension(size(segment_info)) :: sum_global_seg, sum_2NN_seg, sum_3NN_seg, sum_4NN_seg
        integer, dimension(size(segment_info)) :: c_global_seg !Surface atoms in each segment
        sum_global = 0.0_dp
        sum_2NN = 0.0_dp
        sum_3NN = 0.0_dp
        sum_4NN = 0.0_dp
        c_global = 0
        sum_global_seg = 0.0_dp
        sum_2NN_seg = 0.0_dp
        sum_3NN_seg = 0.0_dp
        sum_4NN_seg = 0.0_dp
        c_global_seg = 0
        iseg = 0
        !Method
        !########################################################################
        !================================= NOTE =================================
        !Segment-wise roughness still compares with average height of entire slab
        !########################################################################
        call return_ilattice_limits(s_info%k_min%val,s_info%k_max%val,ilattice_limits)

        do index=ilattice_limits(1),ilattice_limits(2)
            if (.not. atoms(index)%surface) cycle

            atom = atoms(index)%atom
            c_global = c_global + 1
            atom_coords = get_atom_position(atom, atoms)
            if (simulation%use_segments) then
                iseg = iseg_from_cell(atom, segment_bounds)
                c_global_seg(iseg) = c_global_seg(iseg) + 1
            end if
            !Global
            sum_global = sum_global + (atom_coords%z - s_info%average_height)**2
            if (simulation%use_segments) then
                sum_global_seg(iseg) = sum_global_seg(iseg) + (atom_coords%z - s_info%average_height)**2
            end if

            if (outputs%L%roughness_NN) then
                call NN_r(atom, atoms, s_info, NNr2, NNr3, NNr4)
                sum_2NN = sum_2NN + NNr2
                sum_3NN = sum_3NN + NNr3
                sum_4NN = sum_4NN + NNr4
                if (simulation%use_segments) then
                    sum_2NN_seg(iseg) = sum_2NN_seg(iseg) + NNr2
                    sum_3NN_seg(iseg) = sum_3NN_seg(iseg) + NNr3
                    sum_4NN_seg(iseg) = sum_4NN_seg(iseg) + NNr4
                end if
            end if

        end do

        global = sqrt(sum_global/c_global)
        NN2 = sqrt(sum_2NN/c_global)
        NN3 = sqrt(sum_3NN/c_global)
        NN4 = sqrt(sum_4NN/c_global)
        if (simulation%use_segments) then
            do iseg=1, size(segment_info)
                segment_info(iseg)%rms_roughness = sqrt(sum_global_seg(iseg)/c_global_seg(iseg))
                segment_info(iseg)%rms_2NN = sqrt(sum_2NN_seg(iseg)/c_global_seg(iseg))
                segment_info(iseg)%rms_3NN = sqrt(sum_3NN_seg(iseg)/c_global_seg(iseg))
                segment_info(iseg)%rms_4NN = sqrt(sum_4NN_seg(iseg)/c_global_seg(iseg))
            end do
        end if

    end subroutine rms_roughness

    !Return the rms displacment of an atom at various distances
    subroutine NN_r(atom, atoms, s_info, NN2, NN3, NN4)
        !Modules
        use types, only : atom_type_ext,atom_type,coord_type,surface_type
        use variables, only : NN_dist, return_ilattice_limits
        use basis, only : ret_height
        !Arguments
        type(atom_type),intent(in) :: atom
        type(atom_type_ext),intent(in),dimension(:) :: atoms
        type(surface_type),intent(in) :: s_info
        real(dp), intent(out) :: NN2, NN3, NN4
        !Variables
        integer :: iatom
        integer,dimension(2) :: ilattice_limits
        type(atom_type) :: atom2
        real(dp) :: xy_sep, a_height    !atom1-atom2 separation, atom2_height
        real(dp) :: s_NN2,s_NN3,s_NN4   !Height Sums
        integer :: c_NN2,c_NN3,c_NN4    !Counts
        !Method
        s_NN2 = 0.0_dp
        s_NN3 = 0.0_dp
        s_NN4 = 0.0_dp
        c_NN2 = 0
        c_NN3 = 0
        c_NN4 = 0

        call return_ilattice_limits(s_info%k_min%val,s_info%k_max%val,ilattice_limits)
        do iatom=ilattice_limits(1),ilattice_limits(2)
            if (.not. atoms(iatom)%surface) cycle

            atom2 = atoms(iatom)%atom
            xy_sep = atom_xyseparation(atom, atom2, atoms)
            a_height = ret_height(atom2, atoms)
            if (xy_sep <= NN_dist%NN4) then
                c_NN4 = c_NN4 + 1
                s_NN4 = s_NN4 + a_height

                if (xy_sep <= NN_dist%NN3) then
                    c_NN3 = c_NN3 + 1
                    s_NN3 = s_NN3 + a_height

                    if (xy_sep <= NN_dist%NN2) then
                        c_NN2 = c_NN2 + 1
                        s_NN2 = s_NN2 + a_height
                    end if
                end if
            end if

        end do

        NN2 = (ret_height(atom, atoms) - (s_NN2/c_NN2))**2
        NN3 = (ret_height(atom, atoms) - (s_NN3/c_NN3))**2
        NN4 = (ret_height(atom, atoms) - (s_NN4/c_NN4))**2

    end subroutine NN_r

    !Return the real space (x,y) separation of 2 atoms
    function atom_xyseparation(atom1, atom2, atoms) result(separation)
        !Modules
        use types, only : atom_type, atom_type_ext, coord_type
        use basis, only : get_atom_position
        !Arguments
        type(atom_type),intent(in) :: atom1,atom2
        type(atom_type_ext), intent(in), dimension(:) :: atoms
        real(dp) :: separation
        !Variables
        type(coord_type) :: coords1,coords2
        !Method
        coords1 = get_atom_position(atom1, atoms)
        coords2 = get_atom_position(atom2, atoms)
        separation = sqrt((coords1%x-coords2%x)**2 + (coords1%y-coords2%y)**2)

    end function atom_xyseparation

    !---------------------------------
    !Get average height of the surface
    !---------------------------------
    real(dp) function get_average_height(atoms) result(average_height)
        !Modules
        use types,      only : atom_type, atom_type_ext, coord_type
        use variables,  only : surface, return_ilattice_limits
        use basis,      only : position
        use enumerate,  only : types_of_atom
        !Variables
        type(atom_type_ext),dimension(:),intent(in) :: atoms
        integer                 :: total_sites,index
        integer,dimension(2)    :: ilattice_limits
        type(coord_type) :: coords
        real(dp)                :: height_sum
        type(atom_type)         :: atom

        total_sites = 0
        height_sum = 0.0_dp

        !Main Method
        call return_ilattice_limits(surface%k_min%val,surface%k_max%val,ilattice_limits)
        do index=ilattice_limits(1),ilattice_limits(2)
            if (atoms(index)%state /= types_of_atom%empty_site .and. atoms(index)%surface) then
                total_sites = total_sites + 1
                atom = atoms(index)%atom
                call position(atom, atoms, coords)
                height_sum = height_sum + coords%z
            end if
        end do

        average_height = height_sum/total_sites

    end function get_average_height

    !Fetch height of surface and determine growth height from input initial height
    real(dp) function get_growth_height(atoms, start_height) result(growth_height)
        !Modules
        use types,      only : atom_type_ext
        !Variables
        type(atom_type_ext),dimension(:),intent(inout) :: atoms
        real(dp)    :: start_height     !Starting height of the simulation surface
        !Main Method
        growth_height = get_average_height(atoms) - start_height

    end function get_growth_height

    real(dp) function calc_growth_height(surface_height,start_height) result(growth_height)
        real(dp), intent(in)    :: surface_height
        real(dp), intent(in)    :: start_height

        growth_height = surface_height - start_height

    end function calc_growth_height

    !Return the sqrt deviation of input atom from the mean height of all its 2rd NNs (and 3NN and 4NN)


    subroutine count_dimers(lattice,k_min,k_max,surface_dimers)

        use variables,  only : read_dimer, return_ilattice_limits
        use types,      only : atom_type_ext, atom_type, minmax_k
        use enumerate,  only : no_dimer, types_of_atom

        type(atom_type_ext), dimension(:), intent(in)   :: lattice
        type(minmax_k)                   , intent(in)   :: k_min,k_max
        integer, intent(out)                            :: surface_dimers
        integer                                         :: iatom
        integer                                         :: sum_surface_dimers
        integer,dimension(2)                            :: ilat_lims

        sum_surface_dimers = 0
        !Get ilattice limits
        call return_ilattice_limits(k_min%val,k_max%val,    ilat_lims)
        !Loop over atoms and count dimers
        do iatom=ilat_lims(1),ilat_lims(2)
            if (lattice(iatom)%state == types_of_atom%empty_site) cycle

            !Increment if atom part of a dimer
            if (lattice(iatom)%dimer) sum_surface_dimers = sum_surface_dimers + 1

        end do

        surface_dimers = sum_surface_dimers

    end subroutine count_dimers

    !--------------------------------------------------------------------------
    !Return the environment of an empty site.
    !Above a dimer unit, above a trough with either 0,1 or 2 dimers either side
    !--------------------------------------------------------------------------
    integer function detect_site_env(site_in, atoms) result(site_type)
        !Modules
        use types,      only : atom_type, atom_type_ext
        use variables,  only : read_dimer, ilattice
        use enumerate,  only : bulk_site, dimer_site, trough_site_2, trough_site_1, &
                trough_site_0
        use periodic,   only : ret_neighbour
        !Arguments
        type(atom_type), intent(in) :: site_in
        type(atom_type_ext), intent(in), dimension(:) :: atoms
        !Variables
        type(atom_type) :: N2, N3
        logical :: N2dimer, N3dimer          !Is N2/N3 forming a dimer?
        !Main Method
        !Get key neighbouts of site_in
        N2 = ret_neighbour(site_in, 2)
        N3 = ret_neighbour(site_in, 3)

        !Are N2 or N3 Dimers?
        N2dimer = read_dimer(N2, atoms)
        N3dimer = read_dimer(N3, atoms)

        !-----------------------------------
        !Dimer Site? (else is a trough site)
        !-----------------------------------
        if (N2dimer .and. N3dimer) then
            !Both neighbours are dimers...to each other?

            if (atoms(ilattice(N2))%ipair == ilattice(N3)) then
                site_type = dimer_site
                return
            end if

        end if

        !-------------------
        !Type of trough site
        !-------------------
        !increment -1 for each dimer present
        site_type = trough_site_0 !No dimers adjacent
        if (N2dimer) site_type = site_type - 1
        if (N3dimer) site_type = site_type - 1
        return

    end function detect_site_env

    !-----------------------------------------------------------------------------
    !Iterate over the lowest 'surface' atoms to determine whether this has changed
    !-----------------------------------------------------------------------------
    subroutine update_surface_lbound(k_min)
        use types,      only : minmax_k
        use variables,  only : lattice,ilattice,return_ilattice_limits

        type(minmax_k),intent(inout)    :: k_min
        integer,dimension(2)            :: ilattice_lims
        integer                         :: iatom

        type(minmax_k)                  :: local_k_min

        local_k_min%val = 1000000

        !Main Method
        !Iterate over 3 total k values (min -1, min + 2)
        call return_ilattice_limits(k_min%val-1,k_min%val + 2,ilattice_lims)

        do iatom=ilattice_lims(1),ilattice_lims(2)
            if (lattice(iatom)%surface .eqv. .false.) cycle

            if (lattice(iatom)%atom%k < local_k_min%val) then
                local_k_min%val   = lattice(iatom)%atom%k

                local_k_min%loc_i = lattice(iatom)%atom%i
                local_k_min%loc_j = lattice(iatom)%atom%j
            end if

        end do

        k_min = local_k_min

    end subroutine update_surface_lbound

    !-----------------------------------------------------------------------------
    !Check an atom site for blocking by the 2nd Carbon from an adsorbed C2H2 atom
    !X  C   X
    ! \ || / \
    !   C
    !  / \
    !-----------------------------------------------------------------------------
    logical function C2H2_pendant(atom, atoms) result(pendant_present)
        !Modules
        use types, only : atom_type_ext,atom_type
        use periodic, only : ret_neighbour
        use variables, only : read_ilink
        !Variables
        type(atom_type), intent(in) :: atom
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        type(atom_type) :: N2,N3

        !Get neighbours 2 and 3 of atom site
        N2 = ret_neighbour(atom, 2)
        N3 = ret_neighbour(atom, 3)

        !Check N2 and N3 for linked atoms (i.e attached carbon)
        if (read_ilink(N2, atoms) /= 0 .or. read_ilink(N3, atoms) /= 0) then
            pendant_present = .true.
        else
            pendant_present = .false.
        end if

        return

    end function C2H2_pendant

    !---------------------------------------------------------------------------------
    !Return an atoms status as part of a S_A, S_B edge or non-edge OR both S_A and S_B
    !---------------------------------------------------------------------------------
    function identify_edge_state(atom, atoms) result(state)
        !Modules
        use types, only : atom_type_ext,atom_type
        use enumerate, only : edge_state,file_units
        use periodic, only : count_TN
        !Arguments
        type(atom_type),intent(in) :: atom
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        integer :: state
        !Variables
        integer,dimension(4) :: TN_arr
        integer :: layer_type       !Type of layer atom is part of [1,2]
        !Method
        TN_arr = count_TN(atom, atoms)
        layer_type = tlayer_from_basis(atom%basis)
        state = edge_state%terrace
        !ISOLATED
        if (sum(TN_arr) == 0) then
            state = edge_state%isolated
        !TERRACED
        else if (sum(TN_arr) == 4) then
            state = edge_state%terrace
        !MIXED CHARACTER STEP EDGE
        else if (sum(TN_arr) == 3) then
        !Must be mixed character (AB) so diambiguation required
            state = edge_state%S_AB_edge
        !Step-edge or Corner Edge
        else if (sum(TN_arr) == 2) then
            !Detect flat step edge
            if (TN_arr(1) == 0 .and. TN_arr(3) == 0) then
                if (layer_type == 1) then
                    state = edge_state%S_B_edge
                else
                    state = edge_state%S_A_edge
                end if
            else if (TN_arr(2) == 0 .and. TN_arr(4) == 0) then
                if (layer_type == 1) then
                    state = edge_state%S_A_edge
                else
                    state = edge_state%S_B_edge
                end if
            else
                !Must be a mixture of S_A and S_B character
                state = edge_state%S_AB_edge
            end if
        !Either SA or SB character
        else if (sum(TN_arr) == 1) then
            if (TN_arr(1) == 1 .or. TN_arr(3) == 1) then
                if (layer_type == 1) then
                    state = edge_state%S_A_edge
                else
                    state = edge_state%S_B_edge
                end if
            else if (TN_arr(2) == 1 .or. TN_arr(4) == 1) then
                if (layer_type == 1) then
                    state = edge_state%S_B_edge
                else
                    state = edge_state%S_A_edge
                end if
            end if
        else
            write(file_units%error,*) 'identify_edge_state - atom has no valid state',TN_arr
            state = edge_state%terrace
        end if

    end function identify_edge_state

    subroutine fetch_surface_average_migration(atoms,surf_info, kmc_info)
        !Modules
        use types, only : atom_type,atom_type_ext,surface_type,kmc_type,average_add
        use variables, only : pop_mig,read_mig,return_ilattice_limits
        !Arguments
        type(atom_type_ext),intent(inout),dimension(:) :: atoms
        type(surface_type),intent(in) :: surf_info
        type(kmc_type),intent(inout) :: kmc_info
        !Variables
        type(atom_type) :: atom
        integer :: iatom
        integer,dimension(2) :: ilattice_limits
        !Method
        call return_ilattice_limits(surf_info%k_min%val-1,surf_info%k_max%val,ilattice_limits)

        do iatom=ilattice_limits(1),ilattice_limits(2)
            if (atoms(iatom)%surface .eqv. .false.) cycle
            atom = atoms(iatom)%atom

            if (read_mig(atom, atoms) /= 0) then
                call average_add(kmc_info%migs, real(pop_mig(atom, atoms),dp))
            end if

        end do
    end subroutine fetch_surface_average_migration

    !Increment the integer array layers by 1 for input atom
    subroutine incre_layers(int_layers, atom, atoms)
        !Modules
        use types, only : atom_type, atom_type_ext
        use basis, only : ret_height
        use enumerate, only : file_units
        !Arguments
        integer, dimension(:), intent(inout) :: int_layers
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in), dimension(:) :: atoms
        !Variables
        integer :: ilayer
        !Method
        ilayer = (NINT(ret_height(atom, atoms)/0.892452) + 1)

        if (ilayer > 0 .and. ilayer <= size(int_layers)) then
            int_layers(ilayer) = int_layers(ilayer) + 1
        else
            write(file_units%error, *) "invalid layer detected from atom:"
            write(file_units%error, *) ilayer
            write(file_units%error, *) atom
            flush(file_units%error)
            STOP
        end if

    end subroutine incre_layers


    !Is the atom (Cx) part of a hindered trough environment
    !    Bn   Bn
    !   / \  / \
    ! Cn   Cx   Cn
    !- no dimers
    ! - nb(Cx) == 3
    ! - Bn empty
    ! - nb(Cn) == 3
    ! - is_act(Cx) .and. .not. is_act(Cn)
    logical function hindered_trough(cx, atoms)
        !Modules
        use types, only : atom_type_ext, atom_type, same_atom
        use variables, only : read_atom, read_dimer, is_act
        use enumerate, only : atypes => types_of_atom
        use periodic, only : num_bonds, get_site_from_N_path
        use dimer, only : return_bridging_atom
        use input, only : dimerf
        !Arguments
        type(atom_type), intent(in) :: cx
        type(atom_type_ext), intent(in) :: atoms(:)
        !Variables
        integer :: ic
        integer :: paths(2,2)
        type(atom_type) :: bn, cn
        logical :: cx_act, cn_act

        paths(:,:) = reshape([1,2, 4,3], shape=[2,2], order=[2,1])
        hindered_trough = .false.

        if (read_dimer(cx, atoms)) return
        if (num_bonds(cx, atoms) /= 3) return


        cx_act = is_act(cx, atoms)
        do ic=1, size(paths,1)
            cn = get_site_from_N_path(cx, paths(ic,:))

            if (read_dimer(cn, atoms)) cycle

            call return_bridging_atom(cx, cn, bn)
            if (read_atom(bn, atoms) /= atypes%empty_site) cycle

            cn_act = is_act(cn, atoms)
            if (cn_act) cycle
            if (num_bonds(cn, atoms) /= 3) cycle


            if (dimerf%dihyride_hindered) then
                if ((.not. cx_act) .and. (.not. cn_act)) then
                    hindered_trough = .true.
                    return
                end if
            end if

            !If loops gets this far:
            !Bn empty, Cx activated, Cn not activated, nb(Cx) == nb(Cn) == 3 -> hindered trough
            hindered_trough = .true.
            return
        end do

    end function hindered_trough

end module surface_m