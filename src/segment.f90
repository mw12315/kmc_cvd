!Methods associated with surface segments
module segment

    use kinds, only : dp
    use types, only : xy_segment_type, XYSegment

contains

    !Return a vector of segment upper and lower bounds corresponding to a grid, row by row
    subroutine generate_segment_bounds(Nx, Ny, xseg, yseg, segments)
        !Arguments
        integer, intent(in) :: Nx, Ny, xseg, yseg  !x, y, segments per dimension
        type(xy_segment_type), intent(out), allocatable, dimension(:) :: segments
        !Variables
        integer :: n_seg    !Number of segments to be created
        integer, dimension(xseg) :: xsegment_sizes
        integer, dimension(yseg) :: ysegment_sizes
        integer :: isegx, isegy, iseg
        integer :: xlb, ylb !x lower bound, y lowerbound (current)
        !Allocation
        n_seg = xseg * yseg
        allocate(segments(n_seg))

        !Method
        !Determine size of each segement
        xsegment_sizes = get_segment_sizes(Nx, xseg)
        ysegment_sizes = get_segment_sizes(Ny, yseg)


        !Construct segment upper and lower bounds
        iseg=0
        ylb = 1
        do isegy = 1, xseg
            xlb = 1
            do isegx = 1, yseg
              iseg = iseg + 1

              segments(iseg) = XYSegment(xl = xlb, xu = xlb+xsegment_sizes(isegx), &
                                         yl = ylb, yu = ylb+ysegment_sizes(isegy))

              xlb = xlb + xsegment_sizes(isegx)
            end do
            ylb = ylb + ysegment_sizes(isegy)
        end do

    end subroutine generate_segment_bounds

    !Return an array of unique segment sizes for a given dimension size and required segments
    function get_segment_sizes(dim_size, seg_per_dim) result(seg_sizes)
        !Arguments
        integer, intent(in) :: dim_size, seg_per_dim
        integer, dimension(seg_per_dim) :: seg_sizes
        !Variables
        real(dp) :: average_seg_size, excess
        integer, dimension(seg_per_dim) :: iseg_order
        integer, allocatable, dimension(:) :: unique_seg_sizes
        integer :: iseg
        !Method

        average_seg_size = real(dim_size, dp)/seg_per_dim
        excess = average_seg_size - floor(average_seg_size)

        if (seg_per_dim == 3) then
            allocate(unique_seg_sizes(2)) !2 different sizes of segment expected
            iseg_order = (/ 1 , 2, 1 /)
            if (excess > 0.5) then
                !Smaller central segment
                unique_seg_sizes(1) = int(average_seg_size + excess / 2.0_dp)
                unique_seg_sizes(2) = int(average_seg_size - excess)
            else
                !Larger central segment
                unique_seg_sizes(1) = int(average_seg_size + excess)
                unique_seg_sizes(2) = int(average_seg_size - excess/ 2.0_dp)
            end if
        else
            write(6,*) "Invalid surface segment size", seg_per_dim
            STOP
        end if

        do iseg=1, size(iseg_order)
            seg_sizes(iseg) = unique_seg_sizes(iseg_order(iseg))
        end do

    end function get_segment_sizes

    !Return segment index for an input atom, using i (x) and j (y) attributes
    function iseg_from_cell(atom, seg_bounds) result(iseg)
        use types, only : atom_type
        !Arguments
        type(atom_type), intent(in) :: atom
        type(xy_segment_type), dimension(:) :: seg_bounds
        integer :: iseg !Index of segment
        !Variables
        integer :: i
        iseg = 0
        !Method
        do i=1, size(seg_bounds)
            if (atom%i >= seg_bounds(i)%x%lower .and. atom%i < seg_bounds(i)%x%upper) then
                if (atom%j >= seg_bounds(i)%y%lower .and. atom%j < seg_bounds(i)%y%upper) then
                    iseg = i
                    return
                end if
            end if
        end do

    end function iseg_from_cell
end module segment