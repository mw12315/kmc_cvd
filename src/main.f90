program diamond_lattice
    !Modules
    use kinds, only : dp
    use types, only : atom_type_ext, atom_type, kmc_type, sim_type, counter_type, rate_vis_switch_type, &
            kmc_timings, average_reset
    use input,  only : conditions, reactions, energies, outputs, etchingf, dimerf, colourise
    use initialise, only : read_input_files, initialise_lattice, generate_rates, count_unique_rates, &
            zero_global_counters, init_bookmarked_lattice, init_types, init_surface_type, init_segments
    use variables, only : simulation, lattice, surface, kmc_info, counters, &
                          open_units, add_unit_to_open_list, reset_last_frame, segment_bounds, segments
    use bookmark, only : init_from_bookmark, write_bookmark
    use random, only : init_random_seed
    use randomise, only : count_connectivity
    use execute_rates, only : execute_event
    use enumerate, only : file_units, tRates
    use surface_m, only : get_average_height, fetch_surface_average_migration
    use output,  only : output_input_values, output_final
    use kmc, only : kmc_loop
    use json, only : json_output_input_values
    use bookmark, only : write_bookmark

    implicit none
    !Variables
    !kMC Operation
    !Nucleation
    type(sim_type) :: nucleation_sim
    type(kmc_type) :: nucleation_kmc_info
    type(counter_type) :: nucleation_counter
    !Surface statistic variables
    !Timing
    real(dp) :: t1, t2 !Hold CPU times
    real(dp) :: tstart, tend !Simulation start and end times
    real(dp) :: t_in_lat, t_in_lat_percent
    real(dp) :: t_set_rates_percent
    real(dp) :: t_find_rate_percent
    real(dp) :: t_act_surf_percent
    real(dp) :: t_ex_rate_percent
    real(dp) :: t_create_rsv_percent
    type(kmc_timings) :: timing_kmc
    !Bookmarking
    type(atom_type_ext), allocatable, dimension(:) :: bookmarked_atoms !Array of atoms stored within bookmark.input
    integer :: initial_atoms                                           !Number of initial atoms stored within bookmark.input
    integer, dimension(:), allocatable :: index_list                   !List of atom indices of stored atom
    !Counters
    integer :: i, iseg
    logical :: is_open
    !Start Timing
    call cpu_time(tstart)

    !-------------
    !Begin Program
    !-------------
    call read_input_files(simulation, conditions, energies, etchingf, reactions, outputs, dimerf, colourise)
    !Calculate rates_per_site and set rate_indexes (from 0)
    call count_unique_rates(simulation, reactions)
    !Generate values of rates from simulation conditions and barriers
    call generate_rates(conditions, reactions, energies, etchingf, dimerf)
    !Set rate counters to zero
    call zero_global_counters(counters, kmc_info)
    call init_types()
    call init_surface_type(surface, outputs%values_at_x_s%num_values)

    !=============================================================================
    !Read in bookmark files and write to screen when in bookmark continuation mode
    !=============================================================================
    if (simulation%bookmark_mode_active) then
        open(unit=file_units%bookmark_log,file="bookmark.log")
        call add_unit_to_open_list(file_units%bookmark_log)
        call init_from_bookmark(kmc_info, simulation, surface, outputs, counters, initial_atoms, bookmarked_atoms, index_list)
    else
        kmc_info%total_events = 0
    end if

    !Open files used by all modes
    if (outputs%L%surf_connect) then
        open(unit=file_units%surf_conn, file="surface_connectivity.csv")
        call add_unit_to_open_list(file_units%surf_conn)
        write(file_units%surf_conn,'(a12,a1,a15,a1,a6,a1,a6,a1,a6,a1,a6,a1,a6)') "step",",","time",",","%N1",",","%N2",",","%N3",",","%N4",",","%Isolated"
    end if


    !==================
    !Initialise lattice
    !==================
    call cpu_time(t1)
    if (.not. simulation%bookmark_mode_active) then
        call initialise_lattice(simulation, lattice, surface)
    else
        call init_bookmarked_lattice(bookmarked_atoms, simulation, surface, index_list,lattice)
    end if
    surface%start_height =  get_average_height(lattice)
    write(file_units%log,'(a,f8.2,a,i10,a)') "Initialised Surface Height: ",surface%start_height," A from ",surface%total_atoms," atoms"
    if (.not. simulation%bookmark_mode_active) surface%init_height = surface%start_height
    call cpu_time(t2)
    t_in_lat = t2-t1

    surface%average_height = surface%start_height
    surface%instant_growth_rate%last_height = surface%average_height
    surface%instant_growth_rate%last_time = kmc_info%time

    !===================
    !Open required files
    !===================
    if (outputs%L%kmc_vis) then
        open(unit=file_units%kmc_vis, file="kmc_vis.xyz")
        call add_unit_to_open_list(file_units%kmc_vis)
    end if

    !======================
    !Segment initialisation
    !======================
    call init_segments(simulation, surface, segments, segment_bounds)

    !============
    !UNIT TESTING
    !============
    write(*,'(a)') "------------------"
    write(*,'(a)') "BEGIN UNIT TESTING"
    write(*,'(a)') "------------------"
    write(*,'(a30,a30)') "TEST", "RESULT"
    write(*, '(a30,a30)') "----","------"
    if (test_get_site_from_path()) then
        write(*,'(a30,a30)') "GET_SITE_FROM_N_PATH","PASS"
    else
        write(*,'(a30,a30)') "GET_SITE_FROM_N_PATH","FAIL"
    end if
    write(*,'(a)') "------------------"
    write(*,'(a)') " END UNIT TESTING "
    write(*,'(a)') "------------------"

    !=================
    !RATE VISUALISATON
    !=================
!    if (.not. simulation_rerun) then
!        open(unit=file_units%vis_rates,file='visualise_rates.xyz')
!        call add_unit_to_open_list(file_units%vis_rates)
!        call return_atom(5,5,simulation%fill_to_basis,simulation%Nz+1, atom)
!        call visualise_init_rates(atom)
!    end if


    !=========
    !KMC START
    !=========
    call init_random_seed(simulation%seeding_clock)

    call output_input_values(simulation, conditions, reactions, energies, etchingf, dimerf)
    call json_output_input_values(simulation, conditions, reactions, energies, etchingf, dimerf)
    !Flush Output File
    flush(file_units%m_output)

    write(file_units%log,'(a20)') "--------------------"
    write(file_units%log,'(a)') "Begin kMC Simulation"
    write(file_units%log,'(a20)') "--------------------"
    if (simulation%bookmark_mode_active) write(666,'(a,f5.1,a,i10,a,f5.1,a)') "Continue from ",kmc_info%time,"s (",kmc_info%step,") till ",simulation%end%time," s"
    if (outputs%L%event_csv) then
        write(file_units%event_out,*) simulation%Nx,simulation%Ny,simulation%Nz,simulation%fill_to_basis
    end if

    !Initialise kmc timing variables
    timing_kmc%set_rates = 0.0_dp
    timing_kmc%find_rate = 0.0_dp
    timing_kmc%ex_rate = 0.0_dp
    timing_kmc%create_rsv = 0.0_dp
    timing_kmc%surf_stats%time = 0.0_dp
    timing_kmc%surf_stats%count = 0

    !==========
    !Nucleation
    !==========
    if (.not. simulation%bookmark_mode_active) then
        if (simulation%nucleation_height_on) then
            write(file_units%log,'(a20)') "--------------------"
            write(file_units%log,'(a)') "BEGIN NUCLEATION"
            write(file_units%log,'(a20)') "--------------------"
            flush(file_units%log)
            nucleation_sim = simulation
            nucleation_kmc_info = kmc_info
            call kmc_loop(lattice, nucleation_kmc_info, nucleation_sim, surface, segments, timing_kmc, simulation%nucleation_height_on)

            nucleation_counter = counters
            kmc_info%nucleation_height = nucleation_kmc_info%nucleation_height
            kmc_info%nucleation_time = nucleation_kmc_info%nucleation_time
            call reset_last_frame(lattice)
            call zero_global_counters(counters, kmc_info)
            do i=0, size(surface%ave_envs)-1 !ave_envs starts at 0
                call average_reset(surface%ave_envs(i))
                if (simulation%use_segments) then
                    do iseg=1, size(segments)
                        call average_reset(segments(iseg)%ave_envs(i))
                    end do
                end if
            end do
            call average_reset(surface%ave_active_layers)
            call average_reset(surface%ave_rms)
            call average_reset(surface%ave_2Nrms)
            call average_reset(surface%ave_3Nrms)
            call average_reset(surface%ave_4Nrms)
            write(file_units%log,'(a20)') "--------------------"
            write(file_units%log,'(a)') "END NUCLEATION"
            write(file_units%log,'(a20)') "--------------------"
        end if
    end if

    !==============
    !kMC Simulation
    !==============
    !Reset surf_height10 array for output during kmc
    flush(file_units%log)
    call kmc_loop(lattice, kmc_info, simulation, surface, segments ,timing_kmc, nucleation = .false.)

    !------------------------
    !End of Simulation Output
    !------------------------
    call cpu_time(tend)
    call fetch_surface_average_migration(lattice, surface, kmc_info)
    call output_final(simulation, kmc_info, surface, segments,  lattice, tend-tstart) !Output simulation stats to main output file

    call write_bookmark(lattice,simulation,surface,kmc_info,outputs,counters)

    !-----------------
    !Deallocate Arrays
    !-----------------
    deallocate(lattice)

    !-------------------------------
    !Writing Timing Values to Screen
    !-------------------------------
    !Calculate percentage values
    t_set_rates_percent = 100 * timing_kmc%set_rates/(tend-tstart)
    t_find_rate_percent = 100 * timing_kmc%find_rate / (tend-tstart)
    t_act_surf_percent = 100 * timing_kmc%act_surf / (tend-tstart)
    t_ex_rate_percent = 100 * timing_kmc%ex_rate / (tend-tstart)
    t_in_lat_percent = 100 * t_in_lat/(tend-tstart)
    t_create_rsv_percent = 100 * timing_kmc%create_rsv / (tend-tstart)

    write(6,*) " "
    write(6,'(a60)') "----------Subroutine Timing----------"
    write(6,'(a30,a5,a16,a16,a16)') "Subroutine"," "," Total Time (s) "," Time/Iteration (s) ", "%"
    write(6,'(a30,a5,a16,a16,a16)') "---------------"," ","---------------","---------------","---------------"
    write(6,'(a30,f16.5,a16,a16,a16)') "Program",tend-tstart,"--","--"

    write(6,'(a30,a5,ES16.8,a16,f16.2)') "initialise_lattice"," ",t_in_lat,"-",t_in_lat_percent
    write(6,'(a30,a5,ES16.8,ES16.8,f16.2)') "create rsv"," ",timing_kmc%create_rsv,timing_kmc%create_rsv/(kmc_info%step-1),t_create_rsv_percent
    write(6,'(a30,a5,ES16.8,ES16.8,f16.2)') "set_rates"," ",timing_kmc%set_rates,timing_kmc%set_rates/(kmc_info%step-1),t_set_rates_percent
    write(6,'(a30,a5,ES16.8,ES16.8,f16.2)') "locate"," ",timing_kmc%find_rate,timing_kmc%find_rate/(kmc_info%step-1),t_find_rate_percent

    write(6,'(a30,a5,ES16.8,ES16.8,f16.2)') "activate_surface"," ",timing_kmc%act_surf,timing_kmc%act_surf/(kmc_info%step-1),t_act_surf_percent

    write(6,'(a30,a5,ES16.8,ES16.8,f16.2)') "execute_rates"," ",timing_kmc%ex_rate,timing_kmc%ex_rate/(kmc_info%step-1),t_ex_rate_percent
    write(6,'(a77,a6)')   " ","------"
    write(6,'(a67,f16.2)') " ",t_set_rates_percent + t_find_rate_percent + t_act_surf_percent + t_ex_rate_percent + t_in_lat_percent
    write(6,'(a77,a6)')   " ","------"

    write(6,'(a30,a5,ES16.8,ES16.8,f16.2)') "surface_stats"," ",timing_kmc%surf_stats%time, &
            timing_kmc%surf_stats%time/timing_kmc%surf_stats%count, &
            100*(timing_kmc%surf_stats%time/(tend-tstart))
    write(file_units%log,*) " "
    write(file_units%log,'(a60)') "----------Subroutine Timing----------"
    write(file_units%log,'(a30,a5,a16,a16,a16)') "Subroutine"," "," Total Time (s) "," Time/Iteration (s) ", "%"
    write(file_units%log,'(a30,a5,a16,a16,a16)') "---------------"," ","---------------","---------------","---------------"
    write(file_units%log,'(a30,f16.5,a16,a16,a16)') "Program",tend-tstart,"--","--"

    write(file_units%log,'(a30,a5,ES16.8,a16,f16.2)') "initialise_lattice"," ",t_in_lat,"-",t_in_lat_percent
    write(file_units%log,'(a30,a5,ES16.8,ES16.8,f16.2)') "create rsv"," ",timing_kmc%create_rsv,timing_kmc%create_rsv/(kmc_info%step-1),t_create_rsv_percent
    write(file_units%log,'(a30,a5,ES16.8,ES16.8,f16.2)') "set_rates"," ",timing_kmc%set_rates,timing_kmc%set_rates/(kmc_info%step-1),t_set_rates_percent
    write(file_units%log,'(a30,a5,ES16.8,ES16.8,f16.2)') "locate"," ",timing_kmc%find_rate,timing_kmc%find_rate/(kmc_info%step-1),t_find_rate_percent

    write(file_units%log,'(a30,a5,ES16.8,ES16.8,f16.2)') "activate_surface"," ",timing_kmc%act_surf,timing_kmc%act_surf/(kmc_info%step-1),t_act_surf_percent

    write(file_units%log,'(a30,a5,ES16.8,ES16.8,f16.2)') "execute_rates"," ",timing_kmc%ex_rate,timing_kmc%ex_rate/(kmc_info%step-1),t_ex_rate_percent
    write(file_units%log,'(a77,a6)')   " ","------"
    write(file_units%log,'(a67,f16.2)') " ",t_set_rates_percent + t_find_rate_percent + t_act_surf_percent + t_ex_rate_percent + t_in_lat_percent
    write(file_units%log,'(a77,a6)')   " ","------"

    write(file_units%log,'(a30,a5,ES16.8,ES16.8,f16.2)') "surface_stats"," ",timing_kmc%surf_stats%time, &
            timing_kmc%surf_stats%time/timing_kmc%surf_stats%count, &
            100*(timing_kmc%surf_stats%time/(tend-tstart))

    !Write CPU time used into output file
    write(file_units%m_output,'(a30,ES12.3)') "AVERAGE TIME STEP (s):",kmc_info%time/kmc_info%total_events
    write(file_units%m_output,'(a30,f12.3)') "TOTAL CPU TIME (h):",(tend-tstart)/3600_dp

    !-----------
    !Close Files
    !-----------
    do i=1,size(open_units)-1
        if (open_units(i) /= -1) then
            inquire(unit=open_units(i), opened=is_open)
            if (is_open) close(open_units(i))
        end if
    end do
    call tidy_up_err_files()

contains

    subroutine tidy_up_err_files()

        integer :: file_size,stat

        open(unit=file_units%error, iostat=stat, file='errors.txt', status='old')
        inquire(unit=file_units%error, size=file_size)
        if (stat == 0 .and. file_size == 0) then
            close(file_units%error, status='delete')
        else
            close(file_units%error)
        end if
        open(unit=file_units%warning, iostat=stat, file='warnings.txt', status='old')
        inquire(unit=file_units%warning, size=file_size)
        if (stat == 0 .and. file_size == 0) then
            close(file_units%warning, status='delete')
        else
            close(file_units%warning)
        end if

    end subroutine tidy_up_err_files

    logical function test_get_site_from_path() result(pass)
        use types, only : same_atom
        use periodic, only : ret_neighbour, get_site_from_N_path
        integer, dimension(6) :: path1, path2, path3, path4

        type(atom_type) :: i_site

        type(atom_type) :: r1a,r1b
        type(atom_type) :: r2a,r2b
        type(atom_type) :: r3a,r3b
        type(atom_type) :: r4a,r4b

        i_site = atom_type(5,5,5,2)
        pass = .true.

        path1(:) = (/ 1, 2, 3, 4, 0, 0 /)
        path2(:) = (/ 1, 2, 1, 2, 0, 0 /)
        path3(:) = (/ 4, 3, 0, 0, 0, 0 /)
        path4(:) = (/ 2, 2, 0, 0, 0, 0 /)

        r1a = ret_neighbour(ret_neighbour(ret_neighbour(ret_neighbour(i_site,path1(1)),path1(2)),path1(3)),path1(4))
        r1b = get_site_from_N_path(i_site,path1)
        r2a = ret_neighbour(ret_neighbour(ret_neighbour(ret_neighbour(i_site,path2(1)),path2(2)),path2(3)),path2(4))
        r2b = get_site_from_N_path(i_site,path2)
        r3a = ret_neighbour(ret_neighbour(i_site,path3(1)),path3(2))
        r3b = get_site_from_N_path(i_site,path3)
        r4a = ret_neighbour(ret_neighbour(i_site,path4(1)),path4(2))
        r4b = get_site_from_N_path(i_site,path4)

        if (.not. same_atom(r1a,r1b)) then
            write(*,*) "PATH 1 FAIL",path1,r1a,r1b
            pass = .false.
        end if
        if (.not. same_atom(r2a,r2b)) then
            write(*,*) "PATH 2 FAIL",path2,r2a,r2b
            pass = .false.
        end if
        if (.not. same_atom(r3a,r3b)) then
            write(*,*) "PATH 3 FAIL",path3,r3a,r3b
            pass = .false.
        end if
        if (.not. same_atom(r4a,r4b)) then
            write(*,*) "PATH 4 FAIL",path4,r4a,r4b
            pass = .false.
        end if


    end function test_get_site_from_path

end program diamond_lattice
