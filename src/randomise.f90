module randomise

    use kinds,  only : dp

    implicit none

contains
    !------------------------------------------------------------------------
    !Cycle through surface_atoms and activate according to FmR
    !In the case of a bi-radical site being formed, check for dimer formation
    !Average height of the surface is also calculated
    !------------------------------------------------------------------------
    subroutine activate_surface(atoms, kmc_info, surf_info, segment_info, next_output_time)
        !Modules
        use types, only : atom_type,atom_type_ext,kmc_type,surface_type, &
                          average_type, average_add, average, average_reset
        use input, only : outputs, dimerf
        use basis, only : ret_height
        use variables, only : read_atom,read_dimer,read_ilink,is_act,return_ilattice_limits, read_act, &
                              simulation, segment_bounds, atom_pair,counters
        use enumerate, only : file_units,types_of_atom,edge_state
        use input, only : reactions
        use derived_values, only : Fmr, prob_adj_dimer_break
        use periodic, only : num_bonds,count_TN
        use dimer, only : create_dimer_if_possible, remove_dimer_if_required, improve_dimer_alignment, remove_dimer
        use surface_m, only : identify_edge_state, hindered_trough
        use segment, only : iseg_from_cell
        !Arguments
        type(atom_type_ext),    intent(inout)   :: atoms(:)
        type(kmc_type),         intent(inout)   :: kmc_info
        type(surface_type),     intent(inout)   :: surf_info
        type(surface_type),     intent(inout)   :: segment_info(:)
        real(dp),               intent(in)      :: next_output_time
        !Variables
        type(atom_type)                 :: atom
        integer                         :: iatom
        integer                         :: ilattice_limits(2)
        integer                         :: activated
        integer                         :: deactivated
        integer                         :: breaks_this_iter         !Dimers broken this iteration
        integer                         :: total_activated
        type(atom_type),    allocatable :: activated_list(:)
        integer,            allocatable :: atom_states(:)
        integer                         :: NN, TN                   !Nearest neighbours,Next Nearest Neighbours
        integer                         :: coord_count(5)           !Coordination counters
        real(dp)                        :: coordPercent(5)          !Coordination percentages
        logical                         :: atom_activated
        real(dp)                        :: height_sum
        integer                         :: surf_atoms               !Local count of surface atoms
        integer,            allocatable :: atom_envs(:)             !Local copy of surface atom environments
        integer                         :: a_env                    !Environment of the atom
        real(dp)                        :: z_min, z_max, z_atom     !Min,Max z height on the surface, z height of the atom
        type(average_type)              :: ave_act_state            !Average activation state
        logical                         :: part_of_hindered_trough  !Atom is part of a hindered trough (activated/deactivatin related)
        !Segments
        integer :: iseg
        real(dp),   dimension(size(segment_info))   :: seg_zmin, seg_zmax
        real(dp),   dimension(size(segment_info))   :: seg_z_sum
        integer,    dimension(:,:), allocatable     :: seg_atom_envs                       !Local copy of surface atom environments
        integer,    dimension(size(segment_info))   :: seg_surf_atoms                                      !Local count of surface atoms

        !todo remove activation for clean output of activation in act_fract
        surf_info%total_isolated_dimers = 0
        coord_count = 0
        activated = 0
        deactivated = 0
        breaks_this_iter = 0
        total_activated = 0
        height_sum = 0.0_dp
        surf_atoms = 0
        call average_reset(ave_act_state)
        allocate(atom_envs(edge_state%isolated:edge_state%terrace))
        atom_envs(:) = 0
        z_min = 1E5_dp
        z_max = -1E5_dp
        iseg = 0
        if (simulation%use_segments) then
            allocate(seg_atom_envs(size(segment_info), edge_state%isolated:edge_state%terrace))
            seg_zmin(:) = 1E5_dp
            seg_zmax(:) = -1E5_dp
            seg_atom_envs(:,:) = 0
            seg_surf_atoms(:) = 0
            seg_z_sum(:) = 0
        end if
        !Main Method
        call return_ilattice_limits(surf_info%k_min%val-1,surf_info%k_max%val,ilattice_limits)

        do iatom=ilattice_limits(1),ilattice_limits(2)
            if (atoms(iatom)%surface .eqv. .false.) cycle

            part_of_hindered_trough = .false.
            atom = atoms(iatom)%atom
            !Check for unlinked carbonyls
            if (read_atom(atom, atoms) == types_of_atom%acetyl_c2) then
                !Ensure it is linked
                if (read_ilink(atom, atoms) == 0) then
                    write(file_units%error,*) kmc_info%step,"activate_surface: unlinked acetyl_c2 detected",atom
                end if
            end if
            !Check for underbonded atoms on the surface
            if (num_bonds(atom, atoms) < 2) then
                if (read_atom(atom, atoms) /= types_of_atom%acetyl_c2) then
                    write(file_units%warning,*) kmc_info%step, "underbonded atom!", atom, num_bonds(atom, atoms)
                end if
            end if

            !Include in height sum for average height calculation
            z_atom = ret_height(atom, atoms)
            surf_atoms = surf_atoms + 1
            height_sum = height_sum + z_atom
            !Surface Width
            if (z_atom < z_min) z_min = z_atom
            if(z_atom > z_max) z_max = z_atom
            !Surface Environment
            a_env = identify_edge_state(atom, atoms)
            atom_envs(a_env) = atom_envs(a_env) + 1
            !Segment
            if (simulation%use_segments) then
                iseg = iseg_from_cell(atom, segment_bounds)
                seg_z_sum(iseg) = seg_z_sum(iseg) + z_atom
                seg_surf_atoms(iseg) = seg_surf_atoms(iseg) + 1
                if (z_atom < seg_zmin(iseg)) seg_zmin(iseg) = z_atom
                if(z_atom > seg_zmax(iseg)) seg_zmax(iseg) = z_atom
                seg_atom_envs(iseg,a_env) = seg_atom_envs(iseg, a_env) + 1
            end if

            !====================================
            !Set activation level of surface atom
            !====================================
            NN = num_bonds(atom, atoms)
            !Get Coordination of Atom and Increment counters
            if (outputs%L%surf_connect) then
                !Get NNN (TN) coordination
                TN = sum(count_TN(atom, atoms))
                !Increment coordination counting
                call increment_coord(NN, TN, coord_count)
            end if

            if (dimerf%hind_trough_deact_factor /= 1.0) then
                part_of_hindered_trough = hindered_trough(atom, atoms)
            end if
            call decide_act_deact(atoms, atom, NN, Fmr, activated, deactivated, part_of_hindered_trough, &
                    atom_activated)
            if (outputs%L%act_fraction) then
                call average_add(ave_act_state, real(read_act(atom, atoms),dp))
                end if


        end do

        !=========================
        !Biradical Dimer Formation
        !=========================
        total_activated = 0
        do iatom=ilattice_limits(1),ilattice_limits(2)
            if (atoms(iatom)%surface .eqv. .false.) cycle

            atom = atoms(iatom)%atom

            if (is_act(atom, atoms)) then
                call create_dimer_if_possible(atom, atoms)
                if (reactions%realign_dimers) then
                    if (read_dimer(atom, atoms)) call improve_dimer_alignment(atom, atoms)
                end if


                !Count Number of activated atoms on the surface
                total_activated = total_activated + 1

            end if

        end do

        !-------------------------
        !DIMER BREAKING (ADJ) LOOP
        !-------------------------
        if (dimerf%random_adj_dimer_breaking) then
            total_activated = 0  !Determine activation of the surface independent activation step
            do iatom=ilattice_limits(1),ilattice_limits(2)
                if (atoms(iatom)%surface .eqv. .false.) cycle

                atom = atoms(iatom)%atom

                if (read_dimer(atom, atoms)) then
                    if (decide_break_dimer(atom, atoms, prob_adj_dimer_break)) then
                        call remove_dimer(atom, atom_pair(atom, atoms), atoms)
                        counters%d_break = counters%d_break + 1
                        breaks_this_iter = breaks_this_iter + 1
                    end if
                end if

                if (is_act(atom, atoms)) then
                    total_activated = total_activated + 1
                end if

            end do

        end if



        !Outputing activativation/deactivation to file
        if (outputs%L%activation) write(file_units%activation_log,('(5(i10))')) &
                & kmc_info%step,activated,deactivated,breaks_this_iter*2,total_activated

        if (outputs%L%act_fraction) then
            write(file_units%act_fract,'(3(i10,a1),f10.3,a1,f10.3)') kmc_info%step,",", &
                    total_activated, ",", &
                    surf_info%total_atoms,",", &
                    average(ave_act_state),",", &
                    (100.0_dp*real(total_activated,dp)/surf_info%total_atoms)
        end if

        !Output coordination
        if (outputs%L%surf_connect) then
            if (kmc_info%time >= next_output_time) then
                coordPercent = 100*real(coord_count)/surf_info%total_atoms
                write(file_units%surf_conn,'(i12,a1,f15.9,5(a1,f6.2))') kmc_info%step,",",kmc_info%time,",", &
                        & coordPercent(1),",",coordPercent(2),",",coordPercent(3),",",coordPercent(4),",",coordPercent(5)
            end if
        end if

        !Calculate average height of surface and store in global variable
        surf_info%average_height = height_sum/surf_atoms
        !Update surface thickness
        surf_info%width = z_max - z_min
        !Update surface environments
        surf_info%envs = atom_envs
        do a_env=edge_state%isolated,edge_state%terrace
            call average_add(surf_info%ave_envs(a_env), real(atom_envs(a_env),dp))
        end do

        !Add average activation fraction to kmc_info
        call average_add(kmc_info%Fmr, real(total_activated,dp)/surf_info%total_atoms)

        !Update Segment data from activation loop
        if (simulation%use_segments) then
            do iseg=1, size(segment_info)
                segment_info(iseg)%average_height = seg_z_sum(iseg)/seg_surf_atoms(iseg)
                segment_info(iseg)%width = seg_zmax(iseg) - seg_zmin(iseg)
                do a_env=edge_state%isolated,edge_state%terrace
                    call average_add(segment_info(iseg)%ave_envs(a_env), real(seg_atom_envs(iseg, a_env),dp))
                end do
            end do
        end if


    end subroutine activate_surface


    !-----------------------------------------------------------------
    !Activate or deactivate an atom
    !according to fmr (fraction mono-radical) and random_number(ran)
    !If hindered is passed:
    !take this into account when deactivating (or activating?) atom
    !relating to steric hinderence of 2 hydrogens within a trough site
    !-----------------------------------------------------------------
    subroutine decide_act_deact(atoms, atom, NN, fmr, count_act, count_deact, steric_hinderance, activated)
        !Modules
        use types, only : atom_type, atom_type_ext
        use variables, only : read_act
        use enumerate, only : not_activated
        use input, only : dimerf
        use activate, only : max_act_level, activate_atom, deactivate_atom
        !Arguments
        type(atom_type_ext),dimension(:),intent(inout) :: atoms
        type(atom_type), intent(inout) :: atom
        real(dp), intent(in) :: fmr
        integer, intent(in) :: NN
        integer, intent(inout) :: count_act, count_deact
        logical, intent(in) :: steric_hinderance
        logical, intent(out) :: activated
        !Variables
        real(dp) :: ran
        real(dp) :: use_fmr

        if (steric_hinderance) then
            use_fmr = (1 - dimerf%hind_trough_deact_factor)
        else
            use_fmr = fmr
        end if

        call random_number(ran)
        if (ran < use_fmr) then
            !Activate Atom - but only if not maximally activated already
                call activate_atom(atom, NN, atoms, count_act, activated) !count_act and activation incremented/set on successful activation
                return
        else
            !Deactivate Atom - but only if can be deactivated further
            if ((read_act(atom, atoms) > not_activated) .and. (read_act(atom, atoms) /= not_activated)) then
                call deactivate_atom(atom, atoms, count_deact)
            end if
        end if

        activated = .false.

    end subroutine decide_act_deact

    !-----------------------------------------------
    !Determine whether a ADJ type dimer should break
    !-----------------------------------------------
    logical function decide_break_dimer(atom, atoms, prob_dimer_break)
        !Modules
        use types, only : atom_type, atom_type_ext
        use variables, only : atom_pair, ilattice
        use dimer, only : dimer_bonding
        use enumerate, only : dimer_env
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        real(dp), intent(in) :: prob_dimer_break
        !Variables
        integer :: dimer_type
        integer :: iatom, ipair
        real(dp) :: ran
        decide_break_dimer = .false.

        iatom = ilattice(atom)
        ipair = ilattice(atom_pair(atom, atoms))
        if (dimer_bonding(atom, atoms) /= dimer_env%ADJ) return !Operates on ADJ dimers
        if (iatom > ipair) return !Only operate on lower index of dimer

        call random_number(ran)
        if (ran <= prob_dimer_break) then
            decide_break_dimer = .true.
        end if

    end function decide_break_dimer


    subroutine count_connectivity(atoms,coordPercent)
        use types,      only : atom_type, atom_type_ext
        use periodic,   only : count_nearest_atoms, count_TN
        use variables,  only : surface, ilattice, return_ilattice_limits
        use enumerate,  only : types_of_atom

        type(atom_type_ext), intent(in),dimension(:)    :: atoms
        real(dp),dimension(5),intent(inout)             :: coordPercent
        integer, dimension(5)                           :: coordCount
        integer, dimension(2)                           :: ilattice_limits
        integer                                         :: NN,TN,index
        type(atom_type)                                 :: atom

        coordCount = 0
        !Main Method
        call return_ilattice_limits(surface%k_min%val,surface%k_max%val,ilattice_limits)
        do index=ilattice_limits(1),ilattice_limits(2)
            if (atoms(index)%state /= types_of_atom%empty_site .and. atoms(index)%surface) then
                atom = atoms(index)%atom
                call count_nearest_atoms(atom, atoms, NN)
                TN = sum(count_TN(atom, atoms))
                call increment_coord(NN,TN, coordCount)
            end if
        end do

        coordPercent = 100*real(coordCount)/surface%total_atoms

        return

    end subroutine count_connectivity

    !------------------------------------------------
    !Increment counters within the coordination array
    !according to the NN and TN inputs
    !------------------------------------------------
    subroutine increment_coord(NN,TN,   count)
        !Modules
        use input,  only : reactions
        !Variables
        integer,intent(in)                  :: NN,TN
        integer,intent(inout),dimension(:)  :: count
        !-----------
        !Main Method
        !-----------
        !Increment NN counter (1-4)
        count(NN) = count(NN) + 1

        !Detection Isolation (etching)
        if (TN <= reactions%isolated_TN_threshold) count(5) = count(5) + 1

    end subroutine increment_coord


end module randomise