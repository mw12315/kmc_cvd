! Created by  on 24/02/2020.

module interfaces

    implicit none


    !Interface for adding a value to an average type
    interface average_add
        subroutine average_add_r(obj,val)
            use types, only : average_type
            type(average_type), intent(inout) :: obj
            real(dp), intent(in) :: val
        end subroutine average_add_r
        subroutine average_add_i(obj,val)
            import :: average_type, dp
            type(average_type), intent(inout) :: obj
            integer, intent(in) :: val
        end subroutine average_add_i
    end interface average_add
end module interfaces