module input

    use kinds, only : dp
    use types, only : reactions_ftype, outputs_ftype, conditions_ftype, energy_ftype, etching_ftype, &
                      dimer_ftype, event_colour_type

    implicit none

    type(reactions_ftype) :: reactions
    type(outputs_ftype) :: outputs
    type(conditions_ftype) :: conditions
    type(energy_ftype) :: energies
    type(etching_ftype) :: etchingf
    type(dimer_ftype) :: dimerf
    type(event_colour_type), allocatable :: colourise(:)


contains

    subroutine read_simulation(simulation)
        !Modules
        use types, only : sim_type
        use enumerate, only : file_units
        use fileIO, only : file_exists
        !Arguments
        type(sim_type), intent(inout) :: simulation
        !Variables
        character(150) :: rubbish
        logical :: continued
        !Main Method
        if (file_exists("simulation.input")) then
            open(unit=file_units%input, file="simulation.input", action='read', status='old')
        else
            write(6,*) "simulation.input not found!"
            stop
        end if

        read(file_units%input,*) rubbish    !Title
        read(file_units%input,*) simulation%name
        read(file_units%input,*) rubbish    !Dimensions
        read(file_units%input,*) simulation%Nx,simulation%Ny,simulation%Nz,simulation%fill_to_basis
        !Due to initial surface reconstructions, the surface must have equal x,y conventional unit cell dimensions
        if (mod(simulation%Nx,2) /= 0) then
            simulation%Nx = simulation%Nx + 1
            write(file_units%warning,*) "Input.mod: grid_size(x) odd: changed to even: ",simulation%Nx
        end if
        if (mod(simulation%Ny,2) /= 0) then
            simulation%Ny = simulation%Ny + 1
            write(file_units%warning,*) "Input.mod: grid_size(x) odd: changed to even: ",simulation%Ny
        end if
        read(file_units%input,*) rubbish    !End Conditions
        read(file_units%input,*) simulation%max_height,simulation%end%iteration,simulation%end%time,simulation%end%growth_height
        if (simulation%end%iteration < 0) simulation%end%iteration = 100000000 ! 100 million iteration default
        read(file_units%input,*) rubbish    !SEGMENTS
        read(file_units%input,*) simulation%use_segments
        read(file_units%input,*) rubbish, simulation%xsegments
        read(file_units%input,*) rubbish, simulation%ysegments
        read(file_units%input,*) rubbish, simulation%seeding_clock
        read(file_units%input,*) rubbish    !nucleation Period
        read(file_units%input,*) simulation%nucleation_height
        !Set nucleation Period Logical
        if (simulation%nucleation_height <= 0.0) then
            simulation%nucleation_height_on = .false.
            simulation%nucleation_height = 0.0_dp
        else
            simulation%nucleation_height_on = .true.
        end if
        read(file_units%input,*) rubbish    !Continue from bookmark file
        read(file_units%input,*) simulation%use_bookmark
        read(file_units%input,*) rubbish    !Unfinished Simulation?
        read(file_units%input,*) simulation%continue_unfinished

        continued = (file_exists('bookmark.geom') .and. file_exists('bookmark.json'))

        simulation%bookmark_mode_active = .false.
        if (continued .and. simulation%use_bookmark) then
            simulation%bookmark_mode_active = .true.
            !NUCLEATION PERIOD NOT ALLOWED WITH BOOKMARK MODE!
            simulation%nucleation_height = 0.0_dp
            simulation%nucleation_height_on =.false.
        end if

        close(file_units%input)

    end subroutine read_simulation

    subroutine read_conditions(conditions_i)
        !Modules
        use enumerate, only : file_units
        use fileIO, only : file_exists
        !Arguments
        type(conditions_ftype), intent(inout) :: conditions_i
        !Variables
        character(150) :: rubbish
        integer :: i
        !Main Method
        if (file_exists("conditions.input")) then
            open(unit=file_units%input, file="conditions.input", action='read', status='old')
        else
            write(6,*) "conditions.input not found!"
            stop
        end if

        read(file_units%input,*) rubbish    !Title
        read(file_units%input,*) conditions_i%condition_id    !CONDITION TITLE
        read(file_units%input,*) rubbish    !Temperatures
        read(file_units%input,*) conditions_i%Ts, conditions_i%Tns
        read(file_units%input,*) rubbish    ![Gas] at Surface
        read(file_units%input,*) rubbish, conditions_i%Hconc
        read(file_units%input,*) rubbish, conditions_i%H2conc
        do i=1, 4
            read(file_units%input,*) rubbish, conditions_i%CHxconc(i), conditions_i%CHx_s(i), conditions_i%CHx_g(i)
        end do
        read(file_units%input,*) rubbish, conditions_i%C2a_conc
        read(file_units%input,*) rubbish, conditions_i%C2X_conc
        read(file_units%input,*) rubbish, conditions_i%C2H2_conc, conditions_i%C2H2_s

        close(file_units%input)

    end subroutine read_conditions

    subroutine read_energy(energies_i)
        !Modules
        use types, only : ahhr_type
        use enumerate, only : file_units
        use fileIO, only : file_exists
        !Arguments
        type(energy_ftype), intent(inout) :: energies_i
        !Variables
        character(150) :: rubbish
        integer :: i
        real(dp) :: prexp, ebarrier
        !Main Method
        if (file_exists("energy.input")) then
            open(unit=file_units%input, file="energy.input", action='read', status='old')
        else
            write(6,*) "energy.input not found!"
            stop
        end if

        read(file_units%input,*) rubbish    !Title
        read(file_units%input,*) rubbish    !-----
        read(file_units%input,*) rubbish    !H abstraction
        read(file_units%input,*) energies_i%Ak1,energies_i%Ek1
        read(file_units%input,*) rubbish    !H2 addition
        read(file_units%input,*) energies_i%Akm1,energies_i%Ekm1
        read(file_units%input,*) rubbish    !H addition
        read(file_units%input,*) energies_i%Ak2
        read(file_units%input,*) rubbish    !---MIGRATION---
        read(file_units%input,*) rubbish    !A / s^-1  E / Jmol^-1
        read(file_units%input,*) rubbish    !DIMER
        read(file_units%input,*) energies_i%A_dmig,energies_i%E_dmig
        read(file_units%input,*) rubbish    !-GAP-
        read(file_units%input,*) rubbish, energies_i%A_gmig1f, energies_i%E_gmig1f
        read(file_units%input,*) rubbish, energies_i%A_gmig1r, energies_i%E_gmig1r
        read(file_units%input,*) rubbish, energies_i%A_gmig2f, energies_i%E_gmig2f
        read(file_units%input,*) rubbish, energies_i%A_gmig2r, energies_i%E_gmig2r
        read(file_units%input,*) rubbish    !-DOWN-
        read(file_units%input,*) energies_i%A_down_mig, energies_i%E_down_mig
        read(file_units%input,*) rubbish    !BetaScission
        read(file_units%input,*) energies_i%EbetaScission
        read(file_units%input,*) rubbish    !--------------------
        read(file_units%input,*) rubbish    !ACETYLENE ADSORPTION
        read(file_units%input,*) rubbish    !--------------------
        read(file_units%input,*) rubbish    !MECHANISM 1 (mono-radical A1) Skokov1995
        read(file_units%input,*) energies_i%acetylene_ads%M1%A,energies_i%acetylene_ads%M1%E
        read(file_units%input,*) rubbish    !MECHANISM 2 (bi-radical B5) Skokov1995
        read(file_units%input,*) energies_i%acetylene_ads%M2%A,energies_i%acetylene_ads%M2%E
        read(file_units%input,*) rubbish    !MECHANISM 3a (bi-radical C) Skokov1995
        read(file_units%input,*) energies_i%acetylene_ads%M3a%A,energies_i%acetylene_ads%M3a%E
        read(file_units%input,*) rubbish    !MECHANISM 3b (bi-radical C) Skokov1995
        read(file_units%input,*) energies_i%acetylene_ads%M3b%A,energies_i%acetylene_ads%M3b%E
        read(file_units%input,*) rubbish    !Etch CH3 (C-C bond enthalpy)
        read(file_units%input,*) energies_i%acetylene_ads%etchCH3%A,energies_i%acetylene_ads%etchCH3%E
        read(file_units%input,*) rubbish    !ETCH C2H2 (random guess) based on [H]
        read(file_units%input,*) energies_i%acetylene_ads%etchC2H2%A,energies_i%acetylene_ads%etchC2H2%E
        read(file_units%input,*) rubbish    !----------------------
        read(file_units%input,*) rubbish    !C2H2 PENDANT REACTIONS
        read(file_units%input,*) rubbish    !----------------------
        read(file_units%input,*) rubbish    !C2H2 MIGRATION
        read(file_units%input,*) rubbish, energies_i%C2H2p_nreactions
        allocate(energies_i%C2H2p_mig(energies_i%C2H2p_nreactions))
        read(file_units%input,*) rubbish    !R    A (/s) E (kJ/mol)
        do i=1,energies_i%C2H2p_nreactions ! N reactions spread over 2N lines (forward, reverse)
            read(file_units%input,*) rubbish, prexp, ebarrier !Label A E
            energies_i%C2H2p_mig(i)%f= ahhr_type(A=prexp, E=ebarrier)
            read(file_units%input,*) rubbish, prexp, ebarrier !Label A E
            energies_i%C2H2p_mig(i)%r = ahhr_type(A=prexp, E=ebarrier)
        end do
        read(file_units%input,*) rubbish !C2H2p to dimer (non-isolated)
        read(file_units%input,*) rubbish !A (/s) E(kJ/mol)
        read(file_units%input,*) energies_i%c2h2p_dimer%A, energies_i%c2h2p_dimer%E


        close(file_units%input)
    end subroutine read_energy


    subroutine read_etching(etching_i)
        !Modules
        use types, only : ahhr_type
        use fileIO, only : file_exists
        use enumerate, only : file_units, edge_state
        !Arguments
        type(etching_ftype), intent(inout) :: etching_i
        !Variables
        character(150) :: rubbish   !Data to be ignored
        integer :: irate,ireaction  !ireaction, isubreaction
        integer :: n, i             !reaction read in, sub-reaction read in
        real(dp) :: fA, fE, rA, rE  !Forward and reverse pre-exp factors and energy barrier values
        !Main Method
        if (file_exists(etching_i%input_fname)) then
            open(unit=file_units%input, file=etching_i%input_fname, action='read', status='old')
        else
            write(6,'(13a,a)') etching_i%input_fname, " not found!"
            stop
        end if

        read(file_units%input,*) rubbish    !ENERGETICS OF ETCHING INPUT FILE FOR DIAMOND LATTICE
        read(file_units%input,*) rubbish    !------------
        read(file_units%input,*) rubbish    !PREFERENTIAL ETCHING (prf)
        read(file_units%input,*) rubbish    !ENV| A(s), E(Jmol-1)
        do i=edge_state%isolated, edge_state%terrace !Isolated, SA, SB, SAB, Terraced
            read(file_units%input,*) rubbish, etching_i%flex_etch(i)%A, etching_i%flex_etch(i)%E
        end do
        read(file_units%input,*) rubbish, etching_i%flex_etch_3rdbond_factor !3rd Bond etch rate factor
        read(file_units%input,*) rubbish    !ISOLATED DIMER ETCH (ide) [isolated dimer etch]
        read(file_units%input,*) rubbish    !R , n, i, fA      , fE      , rA      , rE
        do ireaction=1,5
            !12-j-i
            do irate=1, etching_i%n_ide_subreactions(ireaction)
                read(file_units%input, *) rubbish, n, i, fA, fE, rA, rE
                if (ireaction == n .and. irate == i) then
                    etching_i%ide_ahhr(n,i)%f = ahhr_type(A=fA, E=fE)
                    etching_i%ide_ahhr(n,i)%r = ahhr_type(A=rA, E=rE)
                else
                    write(6,'(a,a,a,i1,a1,i1)') "Expected ", etching_i%input_fname, " layout not found: 12-", ireaction, "-", irate
                    STOP
                end if
            end do
        end do
        read(file_units%input,*) rubbish, rubbish, rubbish, etching_i%explicit_rate_input !Set rates explicitly?
        if (etching_i%explicit_rate_input) then
            read(file_units%input,*) rubbish !R n rate (s-1)
            do irate=1, etching_i%n_reactions
                read(file_units%input,*) rubbish, rubbish, etching_i%explicit_rates(irate)
            end do
        else
            etching_i%explicit_rates(:) = 0.0_dp
        end if

        close(file_units%input)

    end subroutine read_etching

    subroutine read_reactions(reactions_i, conditions_i)
        !Modules
        use enumerate, only : file_units
        use fileIO, only : file_exists
        !Arguments
        type(reactions_ftype), intent(inout) :: reactions_i
        type(conditions_ftype), intent(inout) :: conditions_i
        !Variables
        character(150) :: rubbish
        !Main Method
        if (file_exists("reactions.input")) then
            open(unit=file_units%input,file="reactions.input",action='read',status='old')
        else
            write(6,*) "reactions.input not found!"
            stop
        end if

        read(file_units%input,*) rubbish    !Title
        read(file_units%input,*) rubbish    !-----
        read(file_units%input,*) rubbish    !--------------
        read(file_units%input,*) rubbish    !Actice Species
        read(file_units%input,*) rubbish    !--------------
        read(file_units%input,*) rubbish, reactions%active_species%CH3
        read(file_units%input,*) rubbish, reactions%active_species%CH2
        read(file_units%input,*) rubbish, reactions%active_species%CH
        read(file_units%input,*) rubbish, reactions%active_species%C
        read(file_units%input,*) rubbish, reactions%active_species%C2a
        read(file_units%input,*) rubbish, reactions%active_species%C2X
        read(file_units%input,*) rubbish, reactions%active_species%C2H2
        read(file_units%input,*) rubbish    !---------
        read(file_units%input,*) rubbish    !MODELS
        read(file_units%input,*) rubbish    !---------
        read(file_units%input,*) rubbish    !Radical Site Density
        read(file_units%input,*) reactions_i%RadSiteDensity
        read(file_units%input,*) rubbish    !Defect Density
        read(file_units%input,*) reactions_i%DefectDensity
        read(file_units%input,*) rubbish    !---------
        read(file_units%input,*) rubbish    !REACTIONS
        read(file_units%input,*) rubbish    !---------
        read(file_units%input,*) rubbish, reactions%active%surf_activation_on   !Activate Surface
        read(file_units%input,*) rubbish    !INSERTION
        read(file_units%input,*) rubbish, reactions%active%InsertTroughCHX_on, reactions%trough_insertion_scale  !TROUGH
        read(file_units%input,*) rubbish, reactions%active%InsertDimerCHX_on, reactions%dimer_insertion_scale    !DIMER
        read(file_units%input,*) rubbish    !ETCHING
        read(file_units%input,*) rubbish, reactions%active%pref_etch_C_on
        read(file_units%input,*) rubbish, reactions%isolated_TN_threshold
        read(file_units%input,*) rubbish, reactions_i%active%etch_CHx ! etchCHx
        read(file_units%input,*) rubbish,   reactions%active%ide_etch_on(1), &
                                            reactions%active%ide_etch_on(2), &
                                            reactions%active%ide_etch_on(3), &
                                            reactions%active%ide_etch_on(4), &
                                            reactions%active%ide_etch_on(5)
        !Determine how ide rates operate
        if (reactions_i%active%ide_etch_on(1)) then
            !ide-1 (dimer breaking) is active as well as any following reactions -> individual mode
            reactions_i%ide_individual_active= .true.
        else
            reactions_i%ide_individual_active= .false.
        end if
        write(file_units%log, *) "ide_individual_active: ", reactions_i%ide_individual_active
        read(file_units%input,*) rubbish    !MIGRATION
        read(file_units%input,*) rubbish, reactions%active%dimer_migration_on
        read(file_units%input,*) rubbish, reactions%active%gap_mig_on
        read(file_units%input,*) rubbish, reactions%active%gap_mig_ind(1), &
                                          reactions%active%gap_mig_ind(2), &
                                          reactions%active%gap_mig_ind(3), &
                                          reactions%active%gap_mig_ind(4)
        if (reactions%active%gap_mig_on .eqv. .false.) reactions%active%gap_mig_ind = .false.
        read(file_units%input,*) rubbish, reactions%active%gap_mig_down_on
        read(file_units%input,*) rubbish, reactions%active%C2H2p_mig(1), reactions%active%C2H2p_mig(2)
        read(file_units%input,*) rubbish, reactions%active%C2H2p_dimer
        read(file_units%input,*) rubbish    !Migration critical nucleus
        read(file_units%input,*) reactions_i%mig_crit_nuc
        read(file_units%input,*) rubbish    !Dimer Alignment
        read(file_units%input,*) reactions_i%align_dimers, reactions_i%d_align_range
        read(file_units%input,*) rubbish    !Dimer Realignment
        read(file_units%input,*) reactions_i%realign_dimers
        read(file_units%input,*) rubbish    !DIMER ISOLATION
        read(file_units%input,*) rubbish    !Dimer row isolatation range
        read(file_units%input,*) reactions_i%dimer_row_isolation_range
        if (reactions_i%dimer_row_isolation_range > 2 .or. reactions_i%dimer_row_isolation_range < 1) then
            write(*,*) "Invalid dimer isolation range: [1,2]:", reactions_i%dimer_row_isolation_range
        end if
        read(file_units%input,*) rubbish    !single carbons prevent isolation
        read(file_units%input,*) reactions_i%single_c_prevent_iso
        !Output isolation conditions to log
        if (reactions_i%single_c_prevent_iso) then
            write(file_units%log,'(a,i1,a,a)') "Dimer isolation: carbon within ", &
                    reactions_i%dimer_row_isolation_range," site", &
                    merge("s"," ",reactions_i%dimer_row_isolation_range == 2)
        else
            write(file_units%log,'(a,i1,a,a)') "Dimer isolation: 2 carbons within ", &
                    reactions_i%dimer_row_isolation_range," site", &
                    merge("s"," ",reactions_i%dimer_row_isolation_range == 2)
        end if
        read(file_units%input,*) rubbish    !--------------
        read(file_units%input,*) rubbish    !C2H2 REACTIONS
        read(file_units%input,*) rubbish    !--------------
        read(file_units%input,*) rubbish, reactions%active%acetylene%M1
        read(file_units%input,*) rubbish, reactions%active%acetylene%M2
        read(file_units%input,*) rubbish, reactions%active%acetylene%M3a
        read(file_units%input,*) rubbish, reactions%active%acetylene%M3b
        read(file_units%input,*) rubbish, reactions%active%acetylene%migC2H2_birad
        read(file_units%input,*) rubbish    !Etching (C2H2)
        read(file_units%input,*) rubbish,rubbish,rubbish,rubbish, reactions%active%acetylene%etchCH3
        read(file_units%input,*) rubbish, rubbish, reactions%active%acetylene%etchC2H2

        if (reactions%active_species%CH3 .eqv. .false.) conditions_i%CHxconc(1) = 0.0_dp
        if (reactions%active_species%CH2 .eqv. .false.) conditions_i%CHxconc(2) = 0.0_dp
        if (reactions%active_species%CH .eqv. .false.) conditions_i%CHxconc(3) = 0.0_dp
        if (reactions%active_species%C .eqv. .false.) conditions_i%CHxconc(4) = 0.0_dp
        if (reactions%active_species%C2a .eqv. .false.) conditions_i%C2a_conc = 0.0_dp
        if (reactions%active_species%C2X .eqv. .false.) conditions_i%C2X_conc = 0.0_dp
        if (reactions%active_species%C2H2 .eqv. .false.) conditions_i%C2H2_conc = 0.0_dp

        close(file_units%input)

    end subroutine read_reactions

    !Read output.input
    subroutine read_outputs(simulation, outputs_i)
        !Modules
        use types, only : sim_type
        use variables, only : add_unit_to_open_list, counters, NN_dist
        use enumerate, only : file_units, tRates
        use fileIO, only : file_exists
        !Arguments
        type(sim_type), intent(inout) :: simulation
        type(outputs_ftype), intent(inout) :: outputs_i
        !Variables
        character(150) :: rubbish
        character(10) :: position, status
        integer :: i, ilayer
        logical :: write_h
        !Main Method
        if (file_exists("output.input")) then
            open(unit=file_units%input, file='output.input')
        else
            write(6,*) "output.input not found!"
            stop
        end if

        read(file_units%input,*) rubbish    !Title
        read(file_units%input,*) rubbish    !-----
        read(file_units%input,*) rubbish, outputs_i%L%event_csv
        read(file_units%input,*) rubbish    !Output Timestep
        read(file_units%input,*) outputs_i%timestep
        read(file_units%input,*) rubbish    !Bookmark Timestep
        read(file_units%input,*) outputs_i%bmark_timestep
        read(file_units%input,*) rubbish    !Visualisation,scale_z_mode,highlight last frame, nlookdown
        read(file_units%input,*) outputs_i%L%kmc_vis, outputs_i%height_scale_mode, outputs_i%highlight_last_frame, outputs_i%nlookdown
        if (outputs_i%nlookdown < 2) then
            write(file_units%warning,*) "Invalid nlookdown value passed, defaulting to 4"
            outputs_i%nlookdown = 4
        end if
        read(file_units%input,*) rubbish    !vis start time, vis end_time, scale_delay
        read(file_units%input,*) outputs_i%kmc_out%start, outputs_i%kmc_out%end
        read(file_units%input,*) rubbish !Values @ X inputs
        read(file_units%input,*) outputs_i%values_at_x_s%num_values !Number of input values
        allocate(outputs_i%values_at_x_s%times(outputs_i%values_at_x_s%num_values))
        do i=1, size(outputs_i%values_at_x_s%times)
            read(file_units%input, *) outputs_i%values_at_x_s%times(i)
        end do
        read(file_units%input,*) rubbish    !Additional Outputs
        read(file_units%input,*) rubbish,outputs_i%L%act_fraction
        read(file_units%input,*) rubbish, outputs_i%L%rate_reg
        read(file_units%input,*) rubbish, outputs_i%L%dimer_count
        read(file_units%input,*) rubbish, outputs_i%L%surf_connect
        read(file_units%input,*) rubbish, outputs_i%L%dt
        read(file_units%input,*) rubbish, outputs_i%L%height
        read(file_units%input,*) rubbish, outputs_i%L%roughness,outputs_i%L%roughness_NN
        read(file_units%input,*) rubbish, outputs_i%L%atom_envs
        read(file_units%input,*) rubbish, outputs_i%L%growth_rate
        read(file_units%input,*) rubbish, outputs_i%L%layers
        read(file_units%input,*) rubbish, outputs_i%L%events_time
        !Read in debugging output file bools
        read(file_units%input,*) rubbish
        read(file_units%input,*) rubbish, outputs_i%L%check_dimer
        read(file_units%input,*) rubbish, outputs_i%L%all_rates
        read(file_units%input,*) rubbish, outputs_i%L%activation
        read(file_units%input,*) rubbish, outputs_i%L%surf_list

        close(file_units%input)

        if (simulation%end%time <= outputs_i%timestep * 10) then
            outputs_i%timestep = simulation%end%time/10.0
            write(file_units%log,'(a,f7.3)') "Output Timestep updated to ", outputs_i%timestep
        end if

        !If highlight_last_frame is false
        if (outputs_i%highlight_last_frame .eqv. .false.) then
            outputs_i%selection_on = .true.
        end if


        if (outputs_i%kmc_out%end < 0.0) then
            outputs_i%kmc_out%end = simulation%end%time
            outputs_i%kmc_out%minus_1 = .true.
        else
            outputs_i%kmc_out%minus_1 = .false.
        end if

        !-------------------
        !Open required files
        !todo in own subroutine
        !-------------------
        if (simulation%bookmark_mode_active) then
            position = "append"
            status = "unknown"
        else
            position = "asis"
            status = "replace"
        end if
        
        write_h = .not. (simulation%bookmark_mode_active .and. simulation%continue_unfinished)


        if (simulation%bookmark_mode_active) then
            open(unit=file_units%m_output, file="diamond.out", position=position, status=status)
        else
            open(unit=file_units%m_output,file="diamond.out")
        end if
        open(unit=file_units%json_m_output, file="diamond.json", status='replace')
        call add_unit_to_open_list(file_units%m_output)
        call add_unit_to_open_list(file_units%json_m_output)
        if (outputs_i%L%event_csv) then
            open(unit=file_units%event_out,file="events.csv", position=position, status=status)
            call add_unit_to_open_list(file_units%event_out)
        end if
        if (outputs_i%L%act_fraction) then
            open(unit=file_units%act_fract,file="activated_fraction.csv", position=position, status=status)
            call add_unit_to_open_list(file_units%act_fract)
            if (write_h) write(file_units%act_fract,'(4(a10,a1),a10)') &
                    "step",",","activated",",","surf atoms",",","ave_state",",","frac"
        end if
        if (outputs_i%L%rate_reg) then
            open(unit=file_units%rate_list_count,file="rate_reg_tot.csv", position=position, status=status)
            call add_unit_to_open_list(file_units%rate_list_count)
            if (write_h) then
                do i=1, size(counters%rates_set_per_it)
                    write(file_units%rate_list_count, '(i10)', advance='no') i
                    if (i /= size(counters%rates_set_per_it)) then
                        write(file_units%rate_list_count, '(a1)', advance='no') ","
                    end if
                end do
                write(file_units%rate_list_count,*) " "
            end if
            open(unit=file_units%rate_list_sum,file="rate_sums.csv", position=position, status=status)
            call add_unit_to_open_list(file_units%rate_list_sum)
            if (write_h) write(file_units%rate_list_sum,'(a12,5(a1,a16))') &
                    "step",",","time",",","Abs",",","Migrate",",","Etch",",","tot."
        end if
        if (outputs_i%L%dimer_count) then
            open(unit=file_units%dimer_count,file='dimer_count.csv', position=position, status=status)
            call add_unit_to_open_list(file_units%dimer_count)
            if (write_h) write(file_units%dimer_count,'(a12,a1,a10,a1,a10)') "step",",","time/s",",","dimers"
        end if
        if (outputs_i%L%check_dimer) then
            open(unit=file_units%check_dimer,file="check_dimer.txt", position=position, status=status)
            call add_unit_to_open_list(file_units%check_dimer)
        end if
        if (outputs_i%L%all_rates) then
            open(unit=file_units%non_zero_rates, file="non0rates.txt", position=position, status=status)
            call add_unit_to_open_list(file_units%non_zero_rates)
            end if
        if (outputs_i%L%activation) then
            open(unit=file_units%activation_log, file="randomise.log", position=position, status=status)
            call add_unit_to_open_list(file_units%activation_log)
            if (write_h) write(file_units%activation_log,'(5(a10))') "Step","Act.","Deact.", "Breaks","Tot. act."
        end if
        if (outputs_i%L%surf_list) then
            open(unit=file_units%surf_list, file = "surface_list.txt", position=position, status=status)
            call add_unit_to_open_list(file_units%surf_list)
        end if
        if (outputs_i%L%dt) then
            open(unit=file_units%dt, file="dt.csv")
            call add_unit_to_open_list(file_units%dt)
            if (write_h) write(file_units%dt,'(a10,4(a1,a15))') &
                    "Iteration",",","dt (s)",",","time (s)",",","ran",",","rsv[-1]"
        end if
        if (outputs_i%L%height) then
            open(unit=file_units%height, file="height.csv", position=position, status=status)
            call add_unit_to_open_list(file_units%height)
            if (write_h) write(file_units%height,'(a10,3(a3,a16))') &
                    "It.",",","time s",",","height A",",","growth height A"
        end if
        if (outputs_i%L%roughness) then
            open(unit=file_units%roughness, file="roughness.csv", position=position, status=status)
            call add_unit_to_open_list(file_units%roughness)
            if(write_h) write(file_units%roughness, &
                    '(a10,a1,a16,a1,a16,3(a1,f16.1))') "It.",",","time s",",","global",",", &
                                                                            NN_dist%NN4,",", &
                                                                            NN_dist%NN3,",", &
                                                                            NN_dist%NN2
        end if
        if (outputs_i%L%atom_envs) then
            open(unit=file_units%atom_envs, file="atoms_envs.csv", position=position, status=status)
            call add_unit_to_open_list(file_units%atom_envs)
            if (write_h) write(file_units%atom_envs, '(a10,a1,a10,5(a1,a10))') &
                    "It.",",","time s",",","Isolated",",","S_A",",","S_B",",","S_AB",",","Terraced"
        end if
        if (outputs_i%L%growth_rate) then
            open(unit=file_units%growth, file='growth_rate.csv', position=position, status=status)
            call add_unit_to_open_list(file_units%growth)
            if(write_h) write(file_units%growth, '(a10,3(a1,a15))') &
                    "it.",",","time s",",","overall growth rate (m/h)",",","instant growth rate (m/h)"
            open(unit=file_units%dimer_stat, file='dimer_stats.csv', position=position, status=status)
            call add_unit_to_open_list(file_units%dimer_stat)
            if(write_h) write(file_units%dimer_stat, '(a10,3(a1,a15))') &
                    "it.",",","time s",",","dimer alignment",",","dimer fraction"
        end if

        if (outputs_i%L%layers) then
            open(unit=file_units%layers, file='layers.csv', position=position, status=status)
            call add_unit_to_open_list(file_units%layers)
            if(write_h) write(file_units%layers, '(a10,a1,a15,3(a1,a6))', advance='NO') &
                    "it.",",","time s",",","active",",","min",",","max"
            do ilayer=1, 4*simulation%max_height
                write(file_units%layers, '(a4,i3)', advance='no') " ,%l",ilayer-1
            end do
            write(file_units%layers, *) " "
        end if

        if (outputs_i%L%events_time) then
            open(unit=file_units%events_time, file='events_time.csv', position=position, status=status)
            call add_unit_to_open_list(file_units%events_time)
            write(file_units%events_time, '(a10,a1,a15,a1)', advance='NO') "it.",",","time s",","
            do i=1, tRates%nrates
                write(file_units%events_time, '(i10,a1)', advance='no') i, ","
            end do
            write(file_units%events_time, *) " "

        end if

    end subroutine read_outputs



    subroutine read_dimer_file(dimerf_i, file_read)
        !Modules
        use enumerate, only : file_units
        use fileIO, only : file_exists
        !Arguments
        type(dimer_ftype), intent(inout) :: dimerf_i
        logical, intent(out) :: file_read
        !Variables
        character(150) :: rubbish
        !Main Method
        if (file_exists(dimerf_i%fname)) then
            open(unit=file_units%input, file=dimerf_i%fname, action='read', status='old')
            file_read = .true.
        else
            file_read = .false.
            write(file_units%log, *) dimerf_i%fname, " not found, using default values"
            dimerf_i%nb_form_thres = 3
            dimerf_i%incorp_CHx_sat_dimer = .false.
            dimerf_i%sat_dimer_incorp_scale = 1.0_dp
            dimerf_i%hind_trough_deact_factor = 1.0_dp
            dimerf_i%dihyride_hindered = .false.
            dimerf_i%gap_via_dimer = .false.
            return
        end if

        read(file_units%input,*) rubbish    !Title
        read(file_units%input,*) rubbish    !-----
        read(file_units%input,*) rubbish    !FORM DIMER NB THRESHOLD (per atom)
        read(file_units%input,*) dimerf_i%nb_form_thres
        read(file_units%input,*) rubbish    !DIRECT CHx INCORPORATION INTO SATURATED DIMER
        read(file_units%input,*) dimerf_i%incorp_CHx_sat_dimer
        read(file_units%input,*) rubbish    !CHx incorporation rate scaling
        read(file_units%input,*) dimerf_i%sat_dimer_incorp_scale
        read(file_units%input,*) rubbish    !HINDERED DEACTIVATION MODIFIER
        read(file_units%input,*) dimerf_i%hind_trough_deact_factor
        read(file_units%input,*) rubbish    !DIHYDRIDE TROUGH IS HINDERED
        read(file_units%input,*) dimerf_i%dihyride_hindered
        read(file_units%input,*) rubbish    !ALLOW GAP MIGRATION VIA DIMER
        read(file_units%input,*) dimerf_i%gap_via_dimer
        read(file_units%input,*) rubbish    !RESTRICT BREAKING OF ISO DIMERS
        read(file_units%input,*) dimerf_i%iso_dimer_can_break
        read(file_units%input,*) rubbish    !ADJ DIMER BREAKING, probability
        read(file_units%input,*) dimerf_i%random_adj_dimer_breaking, dimerf%prob_adj_dimer_break
        read(file_units%input,*) rubbish    !DIMER BREAK MODEL
        read(file_units%input,*) dimerf_i%dimer_break_model

        if (.not. ANY(['netto ', 'skokov'] == trim(dimerf_i%dimer_break_model))) then
            write(*,*) trim(dimerf_i%dimer_break_model), ' is not a valid dimer break model'
            ERROR STOP
        end if

    end subroutine


    !Read in last_event atom colours from file
    subroutine read_atom_colours(file_read, colourise_i)
        !Modules
        use types, only : rgb_type
        use enumerate, only : file_units, tRates
        use fileIO, only : file_exists
        !Arguments
        type(event_colour_type), allocatable, intent(inout) :: colourise_i(:)
        logical, intent(out) :: file_read
        !Variables
        character(150) :: rubbish
        integer :: red, green, blue !0-255
        integer :: i, n_reactions
        !Main Method
        if (file_exists("colours.input")) then
            open(unit=file_units%input, file="colours.input", action='read', status='old')
            file_read = .true.
            read(file_units%input,*) rubbish
            read(file_units%input,*) rubbish
            read(file_units%input,*) rubbish !EVENT R G B
            read(file_units%input,*) n_reactions
            allocate(colourise(-1:n_reactions))
            do i=1,n_reactions
                read(file_units%input,*) rubbish, rubbish, red, green, blue
                if (red < 0.0_dp .or. green < 0.0_dp .or. blue < 0.0_dp) then
                    colourise(i) = event_colour_type(rgb_type(144/255.0, 144/255.0, 144/255.0), .false.)
                else
                    colourise(i) = event_colour_type(rgb_type(red/255.0, green/255.0, blue/255.0), .true.)
                end if
            end do
            !Read dimer alignment into -1
            read(file_units%input,*) rubbish, rubbish, red, green, blue
            if (red < 0.0_dp .or. green < 0.0_dp .or. blue < 0.0_dp) then
                colourise(-1) = event_colour_type(rgb_type(144/255.0, 144/255.0, 144/255.0), .false.)
            else
                colourise(-1) = event_colour_type(rgb_type(red/255.0, green/255.0, blue/255.0), .true.)
            end if

            close(file_units%input)

        else !Revert to default values on missing atom_colours.input file
            write(file_units%log, *) "atom_colours.input not present, using default values"
            file_read = .false.
            allocate(colourise_i(tRates%nrates))
            colourise_i = event_colour_type(rgb_type(144, 144, 144), .false.)
        end if


    end subroutine read_atom_colours

end module input
