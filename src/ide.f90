!Contain methods for computing quanties related to isolated dimer etching (ide)
!Isolated Dimer Etching includes reactions such as C2H2 migration and dimer reformation
module ide

    use kinds, only : dp

    implicit none

    logical :: ide_permutations(16,4) = reshape( &
            [ .true., .true., .true., .true., &
            .true., .true., .true., .false., &
            .true., .true., .false., .true., &
            .true., .true., .false., .false., &
            .true., .false., .true., .true., &
            .true., .false., .true., .false., &
            .true., .false., .false., .true., &
            .true., .false., .false., .false., &
            .false., .true., .true., .true., &
            .false., .true., .true., .false., &
            .false., .true., .false., .true., &
            .false., .true., .false., .false., &
            .false., .false., .true., .true., &
            .false., .false., .true., .false., &
            .false., .false., .false., .true., &
            .false., .false., .false., .false. ], &
           shape=[16,4], order=[2,1])


contains

    !------------------------------------------------------------------------
    !Calculate arrhenius rate constants for 12-n-i() both forward and reverse
    !Taking ahhrenius constants for a subset (n) of reactions (i), rates of
    !reactions for each subreaction (i) are returned
    !------------------------------------------------------------------------
    subroutine ide_k_all_fr(etching_i, conditions_i, n, n_i_ahhr, k_n_i_fr)
        !Modules
        use types, only : etching_ftype, ahhr_fr_type, fr_k_type, conditions_ftype
        use arrhenius, only : k_arrhenius
        use variables, only : Rgas
        !Arguments
        type(etching_ftype), intent(in) :: etching_i
        type(conditions_ftype), intent(in) :: conditions_i
        type(ahhr_fr_type), intent(in) :: n_i_ahhr(:) !(n)_i_ahhr(i) !Slice of 2D array
        integer, intent(in) :: n    !12-n-1 reaction to compute
        type(fr_k_type), intent(out) :: k_n_i_fr(:)    !12-n-(i) !Slice of 2D array
        !Variables
        integer :: i !Subreaction index 12-n-i

        do i=1,etching_i%n_ide_subreactions(n)
            !Rgas factor is due to E values being in Kelvin [Netto2005]
            k_n_i_fr(i)%f = k_arrhenius(n_i_ahhr(i)%f%A, Rgas*n_i_ahhr(i)%f%E, conditions_i%Ts)
            k_n_i_fr(i)%r = k_arrhenius(n_i_ahhr(i)%r%A, Rgas*n_i_ahhr(i)%r%E, conditions_i%Ts)
        end do

    end subroutine ide_k_all_fr


    !---------------------------------------------------------------------------------
    !Calculate the forward and reverse reaction rate of reaction n, passed the forward
    !and reverse rates of its subreactions.
    !Equations sourced from:
    !A. Netto and M. Frenklach, Diam. Relat. Mater., 2005, 14, 1630–1646.
    !---------------------------------------------------------------------------------
    pure subroutine ide_k_12_n(k_fr, n, k)
        !Modules
        use types, only : etching_ftype, fr_k_type, ahhr_fr_type
        !Arguments
        type(fr_k_type), intent(in) :: k_fr(:)
        integer, intent(in) :: n !Selected reaction 12-n
        real(dp), intent(out) :: k !Forward rate 12-n
        !Variables
        real(dp) :: top, bottom   !Top and bottom components of the overall rate equation

        select case(n)
            case(1)
            top = k_fr(1)%f * k_fr(2)%f * k_fr(3)%f * k_fr(4)%f
            bottom = (k_fr(1)%r + k_fr(2)%f) * k_fr(3)%f * k_fr(4)%f + (k_fr(3)%r + k_fr(4)%f) * k_fr(1)%r * k_fr(2)%r
            case(2)
            top = k_fr(1)%f * k_fr(2)%f * k_fr(3)%f
            bottom = k_fr(1)%r * k_fr(2)%r + k_fr(1)%r * k_fr(3)%f + k_fr(3)%f * k_fr(2)%f
            case(3)
            top = k_fr(1)%f * k_fr(2)%f * k_fr(3)%f * k_fr(4)%f * k_fr(5)%f
            bottom = (k_fr(4)%r + k_fr(5)%f) * k_fr(1)%r * k_fr(2)%r * k_fr(3)%r + &
                     (k_fr(1)%r * k_fr(2)%r + k_fr(2)%f * k_fr(3)%f) * k_fr(4)%f * k_fr(5)%f
            case(4)
            top = k_fr(1)%f * k_fr(2)%f * k_fr(3)%f * k_fr(4)%f * k_fr(5)%f * k_fr(6)%f
            bottom = (k_fr(5)%r + k_fr(6)%f) * k_fr(2)%r * k_fr(3)%r * k_fr(4)%r + &
                     (k_fr(1)%r * k_fr(2)%r * k_fr(4)%f + k_fr(1)%r * k_fr(2)%r * k_fr(4)%f + k_fr(1)%r * k_fr(3)%f * k_fr(4)%f + k_fr(2)%f * k_fr(3)%f * k_fr(4)%f) * k_fr(5)%f * k_fr(6)%f
            case(5)
            top = k_fr(1)%f * k_fr(2)%f * k_fr(3)%f * k_fr(4)%f * k_fr(5)%f
            bottom = (k_fr(1)%r + k_fr(2)%f) * (k_fr(2)%r * (k_fr(3)%r * k_fr(4)%r + k_fr(3)%r * k_fr(5)%f + k_fr(4)%f * k_fr(5)%f) + k_fr(3)%f * k_fr(4)%f * k_fr(5)%f) + k_fr(2)%f * k_fr(2)%r * k_fr(4)%f * k_fr(4)%r
            case default
            top = 0
            bottom = 1
        end select

        k = top/bottom

    end subroutine ide_k_12_n

    !----------------------------------------------------------------------------
    !Calculate the overall rate constant for chaining subsequent reactions of
    !12-1 and 12-n. In the case of n == 1, the rate of 12-1 is returned unchanged
    !----------------------------------------------------------------------------
    real(dp) function ide_k_n_comb(etching_i, n, k_12_1, k_12_n)
        !Modules
        use types, only : etching_ftype
        !Arguments
        type(etching_ftype), intent(in) :: etching_i
        integer, intent(in) :: n        !12-n reaction to be calculated
        real(dp), intent(in) :: k_12_1  !Rate of 12-1
        real(dp), intent(in) :: k_12_n  !Rate of 12-n

        if (n < 1 .or. n > etching_i%n_reactions) then
            write(6,*) "Invalid n value", n, "passed to ide_k_n_comb when calculating &
                       & the overall rate of chaining 12-1 with 12-n (n=1,5)"
            STOP
        end if

        if (n == 1) then
            ide_k_n_comb = k_12_1
        else
            ide_k_n_comb = ((1/k_12_1) + (1/k_12_n)) ** (-1)
        end if

    end function ide_k_n_comb

    !-----------------------------------------------------------------------------------
    !Return atom sites which will be included in a rate_type element during rate setting
    !Even if only 1 atom should be etched (12-2), both atom sites will be returned as
    !the breaking of the dimer is key even if it's not removed.
    !If the passed atom is neither a pendant nor part of a dimer: return unitialised
    !atom_type and dimer and pendant flags set to false.
    !-----------------------------------------------------------------------------------
    subroutine ide_sites(atom, atoms, atom1, atom2, are_dimer, are_pendant)
        !Modules
        use types, only : atom_type, atom_type_ext
        use variables, only : atom_pair, linked_atom, read_dimer
        use pendant, only : is_pendant
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        type(atom_type), intent(out) :: atom1, atom2
        logical, intent(out) :: are_dimer, are_pendant

        atom1 = atom

        if (read_dimer(atom,atoms)) then
            are_dimer = .true.
            are_pendant = .false.
            atom2 = atom_pair(atom, atoms)
        else
            if (is_pendant(atom, atoms)) then
                are_dimer = .false.
                are_pendant = .true.
                atom2 = linked_atom(atom, atoms)
            else
                are_dimer = .false.
                are_pendant = .false.
                atom2 = atom_type()
            end if
        end if

    end subroutine ide_sites


    !--------------------------------------------------------------------
    !Transform an isolated dimer into a pendant C2H2
    !Reaction 12-1 from
    !A. Netto and M. Frenklach, Diam. Relat. Mater., 2005, 14, 1630–1646.
    !--------------------------------------------------------------------
    subroutine execute_ide_1_transformation(rate_to_execute, kmc_info, atoms)
        !Modules
        use types, only : rate_type, kmc_type, atom_type, atom_type_ext
        use variables, only : read_atom, set_last_frame, link_atoms
        use periodic, only : num_bonds, ret_neighbour
        use enumerate, only : actstate_min, not_activated, types_of_atom
        use dimer, only : create_dimer
        use activate, only : activate_atom
        use surface_m, only : remove_surface_atom, add_surface_atom
        use pendant, only : patom2_store
        !Arguments
        type(rate_type), intent(in) :: rate_to_execute
        type(kmc_type), intent(inout) :: kmc_info
        type(atom_type_ext), intent(inout) :: atoms(:)

        !Variables
        type(atom_type) :: base_atom, top_atom

        base_atom = rate_to_execute%site2

        !Remove both surface atoms
        call remove_surface_atom(base_atom, kmc_info, atoms)
        call remove_surface_atom(rate_to_execute%site, kmc_info, atoms)

        !Add base atom
        call add_surface_atom(base_atom, types_of_atom%surf_carb, atoms)

        !Add pendant atom above base atom
        top_atom = patom2_store(base_atom, atoms)
        call add_surface_atom(top_atom, types_of_atom%acetyl_c2, atoms)
        call link_atoms(base_atom, top_atom, atoms)

        !Dimer below etched atom
        call create_dimer(ret_neighbour(rate_to_execute%site,2), &
                          ret_neighbour(rate_to_execute%site,3),atoms)

        !Set last frame for both atoms
        call set_last_frame(base_atom, atoms, .true., rate_to_execute%event)
        call set_last_frame(top_atom, atoms, .true., rate_to_execute%event)


    end subroutine execute_ide_1_transformation


    !-------------------------------------------------------------------
    !Set the rate for the reformation of a dimer (non-isolated) from a
    !pendant C2H2
    !Mechanism from
    !S. Skokov, B. Weiner, M. Frenklach, T. Frauenheim and M. Sternberg,
    !Phys. Rev. B, 1995, 52, 5426–5432. Fig. 4, Table II
    !Loops through the possible sites, activation conditions. When a
    !condition is failed, the corresponding flag is set to .false. and
    !the next set of sites is checked
    !Storage in rate_list: site: atom (base atom), site2: final site
    !-------------------------------------------------------------------
    subroutine set_dimer_reformation_rate(atom, atoms, rate_list, ilr)
        use types, only : atom_type_ext, atom_type, rate_type
        use variables, only : ilattice, read_atom, read_dimer, is_act, &
                set_rate
        use enumerate, only : tRates, types_of_atom
        use dimer, only : isolated_dimer
        use pendant, only : is_pendant
        use derived_values, only : kc2h2p_dimer
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        type(rate_type), intent(inout) :: rate_list(:)
        integer, intent(inout) :: ilr
        !Variables
        integer :: if_site
        type(atom_type) :: f_sites(2), act_all(2,1), support_atoms(2,2), pot_dimer_atoms(2,2)
        logical :: rate_set(2) !Could set a rate for this combination of sites, activations..
        rate_set = .true.
        !Starting from structure III = activated C2H pendant adjacent to radical site
        !--------------------------------------
        !       .CH
        !       ||
        !       C    C
        !      / \X /C \
        ! C - C / C\/ \ C
        !  C - S  .S   C
        !X = final site
        !S = supporting atoms (1/2 activated)
        !.CH = activated pendant Carbon
        !--------------------------------------
        !Must be a pendant C2H2
        if (.not. is_pendant(atom, atoms)) return
        !Only rate set using the base of the C2H2 pendant
        if (read_atom(atom, atoms) == types_of_atom%acetyl_c2) return
        call get_c2h2p_dimer_sites(atom, atoms, f_sites, act_all, support_atoms, pot_dimer_atoms)
        do if_site=1, size(f_sites)
            !Can't form an isolated dimer
            if (isolated_dimer(pot_dimer_atoms(if_site,1), pot_dimer_atoms(if_site,2), atoms)) then
                rate_set(if_site) = .false.
                cycle
            end if
            !Filled site must be empty
            if (read_atom(f_sites(if_site), atoms) /= types_of_atom%empty_site) then
                rate_set(if_site) = .false.
                cycle
            end if
            !Carbon 2 of C2H2 pendant must be activated
            if (.not. is_act(act_all(if_site,1), atoms)) then
                rate_set(if_site) = .false.
                cycle
            end if
            !Neither of supporting atoms part of a dimer
            if (read_dimer(support_atoms(if_site,1), atoms) .or. read_dimer(support_atoms(if_site,2), atoms)) then
                rate_set(if_site) = .false.
                cycle
            end if
            !At least 1 of the atoms below the C2H2 should be activated
            if ((.not. is_act(support_atoms(if_site,1), atoms)) .and. (.not. is_act(support_atoms(if_site,2), atoms))) then
                rate_set(if_site) = .false.
                cycle
            end if
        end do


        do if_site=1, size(f_sites)
            if (rate_set(if_site)) then
                call set_rate(atom, tRates%c2H2p_dimer, kc2h2p_dimer, rate_list, ilr, opt_site=f_sites(if_site))
            end if
        end do

    end subroutine set_dimer_reformation_rate


    !---------------------------------------------------------------------
    !From an input atom (base atom of a C2H2 pendant), return possible
    !final sites for the pendant carbon atom and required activation sites
    !for the C2H2p to collapse to form a surface dimer (non-isolated)
    !The potential dimer reconstruction formed (2 atoms) is also returned,
    !although it is the same as the act_or array
    !---------------------------------------------------------------------
    subroutine get_c2h2p_dimer_sites(atom, atoms, f_sites, act_all, act_or, potential_dimer_atoms)
        !Modules
        use types, only : atom_type, atom_type_ext
        use variables, only : linked_atom
        use periodic, only : get_site_from_N_path, ret_neighbour
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        !Final sites of the pendant carbon
        type(atom_type), intent(out) :: f_sites(2)
        !For each final site, the atoms which should all be activated
        type(atom_type), intent(out) :: act_all(2,1)
        !For each final site, the atoms of which 1 should be activated (supporting atoms)
        type(atom_type), intent(out) :: act_or(2,2)
        !Atoms making up the potential dimer reconstruction created
        type(atom_type), intent(out) :: potential_dimer_atoms(2,2)
        !Variables
        integer :: f_site_paths(2,2) = reshape([1,2, 4,3], shape=[2,2], order=[2,1])
        integer :: ipath

        do ipath=1, size(f_site_paths,1)
            f_sites(ipath) = get_site_from_N_path(atom, f_site_paths(ipath,:))
            act_all(ipath,:) = linked_atom(atom, atoms) !top atom of C2H2 pendant
            act_or(ipath,:) = [ret_neighbour(f_sites(ipath),2), ret_neighbour(f_sites(ipath),3)]
            potential_dimer_atoms(ipath,:) = [atom, f_sites(ipath)]
        end do

    end subroutine get_c2h2p_dimer_sites



    !------------------------------------------------------------------
    !Execute C2H2 pendant forming a (non-isolated) dimer on the surface
    !If rate exists in the rate_list:
    !formed dimer (will be formed between rate_list(:)%site and
    !rate_list(:)%site2, with 'migration' of C2H2 pendant into site2
    !------------------------------------------------------------------
    subroutine execute_c2h2p_dimer_reformation(rate_to_execute, kmc_info, atoms)
        !Modules
        use types, only : rate_type, kmc_type, atom_type, atom_type_ext
        use variables, only : read_atom, set_last_frame, linked_atom
        use periodic, only : num_bonds, ret_neighbour
        use enumerate, only : actstate_min, not_activated, types_of_atom
        use dimer, only : create_dimer
        use surface_m, only : remove_surface_atom, add_surface_atom
        !Arguments
        type(rate_type), intent(in) :: rate_to_execute
        type(kmc_type), intent(inout) :: kmc_info
        type(atom_type_ext), intent(inout) :: atoms(:)
        !Variables
        type(atom_type) :: pen_carbon !Carbon at top of C2H2
        type(atom_type) :: atom, f_site
        integer :: event

        !Unwrap rate_type
        atom = rate_to_execute%site !Dimer atom 1
        f_site = rate_to_execute%site2 !Dimer site (2) (to be filled)
        event = rate_to_execute%event
        pen_carbon = linked_atom(atom, atoms) !Pendant carbon attached to atom

        !Remove pendant atom1
        call remove_surface_atom(pen_carbon, kmc_info, atoms)
        !Add atom to f_site
        call add_surface_atom(f_site, types_of_atom%surf_carb, atoms)
        !Form dimer between atom and f_site
        call create_dimer(atom, f_site, atoms)

        call set_last_frame(atom, atoms, last_frame=.true., event=event)
        call set_last_frame(f_site, atoms, last_frame=.true., event=event)


    end subroutine execute_c2h2p_dimer_reformation


    !-------------------------------------------------------------------------
    !Return isolated-dimer etching rates scaled according to what
    !rates are possible (avail_rates)
    !Calulate the average rate for all available pathways
    !Scale each pathway according to it's fraction of the total
    !- Get the sum and average of available ide rates
    !- Construct an array of factors for each ide rate
    !- Construct scaled k values by combining the average rate with the factor
    !Modified scheme originally reported in:
    !A. Netto and M. Frenklach, Diam. Relat. Mater., 2005, 14, 1630–1646.
    !DOI: 10.1016/j.diamond.2005.05.009
    !-------------------------------------------------------------------------
    function scale_kide(avail_rates, k, dimer_mode) result(scaled_k)
        !Arguments
        logical,    intent(in)  :: avail_rates(4)
        real(dp),   intent(in)  :: k(5)
        logical,    intent(in)  :: dimer_mode
        real(dp)                :: scaled_k(4)
        !Variables
        real(dp) :: k_sum_all, k_sum
        real(dp) :: factors(4)
        integer :: n

        if (all(avail_rates .eqv. .false.)) then
            scaled_k = 0
            return
        end if

        call ide_k_sum_sum_all(avail_rates, k, dimer_mode, k_sum, k_sum_all)

        factors = ide_factors(avail_rates, k, k_sum)

        scaled_k = k_sum_all * factors

    end function scale_kide


    !-------------------------------------------------------------
    !Compute the average time taken for an isolated-dimer etching
    !taking into account the possible pathways available
    !dimer_mode indicates that ide is occurring over a single step
    !and therefore ide-1 must be included in the average
    !-------------------------------------------------------------
    subroutine ide_k_sum_sum_all(rates, k, dimer_mode, k_sum, k_sum_all)
        !Arguments
        logical,    intent(in)  :: rates(4) !What rates (2-5) are possible
        real(dp),   intent(in)  :: k(5) ! Rates of ide-1 - ide-5
        logical,    intent(in)  :: dimer_mode
        real(dp),   intent(out) :: k_sum, k_sum_all
        !Variables
        integer :: n

        !Method

        k_sum = 0.0_dp
        k_sum_all = 0.0_dp
        do n=1,size(rates)
            if (rates(n)) k_sum = k_sum + k(n+1)
        end do

        if (dimer_mode) then
            k_sum_all = (1/k(1) + 1/k_sum)**(-1)
        else
            k_sum_all = k_sum
        end if

    end subroutine ide_k_sum_sum_all


    !---------------------------------------------------
    !Calculate the share that each rate k contributes to
    !the average
    !k(n)/k_sum
    !---------------------------------------------------
    function ide_factors(rates, k, k_sum)
        !Arguments
        logical,    intent(in)  :: rates(4)
        real(dp),   intent(in)  :: k(5)
        real(dp),   intent(in)  :: k_sum
        real(dp)                :: ide_factors(size(rates))
        !Variables
        integer :: n

        ide_factors = 0.0_dp
        do n=1, size(rates)
            if (rates(n)) then
                ide_factors(n) = k(n+1) / k_sum
            end if
        end do

    end function ide_factors


    pure integer function i_kide(array)
        !Arguments
        logical, intent(in) :: array(4)
        !Variables
        integer :: mults(4), zeros(4)
        integer :: i

        mults = [8, 4, 2, 1]
        zeros = 0
        i_kide = 1
        do i=1, size(array)
            i_kide = i_kide + merge(mults(i), zeros(i), array(i))
        end do

    end function i_kide

end module ide