!Store routines relating to C2H2 - migrations, etching etc
!Rate Setting and supporting routines
!Rate Execution
module c2h2

    use types, only : dp

    implicit none


contains


    !------------------------------------------------------------------------------
    !Set rate (if possible) for the migration of C2H2 pendants into an adjacent gap
    !Similar tp CHx gap migration: requirements:
    !biradical site adjacent to C2H2 pendant
    !final site empty
    !No pendants attached to either supporting atoms of final site
    !------------------------------------------------------------------------------
    subroutine set_C2H2p_gap_migrate(atom, atoms, rate_list, ilr)
        !Modules
        use types, only : atom_type, atom_type_ext, rate_type
        use variables, only : set_rate, read_atom, read_linked_atom, read_act
        use enumerate, only : tRates, types_of_atom, actstate_min
        use pendant, only : patom2_store, adjacant_to_pendant, is_linked
        use migration, only : get_gap_mig_sites
        use derived_values, only : kC2H2p_mig
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        type(rate_type), intent(inout) :: rate_list(:)
        integer, intent(inout) :: ilr !Last index set in rate_List
        !Variables
        integer :: isite
        type(atom_type) :: a1_fsites(2), act_sites(2,2)
        !Checks
        if (read_linked_atom(atom, atoms) /= types_of_atom%acetyl_c2) return

        !Method
        call get_gap_mig_sites(atom, a1_fsites, act_sites)
        do isite=1,2
            !Final Site empty, both activation sites met
            if (read_atom(a1_fsites(isite), atoms) == types_of_atom%empty_site) then
                !No pendants attached to supporting atoms
                if ((is_linked(a1_fsites(1),atoms) .eqv. .false.) &
                    .and. (is_linked(a1_fsites(2),atoms) .eqv. .false.)) then
                    if ((read_act(act_sites(isite,1), atoms) >= actstate_min) .and. &
                        (read_act(act_sites(isite,2), atoms) >= actstate_min)) then
                        !Can set rate
                        if (adjacant_to_pendant(a1_fsites(isite), atoms, atom)) then
                            call set_rate(atom, tRates%gapMigrateC2H2_1, kC2H2p_mig(2), rate_list, ilr, a1_fsites(isite))
                        else
                            call set_rate(atom, tRates%gapMigrateC2H2_0, kC2H2p_mig(1), rate_list, ilr, a1_fsites(isite))
                        end if
                    end if
                end if
            end if
        end do

    end subroutine set_C2H2p_gap_migrate

end module c2h2