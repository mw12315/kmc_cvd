module etching

    use kinds,  only : dp

    implicit none

contains

    !-------------------------
    !Set etch rate for an atom
    !-------------------------
    subroutine set_pref_etch_rate(atom, atoms, rate_list, ilr)
        !Modules
        use types, only : atom_type_ext, atom_type, rate_type
        use variables, only : read_atom,set_rate, read_ilink
        use enumerate, only : types_of_atom, tRates
        use input, only : reactions
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), dimension(:), intent(in) :: atoms
        type(rate_type), dimension(:), intent(inout) :: rate_list
        integer, intent(inout) :: ilr
        !Variables
        real(dp) :: rate_value
        integer :: ilink
        !Method
        rate_value = 0.0_dp
        ilink = read_ilink(atom, atoms)
        !Etching of C2H2 atoms (whole or otherwise) is set using a different subroutine
        if (ilink /= 0 .or. read_atom(atom, atoms) == types_of_atom%acetyl_c2) then
            return
        end if
        rate_value = 0.0_dp
        if (reactions%active%pref_etch_C_on) then
            rate_value =  edge_etch_rate(atom, atoms)
        endif

        if (rate_value == 0.0_dp) then
            return
        else
            call set_rate(atom,tRates%pref_etch_CHx,rate_value, rate_list, ilr)
        end if

    end subroutine set_pref_etch_rate


    !------------------------------------------------------------
    !Etch rate set according to the local environment of the atom
    !Isolated, Step Edge, Terrace
    !If more than 2 bonds, then bonding factor is applied (default 0)
    !------------------------------------------------------------
    function edge_etch_rate(atom, atoms) result(value)
        !Modules
        use types, only : atom_type_ext,atom_type
        use variables, only : NN_bulk
        use derived_values, only : pref_etch_rates
        use input, only : etchingf
        use surface_m, only : identify_edge_state
        use periodic, only : num_bonds, count_nearest_atoms
        !Arguments
        type(atom_type),intent(in) :: atom
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        real(dp) :: value
        !Variables
        real(dp) :: bonding_factor  !Increase etch rate according to additional bonding (dimer bond)
        integer :: n_bonds          !C-C bonds
        integer :: n_neighbours     !Bonding neighbours (doesn't include dimer pair)
        !Initialisation
        value = 0.0_dp
        n_bonds = num_bonds(atom, atoms)
        call count_nearest_atoms(atom, atoms, n_neighbours)
        if (n_bonds > 2 .or. n_neighbours > 2) then    !n_neighbours prevents creation of unsupported
            bonding_factor = etchingf%flex_etch_3rdbond_factor   !species through etching of their support below
        else
            bonding_factor = 1.0_dp
        end if
        !-----------
        !Main Method
        !-----------
        if (n_bonds < NN_bulk) then
            value = pref_etch_rates(identify_edge_state(atom, atoms)) * bonding_factor
        end if

    end function edge_etch_rate

    !---------------------------------------------------------------------
    !Etch etching of adsorbed C2H2 molecule (Can be C2H4, C2H4, C2H2, C2H)
    !---------------------------------------------------------------------
    subroutine set_etch_c2h2_rate(atom, atoms, rate_list, ilr)
        !-------
        !Modules
        !-------
        use types,          only : atom_type_ext,atom_type,rate_type
        use variables,      only : read_dimer,read_atom,NN_bulk,set_rate,is_act,read_ilink
        use enumerate,      only : types_of_atom,tRates,no_dimer
        use periodic,       only : count_nearest_atoms,count_TN,ret_neighbour
        use derived_values, only : C2H2_ads_rates
        use input,          only : reactions
        !---------
        !Variables
        !---------
        type(atom_type), intent(in) :: atom
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        type(rate_type),dimension(:),intent(inout) :: rate_list
        integer, intent(inout) :: ilr
        integer :: atom_state, ilink
        type(atom_type) :: base_atom   !Supporting atom of the acetyl_c2 atom
        !-----------
        !Main Method
        !-----------
        atom_state =  read_atom(atom, atoms)
        ilink = read_ilink(atom, atoms)
        !Determine if etching via C2H2 etching is valid - only set etch rates from the 'pendant' atom  to avoid double rate counting
        if (ilink /= 0) then
            if (atom_state == types_of_atom%acetyl_c2) then
                base_atom = atoms(ilink)%atom
                if (is_act(atom, atoms)) then
                    !Etch entire unit from the surface
                    if (reactions%active%acetylene%etchC2H2) then
                        call set_rate(base_atom, tRates%etchC2H2, C2H2_ads_rates%etchC2H2, rate_list, ilr, atom)
                    end if
                else
                    !Etch CH3 (trailing C2) from C2H2 (etch mechanism 1)
                    if (reactions%active%acetylene%etchCH3) then
                        call set_rate(atom, tRates%etchCH3_C2H2, C2H2_ads_rates%etchCH3, rate_list, ilr)
                    end if
                end if
            end if
        end if

    end subroutine set_etch_c2h2_rate


    !-----------------------------------------------------
    !Set rates of etching for Isolated Dimer Etching (ide)
    !12-1 (ide-1) acts on an isolated dimer
    !12-n (n=2,3,4,5) acts on C2H2 pendants
    !If individual ide etching is active. ide-1 is rate set separately
    !Otherwise - ide-n (n=2-5) is included in rate setting
    !Storage in the rate_list:
    !site: atom1 of dimer / base of C2H2p
    !site2: atom2 of dimer / top of C2H2p
    !-----------------------------------------------------
    subroutine set_ide_rate(atom, atoms, rate_list, ilr)
        !Modules
        use types, only : atom_type_ext, atom_type, rate_type
        use variables, only : ilattice, read_atom, read_dimer, read_ilink, &
            set_rate
        use enumerate, only : types_of_atom
        use input, only : reactions
        use ide, only : ide_sites
        use dimer, only : isolated_dimer
        use pendant, only : is_pendant
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        type(rate_type), intent(inout) :: rate_list(:)
        integer, intent(inout) :: ilr
        !Variables
        type(atom_type) :: atom1, atom2
        logical :: are_dimer, are_pendant !atoms are part of a dimer, part of a pendant

        !Retrieve sites associated with the dimer or with the pendant (or neither)
        call ide_sites(atom, atoms, atom1, atom2, are_dimer, are_pendant)

        !--------------
        !ISOLATED DIMER
        !--------------
        !Initial position for ide-1 (or ide-1 + ide-n combination)
        if (are_dimer) then
            if (isolated_dimer(atom1, atom2, atoms)) then
                !Individual ide rates. Requires ide-1 to be set before further reactions (ide-n)
                if (reactions%ide_individual_active) then
                        call set_ide_1_transformation_rate(atom1, atom2, atoms, rate_list, ilr)
                    return
                else
                    !Combined (ide-1 + ide-n (n=2-5)) rate setting
                    !In non-individual mode ide-2 = ide-1 + ide-2
                    call set_ide_n_etch_rate(atom1, atom2, atoms, rate_list, ilr, .true.)
                end if
            else
                !Part of a dimer that isn't isolated - no rate setting possible
                return
            end if
        else
            !Not a dimer, could be a pendant C2H2?
            !If not a pendant, no rate setting possible
            if (.not. are_pendant) return
            !------------
            !C2H2 PENDANT
            !------------
            !Only rate set on the lower carbon
            if (read_atom(atom1, atoms) /= types_of_atom%surf_carb) return
            !Individual ide-n (n=2-5)) rate setting
            !Atom2 passed as site1 as it is garanteed to be etched and
            !site is always etched
            call set_ide_n_etch_rate(atom2, atom1, atoms, rate_list, ilr, .false.)

        end if

    end subroutine set_ide_rate


    !----------------------------------------------------------------
    !Rate setting for ide-1. Transformation of an isolated dimer to a
    !pendant C2H2
    !atom1 of the dimer becomes the top of the C2H2 pendant, atom2
    !the base
    !----------------------------------------------------------------
    subroutine set_ide_1_transformation_rate(atom1, atom2, atoms, rate_list, ilr)
        use types, only : atom_type_ext, atom_type, rate_type
        use variables, only : ilattice, read_atom, read_dimer, read_ilink, &
                set_rate
        use enumerate, only : tRates
        use input, only : reactions
        use dimer, only : isolated_dimer
        use pendant, only : is_pendant
        use derived_values, only : kide
        !Arguments
        type(atom_type), intent(in) :: atom1, atom2
        type(atom_type_ext), intent(in) :: atoms(:)
        type(rate_type), intent(inout) :: rate_list(:)
        integer, intent(inout) :: ilr

        if (reactions%active%ide_etch_on(1)) then
                call set_rate(atom1, tRates%ide_12_1, kide(1), rate_list, ilr, atom2)
            end if

    end subroutine set_ide_1_transformation_rate

    !--------------------------------------------------------------------------------
    !Set rate for ide-n (n=2-5)
    !If individual etching is active, then ide-n operates over an input C2H2 pendant
    !Otherwise, it acts on a C2H2 pendant
    !As access of reaction rates and setting of atom storage in the rate_type
    !are consistent between the 2 options can share the same subroutine
    !Atom 1 and 2 are either atoms of the isolated dimer OR atoms of the C2H2 pendant
    !When ide-n acts on a C2H2 pendant - site1 (the pendant C) is etched
    !when ide-n acts on a isolated dimer - site1 (the 1st atom of the pair) is etched
    !For ide-n (n=3-5) - a dimer must be able to form below site2
    !Etched atoms are stored in rate_site%site
    !DIMER_MODE: the atoms are part of an isolated dimer (allow dimer formation below
    !the atom that might be removed to be checked)
    !Check which rates can be set first before scaling them
    !--------------------------------------------------------------------------------
    subroutine set_ide_n_etch_rate(atom1, atom2, atoms, rate_list, ilr, dimer_mode)
        use types, only : atom_type_ext, atom_type, rate_type
        use variables, only : ilattice, read_atom, read_dimer, read_ilink, &
                set_rate
        use enumerate, only : tRates, types_of_atom
        use periodic, only : ret_neighbour, num_bonds
        use input, only : reactions, dimerf
        use dimer, only : isolated_dimer, can_form_dimer_nb
        use pendant, only : is_pendant
        use derived_values, only : kide_perms
        use ide, only : i_kide
        !Arguments
        type(atom_type), intent(in) :: atom1, atom2
        type(atom_type_ext), intent(in) :: atoms(:)
        type(rate_type), intent(inout) :: rate_list(:)
        integer, intent(inout) :: ilr
        logical, intent(in) :: dimer_mode
        !Variables
        integer :: irate
        logical :: rate_set(4)
        real(dp) :: kide(4)

        rate_set = .false.

        if (dimer_mode) then
            !Dimer must be able to form below atom 1
            if (.not. can_form_dimer_nb(ret_neighbour(atom1, 2), ret_neighbour(atom1, 3), atoms, 2)) return
            do irate=1, size(rate_set)
                if (reactions%active%ide_etch_on(irate+1)) then
                    if(irate == 3) then
                        if (can_form_dimer_nb(ret_neighbour(atom2, 2), ret_neighbour(atom2, 3), atoms, 2)) then
                            rate_set(irate) = .true.
                        end if
                    else
                        rate_set(irate) = .true.
                    end if
                end if
            end do
        else
            !Site 1: pendant carbon (top), side 2: bottom supporting carbon
            do irate=1, size(rate_set)
                if (reactions%active%ide_etch_on(irate+1)) then
                    if (irate == 3) then  !12-4 requires dimer formation after etching of site 2
                        if (can_form_dimer_nb(ret_neighbour(atom2, 2), ret_neighbour(atom2, 3), atoms, 2)) then
                            rate_set(irate) = .true.
                        end if
                    else
                        rate_set(irate) = .true.
                    end if
                end if
            end do
        end if

        kide = kide_perms(i_kide(rate_set), :)
        do irate=1, size(rate_set)
            if (rate_set(irate)) then
                call set_rate(atom1, tRates%ide_12_1 + irate, kide(irate), &
                    rate_list, ilr, atom2)
            end if
        end do


    end subroutine set_ide_n_etch_rate


    !---------------------------------------------------------------------
    !Set etching rate for input atom according to conditions/requirements
    !A. Netto et al., Diam. Relat. Mater., 2005, 14, 1630–1646. [rate]
    !S. Skokov, B et al., J. Phys. Chem., 1995, 99, 5616–5625. [mechanism]
    ! Mechanism appears to hinge on reformation of a dimer following CH2
    ! removal. This is therefore similar to the preferential etching model
    ! of Battaile et al. (1999).
    ! REQUIRES NO ACTIVATION of N2 AND N3
    ! Using the bonding threshold - like dimer breaking
    ! Check that both N2 and N3 atoms don't both have 3 bonds (ignoring
    ! the C-C between N and atom in question:
    !    C
    ! \ / \ /
    !  N3 N2   total bonds = 8; total - N-C bonding = 6
    !  || ||   6 bonds >= 2 * Nb thres => No dimer can be formed below
    !    C
    ! \ / \
    !  N3 N2  total bonds =  7; total - N-C bonding = 5
    !  || ||  5 bonds < 2 * Nb thresh => dimer can be formed - can etch
    ! atom must be a surface carbon and not linked to any other atoms
    ! atom cannot be part of a dimer
    !---------------------------------------------------------------------
    subroutine set_chx_etch_k11_rate(atom, atoms, rate_list, ilr)
        !Modules
        use types, only : atom_type, atom_type_ext, rate_type
        use variables, only : set_rate, read_atom, read_ilink, read_dimer, is_act
        use enumerate, only : tRates, types_of_atom
        use periodic, only : ret_neighbour, num_bonds
        use input, only : dimerf
        use derived_values, only : ketch_CHx
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        type(rate_type), intent(inout) :: rate_list(:)
        integer, intent(inout) :: ilr !Last index set in rate_List
        !Variables
        type(atom_type) :: N2, N3
        N2 = ret_neighbour(atom, 2)
        N3 = ret_neighbour(atom, 3)
        if (is_act(N2, atoms) .or. is_act(N3, atoms)) return
        if ((num_bonds(N2, atoms) + num_bonds(N3, atoms) - 2) >= (2 * dimerf%nb_form_thres)) then
            return
        end if


        if (read_atom(atom, atoms) /= types_of_atom%surf_carb) return
        if (read_ilink(atom, atoms) /= 0) return
        if (read_dimer(atom, atoms) .eqv. .true.) return
        if (num_bonds(atom, atoms) > 2) return

        call set_rate(atom, tRates%etch_chx, ketch_CHx, rate_list, ilr)

    end subroutine set_chx_etch_k11_rate


    !---------------------------------------------------------
    !Execute an etch event
    !The removal or 1 or more carbons from the surface
    ! Handled reactions:
    ! Chx etch/preferential etching - simple removal of 1 atom
    ! Removal of a C2H2 (only) or entire unit
    !---------------------------------------------------------
    subroutine execute_etch(rate_to_execute, kmc_info, atoms)
        !Modules
        use types, only : atom_type, atom_type_ext, rate_type, kmc_type, average_add, same_atom
        use enumerate, only : types_of_atom, file_units
        use variables, only : read_atom, read_ilink, pop_mig, read_mig, set_last_frame
        use periodic, only : ret_neighbour, num_bonds
        use dimer, only : remove_dimer_if_required, dimer_assessment
        use activate, only : activate_atom
        use surface_m, only : remove_surface_atom
        !Arguments
        type(rate_type), intent(in) :: rate_to_execute
        type(kmc_type), intent(inout) :: kmc_info
        type(atom_type_ext), intent(inout) :: atoms(:)
        !Variables
        type(atom_type) :: n2, n3    !Neighbours 2 and 3 of etched atom
        integer :: atom_state, ilink
        type(atom_type) :: linked_atom

        !Main Method
        atom_state =  read_atom(rate_to_execute%site, atoms)
        ilink = read_ilink(rate_to_execute%site, atoms)

        !Add migration counter to migration and remove
        if (read_mig(rate_to_execute%site, atoms) /= 0) call average_add(kmc_info%migs, real(pop_mig(rate_to_execute%site, atoms),dp))
        call remove_surface_atom(rate_to_execute%site, kmc_info, atoms)

        !Carbon etching
        if (atom_state == types_of_atom%surf_carb) then
            if (ilink == 0) then !Single carbon etching
                n2 = ret_neighbour(rate_to_execute%site,2)
                n3 = ret_neighbour(rate_to_execute%site,3)
                !Activate N2 and N3 (supporting atoms)
                call activate_atom(n2, num_bonds(n2, atoms), atoms)
                call activate_atom(n3, num_bonds(n3, atoms), atoms)
            else if (ilink /= 0) then
                !Normal surface carbon etching but there is a linked atom above (Etching entire C2H2 off the surface)
                !Remove linked atom]
                linked_atom = atoms(ilink)%atom
                call remove_surface_atom(linked_atom, kmc_info, atoms)
            end if
        !Etching the top atom of a C2H2 unit
        else if (atom_state == types_of_atom%acetyl_c2) then
            !Activate linked atom below
            if (ilink == 0) then
                write(file_units%error, *) 'execute_etch', 'etched acetyle_c2 not linked', rate_to_execute%site, linked_atom
            else
                call activate_atom(atoms(ilink)%atom, num_bonds(atoms(ilink)%atom, atoms), atoms)
            end if
        end if

        !Post execution tasks
        if (.not. same_atom(rate_to_execute%site2, atom_type(0,0,0,0))) then
            call set_last_frame(rate_to_execute%site2, atoms, .true., rate_to_execute%event)
            call remove_surface_atom(rate_to_execute%site2, kmc_info, atoms)
        end if

        call dimer_assessment(rate_to_execute%site, atoms)

    end subroutine execute_etch

    !-----------------------------------------------------------------
    !Execute an isolated dimer etch (ide) reaction
    !ide_1 transforms an isolated dimer to a pendant C2H2
    !ide_n (n=2-5) etches said pendant C2H2 from the surface to leave
    !either CH2 (ide-2) or nothing remaining on the surface
    !ide-n (n=3-5) result in differing final surface arrangements
    !following C2H2 removal
    !In the case that only 1 atom etches (ide-2), atom1 is etched (pendant)
    !-----------------------------------------------------------------
    subroutine execute_ide_etch(rate_to_execute, kmc_info, atoms)
        !Modules
        use types, only : rate_type, kmc_type, atom_type, atom_type_ext
        use variables, only : read_atom, set_last_frame, set_act_state
        use input, only : reactions
        use periodic, only : num_bonds, ret_neighbour
        use enumerate, only : tRates, actstate_min, not_activated
        use dimer, only : create_dimer
        use activate, only : activate_atom
        use surface_m, only : remove_surface_atom
        use ide, only : execute_ide_1_transformation
        !Arguments
        type(rate_type), intent(in) :: rate_to_execute
        type(kmc_type), intent(inout) :: kmc_info
        type(atom_type_ext), intent(inout) :: atoms(:)
        !Variables
        type(atom_type) :: atom1, atom2, neigh
        integer :: event
        !Unwrap derived type into local variables
        atom1 = rate_to_execute%site
        atom2 = rate_to_execute%site2
        event = rate_to_execute%event

        !No etching, just transformation to C2H2 pendant
        if (event == tRates%ide_12_1) then
            call execute_ide_1_transformation(rate_to_execute, kmc_info, atoms)
            return
        end if

        !-------------------------
        !ide-n act on C2H2 pendant
        !-------------------------
        if (reactions%ide_individual_active) then
            !In all cases, atom1 is removed
            call remove_surface_atom(atom1, kmc_info, atoms)

            !Only atom2 is removed
            if (event == tRates%ide_12_2) then
                call set_last_frame(atom2, atoms, .true., event)
                call set_act_state(atom2, atoms, actstate_min)
            else
                call remove_surface_atom(atom2, kmc_info, atoms)
                !Dimer formation/activation under exposed atoms below etched atom2
                !(atom1, a pendant carbon now longer involved)
                if (event == tRates%ide_12_4) then
                    call create_dimer(ret_neighbour(atom2,2), ret_neighbour(atom2,3), atoms, .true.)
                end if
                !N3 is radical
                neigh = ret_neighbour(atom2, 3)
                call set_act_state(neigh, atoms, actstate_min)
            end if
        else
        !---------------------------
        !ide-n act on isolated dimer
        !---------------------------
            !In all cases, atom1 is removed
            call remove_surface_atom(atom1, kmc_info, atoms)
            !Dimer left below etched atom1
            call create_dimer(ret_neighbour(atom1, 2), ret_neighbour(atom1, 3), atoms, .true.)

            if (event == tRates%ide_12_2) then

                !Atom 2 is left in activated state
                call activate_atom(atom2, num_bonds(atom2, atoms), atoms)

            else !Atom 2 (site 2) also etched (2-atom etch)

                call remove_surface_atom(atom2, kmc_info, atoms)
                !12-4 dimer with lhs activated
                if (event == tRates%ide_12_4) then
                    call create_dimer(ret_neighbour(atom2, 2), ret_neighbour(atom2, 3), atoms, .true.)
                    call set_act_state(ret_neighbour(atom2, 3), atoms, actstate_min)
                else !12-3/12-5 fully hydrogenated rhs carbon, singley activated lhs carbon
                    call set_act_state(ret_neighbour(atom2, 2), atoms, not_activated)
                    call set_act_state(ret_neighbour(atom2, 3), atoms, actstate_min)
                end if
            end if

        end if

    end subroutine execute_ide_etch

end module etching
