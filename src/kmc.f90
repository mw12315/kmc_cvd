! Contain KMC loop and related routines
module kmc

    use kinds,  only : dp

    implicit none

contains

    subroutine kmc_loop(atoms, kmc_info, sim_info, surface_info, segment_info, timings, nucleation)
        !Modules
        use types, only : atom_type_ext, kmc_type, sim_type, surface_type, kmc_timings, &
                rate_type, atom_type, average_add_rates_set, average_reset_rates_set, &
                average_rates_set, average_add
        use input, only : outputs
        use variables, only : reached_max_height, reached_min_height, &
                EPS, counters, reset_last_frame, open_units
        use enumerate, only : file_units, edge_state
        use output, only : output_surface, write_to_layers_file, &
                int_array_as_csv, flush_units
        use surface_m, only : calc_growth_height, surface_stats, get_average_height
        use randomise, only : activate_surface
        use rates, only : call_rate_setting, locate
        use execute_rates, only : execute_event
        use bookmark, only : write_bookmark
        !Arguments
        type(atom_type_ext), dimension(:), intent(inout) :: atoms
        !KMC current time and iteration (step)
        type(kmc_type), intent(inout) :: kmc_info
        !Simulation bounds, end conditions etc. If end conditions are updated (potentially due to nucleation time)
        type(sim_type), intent(inout) :: sim_info
        !Surface info used to generate growth heights, total atoms etc
        type(surface_type), intent(inout) :: surface_info
        type(surface_type), dimension(:), intent(inout) :: segment_info
        !KMC Timings
        type(kmc_timings), intent(inout) :: timings
        !Nucleation or KMC Run
        logical, intent(in) :: nucleation
        !Variables
        logical :: write_events_file
        integer :: zero_rate_count
        real(dp) :: next_bookmark_time
        type(rate_type), allocatable, dimension(:) :: rate_list
        real(dp), allocatable, dimension(:) :: rsv
        real(dp) :: ran, ran1
        real(dp) :: dt
        real(dp) :: t1, t2
        real(dp) :: growth_height
        !Events
        integer :: ichosen_event
        integer :: ilast_rate_set
        !Output
        logical :: xtime_output_active, next_output_time_active
        logical :: surf_stats_called_this_iteration
        real(dp) :: next_output_time
        logical :: fetch_instant_growth_rate
        integer :: return_value
        !Initialisation
        zero_rate_count = 0
        next_output_time = kmc_info%time + outputs%timestep
        next_bookmark_time = kmc_info%time + outputs%bmark_timestep
        ilast_rate_set = 0
        if (nucleation) then
            write_events_file = .false.
        else
            write_events_file = .true.
        end if

        !----------------------------------
        !OUTPUT FRAME 0 in visualisation on
        !----------------------------------
        if (outputs%L%kmc_vis) then
            if (kmc_info%time >= outputs%kmc_out%start .and. (kmc_info%time <= outputs%kmc_out%end .or. outputs%kmc_out%minus_1)) then
                call output_surface(atoms, surface_info, kmc_info, file_units%kmc_vis)
            end if
        end if
        !--------
        !KMC Loop
        !--------
        do
            surf_stats_called_this_iteration = .false.
            kmc_info%step = kmc_info%step + 1
            growth_height = calc_growth_height(surface_info%average_height,surface_info%start_height)
            !Nucleation Time/Height
            if (nucleation) then
                if (growth_height >= sim_info%nucleation_height) then
                    write(file_units%log,'(a,ES10.2E2,a,f10.5,a)') "Reached nucleation height after ",kmc_info%time," s at ",surface_info%average_height," A"

                    !Set sim_info and surface_info to be returned
                    kmc_info%nucleation_height = growth_height
                    kmc_info%nucleation_time = kmc_info%time
                    call surface_stats(atoms, kmc_info, surface_info%gr_height, 0.0_dp, segment_info(:)%gr_height, surface_info, segment_info)
                    surface_info%start_height = surface_info%average_height
                    write(file_units%log,'(a,f8.3,a)') "Actual nucleation growth: ",kmc_info%nucleation_height," A"
                    !Output surface frame to nucleation_frame file
                    open(unit=file_units%nucleation,file="nucleation.xyz")
                    call output_surface(atoms, surface_info, kmc_info, file_units%nucleation)
                    call reset_last_frame(atoms)
                    close(file_units%nucleation)
                    EXIT
                end if
            else
                !---------------
                !Exit conditions
                !---------------
                !Iteration
                if (kmc_info%step > sim_info%end%iteration) then
                    kmc_info%step = kmc_info%step - 1
                    EXIT
                end if
                !Time
                if ((sim_info%end%time - kmc_info%time) <= 0.0_dp) then
                    kmc_info%step = kmc_info%step - 1
                    EXIT
                end if
                !Growth Height
                if (growth_height >= sim_info%end%growth_height &
                & .and. sim_info%end%growth_height >= 0.0_dp) then
                    write(file_units%m_output,'(a,f10.2,a)') "Reached Growth Height Limit (A): ",growth_height," -> Exiting"
                    kmc_info%step = kmc_info%step - 1
                    EXIT
                end if
            end if


            call cpu_time(t1)
            call activate_surface(atoms, kmc_info, surface_info, segment_info, next_output_time)
            call cpu_time(t2)
            timings%act_surf = timings%act_surf + (t2-t1)
            allocate(rate_list(surface_info%total_atoms * sim_info%rates_per_site))
            call cpu_time(t1)
            call call_rate_setting(atoms,rate_list, ilast_rate_set)
            call cpu_time(t2)
            timings%set_rates = timings%set_rates + (t2-t1)

            !------------
            !NO RATES SET
            !------------
            if (ilast_rate_set == 0) then
                zero_rate_count = zero_rate_count + 1
                deallocate(rate_list)
                !Exit if too many empty rate list events (10% of steps)
                !todo make this a simulation.input parameter
                if (zero_rate_count >= (kmc_info%step - kmc_info%start_step) / 2 .and. &
                        (zero_rate_count > 1000)) then
                    write(file_units%m_output,'(a29,i10,a11)') "Reached empty RSV limit: ",zero_rate_count," -> Exiting"
                    kmc_info%step = kmc_info%step - 1
                    EXIT
                end if
                counters%rates_set_per_it(:) = 0
                cycle
            else
                !Continue by constructing rsv from non-zero sized rate_list
                call cpu_time(t1)
                call create_rsv(rate_list, rsv, ilast_rate_set)
                call cpu_time(t2)
                timings%create_rsv = timings%create_rsv + (t2-t1)
            end if
            call average_add(kmc_info%ave_rsum, rsv(size(rsv)))
            !------------------------
            !kMC Rate Choosing Method
            !------------------------
            call random_number(ran)
            ran1 = ran * rsv(size(rsv))
            !Increment time by timestep dt
            dt = -log(ran)/rsv(size(rsv))
            kmc_info%time = kmc_info%time + dt
            if (outputs%L%dt) write(file_units%dt,'(i10,4(a1,ES15.5))') kmc_info%step,",", dt, ",", kmc_info%time,",", ran,",", rsv(size(rsv))
            !Find Chosen Rate
            call cpu_time(t1)
            call locate(rsv, ran1, ichosen_event)
            call cpu_time(t2)
            timings%find_rate = timings%find_rate + (t2-t1)

            !Execute Rate
            call cpu_time(t1)
            kmc_info%total_events = kmc_info%total_events + 1
            call execute_event(rate_list(ichosen_event), atoms, kmc_info, write_events_file)
            call cpu_time(t2)
            timings%ex_rate = timings%ex_rate + (t2-t1)

            !-------
            !Outputs
            !-------
            if ((kmc_info%time > outputs%values_at_x_s%times(kmc_info%icurr_xtime)) .and. (.not. nucleation)) then
                xtime_output_active  = .true.
            else
                xtime_output_active  = .false.
            end if
            !----------------------------
            !Rate Registry Quantification
            !----------------------------
            if (.not. nucleation) then
                if (outputs%L%all_rates) then
                    write(file_units%non_zero_rates,'(a17,i12,a17)') "---------------",kmc_info%step,"---------------"
                end if
                call average_add_rates_set(kmc_info%ave_rate_set_since_last, counters%rates_set_per_it)
                if (xtime_output_active) then
                    surface_info%reg_at_x(kmc_info%icurr_xtime, :)  = average_rates_set(kmc_info%ave_rate_set_since_last)
                    call average_reset_rates_set(kmc_info%ave_rate_set_since_last)
                end if
                if (outputs%L%all_rates .or. outputs%L%rate_reg) then
                    call count_rates(rate_list, kmc_info)
                end if
                if (outputs%L%rate_reg) then
                    return_value = int_array_as_csv(counters%rates_set_per_it, file_units%rate_list_count)
                end if
            end if

            !Deallocate rate_list and rsv in preparation for reallocation for new size
            deallocate(rate_list)
            deallocate(rsv)
            ilast_rate_set = 0
            call average_add_rates_set(kmc_info%ave_rates_set, counters%rates_set_per_it)
            counters%rates_set_per_it(:) = 0

            !todo separate time_step of kmc visualisation
            if (kmc_info%time > next_output_time) then
                next_output_time_active  = .true.
                fetch_instant_growth_rate = .true.
            else
                next_output_time_active  = .false.
                fetch_instant_growth_rate = .false.
            end if

            if (next_output_time_active .or. xtime_output_active) then
                if (nucleation .eqv. .false.) then

                    if (next_output_time_active) then
                        !Dimer Count
                        if (outputs%L%dimer_count) then
                            write(file_units%dimer_count,'(i10,a1,f10.5,a1,i10)') &
                                    kmc_info%total_events, ",", kmc_info%time, ",", surface_info%total_dimers
                        end if
                        !Surface Atom Environments
                        if (outputs%L%atom_envs) then
                            call get_surface_stats(atoms, kmc_info, surface_info, segment_info, &
                                    fetch_instant_growth_rate, timings ,surf_stats_called_this_iteration)
                            write(file_units%atom_envs,'(i10,a1,EN10.2,5(a1,f10.2))') &
                                kmc_info%total_events,",", kmc_info%time,",", &
                                100.0_dp*real(surface_info%envs(edge_state%isolated))/sum(surface_info%envs),",", &
                                100.0_dp*real(surface_info%envs(edge_state%S_A_edge))/sum(surface_info%envs),",", &
                                100.0_dp*real(surface_info%envs(edge_state%S_B_edge))/sum(surface_info%envs),",", &
                                100.0_dp*real(surface_info%envs(edge_state%S_AB_edge))/sum(surface_info%envs),",", &
                                100.0_dp*real(surface_info%envs(edge_state%terrace))/sum(surface_info%envs)
                        end if

                        !todo create surface_stats output derived type
                        if (outputs%L%roughness) then
                            call get_surface_stats(atoms, kmc_info, surface_info, segment_info, &
                                    fetch_instant_growth_rate, timings, surf_stats_called_this_iteration)
                            write(file_units%roughness, '(i10,a1,ES16.8,a1,ES16.8,3(a1,ES16.8))') &
                                kmc_info%step,",",kmc_info%time,",", &
                                surface_info%rms_roughness,",", &
                                surface_info%rms_4NN,",", &
                                surface_info%rms_3NN,",", &
                                surface_info%rms_2NN
                        end if
                        if (outputs%L%height) then
                            call get_surface_stats(atoms, kmc_info, surface_info, segment_info, &
                                    fetch_instant_growth_rate, timings, surf_stats_called_this_iteration)
                            write(file_units%height,'(i10,3(a3,ES16.8))') kmc_info%step,",",  &
                                                                          kmc_info%time,",",  &
                                                                          surface_info%average_height,",", &
                                                                          surface_info%ave_growth_height
                        end if
                        if (surf_stats_called_this_iteration) call add_to_rms_moving_average(surface_info%rms_roughness)

                        !Visualisation
                        if (outputs%L%kmc_vis) then
                            if (kmc_info%time >= outputs%kmc_out%start .and. &
                                    (kmc_info%time <= outputs%kmc_out%end .or. outputs%kmc_out%minus_1)) then
                                call get_surface_stats(atoms, kmc_info, surface_info, segment_info, &
                                        fetch_instant_growth_rate, timings, surf_stats_called_this_iteration)
                                call output_surface(atoms, surface_info, kmc_info, file_units%kmc_vis)
                                call reset_last_frame(atoms)
                            end if
                        end if

                        !Growth Rate output
                        if (outputs%L%growth_rate) then
                            call get_surface_stats(atoms, kmc_info, surface_info, segment_info, &
                                    fetch_instant_growth_rate, timings, surf_stats_called_this_iteration)
                            write(file_units%growth, '(i10,3(a1, ES15.3))') kmc_info%step,",",kmc_info%time,",", &
                                    1E-10*surface_info%ave_growth_height/(kmc_info%time/3600),",", &
                                    3600 * 1E-10 * surface_info%instant_growth_rate%rate
                            write(file_units%dimer_stat, '(i10,3(a1, ES15.3))') kmc_info%step,",",kmc_info%time,",", &
                                    surface_info%dimer_deviation,",", surface_info%dimer_prop
                        end if

                        !Layers output
                        if (outputs%L%layers) then
                            call get_surface_stats(atoms, kmc_info, surface_info, segment_info, &
                                    fetch_instant_growth_rate, timings, surf_stats_called_this_iteration)
                            call write_to_layers_file(file_units%layers, surface_info, kmc_info)
                        end if

                        !Events completed at by time t output
                        if (outputs%L%events_time) then
                            write(file_units%events_time, '(i10,a1,ES15.5,a1)', advance='no') kmc_info%step,",",kmc_info%time,","
                            return_value = int_array_as_csv(counters%events(:,1), file_units%events_time)
                        end if

                        next_output_time =  next_output_time + outputs%timestep
                        !Flush open units when next output time is reached
                        !Small payload outputs (like surface height) can often never be flushed before
                        !a simulation is terminated by wall time and will allow simulation progress
                        !to be monitored
                        call flush_units(open_units)
                    end if

                    !X time output
                    if (xtime_output_active) then
                        call get_surface_stats(atoms, kmc_info, surface_info, segment_info, &
                                fetch_instant_growth_rate, timings, surf_stats_called_this_iteration)

                        surface_info%grate_at_x(kmc_info%icurr_xtime) = surface_info%ave_growth_rate
                        surface_info%inst_grate_x(kmc_info%icurr_xtime) = surface_info%instant_growth_rate%rate
                        surface_info%rms_at_x(kmc_info%icurr_xtime) = surface_info%rms_roughness
                        surface_info%rms2N_at_x(kmc_info%icurr_xtime) = surface_info%rms_2NN
                        surface_info%rms3N_at_x(kmc_info%icurr_xtime) = surface_info%rms_3NN
                        surface_info%rms4N_at_x(kmc_info%icurr_xtime) = surface_info%rms_4NN
                        surface_info%events_at_x(kmc_info%icurr_xtime, :) = counters%events(:,1)
                        if (kmc_info%icurr_xtime < size(outputs%values_at_x_s%times)) then
                            kmc_info%icurr_xtime = kmc_info%icurr_xtime + 1
                        else
                            kmc_info%xtime_finished = .true.
                        end if
                    end if
                end if
            end if

            !-----------
            !Bookmarking
            !-----------
            if (kmc_info%time > next_bookmark_time) then
                call get_surface_stats(atoms, kmc_info, surface_info, segment_info, &
                        fetch_instant_growth_rate, timings, surf_stats_called_this_iteration)
                next_bookmark_time = next_bookmark_time + outputs%bmark_timestep
                !Write bookmark file
                call write_bookmark(atoms, sim_info, surface_info, kmc_info, outputs, counters)
            end if

            !Exit Loop if max_height reached
            if (reached_max_height .or. reached_min_height) then
                write(6,*) reached_max_height, reached_min_height
                if (reached_max_height) then
                    write(file_units%m_output,'(a29,i5,a11)') &
                            "Maximum atom height reached: ",sim_info%max_height," -> Exiting"
                    EXIT
                else
                    write(file_units%m_output,'(a29,i5,a11)') &
                            "Minimum atom height reached: ",1," -> Exiting"
                    EXIT
                end if
            end if

        end do
        !------------------
        !Post Loop Clean Up
        !------------------

!        if (nucleation) then
!            kmc_info%step = 0
!            kmc_info%time = 0
!            kmc_info%total_events = 0
!        end if


    end subroutine kmc_loop

    !-------------------------------------------------------------------------------------------------
    !Construct RateSumVector from input rate_list, returning an allocated and initialised RSV array(:)
    !-------------------------------------------------------------------------------------------------
    subroutine create_rsv(rate_list, rsv, ilr)
        !-------
        !Modules
        !-------
        use types,  only : rate_type
        use variables, only : kmc_info
        use enumerate, only : file_units
        !---------
        !Arguments
        !---------
        type(rate_type), dimension(:), intent(in) :: rate_list
        real(dp), allocatable, dimension(:), intent(out) :: rsv
        integer, intent(in) :: ilr
        !---------
        !Variables
        !---------
        integer :: irsv
        !-----------
        !Main Method
        !-----------
        !Allocate rate sum vector (rsv)
        allocate(rsv(ilr))
        rsv = 0.0_dp
        if (ilr /= 0) then
            !Initial RSV element value
            rsv(1) = rate_list(1)%rate_val
            do irsv=2,ilr
                rsv(irsv) = rsv(irsv - 1) + rate_list(irsv)%rate_val
            end do
        else
            write(file_units%warning,*) kmc_info%step, "create_rsv(rate_list, rsv) last_rate_set = 0"
        end if

    end subroutine create_rsv

    !----------------------------------------------------
    !Counts rates within rate registry and output to file
    !----------------------------------------------------
    subroutine count_rates(rate_list, kmc_info)
        !Modules
        use types, only : rate_type, kmc_type
        use input, only : outputs
        use variables,  only : EPS
        use enumerate,  only : file_units, tRates
        !Arguments/Variables
        type (rate_type), dimension(:), intent(in) :: rate_list
        type (kmc_type), intent(in) :: kmc_info
        integer, dimension(tRates%nrates) :: rate_totals
        real(dp), dimension(tRates%nrates) :: rate_sum
        integer :: irate

        rate_totals = 0
        rate_sum = 0.0_dp
        !Main Method
        do irate=1,size(rate_list)
            if (rate_list(irate)%rate_val > EPS) then
                if (rate_list(irate)%event == 0) then
                    write(file_units%warning) kmc_info%step, "rate val > 1E-6 but event == 0",irate,rate_list(irate)%rate_val,rate_list(irate)%event
                    cycle
                end if
                if (outputs%L%all_rates) then
                    write(file_units%non_zero_rates,'(i10,ES15.5,i5,i5,i5,i5,i10)') irate,rate_list(irate)%rate_val,rate_list(irate)%site,rate_list(irate)%event
                end if
                rate_totals(rate_list(irate)%event) = rate_totals(rate_list(irate)%event) + 1
                rate_sum(rate_list(irate)%event) = rate_sum(rate_list(irate)%event) + rate_list(irate)%rate_val
            end if
        end do

        if (outputs%L%rate_reg) then
            write(file_units%rate_list_sum,'(i12,5(a1,ES16.3))') kmc_info%step,",",kmc_info%time,",", &
                    sum(rate_sum(tRates%DinsertCH3:tRates%C2H2_M3)),",", &
                    sum(rate_sum(tRates%dimerMigrate_r:tRates%migrateC2H2)),",", &
                    sum(rate_sum(tRates%pref_etch_CHx:tRates%etchCH3_C2H2)),",", &
                    sum(rate_sum)
        end if

    end subroutine count_rates


    !-----------------------------------------------------------------------------------------------------
    !Add current roughness into 10 element roughness array and calculate current 10 element moving average
    !-----------------------------------------------------------------------------------------------------
    subroutine add_to_rms_moving_average(current_roughness)
        !Module Variables
        use variables,  only : i_ma_last, roughness_array
        !Variables
        real(dp), intent(in) :: current_roughness

        !Main Method
        !Update array element with current roughness
        roughness_array(i_ma_last + 1) =  current_roughness
        i_ma_last = i_ma_last + 1
        !Get average of array (last 10 time steps)

        !If i_ma_last is at end of roughness array
        if (i_ma_last >= size(roughness_array)) i_ma_last = 0

    end subroutine add_to_rms_moving_average


    !Wrapper for calling surface_stats in a single line
    subroutine get_surface_stats(atoms, kmc_info, surf_info, seg_info, fetch_inst_gr, timings, called)
        !Modules
        use types, only : atom_type_ext, kmc_type, kmc_timings, surface_type
        use surface_m, only : surface_stats
        !Arguments
        type(atom_type_ext), intent(in) :: atoms(:)
        type(kmc_type), intent(inout) :: kmc_info
        type(surface_type), intent(inout) :: surf_info
        type(surface_type), intent(inout) :: seg_info(:)
        logical, intent(in) :: fetch_inst_gr
        type(kmc_timings), intent(inout) :: timings
        logical, intent(inout) :: called
        !Variables
        real(dp) :: t1,t2

        if (called .eqv. .false.) then
            call cpu_time(t1)
            call surface_stats(atoms, kmc_info, &
                    surf_info%start_height, kmc_info%start_time, seg_info(:)%start_height, &
                    surf_info, seg_info, fetch_inst_gr)
            call cpu_time(t2)
            timings%surf_stats%time = timings%surf_stats%time + (t2-t1)
            timings%surf_stats%count = timings%surf_stats%count + 1
            called = .true.
        end if

    end subroutine get_surface_stats

end module kmc