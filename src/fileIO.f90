module fileIO

    implicit none

contains

    logical function file_exists(filename)
        character(len=*), intent(in) :: filename

        inquire(file=trim(filename), exist=file_exists)

    end function

end module