module json

    use json_module, only : json_value, json_value, json_core
    use kinds, only : dp

    implicit none

!    interface get_array
!        module procedure :: get_array_of_real_dp, get_array_of_int
!    end interface get_array

    type(json_value), pointer :: root !JSON root

contains

    !------------------------------------------------------------
    !Output initial simulation values to diamond.json output file
    !------------------------------------------------------------
    subroutine json_output_input_values(sim_info, conditions, reactions, energies, etching_i, dimerf, flush)
        !Modules
        use types, only : sim_type, conditions_ftype, energy_ftype, reactions_ftype, etching_ftype, &
            dimer_ftype
        use enumerate, only : file_units, edge_state
        use derived_values, only : k_down_mig, pref_etch_rates, rDimerInsertCHX, rTroughInsertCHX, &
                k_dimer_mig, kgmig_all_rates, Fmr, Fbr, C2H2_ads_rates, kide, &
                kC2H2p_mig_sub, kC2H2p_mig, ketch_CHx, kc2h2p_dimer, kf_adj_dimer, kr_adj_dimer, prob_adj_dimer_break
        !Arguments
        type(sim_type), intent(in) :: sim_info
        type(conditions_ftype), intent(in) :: conditions
        type(reactions_ftype), intent(in) :: reactions
        type(energy_ftype), intent(in) :: energies
        type(etching_ftype), intent(in) :: etching_i
        type(dimer_ftype), intent(in) :: dimerf
        logical, optional, intent(in) :: flush
        !Variables
        integer :: i, n
        character(7) :: ide_label
        type(json_core) :: json
        type(json_value), pointer :: &
                end_conditions, &
                gases, &
                reaction_energies, &
                reaction_rates, &
                dimer_insertion, &
                trough_insertion, &
                C2H2_adsorption, &
                pref_etch, ide_etch, c2h2_etch, chx_etch_k11, &
                scaling_factors

        call json%initialize()
        call json%create_object(root, 'simulation') !Create root
        call json%add(root, 'name', trim(sim_info%name))

        !Simulation Parameters
        call json%create_object(end_conditions, 'end')
        !Initial Surface
        call json%add(root, 'Nx', sim_info%Nx)
        call json%add(root, 'Ny', sim_info%Ny)
        call json%add(root, 'Nb', sim_info%Nbasis)
        call json%add(root, 'Nz', sim_info%Nz)
        !End Conditions
        call json%add(end_conditions, 't-s', sim_info%end%time)
        call json%add(end_conditions, 'it', sim_info%end%iteration)
        call json%add(end_conditions, 'height-A', sim_info%end%growth_height)
        call json%add(root, end_conditions) !Add parameter object to root
        !Nullify Pointers
        nullify(end_conditions)

        !Gas Conditions
        call json%add(root, 'condition_id', trim(conditions%condition_id))
        call json%create_object(gases, 'conc')
        call json%add(root, gases)
        !Temperatures
        call json%add(root, 'Ts', conditions%Ts)
        call json%add(root, 'Tns', conditions%Tns)
        !Gas Concentrations + g/s
        call json%add(gases, 'H', conditions%Hconc)
        call json%add(gases, 'H2', conditions%H2conc)
        do i=1,size(conditions%CHxconc,1)
            call json%add(gases, conditions%CHx_labels(i), [conditions%CHxconc(i), conditions%CHx_s(i), conditions%CHx_g(i)])
        end do
        call json%add(gases, 'C2_a', conditions%C2a_conc)
        call json%add(gases, 'C2_X', conditions%C2X_conc)
        call json%add(gases, 'C2H2', [conditions%C2H2_conc, conditions%C2H2_s])
        !Nullify
        nullify(gases)
        !Reaction Energies
        call json%create_object(reaction_energies, 'Eng')
        call json%add(root, reaction_energies)
        call json%add(reaction_energies, 'k1', [real(energies%Ak1,dp), real(energies%Ek1,dp)])
        call json%add(reaction_energies, 'km1', [energies%Akm1, energies%Ekm1])
        call json%add(reaction_energies, 'k2', [real(energies%Ak2,dp), 0.0_dp])
        do i=edge_state%isolated,edge_state%terrace
            call json%add(reaction_energies, etching_i%prf_rate_labels(i), [etching_i%flex_etch(i)%A, etching_i%flex_etch(i)%E])
        end do
        do n=1, etching_i%n_reactions
            do i=1,etching_i%n_ide_subreactions(n)
                write(ide_label, '(a3,i1,a1,i1,a1)') '12-', n, '-', i, 'f'
                call json%add(reaction_energies, ide_label, [etching_i%ide_ahhr(n,i)%f%A, etching_i%ide_ahhr(n,i)%f%E])
                write(ide_label, '(a3,i1,a1,i1,a1)') '12-', n, '-', i, 'r'
                call json%add(reaction_energies, ide_label, [etching_i%ide_ahhr(n,i)%r%A, etching_i%ide_ahhr(n,i)%r%E])
            end do
        end do
        call json%add(reaction_energies, 'D_Migrate', [energies%A_dmig, energies%E_dmig])
        call json%add(reaction_energies, 'G_Migrate_1f', [energies%A_gmig1f, energies%E_gmig1f])
        call json%add(reaction_energies, 'G_Migrate_1r', [energies%A_gmig1r, energies%E_gmig1r])
        call json%add(reaction_energies, 'G_Migrate_2f', [energies%A_gmig2f, energies%E_gmig2f])
        call json%add(reaction_energies, 'G_Migrate_2r', [energies%A_gmig2r, energies%E_gmig2r])
        call json%add(reaction_energies, 'Down_Migrate', [energies%A_down_mig, energies%E_down_mig])
        call json%add(reaction_energies, 'C2H2_M1', [energies%acetylene_ads%M1%A, energies%acetylene_ads%M1%E])
        call json%add(reaction_energies, 'C2H2_M2', [energies%acetylene_ads%M2%A, energies%acetylene_ads%M2%E])
        call json%add(reaction_energies, 'C2H2_3a', [energies%acetylene_ads%M3a%A, energies%acetylene_ads%M3a%E])
        call json%add(reaction_energies, 'C2H2_M3b', [energies%acetylene_ads%M3b%A, energies%acetylene_ads%M3b%E])
        call json%add(reaction_energies, 'C2H2p_mig_fA', [energies%C2H2p_mig(:)%f%A])
        call json%add(reaction_energies, 'C2H2p_mig_fE', [energies%C2H2p_mig(:)%f%E])
        call json%add(reaction_energies, 'C2H2p_mig_rA', [energies%C2H2p_mig(:)%r%A])
        call json%add(reaction_energies, 'C2H2p_mig_rE', [energies%C2H2p_mig(:)%r%E])
        !Nullify
        nullify(reaction_energies)

        !Reaction Rates
        call json%create_object(reaction_rates, 'rate')
        call json%create_object(dimer_insertion, 'd_ins')
        call json%create_object(trough_insertion, 't_ins')
        call json%create_object(C2H2_adsorption, 'C2H2_ads')
        call json%create_object(pref_etch, 'pref_etch')
        call json%create_object(c2h2_etch, 'c2h2_etch')
        call json%create_object(ide_etch, 'ide_etch')
        call json%create_object(chx_etch_k11, 'etch_k11')
        call json%add(reaction_rates, dimer_insertion)
        call json%add(reaction_rates, trough_insertion)
        call json%add(reaction_rates, C2H2_adsorption)
        call json%add(reaction_rates, pref_etch)
        call json%add(reaction_rates, c2h2_etch)
        call json%add(reaction_rates, ide_etch)
        call json%add(reaction_rates, chx_etch_k11)
        call json%add(root, reaction_rates)
        do i=1,size(rDimerInsertCHX,1)
            call json%add(dimer_insertion, conditions%CHx_labels(i), rDimerInsertCHX(i))
        end do
        do i=1,size(rDimerInsertCHX,1)
            call json%add(trough_insertion, conditions%CHx_labels(i), rTroughInsertCHX(i))
        end do
        call json%add(C2H2_adsorption, 'C2H2_M1', C2H2_ads_rates%M1a)
        call json%add(C2H2_adsorption, 'C2H2_M2', C2H2_ads_rates%M2)
        call json%add(C2H2_adsorption, 'C2H2_M3a', C2H2_ads_rates%M3a)
        call json%add(C2H2_adsorption, 'C2H2_M3b', C2H2_ads_rates%M3b)
        call json%add(pref_etch, 'C2H2', C2H2_ads_rates%etchC2H2)
        call json%add(ide_etch, 'CxHy', [kide])
        call json%add(pref_etch, 'CHx', [pref_etch_rates(:)])
        call json%add(chx_etch_k11, 'CHx', ketch_CHx)
        call json%add(c2h2_etch, 'C2H2_CH3', C2H2_ads_rates%etchCH3)
        call json%add(c2h2_etch, 'C2H2', C2H2_ads_rates%etchC2H2)
        call json%add(reaction_rates, 'dimer_migration', k_dimer_mig)
        call json%add(reaction_rates, 'gap_migration', [kgmig_all_rates(:)])
        call json%add(reaction_rates, 'down_migration', k_down_mig)
        call json%add(reaction_rates, 'c2h2p_mig_sub_f', [kC2H2p_mig_sub(:)%f])
        call json%add(reaction_rates, 'c2h2p_mig_sub_r', [kC2H2p_mig_sub(:)%r])
        call json%add(reaction_rates, 'c2h2p_mig', kC2H2p_mig(:))
        call json%add(reaction_rates, 'c2h2p_dimer', kc2h2p_dimer)
        call json%add(reaction_rates, 'adj_dimer_form', kf_adj_dimer)
        call json%add(reaction_rates, 'adj_dimer_break', kr_adj_dimer)
        !Nullify
        nullify(reaction_rates, dimer_insertion, trough_insertion, C2H2_adsorption)
        !Scaling Factors
        call json%create_object(scaling_factors, 'scaling')
        call json%add(root, scaling_factors)
        call json%add(scaling_factors, 'migration_critical_nucleus', reactions%mig_crit_nuc)
        call json%add(scaling_factors, '3rd_bond_factor', etching_i%flex_etch_3rdbond_factor)
        call json%add(scaling_factors, 'dimer_isolation_range', reactions%dimer_row_isolation_range)
        call json%add(scaling_factors, 'single_c_prevent_iso.', reactions%single_c_prevent_iso)
        call json%add(scaling_factors, 'dimer_form_threshold', dimerf%nb_form_thres)
        call json%add(scaling_factors, 'incorp_sat_dimer', dimerf%incorp_CHx_sat_dimer)
        call json%add(scaling_factors, 'incorp_sat_dimer_rscale', dimerf%sat_dimer_incorp_scale)
        call json%add(scaling_factors, 'hindered_trough_deactivation_scale', dimerf%hind_trough_deact_factor)
        call json%add(scaling_factors, 'dihydride_trough_hindered', dimerf%dihyride_hindered)
        call json%add(scaling_factors, 'gap_mig_via_dimer', dimerf%gap_via_dimer)
        call json%add(scaling_factors, 'iso_dimer_breaking', dimerf%iso_dimer_can_break)
        call json%add(scaling_factors, 'rand_adj_dimer_breaking', dimerf%random_adj_dimer_breaking)
        call json%add(scaling_factors, 'prob_adj_dimer_break', prob_adj_dimer_break)
        !Nullify
        nullify(scaling_factors)
        call json%add(root, 'align_dimers', reactions%align_dimers)
        call json%add(root, 'd_align_range', reactions%d_align_range)
        call json%add(root, 'realign_dimers', reactions%realign_dimers)
        !Radical Sites
        call json%add(root, 'Fmr', Fmr)
        call json%add(root, 'Bmr', Fbr)
        !Active Rates
        call json%add(root, 'active_rates', sim_info%unique_rates)
        call json%add(root, 'rates_per_site', sim_info%rates_per_site)
        !Nullify
        !Seeding Clock
        call json%add(root, 'seed', sim_info%seeding_clock)

        if (present(flush)) then
            if (flush) then
                call json%print(root, file_units%json_m_output)
                flush(file_units%json_m_output)
            end if
        end if

    end subroutine json_output_input_values

    !----------------------------------------------------
    !Output simulation information at end of a simulation
    !----------------------------------------------------
    subroutine json_sim_end_output(sim_info, kmc_info, surface_info, segment_info, surf_bmark_info, cpu_time)
        !Modules
        use types, only : sim_type, kmc_type, surface_type, average, average_rates_set
        use input, only : outputs
        use variables, only : counters
        use enumerate, only : file_units, tRates, edge_state
        !Arguments
        type(sim_type), intent(in) :: sim_info
        type(kmc_type), intent(in) :: kmc_info
        type(surface_type), intent(in) :: surface_info, surf_bmark_info
        type(surface_type), dimension(:), intent(in) :: segment_info
        real(dp), intent(in) :: cpu_time
        !Variables
        integer :: sim_it
        real(dp) :: sim_time
        integer :: ienv, i
        character(5) :: i_as_str
        real(dp) :: total_envs = 0
        real(dp), dimension(size(segment_info)) :: seg_tot_ave_envs, seg_tot_envs
        integer :: iseg
        real(dp), dimension(size(segment_info),edge_state%isolated:edge_state%terrace) :: segment_ave_atom_envs, segment_atom_envs
        real(dp), allocatable, dimension(:) :: blank
        integer :: total_incorp_events
        integer :: surf_occ_padding

        type(json_core) :: json
        type(json_value),pointer :: reaction_counts, &
                dimer_insertion, &
                trough_insertion, &
                pref_etch, &
                ide_etch, &
                chx_k11_etch, &
                C2H2, &
                migration

        type(json_value), pointer :: reg_at_x
        type(json_value), pointer :: events_at_x


        call json%initialize()

        sim_it = kmc_info%step - kmc_info%start_step
        sim_time = kmc_info%time - kmc_info%start_time

        !Simulation Info (time, iterations)
        call json%add(root, 'iterations', kmc_info%step - kmc_info%start_step)
        call json%add(root, 'total_iterations', kmc_info%step)
        call json%add(root, 'events', [kmc_info%total_events-kmc_info%start_step,nint(100*real(kmc_info%total_events-kmc_info%start_step)/sim_it)])
        call json%add(root, 'sim_time', sim_time)
        call json%add(root, 'total_time', kmc_info%time)
        call json%add(root, 'nucleation_height', sim_info%nucleation_height)
        call json%add(root, 'nucleation_time', kmc_info%nucleation_time)
        call json%add(root, 'dimers_aligned', [counters%d_align(:)])
        call json%add(root, 'adj_dimers_broken', counters%d_break)
        call json%add(root, 'adj_dimers_broken_per_it', real(counters%d_break, dp) / (kmc_info%step - kmc_info%start_step))
        call json%add(root, 'migrations_per_atom', [average(kmc_info%migs), kmc_info%migs%sum, real(kmc_info%migs%count, dp) ])
        call json%add(root, 'average_Fmr', average(kmc_info%Fmr))
        call json%add(root, 'average_active_layers', average(surface_info%ave_active_layers))
        call json%add(root, 'average_rsum', average(kmc_info%ave_rsum))

        !Reaction Counts
        call json%create_object(reaction_counts, 'count')
        call json%create_object(trough_insertion, 't_ins')
        call json%create_object(dimer_insertion, 'd_ins')
        call json%create_object(pref_etch, 'pref_etch')
        call json%create_object(ide_etch, 'ide_etch')
        call json%create_object(chx_k11_etch, 'k11_etch')
        call json%create_object(C2H2, 'C2H2')
        call json%create_object(migration, 'mig')
        call json%add(reaction_counts, dimer_insertion)
        call json%add(reaction_counts, trough_insertion)
        call json%add(reaction_counts, pref_etch)
        call json%add(reaction_counts, ide_etch)
        call json%add(reaction_counts, chx_k11_etch)
        call json%add(reaction_counts, C2H2)
        call json%add(reaction_counts, migration)
        call json%add(root, reaction_counts)
        !Insertion
        call json%add(dimer_insertion, 'CH3', counters%events(tRates%DinsertCH3,1))
        call json%add(dimer_insertion, 'CH2', counters%events(tRates%DinsertCH2,1))
        call json%add(dimer_insertion, 'CH', counters%events(tRates%DinsertCH,1))
        call json%add(dimer_insertion, 'C', counters%events(tRates%DinsertC,1))
        call json%add(trough_insertion, 'CH3', counters%events(tRates%TinsertCH3,1))
        call json%add(trough_insertion, 'CH2', counters%events(tRates%TinsertCH2,1))
        call json%add(trough_insertion, 'CH', counters%events(tRates%TinsertCH,1))
        call json%add(trough_insertion, 'C', counters%events(tRates%TinsertC,1))
        call json%add(C2H2, 'M1', [counters%events(tRates%C2H2_M1a,1),counters%events(tRates%C2H2_M1b,1)])
        call json%add(C2H2, 'M2', counters%events(tRates%C2H2_M2,1))
        call json%add(C2H2, 'M3', counters%events(tRates%C2H2_M3,1))
        total_incorp_events = counters%events(tRates%DinsertCH3,1) + &
                             counters%events(tRates%DinsertCH2,1) + &
                             counters%events(tRates%DinsertCH,1) + &
                             counters%events(tRates%DinsertC,1) + &
                             counters%events(tRates%TinsertCH3,1) + &
                             counters%events(tRates%TinsertCH2,1) + &
                             counters%events(tRates%TinsertCH,1) + &
                             counters%events(tRates%TinsertC,1)
        !Etch
        call json%add(pref_etch, 'CHx', [counters%events(tRates%pref_etch_CHx,1),counters%events(tRates%pref_etch_CHx,2)])
        call json%add(pref_etch, 'C2H2_CH3', [counters%events(tRates%etchCH3_C2H2,1),counters%events(tRates%etchCH3_C2H2,2)])
        call json%add(pref_etch, 'C2H2', [counters%events(tRates%etchC2H2,1),counters%events(tRates%etchC2H2,2)])
        call json%add(pref_etch, 'total', [sum(counters%events(tRates%pref_etch_CHx:tRates%etchCH3_C2H2,1)),sum(counters%events(tRates%pref_etch_CHx:tRates%etchCH3_C2H2,2))])
        call json%add(chx_k11_etch, 'CHx', counters%events(tRates%etch_chx,1))
        call json%add(ide_etch, 'ide_12_1', counters%events(tRates%ide_12_1,1))
        call json%add(ide_etch, 'ide_12_2', counters%events(tRates%ide_12_2,1))
        call json%add(ide_etch, 'ide_12_3', counters%events(tRates%ide_12_3,1))
        call json%add(ide_etch, 'ide_12_4', counters%events(tRates%ide_12_4,1))
        call json%add(ide_etch, 'ide_12_5', counters%events(tRates%ide_12_5,1))
        !Migration
        ![row, chain, total]
        call json%add(migration, 'dimer', [counters%events(tRates%dimerMigrate_r:tRates%dimerMigrate_c,1), &
                sum(counters%events(tRates%dimerMigrate_r:tRates%dimerMigrate_c,1))])
        ![A,B,C,D, total]
        call json%add(migration, 'gap', [counters%events(tRates%gapMigrateA:tRates%gapMigrateD,1), &
                sum(counters%events(tRates%gapMigrateA:tRates%gapMigrateD,1))])
        call json%add(migration, 'down', counters%events(tRates%migDown,1))
        call json%add(migration, 'C2H2_biradical', counters%events(tRates%migC2H2_birad,1))
        call json%add(migration, 'C2H2', counters%events(tRates%migrateC2H2,1))
        call json%add(migration, 'C2H2p_gap', [counters%events(tRates%gapMigrateC2H2_0,1),counters%events(tRates%gapMigrateC2H2_1,1)])
        call json%add(migration, 'C2H2p_dimer', counters%events(tRates%c2H2p_dimer,1))


        !Surface/Growth Information
        call json%add(root, 'average_growth', 1E-10*surface_info%ave_growth_height)
        call json%add(root, 'average_growth_rate', 3600*1E-10*surface_info%ave_growth_rate)
        call json%add(root, 'rms_roughness', 1E-10*surface_info%rms_roughness)
        call json%add(root, 'ave_rms_roughness', 1E-10*average(surface_info%ave_rms))
        call json%add(root, '2N_rms_roughness', 1E-10*surface_info%rms_2NN)
        call json%add(root, '3N_rms_roughness', 1E-10*surface_info%rms_3NN)
        call json%add(root, '4N_rms_roughness', 1E-10*surface_info%rms_4NN)
        call json%add(root, 'ave_2N_rms_roughness', 1E-10*average(surface_info%ave_2Nrms))
        call json%add(root, 'ave_3N_rms_roughness', 1E-10*average(surface_info%ave_3Nrms))
        call json%add(root, 'ave_4N_rms_roughness', 1E-10*average(surface_info%ave_4Nrms))
        call json%add(root, 'total_growth', 1E-10*surf_bmark_info%ave_growth_height)
        call json%add(root, 'total_growth_rate', 3600*1E-10*surf_bmark_info%ave_growth_height/(kmc_info%time))
        call json%add(root, 'surface_width_nm', 1E-1*surface_info%width)
        call json%add(root, 'net_incorporation_%', 100*(real(total_incorp_events-counters%events(tRates%pref_etch_CHx,1),dp))/ &
                                                       (total_incorp_events+counters%events(tRates%pref_etch_CHx,1)))
        call json%add(root, 'dimer_prop', surface_info%dimer_prop)
        call json%add(root, 'dimer_deviation', surface_info%dimer_deviation)
        call json%add(root, 'ave_dimer_prop', average(surface_info%ave_dimers_prop))
        call json%add(root, 'ave_dimer_deviation', average(surface_info%ave_dimer_deviation))

        call json%add(root, 'raw_counts', counters%events(:,1))
        !Output @ x

        call json%add(root, 'n_xvals', outputs%values_at_x_s%num_values)
        call json%add(root, 'xtimes', [outputs%values_at_x_s%times(1:size(outputs%values_at_x_s%times))])
        call json%add(root, 'xgrowth_rate', 3600 * 1E-1 *[surface_info%grate_at_x]) !Convert to nm/h from Ang/s
        call json%add(root, 'xinstgr', 3600 * 1E-1 *[surface_info%inst_grate_x]) !Convert to nm/h from Ang/s
        call json%add(root, 'xrms', 1E-10 * [surface_info%rms_at_x]) !Convert from A to nm
        call json%add(root, 'xrms2N', 1E-10 * [surface_info%rms2N_at_x]) !Convert from A to nm
        call json%add(root, 'xrms3N', 1E-10 * [surface_info%rms3N_at_x]) !Convert from A to nm
        call json%add(root, 'xrms4N', 1E-10 * [surface_info%rms4N_at_x]) !Convert from A to nm
        ! Rate Register @ x
        call json%create_object(reg_at_x, 'xreg')
        do i=1, size(surface_info%reg_at_x,2)
            write(i_as_str, '(i5)') i-1
                call json%add(reg_at_x, trim(adjustl(i_as_str)) ,[surface_info%reg_at_x(:, i)])
        end do
        call json%add(root, reg_at_x)
        nullify(reg_at_x)
        ! Event counters @ x
        call json%create_object(events_at_x, 'xevents')
        do i=1, size(surface_info%events_at_x,2)
            write(i_as_str, '(i5)') i-1
            call json%add(events_at_x, trim(adjustl(i_as_str)) ,[surface_info%events_at_x(:, i)])
        end do
        call json%add(root, events_at_x)
        nullify(events_at_x)

        call json%add(root, 'atom_envs', 100*[real(surface_info%envs)]/sum(surface_info%envs(:)))
        do ienv=edge_state%isolated,edge_state%terrace
            total_envs = total_envs + average(surface_info%ave_envs(ienv))
        end do
        call json%add(root, 'ave_atom_envs', 100*[real(average(surface_info%ave_envs(edge_state%isolated)))/total_envs, &
                real(average(surface_info%ave_envs(edge_state%S_A_edge)))/total_envs, &
                real(average(surface_info%ave_envs(edge_state%S_B_edge)))/total_envs, &
                real(average(surface_info%ave_envs(edge_state%S_AB_edge)))/total_envs, &
                real(average(surface_info%ave_envs(edge_state%terrace)))/total_envs])

        !CPU TIME INFO
        call json%add(root, 'average_timestep', kmc_info%time/kmc_info%total_events)
        call json%add(root, 'CPU_time', cpu_time)

        !========
        !SEGMENTS
        !========
        if (sim_info%use_segments) then
            call json%add(root, 'seg_ave_growth', 1E-10*[segment_info(:)%ave_growth_height])
            call json%add(root, 'seg_ave_growth_rate', 3600*1E-10*[segment_info(:)%ave_growth_height]/sim_time)
            call json%add(root, 'seg_rms', 1E-10*[segment_info(:)%rms_roughness])
            call json%add(root, 'seg_2N', 1E-10*[segment_info(:)%rms_2NN])
            call json%add(root, 'seg_3N', 1E-10*[segment_info(:)%rms_3NN])
            call json%add(root, 'seg_4N', 1E-10*[segment_info(:)%rms_4NN])
            call json%add(root, 'seg_surf_width', 1E-1*[segment_info(:)%width])

            segment_ave_atom_envs(:,:) = 0
            segment_atom_envs(:,:) = 0
            !Sum ave and final environments
            do iseg=1, size(seg_tot_envs)
                do ienv=edge_state%isolated,edge_state%terrace
                    seg_tot_ave_envs(iseg) = seg_tot_ave_envs(iseg) + average(segment_info(iseg)%ave_envs(ienv))
                    seg_tot_envs(iseg) = seg_tot_envs(iseg) + segment_info(iseg)%envs(ienv)
                end do
            end do
            do iseg=1, size(seg_tot_envs)
                do ienv=edge_state%isolated,edge_state%terrace
                    segment_ave_atom_envs(iseg, ienv) = 100.0_dp * real(average(segment_info(iseg)%ave_envs(ienv)),dp) / seg_tot_ave_envs(iseg)
                    segment_atom_envs(iseg, ienv) = 100.0_dp * real(segment_info(iseg)%envs(ienv),dp) / seg_tot_envs(iseg)
                end do
            end do

            call json%add(root, 'seg_ISO', [segment_atom_envs(:, edge_state%isolated)])
            call json%add(root, 'seg_SA', [segment_atom_envs(:, edge_state%S_A_edge)])
            call json%add(root, 'seg_SB', [segment_atom_envs(:, edge_state%S_B_edge)])
            call json%add(root, 'seg_SAB', [segment_atom_envs(:, edge_state%S_AB_edge)])
            call json%add(root, 'seg_TERR', [segment_atom_envs(:, edge_state%terrace)])
            call json%add(root, 'seg_ave_ISO', [segment_ave_atom_envs(:, edge_state%isolated)])
            call json%add(root, 'seg_ave_SA', [segment_ave_atom_envs(:, edge_state%S_A_edge)])
            call json%add(root, 'seg_ave_SB', [segment_ave_atom_envs(:, edge_state%S_B_edge)])
            call json%add(root, 'seg_ave_SAB', [segment_ave_atom_envs(:, edge_state%S_AB_edge)])
            call json%add(root, 'seg_ave_TERR', [segment_ave_atom_envs(:, edge_state%terrace)])
        end if

        !------
        !LAYERS
        !------
        call json%add(root, 'surface_atom_min_layer', surface_info%min_layer)
        !Ensure that output of layers is always 20 deep to ensure consistency
        surf_occ_padding = 20-surface_info%active_layers
        if (surf_occ_padding > 0) then
            allocate(blank(20-surface_info%active_layers))
            blank(:) = 0.0_dp
            call json%add(root, 'surface_atom_occ', [surface_info%top_occupancy(:), blank(:)])
        else
            call json%add(root, 'surface_atom_occ', [surface_info%top_occupancy(1:20)])
        end if
        call json%add(root, 'n_final_layers', surface_info%active_layers)

        !---------
        !RATES SET
        !---------
        call json%add(root, 'average_rates_set', [average_rates_set(kmc_info%ave_rates_set)])

        !Output to file
        call json%print(root, file_units%json_m_output)
        flush(file_units%json_m_output)

        nullify(root)
        call json%destroy()

    end subroutine json_sim_end_output

end module json
