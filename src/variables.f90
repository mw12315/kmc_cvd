!--------------------------------------------------------------------------------
!Hold many of the variables which are can be used globally throughout the program
!--------------------------------------------------------------------------------
module variables
    !-------
    !Modules
    !-------
    use kinds,  only : dp
    use types,  only : sim_type,atom_type,atom_type_ext,minmax_k,counter_type, &
                       event_vis_type,rate_type,rgb_type,kmc_type,surface_type,rate_vis_switch_type, &
                       NN_dist_type, xy_segment_type

    implicit none
    !----------
    !Parameters
    !----------
    !Scientific Constants
    real(dp), parameter :: a0   = 3.5698_dp     !Diamond lattce parameter in angstroms (6.746 au)
    real(dp), parameter :: EPS  = 1E-12_dp      !Real number comparison threshold
    real(dp), parameter :: Rgas = 8.3142_dp     !Gas constant
    real(dp), parameter :: kb   = 1.38E-23      !Boltzmann's constant
    real(dp), parameter :: h    = 6.626E-34     !Plank's constant
    real(dp), parameter :: NA   = 6.02E+23      !Avogadro's number
    integer,  parameter :: distinct_rates = 8   !Number of distinct rates in the system
    !Program Specific
    real(dp), parameter :: dimer_disp = 0.35    !Dimer displacement distance (angstom)
    real(dp), parameter :: c2_disp = 1.34_dp    !Vertical displacement of 2nd carbon above base atom
    integer, parameter  :: NN_bulk = 4          !Number of nearest neighbours of a bulk carbon atom
    type(NN_dist_type),save :: NN_dist          !C-C distance relating to NN length scales (rms roughness)

    !Global Variables
    !Simulation constants/output_flags
    type (sim_type), save                           :: simulation
    !Surface
    type(surface_type)                              :: surface
    type(surface_type), allocatable, dimension(:)   :: segments         !Subset of surface but divided into segments
    type(xy_segment_type), allocatable, dimension(:):: segment_bounds   !Lower and Upper bounds of surface segments
    !KMC
    type(kmc_type)                                  :: kmc_info
    !Open file units
    integer, dimension(0:50)                          :: open_units = -1

    !Lattice Vectors
    integer, parameter, dimension(3)                :: a(1 : 3) = (/ 1, 0, 0 /)
    integer, parameter, dimension(3)                :: b(1 : 3) = (/ 0, 1, 0 /)
    integer, parameter, dimension(3)                :: c(1 : 3) = (/ 0, 0, 1 /)

    !Atoms and Position storage
    type(atom_type_ext),allocatable,dimension(:)    :: lattice      !List containing all atoms within the simulation
    integer,dimension(2)                            :: lat_allocate !Store the allocation bounds of lattice

    real(dp), dimension(10)                         :: roughness_array
    integer                                         :: i_ma_last        !Index of last element updated

    !Events Resimulation
    type(event_vis_type),allocatable,dimension(:)   :: events_vis       !Holding (step,[event,i,j,ibasis,k])
    real(dp), allocatable,dimension(:)              :: events_time_vis  !Store the per iteration time from the events list
    !Instantaneous growth rate calculation

    !KMC
    logical                                         :: reached_max_height = .false., reached_min_height = .false.
    real(dp)                                        :: t_out_nxt

    !Counters
    type(counter_type)                              :: counters
    !Rate Visualisation
    type(rate_vis_switch_type),save                 :: rates_visualised

    !----------
    !Interfaces
    !----------
    interface link_atoms
        module procedure link_atoms_atom
        module procedure link_atoms_index
    end interface link_atoms

    interface parity
        module procedure parity_real
        module procedure parity_int
    end interface parity

contains
    !----------------------------------------------------------------------------------
    !Return dimer (logical) value of an atom object within the atom_type_ext atom array
    !----------------------------------------------------------------------------------
    logical function read_dimer(atom, atoms) result(value)
        !Modules
        use types, only : atom_type, atom_type_ext
        !Variables
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), dimension(:), intent(in) :: atoms
        integer :: index
        !Main Method
        index = ilattice(atom)
        if (index < 1) then
            value = .false.
            return
        end if

        value = atoms(ilattice(atom))%dimer

    end function read_dimer


    !------------------------------------------------
    !Create a dimer between atom1 and atom2
    !Dangerous, no checks are made, ensure this is the
    !lowest level call made following checks
    !------------------------------------------------
    subroutine create_dimer_pair(atom1, atom2, atoms)
        !Modules
        use types, only : atom_type, atom_type_ext
        use enumerate, only : file_units
        !Variables
        type(atom_type), intent(in) :: atom1, atom2
        type(atom_type_ext), dimension(:), intent(inout) :: atoms
        integer :: ilat1, ilat2

        !Neither atom1 or atom2 in a dimer already
        if (read_dimer(atom1, atoms) .or. read_dimer(atom2, atoms)) then
            write(file_units%error) "create_dimer_pair(atom1,atom2) - already part of a dimer:", &
                    read_dimer(atom1, atoms), read_ipair(atom1, atoms), read_dimer(atom2, atoms), read_ipair(atom2, atoms)
        end if
        !Create dimer and ipair registry entries
        ilat1 = ilattice(atom1)
        ilat2 = ilattice(atom2)
        atoms(ilat1)%dimer = .true.
        atoms(ilat2)%dimer = .true.
        atoms(ilat1)%ipair = ilat2
        atoms(ilat2)%ipair = ilat1

    end subroutine create_dimer_pair

    !Return the ilat index of an atoms dimer pair
    pure integer function read_ipair(atom, atoms) result(value)
        !Modules
        use types, only : atom_type, atom_type_ext
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in), dimension(:) :: atoms
        !Variables
        integer :: index
        !Out-bounds-error check
        index = ilattice(atom)
        if (index < 1) then
            value = 0
            return
        end if

        value = atoms(ilattice(atom))%ipair

    end function read_ipair

    !Return the (dimer) pair of an atom as an instance of atom_type
    pure function atom_pair(atom, atoms) result(pair)
        !Modules
        use types, only : atom_type, atom_type_ext
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in), dimension(:) :: atoms
        type(atom_type) :: pair
        integer :: ipair
        ipair = read_ipair(atom, atoms)
        if (ipair == 0) then
            pair = atom_type(-1,-1,-1,-1)
            return
        end if

        pair = atoms(ipair)%atom

    end function atom_pair

    !--------------------------------------
    !Read ilink for input atom from lattice
    !--------------------------------------
    pure integer function read_ilink(atom, atoms) result(ilink)
        !Modules
        use types, only : atom_type_ext,atom_type
        !Variables
        type(atom_type), intent(in) :: atom
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        integer :: ilat
        !Method
        ilat = ilattice(atom)

        if (ilat > 0) then
            ilink = atoms(ilat)%iLink
        else
            ilink = 0
        end if

    end function read_ilink

    !------------------------------------------------
    !If an atom is linked to another, return the atom
    !state of that linked atom. Otherwise return an
    !empty_site (0)
    !------------------------------------------------
    pure integer function read_linked_atom(atom, atoms) result(state)
        !Modules
        use types, only : atom_type_ext, atom_type
        use enumerate, only : types_of_atom
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        !Variables
        integer :: ilink
        !Method
        ilink = read_ilink(atom, atoms)

        if (ilink /= 0) then
            state = atoms(ilink)%state
        else
            state = types_of_atom%empty_site
        end if

    end function read_linked_atom


    !---------------------------------------------------
    !If the input atom is linked to another, return that
    !atom object. If there is no linkage, return an
    !unitilialised atom object
    !---------------------------------------------------
    pure function linked_atom(atom, atoms)
        !Modules
        use types, only : atom_type, atom_type_ext
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        type(atom_type) :: linked_atom
        !Variables
        integer :: ilink
        !Method
        ilink = read_ilink(atom, atoms)

        if (ilink > 0) then
            linked_atom = atoms(ilink)%atom
        else
            linked_atom = atom_type(0,0,0,0)
        end if

    end function linked_atom


    !-----------------------------------------
    !Return atom_type_ext%surface for a passed
    !atom_type
    !Extended atom must first be located
    !-----------------------------------------
    logical function surface_atom(atom, atoms)
        !Modules
        use types, only : atom_type, atom_type_ext
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext),dimension(:),intent(in) :: atoms
        integer :: ilat
        surface_atom = .false.
        !Method
        ilat = ilattice(atom)
        if (ilat < 1) return

        surface_atom = atoms(ilat)%surface

    end function surface_atom


    !------------------------------------------------------
    !Read activation state (act_state) of atom from lattice
    !------------------------------------------------------
    pure integer function read_act(atom, atoms) result(act_state)
        !Modules
        use types, only : atom_type,atom_type_ext
        !Variables
        type(atom_type), intent(in) :: atom
        type(atom_type_ext),dimension(:),intent(in) :: atoms
        integer :: ilat
        !Method
        ilat = ilattice(atom)
        act_state = atoms(ilat)%act_state

    end function read_act

    pure function read_mig(atom, atoms) result(mig)
        !Modules
        use types, only : atom_type,atom_type_ext
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext),dimension(:),intent(in) :: atoms
        integer :: mig
        !Variables
        integer :: ilat
        !Method
        ilat = ilattice(atom)

        mig = atoms(ilat)%mig

    end function read_mig


    !Read mig value of atom and then reset it to 0
    function pop_mig(atom, atoms) result(mig)
        !Modules
        use types, only : atom_type,atom_type_ext
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext),dimension(:),intent(inout) :: atoms
        integer :: mig
        !Variables
        integer :: ilat
        !Method
        ilat = ilattice(atom)

        mig = atoms(ilat)%mig
        atoms(ilat)%mig = 0

    end function pop_mig

    !-------------------------------------------
    !Is input atom activated
    !todo rename to is_activated
    !-------------------------------------------
    pure logical function is_act(atom, atoms) result(activated)
        !Modules
        use types, only : atom_type, atom_type_ext
        use enumerate, only : not_activated, actstate_max
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        !Variables
        integer :: act_state
        !Method

        act_state = read_act(atom, atoms)
        if (act_state > not_activated .and. act_state <= actstate_max) then
            activated = .true.
        else
            activated = .false.
        end if


    end function is_act

    !------------------------------------------------
    !Set an atom site within lat_array to a new value
    !------------------------------------------------
    subroutine set_act_state(atom,atoms,val_new)
        !Modules
        use types,       only : atom_type
        use enumerate,   only : file_units,actstate_max,not_activated
        !Variables
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), dimension(:), intent(inout) :: atoms
        integer, intent(in) :: val_new
        !Main Method
        !Check that value is valid
        if (val_new < not_activated .or. val_new > actstate_max) then
            write(file_units%warning,*) kmc_info%step,"Tried to set activation_state to invalid value",val_new,"|",atom
        else
            atoms(ilattice(atom))%act_state = val_new
        end if

    end subroutine set_act_state

    !--------------------------------------------------
    !Return lattice state value of an input atom object
    !If an 'invalid' atom is passed (eg -1,-1,-1,-1) then
    !the empty_atom value should be returned
    !--------------------------------------------------
    integer function read_atom(atom, atoms) result(value)
        !Modules
        use types,      only : atom_type_ext, atom_type
        use enumerate,  only : file_units, types_of_atom
        !Arguments
        type(atom_type), intent(in)  :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        !Variables
        integer :: ilat

        ilat = ilattice(atom)


        !Ignore atom object designed to be empty (-1,-1,-1,-1)
        if (atom%i == -1 .or. atom%j == -1 .or. atom%basis == -1 .or. atom%k == -1) then
            value = types_of_atom%empty_site
            return
        end if
        !Catch Invalid (through error) atom indices
        if (ilat < 0) then
            write(file_units%warning,*) kmc_info%step, "read_atom() called for 0 atom index:",atom
            value = types_of_atom%empty_site
            return
        end if
        !Catch uninitialised or invalid atom indices larger than simulation bounds
        if (ilat > size(atoms)) then
            write(file_units%warning,*) kmc_info%step, "read_atom() outside simulation bounds", atom
            value = types_of_atom%empty_site
            return
        end if

        !Otherwise, a healthly atom object has been passed - access as normal
        value = atoms(ilat)%state

    end function read_atom

    !------------------------------------------------
    !Set an atom state within lat_array to a new value
    !> If the atom state being passed is 'empty_site'
    !   then it is reset and any atoms it is linked with
    !   are unlinked with it
    !------------------------------------------------
    subroutine set_atom(atom, atoms, val_new)
        !Modules
        use types,       only : atom_type
        use enumerate,   only : file_units, types_of_atom
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext),intent(inout) :: atoms(:)
        integer, intent(in) :: val_new
        !Variables
        integer :: ilat
        !Main Method
        ilat = ilattice(atom)
        !Check that value is valid
        if (val_new < 0 .or. val_new > types_of_atom%acetyl_c2) then
            write(file_units%warning)kmc_info%step,"Tried to set lat_array to invalid atom_type",val_new
        else
            atoms(ilat)%state = val_new
        end if

        !If atom is being removed:
        !Remove
        !activation
        !linking
        !dimer linkage
        !migration counting
        if (val_new == types_of_atom%empty_site) then
            call unlink_atom(atom, atoms)
            call undimer_atom(atom, atoms)
            call atoms(ilat)%reset()

        end if

    end subroutine set_atom

    !---------------------------------------------------
    !Link atom1 and atom2 together using the iLink index
    !Pass atoms via atom_type
    !---------------------------------------------------
    subroutine link_atoms_atom(atom1, atom2, atoms)
        !Modules
        use types, only : atom_type
        !Variables
        type(atom_type), intent(in) :: atom1, atom2
        type(atom_type_ext), dimension(:), intent(inout) :: atoms
        integer :: ilat_1, ilat_2
        !Main Method
        ilat_1 = ilattice(atom1)
        ilat_2 = ilattice(atom2)

        atoms(ilat_1)%iLink = ilat_2
        atoms(ilat_2)%iLink = ilat_1

    end subroutine link_atoms_atom


    !---------------------------------------------------
    !Link atom1 and atom2 together using the iLink index
    !Pass atoms via their index
    !---------------------------------------------------
    subroutine link_atoms_index(ilat1, ilat2, atoms)
        !Modules
        use types, only : atom_type_ext
        !Variables
        integer, intent(in) :: ilat1, ilat2
        type(atom_type_ext), dimension(:), intent(inout) :: atoms
        !Main Method

        atoms(ilat1)%iLink = ilat2
        atoms(ilat2)%iLink = ilat1

    end subroutine link_atoms_index

    !------------------------------------------------
    !Detect whether atoms 1 and 2 are linked together
    !------------------------------------------------
    pure logical function are_linked(atom1, atom2, atoms) result(linked)
        !Modules
        use types, only : atom_type_ext,atom_type
        !Variables
        type(atom_type), intent(in) :: atom1, atom2
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        integer :: ilink1, ilink2, ilat1,ilat2

        ilink1 = read_ilink(atom1, atoms)
        ilat1 = ilattice(atom1)
        ilink2 = read_ilink(atom2, atoms)
        ilat2 = ilattice(atom2)

        if (ilink1 == ilat2 .and. ilink2 == ilat1) then
            linked = .true.
        else
            linked = .false.
        end if

    end function are_linked

    !--------------------------------------------------------------
    !Unlink a linked atom, destroying the link on both linked atoms
    !--------------------------------------------------------------
    subroutine unlink_atom(atom, atoms)
        !Modules
        use types, only : atom_type, atom_type_ext
        !Variables
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), dimension(:), intent(inout) :: atoms
        integer :: ilat_1, ilat_2
        !Main Method
        ilat_1 = ilattice(atom)
        ilat_2 = atoms(ilat_1)%iLink

        if (ilat_2 > 0) then
            atoms(ilat_1)%iLink = 0
            atoms(ilat_2)%iLink = 0
        end if

    end subroutine unlink_atom

    !-----------------------------------------------------------------
    !Remove the dimer reconstruction of an atom as well as for its pair
    !-----------------------------------------------------------------
    subroutine undimer_atom(atom, atoms)
        !Modules
        use types, only : atom_type, atom_type_ext
        !Variables
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), dimension(:), intent(inout) :: atoms
        integer :: ilat_1, ilat_2
        !Main Method
        ilat_1 = ilattice(atom)
        ilat_2 = atoms(ilat_1)%ipair

        if (ilat_2 > 0) then
            atoms(ilat_1)%ipair = 0
            atoms(ilat_1)%dimer = .false.
            atoms(ilat_2)%ipair = 0
            atoms(ilat_2)%dimer = .false.
        end if

    end subroutine undimer_atom


    !-------------------------------------
    !Remove an atom from the surface
    !Deactivate it
    !Set %surface to .false
    !-------------------------------------
    subroutine unsurface_atom(atom, atoms)
        !Modules
        use types, only : atom_type, atom_type_ext
        !Variables
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(inout) :: atoms(:)

        call set_act_state(atom, atoms, 0)
        !Set as non-surface in atoms
        atoms(ilattice(atom))%surface = .false.

    end subroutine unsurface_atom

    pure function read_last_frame_event(atom, atoms) result(last_event)
        !Modules
        use types,  only : atom_type, atom_type_ext
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in), dimension(:) :: atoms
        !Variables
        integer :: last_event

        last_event = atoms(ilattice(atom))%last_event

    end function read_last_frame_event

    !---------------------------------------------------------------------------
    !Set the last_frame logical of an atom within lattice to true or false value
    !And update its last event type
    !---------------------------------------------------------------------------
    subroutine set_last_frame(atom, atoms, last_frame, event)
        !Modules
        use types,  only : atom_type, atom_type_ext
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(inout), dimension(:) :: atoms
        logical, intent(in) :: last_frame
        integer, intent(in) :: event
        !Variables
        integer :: ilat
        ilat = ilattice(atom)

        atoms(ilat)%last_frame = last_frame
        atoms(ilat)%last_event = event

    end subroutine set_last_frame

    !-----------------------------------------------
    !Reset the last_frame field of all atoms
    !%last_frame -> .false.
    !%last_event -> 0
    !-----------------------------------------------
    subroutine reset_last_frame(atoms)
        !Modules
        use types, only : atom_type_ext
        !Arguments
        type(atom_type_ext), intent(inout) :: atoms(:)


        atoms(:)%last_frame = .false.
        atoms(:)%last_event = 0

    end subroutine reset_last_frame

    !-----------------------------------------------------------------------
    !Return the index where input atom is found within the lattice atom list
    !-----------------------------------------------------------------------
    pure integer function ilattice(atom) result(atom_index)
        !Modules
        use types,  only : atom_type
        !Arguments
        type(atom_type),intent(in)  :: atom

        !Method
        atom_index =    atom%basis + &
                        (atom%i-1)*simulation%Nbasis + &
                        (atom%j-1)*simulation%Nx*simulation%Nbasis + &
                        (atom%k-1)*simulation%Ny*simulation%Nx*simulation%Nbasis

    end function ilattice

    !------------------------------------
    !Return atom from input lattice index
    !------------------------------------
    type(atom_type) function atom_from_index(i, atoms) result(atom)
        !Modules
        use types, only : atom_type_ext
        !Variables
        integer, intent(in) :: i
        type(atom_type_ext), intent(in), dimension(:) :: atoms

        if (i > 0) then
            atom = atoms(i)%atom
        else
            atom = atom_type(0,0,0,0)
        end if

        return

    end function atom_from_index

    !----------------------------------------------------------------------
    !Version of ilattice which operates using indicies instead of atom_type
    !----------------------------------------------------------------------
    integer function ilat_index(i,j,basis,k) result(atom_index)

        integer, intent(in) :: i,j,basis,k
        atom_index = basis + (i-1)*simulation%Nbasis + (j-1)*simulation%Nx*simulation%Nbasis + (k-1)*simulation%Ny*simulation%Nx*simulation%Nbasis
        return

    end function ilat_index


    !------------------------------------------------------------
    !Return the limits of lattice indices for k min and max input
    !------------------------------------------------------------
    subroutine return_ilattice_limits(k_min,k_max,limits)
        use types,      only : atom_type

        integer,intent(in)                  :: k_min,k_max
        integer,intent(out),dimension(2)    :: limits
        type(atom_type)                     :: atom_start,atom_end

        atom_start = atom_type(1,1,1,k_min)
        atom_end = atom_type(simulation%Nx,simulation%Ny,simulation%Nbasis,k_max)
        limits(1) = ilattice(atom_start)
        limits(2) = ilattice(atom_end)

    end subroutine return_ilattice_limits

    !------------------------------------------------------
    !Initialises a new rate_type instance within the passed
    !rate list, at ilast_rate + 1.
    !Returns augmented rate_list and increments ilast_rate
    !atom (%site)
    !type_of_rate (%event)
    !rate_val (%rate_val)
    !opt_site (%site2)
    !------------------------------------------------------
    subroutine set_rate(atom, type_of_rate, rate_value, rate_list, ilast_rate, opt_site)
        !Modules
        use types,      only : atom_type, rate_type
        !Arguments
        type(atom_type),intent(in) :: atom
        integer, intent(in) :: type_of_rate
        real(dp), intent(in) :: rate_value
        type(rate_type), intent(inout), dimension(:) :: rate_list
        integer, intent(inout) :: ilast_rate
        type(atom_type),intent(in),optional :: opt_site
        !Variables
        type(rate_type) :: rate
        type(atom_type) :: site2

        if (rate_value <= 1E-10_dp) return

        if (present(opt_site)) then
            site2 = opt_site
        else
            site2 = atom_type(0,0,0,0)
        end if

        rate = rate_type(site=atom, rate_val=rate_value, event=type_of_rate, site2=site2, ilat=0)
        rate_list(ilast_rate + 1) = rate
        ilast_rate = ilast_rate + 1

        counters%rates_set_per_it(type_of_rate) = counters%rates_set_per_it(type_of_rate) + 1

    end subroutine set_rate
    

    !--------------------------------------------------------------------------------------------
    !Return the adsorbing atom species (so that activation can be calculated) from the event type
    !--------------------------------------------------------------------------------------------
    integer function ads_event2species(event) result(event_species)
        !Modules
        use enumerate, only : species, tRates, file_units
        !Variables
        integer, intent(in) :: event
        !Method
        if (event == tRates%DinsertCH3 .or. event == tRates%TinsertCH3) then
            event_species = species%CH2
        elseif (event == tRates%DinsertCH2 .or. event == tRates%TinsertCH2) then
            event_species = species%CH2
        elseif (event == tRates%DinsertCH .or. event == tRates%TinsertCH) then
            event_species = species%CH
        elseif (event == tRates%DinsertC .or. event == tRates%TinsertC) then
            event_species = species%C
        else
            write(file_units%error,*) "ads_event2species: invalid event -",event
            STOP
        end if

    end function ads_event2species


    subroutine add_unit_to_open_list(u)
        !Arguments
        integer, intent(in) :: u    !file unit
        !Variables
        logical :: is_open
        !Method
        !Initialise unit_open counter (0) if not yet initialised
        if (open_units(0) == -1) open_units(0) = 1
        inquire(unit=u, opened=is_open)
        if (is_open) then
            open_units(open_units(0)) = u
            open_units(0) = open_units(0) + 1
        end if

    end subroutine add_unit_to_open_list


    !----------------------------------------
    !Take a value and return the parity (+/-)
    !----------------------------------------
    integer function parity_real(value)
        real(dp), intent(in) :: value

        if (value == 0.0_dp) then
            parity_real = 0
        else
            parity_real = int(value / abs(value))
        end if

    end function parity_real

    integer function parity_int(value)
        integer, intent(in) :: value

        if (value == 0) then
            parity_int = 0
        else
            parity_int = int(value / abs(value))
        end if

    end function parity_int

end module variables

