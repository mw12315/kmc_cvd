module basis

    use kinds,          only : dp

    implicit none

contains

    subroutine position(atom, atoms, atom_coords)
        !Modules
        use types, only : atom_type, atom_type_ext, coord_type
        use variables, only : read_ipair, read_atom, atom_pair, read_ilink, linked_atom, &
                dimer_disp, c2_disp
        use enumerate, only : types_of_atom
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in), dimension(:) :: atoms
        type(coord_type),intent(out) :: atom_coords
        !Variables
        integer :: ipair
        type(atom_type) :: pair, base_atom
        type(coord_type) :: pair_coords, diff
        integer :: atom_state
        integer :: pbc_x, pbc_y   ![1,-1] depending on if across a periodic boundary or not


        atom_state = read_atom(atom, atoms)
        if (atom_state == types_of_atom%surf_carb) then
            atom_coords = raw_xyz(atom)

            ipair = read_ipair(atom, atoms)
            !Dimer reconstructions are moved towards their neighbour
            if (ipair /= 0) then
                pair = atom_pair(atom, atoms)
                pair_coords = raw_xyz(pair)

                call detect_pbc_boundary(atom, pair, pbc_x, pbc_y)
                diff%x = pair_coords%x - atom_coords%x
                diff%y = pair_coords%y - atom_coords%y
                diff%z = 0

                atom_coords%x = atom_coords%x + (diff%x/abs(diff%x)) * (dimer_disp*pbc_x)
                atom_coords%y = atom_coords%y + (diff%y/abs(diff%y)) * (dimer_disp*pbc_y)

            end if
        else if (atom_state == types_of_atom%acetyl_c2) then
            !Acetyl atoms sit above their linked atom
             base_atom = linked_atom(atom, atoms)
             atom_coords = raw_xyz(base_atom)
             atom_coords%z = atom_coords%z + c2_disp
        else
            !Allow position of empty site to be returned
            atom_coords = raw_xyz(atom)
        end if


    end subroutine position


    function ret_height(atom, atoms) result(height)
        !Modules
        use types, only : atom_type, atom_type_ext, coord_type
        !Arguments
        type(atom_type),intent(in) :: atom
        type(atom_type_ext),intent(in), dimension(:) :: atoms
        real(dp) :: height
        !Variables
        type(coord_type) :: coords
        !Method
        call position(atom, atoms, coords)

        height = coords%z

    end function ret_height

    !Return a coord object of an atoms position
    !Function interface for position
    function get_atom_position(atom, atoms) result(coord)
        !Modules
        use types, only : atom_type, atom_type_ext, coord_type
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in), dimension(:) :: atoms
        type(coord_type) :: coord

        call position(atom, atoms, coord)

    end function get_atom_position


    !-----------------------------------------------------
    !Return an coord instance of the raw position returned
    !by an atom at in the cell (x,y,z) and at basis b
    !-----------------------------------------------------
    function raw_xyz(atom) result(xyz)
        !Modules
        use types, only : atom_type, coord_type
        use variables, only : a, b, c, a0
        use enumerate, only : file_units
        !Arguments
        type(atom_type), intent(in) :: atom
        type(coord_type) :: xyz

        select case(atom%basis)
        case(1)
            xyz%x=((((atom%i-1)*a(1))+((atom%j-1)*b(1))+(atom%k*c(1)))*a0)
            xyz%y=((((atom%i-1)*a(2))+((atom%j-1)*b(2))+(atom%k*c(2)))*a0)
            xyz%z=((((atom%i-1)*a(3))+((atom%j-1)*b(3))+(atom%k*c(3)))*a0)
        case(2)
            xyz%x=((((atom%i-1)*a(1))+((atom%j-1)*b(1))+(atom%k*c(1))+0.5)*a0)
            xyz%y=((((atom%i-1)*a(2))+((atom%j-1)*b(2))+(atom%k*c(2))+0.5)*a0)
            xyz%z=((((atom%i-1)*a(3))+((atom%j-1)*b(3))+(atom%k*c(3)))*a0)
        case(3)
            xyz%x=((((atom%i-1)*a(1))+((atom%j-1)*b(1))+(atom%k*c(1))+0.25)*a0)
            xyz%y=((((atom%i-1)*a(2))+((atom%j-1)*b(2))+(atom%k*c(2))+0.25)*a0)
            xyz%z=((((atom%i-1)*a(3))+((atom%j-1)*b(3))+(atom%k*c(3))+0.25)*a0)
        case(4)
            xyz%x=((((atom%i-1)*a(1))+((atom%j-1)*b(1))+(atom%k*c(1))+0.75)*a0)
            xyz%y=((((atom%i-1)*a(2))+((atom%j-1)*b(2))+(atom%k*c(2))+0.75)*a0)
            xyz%z=((((atom%i-1)*a(3))+((atom%j-1)*b(3))+(atom%k*c(3))+0.25)*a0)
        case(5)
            xyz%x=((((atom%i-1)*a(1))+((atom%j-1)*b(1))+(atom%k*c(1))+0.5)*a0)
            xyz%y=((((atom%i-1)*a(2))+((atom%j-1)*b(2))+(atom%k*c(2)))*a0)
            xyz%z=((((atom%i-1)*a(3))+((atom%j-1)*b(3))+(atom%k*c(3))+0.5)*a0)
        case(6)
            xyz%x=((((atom%i-1)*a(1))+((atom%j-1)*b(1))+(atom%k*c(1)))*a0)
            xyz%y=((((atom%i-1)*a(2))+((atom%j-1)*b(2))+(atom%k*c(2))+0.5)*a0)
            xyz%z=((((atom%i-1)*a(3))+((atom%j-1)*b(3))+(atom%k*c(3))+0.5)*a0)
        case(7)
            xyz%x=((((atom%i-1)*a(1))+((atom%j-1)*b(1))+(atom%k*c(1))+0.75)*a0)
            xyz%y=((((atom%i-1)*a(2))+((atom%j-1)*b(2))+(atom%k*c(2))+0.25)*a0)
            xyz%z=((((atom%i-1)*a(3))+((atom%j-1)*b(3))+(atom%k*c(3))+0.75)*a0)
        case(8)
            xyz%x=((((atom%i-1)*a(1))+((atom%j-1)*b(1))+(atom%k*c(1))+0.25)*a0)
            xyz%y=((((atom%i-1)*a(2))+((atom%j-1)*b(2))+(atom%k*c(2))+0.75)*a0)
            xyz%z=((((atom%i-1)*a(3))+((atom%j-1)*b(3))+(atom%k*c(3))+0.75)*a0)
        case default
            write(file_units%error,*) "basis out of range", atom
            xyz = coord_type(0.0, 0.0, 0.0)
        end select

    end function raw_xyz


    subroutine detect_pbc_boundary(atom, pair, x, y)
        !Modules
        use types, only : atom_type
        !Arguments
        type(atom_type), intent(in) :: atom, pair
        integer, intent(out) :: x, y

        !Detect periodic boundary conditions
        if (abs(atom%i - pair%i) > 1) then
            x = -1
        else
            x = 1
        end if
        if (abs(atom%j - pair%j) > 1) then
            y = -1
        else
            y = 1
        end if

    end subroutine detect_pbc_boundary



end module basis
