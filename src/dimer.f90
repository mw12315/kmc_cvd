!-------------------------------------
!Contains variables and modules related to dimerisation
!-------------------------------------
module dimer

    use kinds,      only : dp

    implicit none

    integer, dimension(8,2) :: dimer_directions = &
            reshape((/ 0,6, 2,0, 5,7, 3,1, 3,5, 1,7, 0,4, 8,0  /), &
            shape = (/ 8,2 /), order = (/ 2,1 /))

    !Generic Interfaces
    interface isolated_dimer
        module procedure isolated_dimer_current
        module procedure isolated_dimer_potential
    end interface isolated_dimer

    interface part_same_dimer
        module procedure part_same_dimer_atom
        module procedure part_same_dimer_index
    end interface

    interface could_form_dimer
        module procedure could_form_dimer_atom
        module procedure could_form_dimer_atoms
    end interface could_form_dimer

    interface saturated_dimer
        module procedure saturated_dimer_atom
        module procedure saturated_dimer_atoms
    end interface saturated_dimer

contains

    !------------------------------------------------------------
    !Following a reaction involving site:
    !Check whether a dimer should be removed or formed
    !Atoms around the site are check and dimers removed or formed
    !according to:
    !create_dimer_if_possible()
    !remove_dimer_if_required()
    !------------------------------------------------------------
    subroutine dimer_assessment(site, atoms)
        !Modules
        use types,      only : atom_type, atom_type_ext
        use enumerate,  only : types_of_atom
        use variables,  only : read_atom
        use periodic,   only : ret_neighbour
        !Variables
        type(atom_type), intent(in) :: site
        type(atom_type_ext), intent(inout) :: atoms(:)
        logical :: filled_site                  !Is the input site filled or empty?
        type(atom_type) :: N1,N2,N3,N4          !Neighbours (type atom)
        !Main Method
        !Determine if an atom as been added or removed
        if (read_atom(site, atoms) /= types_of_atom%empty_site) then
            filled_site = .true.
        else
            filled_site = .false.
        end if
        
        !Get Site Neighbours
        N2 = ret_neighbour(site,2)
        N3 = ret_neighbour(site,3)

        !-----------------
        !Atom Added
        !-dimer insertion
        !-trough insertion
        !-atom migration
        !-----------------
        if (filled_site) then
            !Break dimers?
            call remove_dimer_if_required(N2, atoms)
            call remove_dimer_if_required(N3, atoms)
            !Form dimers
            call create_dimer_if_possible(site, atoms)
        else
        !------------------
        !Atom Remove
        !-Surface atom etch
        !------------------
            !Break Dimers
            call remove_dimer_if_required(site, atoms)

            !Form Dimers
            call create_dimer_if_possible(N2, atoms)
            call create_dimer_if_possible(N3, atoms)
        end if

    end subroutine dimer_assessment

    !---------------------------------------------------------------------------------
    !check if a dimer can form between atoms neighbouring "atom": if possible, form it
    !CURRENT REQUIREMENTS
    !- Both atoms can individually form a dimer
    !- Both don't exceed the bonding threshold collectively
    !- References (relating to NN=2 Threshold)
    !   1. Battaile, C. C., Srolovitz, D. J. & Butler, J. E., J. Appl. Phys. 82, 6293–6300 (1997).
    !   2. Srolovitz, D. J., Dandy, D. S., Butler, J. E., Battaile, C. C. & Paritosh, Jom 49, 42–47 (1997).
    !   3. Battaile, C. C., Srolovitz, D. J. & Butler, J. E., Diam. Relat. Mater. 6, 1198–1206 (1997).
    !   4. Battaile, C. C., Srolovitz, D. J. & Butler, J. E., Journal of Electronic Materials 26, 960–965 (1997).
    !   5. Battaile, C. . C., Srolovitz, D. . & Butler, J. . E., J. Cryst. Growth 194, 353–368 (1998).
    !---------------------------------------------------------------------------------
    subroutine create_dimer_if_possible(atom, atoms)
        !Modules
        use types,      only : atom_type, atom_type_ext
        use variables,  only : read_atom, counters, set_last_frame
        use enumerate,  only : types_of_atom
        use periodic,   only : get_periodic_neighbour
        use input,      only : reactions
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(inout) :: atoms(:)
        !Variables
        integer :: directions(2)
        integer :: direction
        logical :: can_form_dimer(2)
        type(atom_type) :: pair(2)
        integer :: i
        logical :: aligned

        can_form_dimer(:) = .false.
        aligned = .false.
        pair(1) = atom_type(0,0,0,0)
        pair(2) = atom_type(0,0,0,0)

        !--------------------------------------
        !REQUIREMENTS
        !- Not part of a dimer already
        !- Not a C2H2 unit
        !- Is activated
        !- Pair is activated (biradical)
        !- Both have NN <= threshold
        !- No bridging site between atoms
        !--------------------------------------
        if (.not. could_form_dimer(atom,atoms)) return

        !Get basis of pairs
        pair(:)%basis = pair_basis(atom%basis)
        !Get directions (2) to look in to pair
        directions = get_dim_dirs(atom%basis)
        pair(:)%k = atom%k

        !Loop through potential pair atoms (2) and assign can_form_dimer(:)
        do i=1,size(directions,1)
            !Construct dimer pair
            direction = directions(i)
            call get_periodic_neighbour(atom%i,atom%j,direction,  pair(i)%i, pair(i)%j)
                if (could_form_dimer(pair(i), atoms)) then
                    can_form_dimer(i) = could_form_dimer(atom, pair(i), atoms, skip_1atom=.true.)
            else
                can_form_dimer(i) = .false.
                cycle
            end if
        enddo

        !Check aligment of adjacent dimers and choose one to take priority
        if (all(can_form_dimer)) then
            if (reactions%align_dimers) then
                call align_dimer_formation(atom, pair, atoms, can_form_dimer)
                aligned = .true.
            else
                can_form_dimer(2) = .false.
            end if
        end if

        !CREATE DIMER IF ABLE
        do i=1,2
            if (can_form_dimer(i)) then
                call create_dimer(atom, pair(i), atoms)
                if (aligned) then
                    call set_last_frame(atom, atoms, .true., -1)
                    call set_last_frame(pair(i), atoms, .true., -1)
                end if
            end if
        end do

    end subroutine create_dimer_if_possible

    !---------------------------------------------------------------
    !Could an SINGLE atom form a dimer based solely on its own state
    !Not part of a dimer already
    !Not empty or a linked atom
    !is activated
    !not forming more than N Bonds
    ! - 3 bonds before dimerisation -> bulk surface atom -> bad
    !---------------------------------------------------------------
    logical function could_form_dimer_atom(atom, atoms) result(can_form)
        !Modules
        use types, only : atom_type, atom_type_ext
        use variables, only : read_dimer, read_ilink, is_act, read_atom
        use enumerate, only : types_of_atom
        use periodic, only : num_bonds
        use input, only : dimerf
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        !Variables
        !Method
        can_form = .false.
        if (read_dimer(atom, atoms)) return !Already in dimer
        if (read_atom(atom, atoms) /= types_of_atom%surf_carb) return !Empty or non-carbon (eg C2H2)
        if (read_ilink(atom, atoms) /= 0) return !Linked within another atom (C2H2 base atom for example)
        !Atom must be activated (biradical) else return without dimer creation
        if (is_act(atom, atoms)) then !Activated
            if (num_bonds(atom, atoms) <= dimerf%nb_form_thres) then
                can_form = .true.
            end if
        end if

    end function could_form_dimer_atom


    !---------------------------------------------------------------
    !Can 2 atoms collectively form a dimer?
    !Assumes that both can form dimers independently - override this
    !by passing skip_1atom = .true.
    !---------------------------------------------------------------
    logical function could_form_dimer_atoms(atom1, atom2, atoms, skip_1atom) result(can_form)
        !Modules
        use types, only : atom_type, atom_type_ext
        use variables, only : read_dimer, read_ilink, is_act, read_atom
        use enumerate, only : types_of_atom
        use periodic, only : num_bonds
        use input, only : dimerf
        !Arguments
        type(atom_type), intent(in) :: atom1, atom2
        type(atom_type_ext), intent(in) :: atoms(:)
        logical, optional, intent(in) :: skip_1atom
        !Variables
        logical :: atom_single_could_form(2)
        type(atom_type) :: bridge_atom
        integer :: n_bonds(2)

        can_form = .false.

        !Check individual atoms if not skipped
        if (present(skip_1atom)) then
            if (.not. skip_1atom) then
                atom_single_could_form(1) = could_form_dimer_atom(atom1, atoms)
                atom_single_could_form(2) = could_form_dimer_atom(atom2, atoms)
                if (any(atom_single_could_form .eqv. .false.)) return
            end if
        end if

        !Bridge atom empty
        !   X
        !  / \
        ! A1 A2
        call return_bridging_atom(atom1, atom2, bridge_atom)
        if (read_atom(bridge_atom, atoms) /= types_of_atom%empty_site) return

        !Both atoms cannot have combined threshold bonds already
        ! nb_threshold = 3 -> combined_threshold = 6
        !  C       C
        !/  \     /  \
        !    A1  A2
        n_bonds(1) = num_bonds(atom1, atoms)
        n_bonds(2) = num_bonds(atom2, atoms)
        if (sum(n_bonds) >= 2 * dimerf%nb_form_thres) return

        can_form = .true.


    end function could_form_dimer_atoms


    !----------------------------------------------------------------
    !Return the type of dimer atom is part of
    !NONE (0): Not part of a dimer
    !ISO (1): C-C without any additional bonds C  C-C  C       C
    !ADJ (2): At least one carbon adjacent to bridging carbon C C-C C
    !----------------------------------------------------------------
    integer function dimer_bonding(atom, atoms)
        use types, only : atom_type, atom_type_ext
        use variables, only : read_dimer, atom_pair
        use periodic, only : num_bonds
        use enumerate, only : dimer_env
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        !Variables
        type(atom_type) :: pair

        if (read_dimer(atom, atoms) .eqv. .false.) then
            dimer_bonding = dimer_env%NONE
            return
        end if

        pair = atom_pair(atom, atoms)

        if (num_bonds(atom, atoms) > 3 .or. num_bonds(pair, atoms) > 3) then
            dimer_bonding = dimer_env%ADJ
        else
            dimer_bonding = dimer_env%ISO
        end if

    end function dimer_bonding

    !-----------------------------------------------
    !Create a dimer between atom1 and atom 2
    !Checks made:
    !Neither atom already part of a dimer
    !   - ERROR if this is the case
    !       - overide error by passing else_activate
    !       - atoms activated instead
    !Both atoms exist
    !Create dimer between atoms and deactivate them
    !-----------------------------------------------
    subroutine create_dimer(atom1, atom2, atoms, else_activate)
        !Modules
        use types,      only : atom_type, atom_type_ext
        use variables,  only : read_dimer, create_dimer_pair, read_atom, &
                kmc_info, surface, ilattice, counters, set_act_state
        use enumerate,  only : file_units, types_of_atom, actstate_min
        use activate,   only : deactivate_atom
        !Arguments
        type (atom_type), intent(in) :: atom1, atom2
        type(atom_type_ext), intent(inout) :: atoms(:)
        logical, optional, intent(in) :: else_activate  !If unable don't warn and activate instead
        !Variables
        integer :: iatom1, iatom2
        logical :: no_warn_on_fail
        logical :: activate_on_fail
        !Main Method
        activate_on_fail = .false.
        if (present(else_activate)) no_warn_on_fail = .true.
        !Check both aren't already in dimers
        if (read_dimer(atom1, atoms) .or. read_dimer(atom2, atoms)) then
            if (no_warn_on_fail) then
                activate_on_fail = .true.
            else
                write(file_units%error,'(i10,a)') kmc_info%step," Trying to create a dimer when a dimer already exists"
                write(file_units%error,'(a10,2(4(i4),L2))') " ", atom1, read_dimer(atom1, atoms), atom2, read_dimer(atom2, atoms)
                return
            end if
        end if

        iatom1 = ilattice(atom1)
        iatom2 = ilattice(atom2)

        if (read_atom(atom1, atoms) == types_of_atom%empty_site .or. &
                read_atom(atom2, atoms) == types_of_atom%empty_site) then
            write(file_units%error,*) kmc_info%step,"Create Dimer: Attempt to form dimer with an empty site"
            return
        end if

        if (activate_on_fail) then
            call set_act_state(atom1, atoms, actstate_min)
            call set_act_state(atom2, atoms, actstate_min)
            counters%failed_dimer_attempts = counters%failed_dimer_attempts + 1
            return
        end if

        !Dimer Creation Proceeds
        call create_dimer_pair(atom1,atom2, atoms)

        !Deactivate atoms constituting dimer
        !representing electrons forming the dimer bond
        call deactivate_atom(atom1, atoms)
        call deactivate_atom(atom2, atoms)

        !Increment dimer count
        surface%total_dimers = surface%total_dimers + 1

    end subroutine create_dimer

    !-------------------------------------------------------------------
    !Remove dimer if required:
    !Returns if atom not in a dimer
    !Requirements for removal:
    !- Atom and Pair collectively form (2 * break_dimer threshold) bonds
    !- Either atom does not exist
    !- Atom bridging the atoms is filled
    !-------------------------------------------------------------------
    subroutine remove_dimer_if_required(atom, atoms)
        !Modules
        use types,      only : atom_type, atom_type_ext
        use enumerate,  only : types_of_atom
        use periodic,   only : num_bonds
        use variables,  only : read_atom, read_dimer, NN_bulk, atom_pair
        use input, only : reactions, dimerf
        !Arguments
        type (atom_type), intent(in) :: atom
        type(atom_type_ext), intent(inout) :: atoms(:)
        !Variables
        type(atom_type) :: bridging_atom
        integer :: iatom
        integer :: NN
        logical :: break_dimer
        type(atom_type) :: dimer_atoms(2)
        !Main Method
        if (.not. read_dimer(atom, atoms)) return

        dimer_atoms = [atom, atom_pair(atom, atoms)]

        !NBs of both atoms (subtract dimer C-C bonds) > threshold
        if ((num_bonds(dimer_atoms(1), atoms) + num_bonds(dimer_atoms(2), atoms)) >= 2 * dimerf%nb_form_thres + 2) then
            call remove_dimer(dimer_atoms(1), dimer_atoms(2), atoms)
            return
        end if

        do iatom=1, size(dimer_atoms)
            !Either of dimer no longer exists
            if (read_atom(dimer_atoms(iatom), atoms) == types_of_atom%empty_site) then
                call remove_dimer(dimer_atoms(1), dimer_atoms(2), atoms)
                return
            end if
        end do

        !Bridging atom above filled
        call return_bridging_atom(dimer_atoms(1), dimer_atoms(2), bridging_atom)
        if (read_atom(bridging_atom, atoms) /= types_of_atom%empty_site) then
            call remove_dimer(dimer_atoms(1), dimer_atoms(2), atoms)
            return
        end if


    end subroutine remove_dimer_if_required

    !-------------------------------------
    !Break a dimer between atom1 and atom2
    !-------------------------------------
    subroutine remove_dimer(atom1, atom2, atoms)
        !Modules
        use types,      only : atom_type,atom_type_ext
        use variables,  only : ilattice
        use periodic,   only : num_bonds
        use activate,   only : activate_atom
        !Arguments
        type(atom_type), intent(in) :: atom1,atom2
        type(atom_type_ext), intent(inout) :: atoms(:)
        !Variables
        integer :: iatom1,iatom2    !Atom indices
        !Main Method
        iatom1 = ilattice(atom1)
        iatom2 = ilattice(atom2)

        !Set dimer state and unlink via ipair
        atoms(iatom1)%dimer = .false.
        atoms(iatom1)%ipair = 0
        atoms(iatom2)%dimer = .false.
        atoms(iatom2)%ipair = 0

        !Only activate atoms if no more than 2 nearest neighbours
        !otherwise, one is not taking to account the presence of a bonded atom
        !which will use these radical electrons
        call activate_atom(atom1,num_bonds(atom1, atoms), atoms)
        call activate_atom(atom2,num_bonds(atom2, atoms), atoms)

    end subroutine remove_dimer

    !-------------------------------------
    !Return atom bridging 2 atoms
    !-------------------------------------
    subroutine return_bridging_atom(atom1,atom2,bridge_atom)
        !-------
        !Modules
        !-------
        use types,      only : atom_type,same_atom
        use enumerate,  only : file_units
        use variables,  only : kmc_info
        use periodic,   only : ret_neighbour
        !---------
        !Variables
        !---------
        type (atom_type), intent(in) :: atom1,atom2
        type (atom_type), intent(out) :: bridge_atom
        !-----------
        !Main Method
        !-----------
        !N1a N4b common
        if (same_atom(ret_neighbour(atom1,1),ret_neighbour(atom2,4))) then
            bridge_atom = ret_neighbour(atom1,1)
            return
        end if
        !N4a N1b common
        if (same_atom(ret_neighbour(atom1,4),ret_neighbour(atom2,1))) then
            bridge_atom = ret_neighbour(atom1,4)
            return
        end if

        !If no common neighbour found
        bridge_atom = atom_type(0,0,0,0)
        write(file_units%warning,*) kmc_info%step,"return_bridging_atom(): no common atom",atom1,atom2

    end subroutine return_bridging_atom

    !-----------------------------------------------------------
    !Taking an input atom (able to form a surface dimer),
    !determining which dimer would allow greatest alignment with
    !adjacent surface dimer units
    !-----------------------------------------------------------
    subroutine align_dimer_formation(atom, pairs_a, atoms, log_arr)
        !Modules
        use types, only : atom_type,atom_type_ext
        use input, only : reactions
        use variables, only : parity, counters
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type), intent(in) :: pairs_a(2)   !Array of pairs
        type(atom_type_ext), intent(in) :: atoms(:)
        logical, intent(inout) :: log_arr(2)
        !Variables
        integer :: i
        integer :: pairs_e(2)                       !Array of pair dimer directions
        real(dp) :: mean_alignment                  !Average alignment of dimers surrounding atom

        !Main Method
        !Look along dimer rows -> column array
        !Align with average direction if possible

        !Get enumerated directions for possible dimer bonds
        do i=1,size(pairs_a)
            pairs_e(i) = enum_dimer(atom,pairs_a(i), atoms)
        end do

        !Get average alignment of dimer rows around atom
        mean_alignment = sum(dimer_alignment(atom, atoms, reactions%d_align_range))/((2*reactions%d_align_range))
        if (parity(pairs_e(1)) + parity(mean_alignment) > 0) then !current dimer is aligned with average alignment
            log_arr(:) = (/ .true., .false. /)
        else
            log_arr(:) = (/ .false., .true. /)
        end if

        counters%d_align(1) = counters%d_align(1) + 1

    end subroutine align_dimer_formation

    !todo split function into a the array constructor and mean return value
    !---------------------------------------------
    !Traverse up and down a 100 chain from atom
    !return an array of dimer alignment directions
    !---------------------------------------------
    function dimer_alignment(atom, atoms, range) result(alignment)
        !Modules
        use types, only : atom_type, atom_type_ext
        use periodic, only : get_site_from_N_path
        use variables, only : read_dimer,read_ipair,read_atom,atom_from_index
        !Arguments
        type(atom_type),intent(in) :: atom
        integer, intent(in) :: range
        type(atom_type_ext), intent(in) :: atoms(:)
        integer :: alignment(-range:range)          !alignment(below:above)
        !Variables
        integer :: i
        integer, allocatable :: upath(:), dpath(:)  !Npaths for up and down dimer rows (up=+x or +y, down =-x or -y)
        type(atom_type) :: a, p                     !Atom, pair
        !Main Method

        !Construct path for successive row traversal
        allocate(upath(2*range))
        allocate(dpath(2*range))
        do i=1,range
            upath((2*i)-1:2*i) = (/ 3, 4 /)
            dpath((2*i)-1:2*i) = (/ 2, 1 /)
        end do

        !Traverse from bottom to top
        alignment = 0
        do i=-range,range
            if (i == 0) cycle

            !UP
            a = get_site_from_N_path(atom, upath((2*abs(i))-1:2*abs(i)))
            if (read_dimer(a, atoms)) then
                p = atom_from_index(read_ipair(a, atoms), atoms)
                alignment(i) = enum_dimer(a,p, atoms)
            end if
            !DOWN
            a = get_site_from_N_path(atom, dpath((2*abs(i))-1:2*abs(i)))
            if (read_dimer(a, atoms)) then
                p = atom_from_index(read_ipair(a, atoms), atoms)
                alignment(i) = enum_dimer(a,p, atoms)
            end if
        end do

    end function dimer_alignment


    !-------------------------------------------------------------------------------
    !Return a number associated with an atoms alignment with its neighbouring dimers
    !If parity of atom and nearby dimers is the same (in same average direction) then
    !return 0
    !-------------------------------------------------------------------------------
    real(dp) function alignment_of_dimer(atom, atoms, range) result(alignment)
        !Modules
        use types, only : atom_type, atom_type_ext
        use variables, only : atom_pair
        use variables, only : parity
        !Arguments
        type(atom_type),intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        integer, intent(in) :: range
        !Variables
        integer :: parity_atom, parity_nearby
        real(dp) :: alignment_dir   !Alignment of nearby dimers

        parity_nearby = parity(sum(dimer_alignment(atom, atoms, range)) / (2 * range))
        parity_atom = parity(enum_dimer(atom, atom_pair(atom, atoms), atoms))

        alignment = 1 - (abs(parity_nearby + parity_atom) / 2)

    end function alignment_of_dimer

    !----------------------------------------------------------------
    !For an input dimer, check it's alignment with surrounding dimers
    !and determine if it can realign for greater alignment
    !iso_dimer_can_break controls whether ISO type dimers could break
    !and therefore be realigned
    !----------------------------------------------------------------
    subroutine improve_dimer_alignment(atom,atoms)
        !Modules
        use types, only : atom_type, atom_type_ext, same_atom
        use variables, only : read_dimer, read_atom, is_act, counters, parity, set_last_frame
        use input, only : reactions, dimerf
        use enumerate, only : file_units, dimer_env
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(inout) :: atoms(:)
        !Variables
        type(atom_type) :: pos_pairs(2)
        type(atom_type) :: alt                              !Alternative pair atom
        type(atom_type) :: cur_pair                         !Current pair of the atom
        integer :: curr_dir                                 !Enumeration of current dimer
        integer :: alt_dir                                  !Enumeration of alternate dimer
        integer :: l_align((2*reactions%d_align_range)+1)   !Local alignment array of nearby dimers
        real(dp) :: alignment_dir                           !Alignment direction of nearby dimers
        integer :: dimer_type                               !The type of dimer currently formed

        !Main Method
        cur_pair = return_pair_atom(atom, atoms)
        dimer_type = dimer_bonding(atom, atoms)
        if (dimer_type == dimer_env%NONE) return
        !Isolated dimers are unbreakable unless overriden using iso_dimer_can_break
        if (dimer_type == dimer_env%ISO) then
            if (.not. dimerf%iso_dimer_can_break) then
                return
            end if
        end if
        pos_pairs = potential_pairs(atom)

        !Find alternative site
        alt = merge(pos_pairs(1),pos_pairs(2), .not. same_atom(cur_pair,pos_pairs(1)))
        !Check possibility of forming a dimer with alternative site
        if (.not. could_form_dimer(alt, atoms)) return

        l_align = dimer_alignment(atom, atoms, reactions%d_align_range)
        alignment_dir = sum(l_align)/((2*reactions%d_align_range))
        !Compare with potential alignment
        curr_dir = enum_dimer(atom,cur_pair, atoms)
        alt_dir = enum_dimer(atom,alt, atoms)
        if (parity(curr_dir) + parity(alignment_dir) > 0) then !current dimer is aligned with average alignment
            return
        else
            !Check dimer formation chance as a unit
            if (could_form_dimer(atom, alt, atoms, skip_1atom=.true.)) then
                call remove_dimer(atom,cur_pair,atoms)
                call create_dimer(atom,alt,atoms)
                counters%d_align(2) = counters%d_align(2) + 1
                call set_last_frame(atom, atoms, .true., -1)
                call set_last_frame(alt, atoms, .true., -1)
            else
                return
            end if
        end if

    end subroutine improve_dimer_alignment


    !----------------------------------------------------------
    !Return atoms which could form a dimer pair with input atom
    !----------------------------------------------------------
    function potential_pairs(atom) result(pairs)
        !Modules
        use types, only : atom_type
        use periodic, only : ij_cell_from_dir
        !Arguments
        type(atom_type),intent(in) :: atom
        !Variables
        type(atom_type) :: pairs(2)
        integer :: d
        integer :: ni, nj
        integer :: dirs(2)    !Periodic look directions
        !Main Method
        dirs = get_dim_dirs(atom%basis)

        do d=1,size(dirs)
            call ij_cell_from_dir(atom%i, atom%j, dirs(d), &
                                      ni, nj)

            pairs(d) = atom_type(ni, nj, pair_basis(atom%basis), atom%k)
        end do

    end function potential_pairs

    !--------------------------------------
    !Enumerate a dimer pairing into a +1/-1
    !--------------------------------------
    integer function enum_dimer(atom, pair, atoms) result(enumeration)
        !Modules
        use types, only : atom_type, atom_type_ext, coord_type
        use basis, only : position
        !Arguments
        type(atom_type), intent(in) :: atom, pair
        type(atom_type_ext), intent(in), dimension(:) :: atoms
        !Variables
        type(coord_type) :: a_pos, p_pos        !xyz positions of atom,pair
        type(coord_type) :: vec                 !vector between a_pos and p_pos (p_pos - a_pos)
        !Main Method
        call position(atom, atoms, a_pos)
        call position(pair, atoms, p_pos)
        vec%x = (p_pos%x - a_pos%x)/abs(p_pos%x - a_pos%x)
        vec%y = (p_pos%y - a_pos%y)/abs(p_pos%y - a_pos%y)
        vec%z = 0.0_dp
        enumeration = 0

        !Assign enumeration
        if (nint(vec%x) == nint(vec%y)) then    !Dimer layer 1
            if (nint(vec%x) == +1) then
                enumeration = +1
            else if (nint(vec%x) == -1) then
                enumeration = -1
            end if
        else
            if (nint(vec%y) == +1) then         !Dimer layer 2
                enumeration = +1
            else
                enumeration = -1
            end if
        end if

    end function enum_dimer

    !----------------------------------------------------------------------------
    !Return cell-wise look directions for potential dimer pairs for a given basis
    !----------------------------------------------------------------------------
    function get_dim_dirs(basis) result(dirs)
        !Variables
        integer, intent(in) :: basis
        integer, dimension(2) :: dirs

        dirs = dimer_directions(basis, :)

    end function get_dim_dirs

    !----------------------------------------------------
    !Return basis of atom which could a create dimer pair
    !----------------------------------------------------
    integer function pair_basis(basis) result(pbasis)
        !Arguments
        integer, intent(in) :: basis
        !Main method
        if (mod(basis,2) == 0) then
            pbasis = basis - 1
        else
            pbasis = basis + 1
        endif
    end function pair_basis

    !-----------------------------------------------
    !Return atom which is a dimer pair of the atom
    !-----------------------------------------------
    function return_pair_atom(atom, atoms) result(pair)
        !Modules
        use types, only : atom_type, atom_type_ext
        use variables, only : read_ipair
        !Arguments
        type(atom_type), intent(in)  :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        type(atom_type) :: pair
        !Variables
        integer :: ipair           !Dimer index
        !-----------
        !Main Method
        !-----------
        ipair = read_ipair(atom, atoms)
        if (ipair /= 0) then
            pair = atoms(ipair)%atom
        else
            pair = atom_type(0,0,0,0)
        end if

        end function return_pair_atom


    !======================================
    !Isolated Dimer Etching Related Methods
    !======================================

    !----------------------------------------------------------------
    !Is the potential dimer solated in both row and chain directions?
    !Dimer defined by passing 2 atoms which could form a dimer
    !----------------------------------------------------------------
    logical function isolated_dimer_potential(atom1, atom2, atoms)
        !Modules
        use types, only : atom_type_ext, atom_type
        use variables, only : read_dimer, ilattice
        use input, only : reactions
        !Arguments
        type(atom_type), intent(in) :: atom1, atom2
        type(atom_type_ext), intent(in) :: atoms(:)
        !Variables

        isolated_dimer_potential = &
                dimer_isolated_along_row(atom1, atom2, reactions%dimer_row_isolation_range, atoms)

    end function isolated_dimer_potential

    !---------------------------------------------------------------------------------
    !Is atom part of a dimer reconstruction isolated in both row and chain directions?
    !atom must be currently part of a dimer
    !---------------------------------------------------------------------------------
    logical function isolated_dimer_current(atom, atoms)
        !Modules
        use types, only : atom_type_ext, atom_type
        use variables, only : read_dimer, ilattice
        use input, only : reactions
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        !Variables
        integer :: iatom, ipair
        !Initialisation
        isolated_dimer_current = .false.
        if (.not. read_dimer(atom, atoms)) return

        !Method
        iatom = ilattice(atom)
        ipair = atoms(iatom)%ipair

         isolated_dimer_current = &
             dimer_isolated_along_row(atom, atoms(ipair)%atom, reactions%dimer_row_isolation_range, atoms)

    end function isolated_dimer_current


    !------------------------------------------------------------
    !Is a dimer isolated along the dimer row directions
    !   D   D   X   D   D
    !  / \ / \ / \ / \ / \
    ! C   C   C   C   C  C
    ! |   |   |   |   |  |
    !Range controls the number of 'D' are traversed in
    !   each directions
    !D need not be dimers if single_c_prevent_iso .true.
    !D need not be part of dimers if dimers_prevent_iso = .false.
    !Requirement dimer only isolated with 3 C-C each atom
    !       C
    !      /  \  dimer (1-2) not isolated! atom 2 has 4 C-C bonds
    ! 1 - 2    C
    ! /   \    |
    !------------------------------------------------------------
    logical function dimer_isolated_along_row(atom, pair, range, atoms)
        !Modules
        use types, only : atom_type_ext, atom_type, same_atom
        use periodic, only : get_site_from_N_path, num_bonds
        use variables, only : read_atom, read_dimer
        use enumerate, only : types_of_atom
        use input, only : reactions
        !Arguments
        type(atom_type), intent(in) :: atom, pair
        integer, intent(in) :: range
        type(atom_type_ext), intent(in) :: atoms(:)
        !Variables
        type(atom_type) :: sites1(4), sites2(4)
        logical :: filled1(4), filled2(4)
        integer :: isite
        dimer_isolated_along_row = .false.

        !4 sites for each atom of the pair (1:2 closest, 3:4, fathest)
        if ((num_bonds(atom, atoms)) > 3 .or. (num_bonds(pair, atoms) > 3)) return
        sites1 = di_row_sites(atom)
        sites2 = di_row_sites(pair)

        filled1 = .false.
        filled2 = .false.
        do isite=1, 2*range
            filled1(isite) = (read_atom(sites1(isite), atoms) /= types_of_atom%empty_site)
            filled2(isite) = (read_atom(sites2(isite), atoms) /= types_of_atom%empty_site)
        end do

        if (reactions%single_c_prevent_iso) then
            !Only consider filledn(:)
            dimer_isolated_along_row = (all(filled1 .eqv. .false.)) .and. (all(filled2 .eqv. .false.))
            return
        else
            !Dimers not required but 2 carbons are
            !Sites lined up = 2 carbons but not forming a dimer
            do isite=1, 2*range
                if (filled1(isite) .and. filled2(isite)) then
                    dimer_isolated_along_row = .false.
                    return
                end if
            end do
            dimer_isolated_along_row = .true.
        end if


    end function dimer_isolated_along_row


    !-------------------------------------------------------------
    !Return 4 sites on either side of the atom (which is part of
    !a dimer) which must be empty for that part of a dimer to be
    !isolated. As dimer is constructed from 2 atoms, this function
    !handles only a single atoms adjacent sites along the row
    !direction because they can be considered distinct due to
    !how the lattice is traversed
    !-------------------------------------------------------------
    pure function di_row_sites(atom)
        !Modules
        use types, only : atom_type_ext, atom_type
        use periodic, only : get_site_from_N_path
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type) :: di_row_sites(4)
        !Variables
        integer :: paths(4,4)
        integer :: ipath

        paths = reshape((/ 2,1,0,0, 3,4,0,0, 2,1,2,1, 3,4,3,4 /), &
                shape = (/ 4,4 /), &
                order = (/ 1,2 /)) !Traverse (:,i)

        do ipath=1, size(paths, 2)
            di_row_sites(ipath) = get_site_from_N_path(atom, paths(:, ipath))
        end do

    end function di_row_sites


    !-------------------------------------------------
    !Do passed atoms create 2 parts of the same dimer?
    !Not being part of a dimer will return false
    !-------------------------------------------------
    logical function part_same_dimer_atom(atom1, atom2, atoms)
        !Modules
        use types, only : atom_type, atom_type_ext
        use variables, only : ilattice
        !Arguments
        type(atom_type), intent(in) :: atom1, atom2
        type(atom_type_ext), intent(in) :: atoms(:)
        !Variables
        integer :: ilat1, ilat2

        ilat1 = ilattice(atom1)
        ilat2 = ilattice(atom2)

        part_same_dimer_atom = part_same_dimer_index(ilat1, ilat2, atoms)

    end function part_same_dimer_atom

    !----------------------------------------------
    !Do passed atoms (indexes)create 2 parts of the
    !same dimer?
    !Not being part of a dimer will return false
    !----------------------------------------------
    logical function part_same_dimer_index(ilat1, ilat2, atoms)
        !Modules
        use types, only : atom_type, atom_type_ext
        use variables, only : ilattice
        !Arguments
        integer, intent(in) :: ilat1, ilat2
        type(atom_type_ext), intent(in) :: atoms(:)

        part_same_dimer_index = .false.
        if (atoms(ilat1)%dimer .and. atoms(ilat2)%dimer) then
            if (ilat1 == atoms(ilat2)%ipair) then
                part_same_dimer_index = .true.
                return
            else
                return
            end if
        else
            return
        end if


    end function part_same_dimer_index


    !----------------------------------------------------
    !Does the geometry of a dimer and its surroundings
    !faciliate it's spontaneous breaking?
    !Cannot be isolated
    !Dimer must exist between passed atom1 and atom2
    !----------------------------------------------------
    logical function dimer_can_break(atom1, atom2, atoms)
        !Modules
        use types, only : atom_type, atom_type_ext
        use variables, only : read_dimer
        !Arguments
        type(atom_type), intent(in) :: atom1, atom2
        type(atom_type_ext), intent(in) :: atoms(:)
        !Variables
        dimer_can_break = .false.
        !Method
        !Must be in a dimer (the same dimer)
        if (.not. part_same_dimer(atom1, atom2, atoms)) return
        !Can't be an isolated dimer
        if (isolated_dimer(atom1, atoms)) return

        dimer_can_break = .true.

    end function dimer_can_break


    !Are both atoms of a dimer reconstruction forming 4 C-C bonds
    !(including the dimer C-C bond)
    !A saturated dimer has no hydrogen termination but also no dangling bonds
    !   C      C
    !  / \    / \
    ! C  A1--A2  C
    ! |  //  \\  |
    logical function saturated_dimer_atoms(atom1, atom2, atoms)
        !Modules
        use types, only : atom_type_ext, atom_type
        use variables, only : NN_bulk
        use periodic, only : num_bonds
        !Arguments
        type(atom_type), intent(in) :: atom1, atom2
        type(atom_type_ext), intent(in) :: atoms(:)
        !Variables
        integer :: nb

        if ((num_bonds(atom1, atoms) >= NN_bulk) .and. (num_bonds(atom2, atoms) >= NN_bulk)) then
            saturated_dimer_atoms = .true.
        else
            saturated_dimer_atoms = .false.
        end if

    end function saturated_dimer_atoms


    !-----------------------------------------------------
    !Interface allowing only a single atom to be passed
    !and its pair is determined via atom_pair(atom, atoms)
    !-----------------------------------------------------
    logical function saturated_dimer_atom(atom, atoms)
        !Modules
        use types, only : atom_type_ext, atom_type
        use variables, only : read_dimer, atom_pair
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        !Variables
        type(atom_type) :: pair
        saturated_dimer_atom = .false.
        if (.not. read_dimer(atom, atoms)) return

        pair = atom_pair(atom, atoms)

        saturated_dimer_atom = saturated_dimer_atoms(atom, pair, atoms)

    end function saturated_dimer_atom


    !-----------------------------------------------------------------
    !Can a future dimer be formed between the 2 atoms?
    !Neglect n bonds between the 2 atoms (for example to allow an etch
    !to occur before deciding on dimer formation
    !-----------------------------------------------------------------
    logical function can_form_dimer_nb(atom1, atom2, atoms, neglect_n_bonds)
        use types, only : atom_type_ext, atom_type
        use variables, only : read_dimer
        use periodic, only : num_bonds
        use input, only : dimerf
        !Arguments
        type(atom_type), intent(in) :: atom1, atom2
        type(atom_type_ext), intent(in) :: atoms(:)
        integer, optional, intent(in) :: neglect_n_bonds
        !Variables
        integer :: nbonds_ignore
        integer :: nb1, nb2
        can_form_dimer_nb = .false.

        if (read_dimer(atom1, atoms) .or. read_dimer(atom2, atoms)) return

        if (present(neglect_n_bonds)) then
            nbonds_ignore = neglect_n_bonds
        else
            nbonds_ignore = 0
        end if

        nb1 = num_bonds(atom1, atoms)
        nb2 = num_bonds(atom2, atoms)

        if ((nb1 + nb2 - nbonds_ignore) < 2 * dimerf%nb_form_thres) then
            can_form_dimer_nb = .true.
        end if


    end function can_form_dimer_nb


end module dimer