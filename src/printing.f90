!Containing functions and subroutines for creating formatted strings for derived type instances
module printing


contains

    function pp_arrhenius(ahhr)
        !Modules
        use types, only : ahhr_type
        !Arguments
        type(ahhr_type), intent(in) :: ahhr
        character(24) :: pp_arrhenius

        write(pp_arrhenius, '(ES12.2,ES12.2)') ahhr%A, ahhr%E


    end function pp_arrhenius

end module printing