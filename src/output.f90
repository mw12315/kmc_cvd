    module output

    use kinds,  only  : dp
    use types,  only : coord_type

    implicit none

contains

    !----------------------------------------------------------------------------------------------------
    !Write simulation, experimental conditions, reaction energies, scaling factors to the main input file
    !----------------------------------------------------------------------------------------------------
    subroutine output_input_values(sim_info, conditions, reactions, energies, etching_i, dimerf)
        !Modules
        use types, only : sim_type, conditions_ftype, reactions_ftype, energy_ftype, etching_ftype, dimer_ftype
        use variables, only : Rgas
        use printing, only : pp_arrhenius
        use enumerate, only : file_units
        use derived_values, only : pref_etch_rates, rDimerInsertCHX, rTroughInsertCHX, &
                k_dimer_mig, kgmig_all_rates, k_down_mig, Fmr, Fbr, C2H2_ads_rates, kide_ind, kide_comb, &
                k6a_1f, k6a_2f, k6a_2r, k6a_1r, ketch_CHx, kC2H2p_mig_sub, kC2H2p_mig, kc2h2p_dimer, &
                k1, k2, km1, km2, kide, kf_adj_dimer, kr_adj_dimer, prob_adj_dimer_break
        !Arguments
        type(sim_type), intent(in) :: sim_info
        type(conditions_ftype), intent(in) :: conditions
        type(reactions_ftype), intent(in) :: reactions
        type(energy_ftype), intent(in) :: energies
        type(etching_ftype), intent(in) :: etching_i
        type(dimer_ftype), intent(in) :: dimerf
        !Variables
        character(48)   :: hash_sep = "################################################"
        character(48)   :: dash_sep = "------------------------------------------------"
        character(5)    :: kx_txt
        integer         :: i
        integer :: ireaction, irate !IDE etching loop indices

        !Main Method
        write(file_units%m_output,'(a)') hash_sep
        write(file_units%m_output,'(a)') "##########DIAMOND LATTICE OUTPUT FILE###########"
        write(file_units%m_output,'(a)') hash_sep
        write(file_units%m_output,'(a)') dash_sep
        write(file_units%m_output,'(a)') "---------------SIMULATION PARAMETERS------------"
        write(file_units%m_output,'(a)') dash_sep
        write(file_units%m_output,'(a18,a6,a6,a6,a6)') "INITIAL SURFACE:","Nx","Ny","Nz","Nb"
        write(file_units%m_output,'(a18,i6,i6,i6,i6)') " ",sim_info%Nx,sim_info%Ny,sim_info%Nz,sim_info%Nbasis
        write(file_units%m_output,'(a18,a8,a8,a10)') "END CONDITIONS:","Time(s)","It.","Growth(A)"
        write(file_units%m_output,'(a18,f8.1,ES8.0,f10.1)') " ",sim_info%end%time,real(sim_info%end%iteration),sim_info%end%growth_height
        write(file_units%m_output,'(a)') dash_sep
        write(file_units%m_output,'(a)') "------------EXPERIMENTAL CONDITIONS-------------"
        write(file_units%m_output,'(a)') dash_sep
        write(file_units%m_output,'(a,a)') "CONDITION FILE ID: ", trim(conditions%condition_id)
        write(file_units%m_output,'(a24,i9,i9)') "TEMPERATURES(K) Ts Tns: ", conditions%Ts, conditions%Tns
        write(file_units%m_output,'(a)') "---------------GAS CONCENTRATIONS---------------"
        write(file_units%m_output,'(a6,a20,a6,a6)') "Gas","Conc /cm-3","s","g"
        write(file_units%m_output,'(a6,a20,a6,a6)') "---","----------","-","-"
        write(file_units%m_output,'(a6,ES20.2,a,a)')        "H",        conditions%Hconc," "," "
        write(file_units%m_output,'(a6,ES20.2,a,a)')        "H2",       conditions%H2conc," "," "
        write(file_units%m_output,'(a6,ES20.2,f6.2,f6.2)')  "CH3",      conditions%CHxconc(1), conditions%CHx_s(1), conditions%CHx_g(1)
        write(file_units%m_output,'(a6,ES20.2,f6.2,f6.2)')  "CH2(2)",   conditions%CHxconc(2), conditions%CHx_s(2), conditions%CHx_g(2)
        write(file_units%m_output,'(a6,ES20.2,f6.2,f6.2)')  "CH",       conditions%CHxconc(3), conditions%CHx_s(3), conditions%CHx_g(3)
        write(file_units%m_output,'(a6,ES20.2,f6.2,f6.2)')  "C",        conditions%CHxconc(4), conditions%CHx_s(4), conditions%CHx_g(4)
        write(file_units%m_output,'(a6,ES20.2,a,a)')        "C2(a)",    conditions%C2a_conc," "," "
        write(file_units%m_output,'(a6,ES20.2,a,a)')        "C2(X)",    conditions%C2X_conc," "," "
        write(file_units%m_output,'(a6,ES20.2,f6.2,f6.2)')   "C2H2",    conditions%C2H2_conc, conditions%C2H2_s, 0.0_dp
        write(file_units%m_output,'(a)') "----------------REACTION ENERGIES---------------"
        write(file_units%m_output,'(a12,a15,a15)') "Reaction","A (\s)","E (J/mol)"
        write(file_units%m_output,'(a12,a15,a15)') "--------","------","---------"
        write(file_units%m_output,'(a12,ES15.2,ES15.2)') "k1",energies%Ak1,energies%Ek1 * Rgas
        write(file_units%m_output,'(a12,ES15.2,ES15.2)') "km1",energies%Akm1,energies%Ekm1 * Rgas
        write(file_units%m_output,'(a12,ES15.2)')        "k2",energies%Ak2
        write(file_units%m_output,'(a12,ES15.2,ES15.2)') "D._Mig.",energies%A_dmig,energies%E_dmig
        write(file_units%m_output,'(a12,ES15.2,ES15.2)') "G._Mig. 1f",energies%A_gmig1f, energies%E_gmig1f
        write(file_units%m_output,'(a12,ES15.2,ES15.2)') "G._Mig. 1r",energies%A_gmig1r, energies%E_gmig1r
        write(file_units%m_output,'(a12,ES15.2,ES15.2)') "G._Mig. 2f",energies%A_gmig2f, energies%E_gmig2f
        write(file_units%m_output,'(a12,ES15.2,ES15.2)') "G._Mig. 2r",energies%A_gmig2r, energies%E_gmig2r
        write(file_units%m_output,'(a12,ES15.2,ES15.2)') "Down_Mig.",energies%A_down_mig, energies%E_down_mig
        write(file_units%m_output,'(a12,ES15.2,ES15.2)') "C2H2_M1a",energies%acetylene_ads%M1%A,energies%acetylene_ads%M1%E
        write(file_units%m_output,'(a12,ES15.2,ES15.2)') "C2H2_M1b",energies%acetylene_ads%M1%A,energies%acetylene_ads%M1%E
        write(file_units%m_output,'(a12,ES15.2,ES15.2)') "C2H2_M2",energies%acetylene_ads%M2%A,energies%acetylene_ads%M2%E
        write(file_units%m_output,'(a12,ES15.2,ES15.2)') "C2H2_M3a",energies%acetylene_ads%M3a%A,energies%acetylene_ads%M3a%E
        write(file_units%m_output,'(a12,ES15.2,ES15.2)') "C2H2_M3b",energies%acetylene_ads%M3b%A,energies%acetylene_ads%M3b%E
        if (any(reactions%active%C2H2p_mig)) then
            write(file_units%m_output, '(a)') "C2H2 PENDANT MIGRATION"
            write(file_units%m_output, '(a6,4(a14))') "V", "A(f)(/s)", "E(f)(kJ/mol)", "A(r)(/s)", "E(r)(kJ/mol)"
            do ireaction=1, energies%C2H2p_nreactions
                write(file_units%m_output, '(a5,i1,2(a24))') "V", ireaction, pp_arrhenius(energies%C2H2p_mig(ireaction)%f), pp_arrhenius(energies%C2H2p_mig(ireaction)%r)
            end do
        end if
        write(file_units%m_output,'(a12,ES15.2,ES15.2)') "C2H2->dimer",energies%c2h2p_dimer%A, energies%c2h2p_dimer%E
        if (reactions%active%pref_etch_C_on) then
            write(file_units%m_output, '(a)') "PREFERENTIAL ETCHING"
            do ireaction=0,4
                write(file_units%m_output,'(a12,ES15.2,ES15.2)') &
                        etching_i%prf_rate_labels(ireaction), &
                        etching_i%flex_etch(ireaction)%A, etching_i%flex_etch(ireaction)%E
            end do
        end if
        if (any(reactions%active%ide_etch_on)) then
            write(file_units%m_output, '(a)') "ISOLATED DIMER ETCHING"
            write(file_units%m_output, '(a3,a1,a1,a1,4(a12))') " R-", "n","-", "i", "A(f)(/s)", "E(f)(K)", "A(r)(/s)", "E(r)(K)"
            do ireaction=1,5
                !12-j-i
                do irate=1, etching_i%n_ide_subreactions(ireaction)
                    write(file_units%m_output, '(a3,i1,a1,i1,2(a24))') "12-",ireaction,"-",irate, &
                            pp_arrhenius(etching_i%ide_ahhr(ireaction,irate)%f), &
                            pp_arrhenius(etching_i%ide_ahhr(ireaction,irate)%r)
                end do
            end do
        end if

        write(file_units%m_output,'(a)') "----------------FACTORS-----------------"
        write(file_units%m_output,'(a25,i10)') "Mig. Crit. Nuc.",reactions%mig_crit_nuc
        write(file_units%m_output,'(a25,i10)') "Isolation(TN) Thres.",reactions%isolated_TN_threshold
        write(file_units%m_output,'(a25,f10.2)') "3rd Bond F Etch Factor",etching_i%flex_etch_3rdbond_factor
        write(file_units%m_output,'(a25,i10)') "Dimer isolation range",reactions%dimer_row_isolation_range
        write(file_units%m_output,'(a25,L10)') "Single C prevent iso.",reactions%single_c_prevent_iso
        write(file_units%m_output,'(a25,i10)') "Dimer form thresh",dimerf%nb_form_thres
        write(file_units%m_output,'(a25,L10)') "CHx into sat. dimer",dimerf%incorp_CHx_sat_dimer
        write(file_units%m_output,'(a25,ES10.3)') "Chx in sat. dimer scale",dimerf%sat_dimer_incorp_scale
        write(file_units%m_output,'(a25,ES10.3)') "Hindered trough deact.",dimerf%hind_trough_deact_factor
        write(file_units%m_output,'(a25,ES10.3)') "Hindered trough act.",1.0 - dimerf%hind_trough_deact_factor
        write(file_units%m_output,'(a25,L10)') "Dihydride trough hind",dimerf%dihyride_hindered
        write(file_units%m_output,'(a25,L10)') "gap mig. via dimer",dimerf%gap_via_dimer
        write(file_units%m_output,'(a25,L10)') "Iso. dimer breaking",dimerf%iso_dimer_can_break
        write(file_units%m_output,'(a25,L10)') "Rand. Adj. dimer breaking",dimerf%random_adj_dimer_breaking
        write(file_units%m_output,'(a25,f10.2)') "Prob. Adj. dimer breaking",prob_adj_dimer_break
        write(file_units%m_output,'(a)') "------------------REACTION RATES----------------"
        write(file_units%m_output,'(a8,a20,a10)') "Species","Reaction","Rate (/s)"
        write(file_units%m_output,'(a8,a20,a10)') "-------","--------","---------"
        write(file_units%m_output,'(a8,a20,ES10.2)') "H","Abstraction",k1
        write(file_units%m_output,'(a8,a20,ES10.2)') "H2","Addition",km1
        write(file_units%m_output,'(a8,a20,ES10.2)') "H","Addition",k2
        write(file_units%m_output,'(a8,a20,ES10.2)') "H2","desorption",km2
        write(file_units%m_output,'(a8,a20,ES10.2)') "CH3","Dimer Insertion",rDimerInsertCHX(1)
        write(file_units%m_output,'(a8,a20,ES10.2)') "CH3","Trough Insertion",rTroughInsertCHX(1)
        write(file_units%m_output,'(a8,a20,ES10.2)') "CH2","Dimer Insertion",rDimerInsertCHX(2)
        write(file_units%m_output,'(a8,a20,ES10.2)') "CH2","Trough Insertion",rTroughInsertCHX(2)
        write(file_units%m_output,'(a8,a20,ES10.2)') "CH", "Dimer Insertion",rDimerInsertCHX(3)
        write(file_units%m_output,'(a8,a20,ES10.2)') "CH", "Trough Insertion",rTroughInsertCHX(3)
        write(file_units%m_output,'(a8,a20,ES10.2)') "C",  "Dimer Insertion",rDimerInsertCHX(4)
        write(file_units%m_output,'(a8,a20,ES10.2)') "C",  "Trough Insertion",rTroughInsertCHX(4)
        write(file_units%m_output,'(a8,a20,ES10.2)') "C2H2", "M1a Mechanism",C2H2_ads_rates%M1a
        write(file_units%m_output,'(a8,a20,ES10.2)') "C2H2", "M1b Mechanism",C2H2_ads_rates%M1b
        write(file_units%m_output,'(a8,a20,ES10.2)') "C2H2", "M2 Mechanism",C2H2_ads_rates%M2
        write(file_units%m_output,'(a8,a20,ES10.2)') "C2H2", "M3a Mechanism",C2H2_ads_rates%M3a
        write(file_units%m_output,'(a8,a20,ES10.2)') "C2H2", "M3b Mechanism",C2H2_ads_rates%M3b
        write(file_units%m_output,'(a8,a20,ES10.2)') "CH2","Dimer Migration",k_dimer_mig
        write(file_units%m_output,'(a8,a20,ES10.2)') "CH2","Gap (6) 1f",     k6a_1f
        write(file_units%m_output,'(a8,a20,ES10.2)') "CH2","Gap (6) 1r",     k6a_1r
        write(file_units%m_output,'(a8,a20,ES10.2)') "CH2","Gap (6) 2f",     k6a_2f
        write(file_units%m_output,'(a8,a20,ES10.2)') "CH2","Gap (6) 2r",     k6a_2r
        write(file_units%m_output,'(a8,a20,ES10.2)') "CH2","Gap Migration A",kgmig_all_rates(1)
        write(file_units%m_output,'(a8,a20,ES10.2)') "CH2","Gap Migration B",kgmig_all_rates(2)
        write(file_units%m_output,'(a8,a20,ES10.2)') "CH2","Gap Migration C",kgmig_all_rates(3)
        write(file_units%m_output,'(a8,a20,ES10.2)') "CH2","Gap Migration D",kgmig_all_rates(4)
        write(file_units%m_output,'(a8,a20,ES10.2)') "CH2","Down Migration", k_down_mig
        write(file_units%m_output,'(a8,a15,a5,2(A10))') "C2H2(p)","Gap Migrate","kn", "kf", "kr"
        do i=1,4 !Currently only when C2H2 not adjacent to final site (1:3)
            write(kx_txt, '(a4,i1)') '   k',i
            write(file_units%m_output,'(a8,a15,a5,2(ES10.2))') "C2H2(p)","Gap Migrate",kx_txt, kC2H2p_mig_sub(i)%f, kC2H2p_mig_sub(i)%r
        end do
        write(file_units%m_output,'(a)') "------------------------------------------------"
        write(file_units%m_output,'(a8,a20,2(ES10.2))') "C2H2(p)","Gap Migrate ktot", kC2H2p_mig(:)
        write(file_units%m_output,'(a8,a20,ES10.2)') "C2H2","C2H2->dimer",kc2h2p_dimer
        if (reactions%active%pref_etch_C_on) then
            write(file_units%m_output, '(a)') "PREFERENTIAL ETCHING"
            do i=0,4
                write(file_units%m_output,'(a8,a20,ES10.2)') etching_i%prf_rate_labels(i)(1:3), etching_i%prf_rate_labels(i)(4:), pref_etch_rates(i)
            end do
        end if
        if (any(reactions%active%ide_etch_on)) then
            if (reactions%ide_individual_active) then
                write(file_units%m_output, '(a)') "ISOLATED DIMER ETCHING (12-n)"
            else
                write(file_units%m_output, '(a)') "ISOLATED DIMER ETCHING (12-1 -> 12-n ->)"
            end if
                do ireaction=1, etching_i%n_reactions
                    write(file_units%m_output,'(a8,a19,i1,ES10.2)') "CxHy",adjustr("k-12-"), ireaction, &
                            kide(ireaction)
                end do
        end if
        if (reactions%active%etch_CHx) then
            write(file_units%m_output, '(a)') "ETCH CHx VIA REVERSE INCORPORATION"
            write(file_units%m_output,'(a8,a20,ES10.2)') "CHx", " ", ketch_CHx
        end if
        !Dimer Formation/Breaking (ADJ type)
        write(file_units%m_output, '(a)') "BREAKING OF DIMERS (ADJ)"
        write(file_units%m_output,'(a8,a20,ES10.2)') "CHx", "d formation", kf_adj_dimer
        write(file_units%m_output,'(a8,a20,ES10.2)') "CHx", "d break", kr_adj_dimer
        write(file_units%m_output,'(a)') " "
        write(file_units%m_output,'(a28,f10.3)') "MONO RADICAL SITE FRACTION",Fmr
        write(file_units%m_output,'(a28,f10.3)') "BI RADICAL SITE FRACTION"  ,Fbr
        write(file_units%m_output,'(a28,i10)') "ACTIVE RATES:", sim_info%unique_rates
        write(file_units%m_output,'(a28,i10)') "RATES PER SITE", sim_info%rates_per_site
        write(file_units%m_output,*) " "
        write(file_units%m_output,*) "SEEDING CLOCK VALUE: ", sim_info%seeding_clock


    end subroutine output_input_values


    !---------------------------------------------------------
    !Output information on the finished run to the output file
    !---------------------------------------------------------
    subroutine output_final(sim_info, kmc_info, surface_info, segment_info,lattice, cpu_time)
        !-------
        !Modules
        !-------
        use types,      only : kmc_type, surface_type, sim_type, atom_type_ext,average
        use input,      only : outputs
        use fileIO,     only : file_exists
        use enumerate,  only : file_units,tRates,edge_state
        use variables,  only : counters, reset_last_frame, add_unit_to_open_list
        use json, only : json_sim_end_output
        use surface_m, only : surface_stats
        !---------
        !Arguments
        !---------
        type(sim_type), intent(in) :: sim_info
        type(kmc_type), intent(in) :: kmc_info
        type(surface_type), intent(inout) :: surface_info
        type(surface_type), dimension(:), intent(inout) :: segment_info
        type(atom_type_ext),dimension(:), intent(inout) :: lattice
        real(dp), intent(in) :: cpu_time
        !---------
        !Variables
        !---------
        type(surface_type) :: surface_bmark_info
        type(surface_type), dimension(size(segment_info)) :: bmark_segment_info
        character(25)   :: filename
        character(48)   :: hash_sep = "################################################"
        integer         :: sim_it, sim_events   !Iterations, events this simulation
        real(dp)        :: sim_time
        integer :: total_dimer_ins, total_trough_ins
        real(dp) :: total_envs = 0
        integer :: ienv, i
        real(dp) :: total_layer_occup   !Total occupancy of top layers
        integer :: total_incorp_events
        character(3) :: species_labels(4) = (/ "CH3", "CH2", "CH ", "C  " /)
        character(8) :: reaction_name
        !Initialisation
        surface_bmark_info =  surface_info
        bmark_segment_info = segment_info
        sim_it = kmc_info%step - kmc_info%start_step
        sim_time = kmc_info%time - kmc_info%start_time    !Time simulation within current simulation, not counting previous
        total_dimer_ins = sum(counters%events(tRates%DinsertCH3:tRates%DinsertC,1))
        total_trough_ins = sum(counters%events(tRates%TinsertCH3:tRates%TinsertC,1))
        total_incorp_events = counters%events(tRates%DinsertCH3,1) + &
                counters%events(tRates%DinsertCH2,1) + &
                counters%events(tRates%DinsertCH,1) + &
                counters%events(tRates%DinsertC,1) + &
                counters%events(tRates%TinsertCH3,1) + &
                counters%events(tRates%TinsertCH2,1) + &
                counters%events(tRates%TinsertCH,1) + &
                counters%events(tRates%TinsertC,1)

        !Surface Stats
        !Simulation Values
        call surface_stats(lattice, kmc_info, &
                surface_info%start_height, kmc_info%start_time, segment_info(:)%start_height, surface_info, segment_info)
        !Bookmark Values
        call surface_stats(lattice, kmc_info, &
                surface_bmark_info%init_height, 0.0_dp, bmark_segment_info(:)%init_height, surface_bmark_info, segment_info)

        call json_sim_end_output(sim_info, kmc_info, surface_info, segment_info, surface_bmark_info, cpu_time)

        !Output final diamond lattice positions
        filename="final_surface.xyz"
        if (file_exists(filename)) then
            open(unit=file_units%pos_final, file=trim(filename), status='replace')
        else
            open(unit=file_units%pos_final, file=trim(filename))
            call add_unit_to_open_list(file_units%pos_final)
        end if
        call reset_last_frame(lattice)
        call output_surface(lattice, surface_info, kmc_info, file_units%pos_final)

        !OUTPUT TO MAIN OUTPUT FILE
        write(file_units%m_output,*) " "
        write(file_units%m_output,'(a)') hash_sep
        write(file_units%m_output,'(a)') "############BEGIN END PROGRAM OUTPUT############"
        write(file_units%m_output,'(a)') hash_sep
        write(file_units%m_output,'(a25,i10)') "ITERATIONS:",kmc_info%step - kmc_info%start_step
        write(file_units%m_output,'(a25,i10)') "TOTAL ITERATIONS:",kmc_info%step
        write(file_units%m_output,'(a25,i10,f10.2,a2)') "EVENTS:", kmc_info%total_events, &
                100*real(kmc_info%total_events)/(kmc_info%step-kmc_info%start_step)," %"
        write(file_units%m_output,'(a25,f21.3)') "SIMULATED TIME (s):", sim_time
        write(file_units%m_output,'(a25,f21.3)') "TOTAL TIME (s):", kmc_info%time
        write(file_units%m_output,'(a25,ES21.5)') "NUCLEATION HEIGHT (A):",sim_info%nucleation_height
        write(file_units%m_output,'(a25,ES21.5)') "NUCLEATION TIME (s):",kmc_info%nucleation_time
        write(file_units%m_output,'(a25,i21)') "DIMERS ALIGNED:",counters%d_align(1)
        write(file_units%m_output,'(a25,i21)') "DIMERS RE-ALIGNED:",counters%d_align(2)
        write(file_units%m_output,'(a25,i21)') "ADJ DIMERS BROKEN:",counters%d_break
        write(file_units%m_output,'(a25,ES21.3)') "AVERAGE MIG/ATOM:",average(kmc_info%migs)
        write(file_units%m_output,'(a25,ES21.3)') "AVERAGE Fmr:",average(kmc_info%Fmr)
        write(file_units%m_output,'(a25,f21.3)') "AVERAGE active layers:",average(surface_info%ave_active_layers)
        write(file_units%m_output,'(a25,ES21.3)') "AVERAGE rate sum (s)",average(kmc_info%ave_rsum)

        write(file_units%m_output,*) " "
        write(file_units%m_output,'(a)') "REACTION COUNTS"

        write(file_units%m_output, '(3(a15),a7)') "Reaction", "Species", "Count", "%"
        write(file_units%m_output, '(3(a15),a7)') "--------", "-------", "-----", "-"
        do i=1,size(species_labels)
            write(file_units%m_output, '(2(a15),i15,f7.2)') "D insert.", species_labels(i), &
                    counters%events(tRates%DinsertCH3+(i-1),1), &
                    100.0*real(counters%events(tRates%DinsertCH3+(i-1),1))/kmc_info%total_events
        end do
        write(file_units%m_output, '(3(a15),a7)') "---------------", "---------------", "---------------", "-------"
        write(file_units%m_output, '(2(a15),i15,f7.2)') "D insert.", "CHx", &
                sum(counters%events(tRates%DinsertCH3:tRates%DinsertC,1)), &
                100.0*real(sum(counters%events(tRates%DinsertCH3:tRates%DinsertC,1)))/kmc_info%total_events
        write(file_units%m_output, '(3(a15),a7)') "---------------", "---------------", "---------------", "-------"
        do i=1,size(species_labels)
            write(file_units%m_output, '(2(a15),i15,f7.2)') "T insert.", species_labels(i), &
                    counters%events(tRates%TinsertCH3+(i-1),1), &
                    100.0*real(counters%events(tRates%TinsertCH3+(i-1),1))/kmc_info%total_events
        end do
        write(file_units%m_output, '(3(a15),a7)') "---------------", "---------------", "---------------", "-------"
        write(file_units%m_output, '(2(a15),i15,f7.2)') "T insert.", "CHx", &
                sum(counters%events(tRates%TinsertCH3:tRates%TinsertC,1)), &
                100.0*real(sum(counters%events(tRates%TinsertCH3:tRates%TinsertC,1)))/kmc_info%total_events
        write(file_units%m_output, '(3(a15),a7)') "---------------", "---------------", "---------------", "-------"
        write(file_units%m_output, '(2(a15),i15,f7.2)') "Pref. Etch", "CHx", &
                counters%events(tRates%pref_etch_CHx,1), &
                100.0*real(counters%events(tRates%pref_etch_CHx,1))/kmc_info%total_events
        write(file_units%m_output, '(2(a15),i15,f7.2)') "CHx Etch (k11)", "CHx", &
                counters%events(tRates%etch_chx,1), &
                100.0*real(counters%events(tRates%etch_chx,1))/kmc_info%total_events
        do i=0,4
            write(reaction_name, '(a7,i1)') "IDE-12-", i+1
            write(file_units%m_output, '(2(a15),i15,f7.2)') reaction_name, "--", &
                    counters%events(tRates%ide_12_1+i,1), &
                    100.0*real(counters%events(tRates%ide_12_1+i,1))/kmc_info%total_events
        end do
        write(file_units%m_output, '(2(a15),i15,f7.2)') "Dimer mig.", "CHx", &
                sum(counters%events(tRates%dimerMigrate_r:tRates%dimerMigrate_c,1)), &
                100.0*real(sum(counters%events(tRates%dimerMigrate_r:tRates%dimerMigrate_c,1)))/kmc_info%total_events
        write(file_units%m_output, '(2(a15),i15,f7.2)') "Gap mig.", "CHx", &
                sum(counters%events(tRates%gapMigrateA:tRates%gapMigrateD,1)), &
                100.0*real(sum(counters%events(tRates%gapMigrateA:tRates%gapMigrateD,1)))/kmc_info%total_events
        write(file_units%m_output, '(2(a15),i15,f7.2)') "Down mig.", "CHx", &
                counters%events(tRates%migDown,1), &
                100.0*real(counters%events(tRates%migDown,1))/kmc_info%total_events
        write(file_units%m_output, '(2(a15),i15,f7.2)') "Migration", "CHx", &
                sum(counters%events(tRates%dimerMigrate_r:tRates%gapMigrateD,1)) + counters%events(tRates%migDown,1), &
                100.0*real(sum(counters%events(tRates%dimerMigrate_r:tRates%gapMigrateD,1))+&
                        counters%events(tRates%migDown,1))/kmc_info%total_events
        write(file_units%m_output, '(2(a15),i15,f7.2)') "C2H2 mig.", "C2H2", &
                counters%events(tRates%migrateC2H2,1), &
                100.0*real(counters%events(tRates%migrateC2H2,1))/kmc_info%total_events
        write(file_units%m_output, '(2(a15),i15,f7.2)') "C2H2 (**) mig.", "C2H2", &
                counters%events(tRates%migrateC2H2,1), &
                100.0*real(counters%events(tRates%migrateC2H2,1))/kmc_info%total_events
        write(file_units%m_output, '(2(a15),i15,f7.2)') "C2H2 gap mig. 0", "C2H2", &
                counters%events(tRates%gapMigrateC2H2_0,1), &
                100.0*real(counters%events(tRates%gapMigrateC2H2_0,1))/kmc_info%total_events
        write(file_units%m_output, '(2(a15),i15,f7.2)') "C2H2 gap mig. 1", "C2H2", &
                counters%events(tRates%gapMigrateC2H2_1,1), &
                100.0*real(counters%events(tRates%gapMigrateC2H2_1,1))/kmc_info%total_events
        write(file_units%m_output, '(2(a15),i15,f7.2)') "C2H2->dimer", "C2H2", &
                counters%events(tRates%c2H2p_dimer,1), &
                100.0*real(counters%events(tRates%c2H2p_dimer,1))/kmc_info%total_events
        write(file_units%m_output, '(3(a15),a7)') "---------------", "---------------", "---------------", "-------"

        !Biradical Events
        write(file_units%m_output,'(a)') " "
        if (sum(counters%events(tRates%TinsertCH3:tRates%TinsertC,2)) == 0.0_dp) then
            write(file_units%m_output,'(a37,f6.2)') "Biradical CHX Trough Insertions(%): ",0.0_dp
        else
            write(file_units%m_output,'(a37,f6.2)') "Biradical CHX Trough Insertions(%): ",100.0_dp*sum(counters%events(tRates%TinsertCH3:tRates%TinsertC,2))/total_trough_ins
        end if

        write(file_units%m_output,'(a)') " "
        write(file_units%m_output,'(a)') "------------SURFACE/GROWTH INFORMATION-----------"
        write(file_units%m_output,'(a30,EN12.2)') "AVERAGE GROWTH (m):",            surface_info%ave_growth_height * 1E-10
        write(file_units%m_output,'(a30,EN12.2)') "AVERAGE GROWTH RATE (m/h):",     1E-10 * surface_info%ave_growth_rate * 3600
        write(file_units%m_output,'(a30,EN12.2)') "FINAL RMS (g) ROUGHNESS (m):",   surface_info%rms_roughness*1E-10
        write(file_units%m_output,'(a30,EN12.2)') "AVERAGE RMS (g) ROUGHNESS (m):",   average(surface_info%ave_rms)*1E-10
        write(file_units%m_output,'(a30,EN12.2)') "'RMS (g) ROUGHNESS (m):",        surface_info%d_rms_roughness*1E-10
        write(file_units%m_output,'(a30,EN12.2)') "2N RMS ROUGHNESS (m):",          surface_info%rms_2NN*1E-10
        write(file_units%m_output,'(a30,EN12.2)') "AVERAGE 2N RMS ROUGHNESS (m):",  average(surface_info%ave_2Nrms)*1E-10
        write(file_units%m_output,'(a30,EN12.2)') "3N RMS ROUGHNESS (m):",          surface_info%rms_3NN*1E-10
        write(file_units%m_output,'(a30,EN13.3)') "AVERAGE 3N RMS ROUGHNESS (m):",  average(surface_info%ave_3Nrms)*1E-10
        write(file_units%m_output,'(a30,EN12.2)') "4N RMS ROUGHNESS (m):",          surface_info%rms_4NN*1E-10
        write(file_units%m_output,'(a30,EN14.4)') "AVERAGE 4N RMS ROUGHNESS (m):",  average(surface_info%ave_4Nrms)*1E-10
        write(file_units%m_output,'(a30,EN12.2)') "TOTAL GROWTH (m):",              surface_bmark_info%ave_growth_height * 1E-10
        write(file_units%m_output,'(a30,EN12.2)') "TOTAL GROWTH RATE (m/h):",(1E-10 * surface_bmark_info%ave_growth_height/(kmc_info%time)) * 3600
        write(file_units%m_output,'(a30,EN12.2)') "SURFACE WIDTH (nm):",              10E-1*surface_info%width
        write(file_units%m_output,'(a30,f12.2)') "INCORP. EFF. (%)", 100*(real(total_incorp_events-counters%events(tRates%pref_etch_CHx,1),dp)) / &
                (total_incorp_events+counters%events(tRates%pref_etch_CHx,1))
        write(file_units%m_output,'(a30,f12.3)') "DIMER DEVIATION",   surface_info%dimer_deviation
        write(file_units%m_output,'(a30,f12.3)') "AVERAGE DIMER DIVATION",   average(surface_info%ave_dimer_deviation)
        write(file_units%m_output,'(a30,f12.3)') "DIMER PROPORTION(%)",   100_dp * surface_info%dimer_prop
        write(file_units%m_output,'(a30,f12.3)') "AVERAGE DIMER PROPORTION(%)",  100_dp * average(surface_info%ave_dimers_prop)
        write(file_units%m_output,'(a30,5(a8))') "SURFACE ENVIRONMENTS (%):","Iso","S_A","S_B","S_AB","Terr"
        write(file_units%m_output,'(a30,5(f8.3))') "SURFACE ENVIRONMENTS (%):", &
                100*real(surface_info%envs(edge_state%isolated))/sum(surface_info%envs(:)), &
                100*real(surface_info%envs(edge_state%S_A_edge))/sum(surface_info%envs(:)), &
                100*real(surface_info%envs(edge_state%S_B_edge))/sum(surface_info%envs(:)), &
                100*real(surface_info%envs(edge_state%S_AB_edge))/sum(surface_info%envs(:)), &
                100*real(surface_info%envs(edge_state%terrace))/sum(surface_info%envs(:))

        do ienv=edge_state%isolated,edge_state%terrace
            total_envs = total_envs + average(surface_info%ave_envs(ienv))
        end do
        write(file_units%m_output,'(a30,5(f8.3))') "SURFACE ENVS AVERAGE (%):", &
                100*real(average(surface_info%ave_envs(edge_state%isolated)))/total_envs, &
                100*real(average(surface_info%ave_envs(edge_state%S_A_edge)))/total_envs, &
                100*real(average(surface_info%ave_envs(edge_state%S_B_edge)))/total_envs, &
                100*real(average(surface_info%ave_envs(edge_state%S_AB_edge)))/total_envs, &
                100*real(average(surface_info%ave_envs(edge_state%terrace)))/total_envs
        write(file_units%m_output,'(a)') " "
        if (kmc_info%time > outputs%values_at_x_s%times(1)) then
            write(file_units%m_output,'(a12)') "OUTPUT @ X s"
            write(file_units%m_output,'(a10, 2(a18), 2(a15))') "--------", "------------------", "------------------", "--------", "--------"
            write(file_units%m_output,'(a10, 2(a18), 2(a15))') "TIME (s)", "Growth Rate (nm/h)", "Inst. Gr (nm/h)", "RMS (nm)", "3A RMS (nm)"
            write(file_units%m_output,'(a10, 2(a18), 2(a15))') "--------", "------------------", "------------------", "--------", "--------"
        end if
        do i=1, size(outputs%values_at_x_s%times)
            if (outputs%values_at_x_s%times(i) <= kmc_info%time) then
                write(file_units%m_output,'(f10.6, 2(f18.3), 2(f15.3))')  outputs%values_at_x_s%times(i), &
                        3600 * 1E-1 * surface_info%grate_at_x(i), & ! Convert to nm/h from Ang/s
                        3600 * 1E-1 * surface_info%inst_grate_x(i), & ! Convert to nm/h from Ang/s
                        1E-1 * surface_info%rms_at_x(i), & ! Convert to nm from Ang
                        1E-1 * surface_info%rms2N_at_x(i) ! Convert to nm from Ang
            end if
        end do
        write(file_units%m_output,'(a10, 2(a18), 2(a15))') "--------", "------------------", "------------------", "--------", "--------"
        write(file_units%m_output,'(a)') " "
        write(file_units%m_output,'(a30)') "ACTIVE LAYER OCCUPANCY"
        write(file_units%m_output,'(a10, a20)') "-----", "--------------------"
        write(file_units%m_output,'(a10, a20)') "LAYER", "% OF SURFACE ATOMS"
        write(file_units%m_output,'(a10, a20)') "-----", "--------------------"
        total_layer_occup = 0.0_dp
        do i=1, size(surface_info%top_occupancy)
            total_layer_occup = total_layer_occup + surface_info%top_occupancy(i)
            write(file_units%m_output,'(i10, f20.2)') surface_info%min_layer+i-1, surface_info%top_occupancy(i)
        end do
        write(file_units%m_output,'(a10, a20)') "-----", "------------------"
        write(file_units%m_output,'(a10, f20.2)') "", total_layer_occup
        write(file_units%m_output,'(a10, a20)') "-----", "------------------"

        write(file_units%m_output,'(a,i10,a1,f4.1,a2)') "FAILED DIMER FORMATION ATTEMPTS: ", &
                counters%failed_dimer_attempts, &
                "(",(100.0_dp * counters%failed_dimer_attempts) / sim_it, "%)"

    end subroutine output_final

    !--------------------------------------------------------------------------------------------------------
    !Taking input of a list of atoms and an index matched list of their atom states, create an xyz file frame.
    !--------------------------------------------------------------------------------------------------------
    subroutine output_xyz(file_unit, atoms,comment, opt_is_lattice_dump)
        !Modules
        use types, only : atom_type,atom_type_ext,coord_type
        use input, only : outputs
        use basis, only : position
        use variables,  only : simulation,kmc_info,surface,lattice,ilattice
        use periodic, only : ret_neighbour
        !Variables
        integer, intent(in) :: file_unit
        type(atom_type_ext), dimension(:),intent(in) :: atoms
        character(*), intent(in) :: comment
        logical, optional, intent(in) :: opt_is_lattice_dump
        type(atom_type_ext), dimension(200) :: other_atoms
        type(atom_type) :: atom
        integer :: total_atoms,total_other_atoms
        integer :: iatom,N,N2
        character(1) :: atom_code
        type(coord_type) :: coords
        character(200) :: txt_lattice,properties,number,time_trim,txt_frame,time,comment_txt

        !Main Method
        total_atoms = 0
        total_other_atoms = 0

        do iatom=1,size(atoms)
            !Get neighbours (2,3) of input atoms and add to other_atoms_list
            if (.not. opt_is_lattice_dump) then
                do N=2,3
                    total_other_atoms = total_other_atoms + 1
                    atom = ret_neighbour(atoms(iatom)%atom,N)
                    other_atoms(total_other_atoms) = lattice(ilattice(atom))
                    !Get enighbours (2,3) of those atoms
                    do N2=2,3
                        total_other_atoms = total_other_atoms + 1
                        other_atoms(total_other_atoms) = lattice(ilattice(ret_neighbour(atom,N2)))
                    end do
                end do
            end if
        end do

        total_atoms = size(atoms) + total_other_atoms

        write(file_unit,'(i10)') total_atoms
        write(txt_lattice,'(a9,2(f7.2,a13),f7.2,a1)') 'Lattice="',simulation%Nx * 3.5698," 0.0 0.0 0.0 ",simulation%Ny * 3.5698, " 0.0 0.0 0.0 ",surface%k_max%val * 3.5698,'"'
        if (outputs%selection_on) then
            write(properties,'(a53)') " Properties=species:S:1:pos:R:3:id:I:1:selection:I:1 "
        else
            write(properties,'(a39)') " Properties=species:S:1:pos:R:3:id:I:1 "
        end if
        write(time_trim,'(f10.4)') kmc_info%time
        time_trim = adjustl(time_trim)
        write(time,'(a7,a10,a3)') ' Time="',trim(time_trim),' s"'
        write(number,'(i10)') kmc_info%step
        number = adjustl(number)
        write(txt_frame,'(a9,a,a1)') ' Frame=" ',trim(number),'"'
        write(comment_txt,'(a,a,a1)') ' Comment=" ',trim(comment),'"'
        write(file_unit,*) trim(txt_lattice),trim(properties),trim(time),trim(txt_frame),trim(comment_txt)

        !Loop through core atoms
        do iatom=1,size(atoms)

            call get_atom_code(atoms(iatom)%state,atoms(iatom)%act_state,atom_code)
            call position(atoms(iatom)%atom, atoms, coords)

            write(file_unit,'(a5,3(EN15.5),i6)') atom_code,coords%x, coords%y, coords%z,iatom
        end do

        !Loop through extra atoms
        do iatom=1,total_other_atoms

            call get_atom_code(other_atoms(iatom)%state,other_atoms(iatom)%act_state,atom_code)
            call position(other_atoms(iatom)%atom, atoms, coords)

            write(file_unit,'(a5,3(EN15.5),i6)') atom_code,coords%x, coords%y, coords%z, iatom
        end do

    end subroutine output_xyz


    !---------------------------------------------------------------
    !Output the simulation surface, passed as an atom list to u_out,
    !in an extended xyz format.
    !---------------------------------------------------------------
    subroutine output_surface(atoms, surf_info, kmc_info, u_out)
        !Modules
        use types, only : atom_type_ext, atom_type, &
                surface_type, kmc_type
        use variables, only : return_ilattice_limits
        use input, only : outputs
        use basis, only : ret_height
        !Arguments
        type(atom_type_ext), intent(in) :: atoms(:)
        type(surface_type), intent(in) :: surf_info
        type(kmc_type), intent(in) :: kmc_info
        integer, intent(in) :: u_out
        !Variables
        integer :: kmin_new
        type(atom_type), allocatable :: frame_atoms(:)
        integer :: nframe_atoms
        integer :: iatom_lims(2)
        real(dp) :: height_sum, ave_s_height, k_scale
        integer :: lowest_k
        character(1000) :: frame_text

        !Get limits of the surface
        if (surf_info%k_min%val < outputs%nlookdown + 1) then
            kmin_new = 1
        else
            kmin_new = surf_info%k_min%val - outputs%nlookdown
        end if
        call return_ilattice_limits(kmin_new, surf_info%k_max%val, iatom_lims)

        !Method
        !> Create list of surface atoms or neighbours of surface atoms
        !> Add atoms n atoms below the surface atom to the atom list
        call get_frame_atoms(atoms, iatom_lims, outputs%nlookdown, &
                             frame_atoms, height_sum, lowest_k)
        nframe_atoms = size(frame_atoms)
        ave_s_height = height_sum/nframe_atoms

        !> Get text for generating the xyz preamble
        call get_output_string(kmc_info%step, kmc_info%time, &
            surf_info%k_max, &
            outputs%height_scale_mode, &
            outputs%selection_on, &
            outputs%highlight_last_frame, &
            !Convert from A to nm
            surf_info%ave_growth_height/1E1, &
            3600*surf_info%ave_growth_rate/1E1, &
            3600*surf_info%instant_growth_rate%rate/1E1, &
            surf_info%envs, &
            surf_info%rms_roughness/1E1, &
            surf_info%rms_2NN/1E1, &
            surf_info%rms_3NN/1E1, &
            surf_info%rms_4NN/1E1, &
            surf_info%top_occupancy, &
            surf_info%min_layer, &
            surf_info%dimer_prop, &
            surf_info%dimer_deviation, &
            frame_text)

        if (outputs%height_scale_mode == 2) then
            k_scale = ave_s_height
        else if (outputs%height_scale_mode == 1) then
            k_scale = ret_height(atom_type(1,1,1,lowest_k), atoms)
        else
            k_scale = 0.0_dp
        end if
        call write_xyz(frame_atoms, atoms, k_scale, frame_text, u_out)

        deallocate(frame_atoms)

    end subroutine output_surface


    !> Create list of surface atoms or neighbours of surface atoms
    !> Add atoms n atoms below the surface atom to the atom list
    !> and the lowest k value within the frame
    subroutine get_frame_atoms(atoms, i_limits, n, fatoms, hsum, lheight)
        !Modules
        use types, only : atom_type_ext, atom_type
        use variables, only : read_atom, ilattice, surface_atom
        use periodic, only : ret_neighbour, get_neighbour_atoms
        use enumerate, only : atypes => types_of_atom, file_units
        !Arguments
        type(atom_type_ext), intent(in) :: atoms(:)
        integer, intent(in) :: n
        integer, intent(in) :: i_limits(:)
        type(atom_type), allocatable, intent(out) :: fatoms(:)
        real(dp), intent(out) :: hsum
        integer, intent(out) :: lheight
        !Variables
        integer :: i, nf_atoms, j
        integer :: ilat
        logical, allocatable :: included(:)
        type(atom_type), allocatable :: tmp(:)
        type(atom_type) :: atom
        type(atom_type) :: atoms_below(n)
        type(atom_type) :: neigh
        type(atom_type) :: neighbours(4)
        logical :: neigh_surf(4)

        allocate(fatoms(size(atoms)))
        allocate(included(size(atoms)))
        included = .false.
        hsum = 0.0_dp
        lheight = 1000
        atoms_below = atom_type()

        nf_atoms = 0
        do i=i_limits(1), i_limits(2)
            if (.not. atoms(i)%surface .and. atoms(i)%dimer) then
                write(file_units%warning, *) i, "non-surface + dimer!"
            end if
            call get_neighbour_atoms(atoms(i)%atom, neighbours(1), neighbours(2), neighbours(3), neighbours(4))
            do j=1,4
                neigh_surf(j) = surface_atom(neighbours(j), atoms)
            end do
            if (any(neigh_surf) .or. atoms(i)%surface) then
                if (included(i)) cycle
                if (read_atom(atoms(i)%atom, atoms) /= atypes%empty_site) then
                    call add_atom_to_frame(atoms(i)%atom, i, atoms, fatoms, nf_atoms, included, hsum, lheight)
                end if
                !Add direct neighbours below this surface atom (and then their neighbours)
                do j=2,3
                    neigh = ret_neighbour(atoms(i)%atom, j)
                    ilat = ilattice(neigh)
                    if (.not. included(ilat)) then
                        if (atoms(ilat)%state /= atypes%empty_site) then
                            call add_atom_to_frame(atoms(ilat)%atom, ilat, atoms, fatoms, nf_atoms, included, hsum, lheight)
                        end if
                    end if
                end do

                !Get atoms in same xy cell, n basis values below
                call look_below(atoms(i)%atom, n, atoms_below)
                do j=1, size(atoms_below)
                    ilat = ilattice(atoms_below(j))
                    if (included(ilat)) cycle

                    if (read_atom(atoms_below(j), atoms) /= atypes%empty_site) then
                        if (ilat < 1) exit

                        call add_atom_to_frame(atoms_below(j), ilat, atoms, fatoms, nf_atoms, included, hsum, lheight)
                    end if
                end do
            end if
        end do

        !Return fatoms as dense array
        allocate(tmp(nf_atoms))
        tmp(:) = fatoms(1:nf_atoms)
        call move_alloc(tmp, fatoms)
        deallocate(included)

    end subroutine get_frame_atoms


    subroutine add_atom_to_frame(atom, ilat, atoms, frame, nf, tracking, hsum, lheight)
        !Modules
        use types, only : atom_type, atom_type_ext
        use basis, only : ret_height
        !Arguments
        type(atom_type), intent(in) :: atom
        integer, intent(in) :: ilat
        type(atom_type_ext), intent(in) :: atoms(:)
        type(atom_type), intent(inout) :: frame(:)
        integer, intent(inout) :: nf
        logical, intent(inout) :: tracking(:)
        real(dp), intent(inout) :: hsum
        integer, intent(inout) :: lheight


        frame(nf+1) = atom
        nf = nf + 1
        tracking(ilat) = .true.

        hsum = hsum + ret_height(atom, atoms)
        if (atom%k < lheight) lheight = atom%k


    end subroutine add_atom_to_frame


    subroutine write_xyz(fatoms, atoms, k_scale, f_text, unit)
        !Modules
        use types, only : atom_type_ext, atom_type, coord_type
        use variables, only : ilattice, add_unit_to_open_list
        use input, only : outputs, colourise
        use basis, only : position
        !Arguments
        type(atom_type), intent(in) :: fatoms(:)
        type(atom_type_ext), intent(in) :: atoms(:)
        real(dp), intent(in) :: k_scale
        character(*), intent(in) :: f_text
        integer, intent(in) :: unit
        !Variables
        integer :: i, ilat
        logical :: is_open
        character(1) :: atom_code
        character(150) :: atom_colour_text
        type(coord_type) :: atom_position
        character(41) :: h_format = '(a3,3(F9.3),i15,i5,a1,a,L3)'
        character(32) :: noh_format = '(a3,3(F9.3),i15,L3)'
        logical :: proceed_colourise !Identify atom as a custom colour

        !Check write_file unit is opened, otherwise open the unit
        inquire(unit=unit, opened=is_open)
        if (.not. is_open) then
            open(unit=unit,file="output_position.xyz")
            call add_unit_to_open_list(unit)
        end if

        write(unit, '(i10)') size(fatoms)
        write(unit, '(a)') trim(f_text)
        do i=1,size(fatoms)
            ilat = ilattice(fatoms(i))
            call get_atom_code(atoms(ilat)%state, atoms(ilat)%act_state, atom_code)
            call position(atoms(ilat)%atom, atoms, atom_position)
            if (outputs%highlight_last_frame) then
                call colour_atom(atoms(ilat), atom_colour_text, proceed_colourise)
                write(unit, fmt=h_format) atom_code, &
                        atom_position%x, atom_position%y, atom_position%z - k_scale, &
                        i, atoms(ilat)%last_event, " ", trim(atom_colour_text), &
                        proceed_colourise
            else
                write(unit, fmt=noh_format) atom_code, &
                        atom_position%x, atom_position%y, atom_position%z - k_scale, &
                        i, atoms(ilat)%surface
            end if

        end do

    end subroutine write_xyz


    !-------------------------------------------------
    !Return atom code (C,D etc) depending on atom type
    !-------------------------------------------------
    subroutine get_atom_code(atom_type, act_state, atom_code)
        !---------------
        !Module use statements
        !---------------
        use enumerate,  only : types_of_atom,not_activated
        !---------------
        !Variable declaration
        !---------------
        integer, intent(in) :: atom_type
        integer, intent(in) :: act_state
        character(1), intent(out) :: atom_code

        !---------------
        !Main Method
        !---------------
        if (atom_type == types_of_atom%surf_carb) then
            if (act_state == not_activated) then
                atom_code = "C"
            else if (act_state == 1) then
                atom_code = "D"
            elseif (act_state == 2) then
                atom_code = "E"
            elseif (act_state == 2) then
                atom_code = "F"
            end if
        else if (atom_type == types_of_atom%acetyl_c2) then
            if (act_state == not_activated) then
                atom_code = "G"
            else if (act_state >= 1) then
                atom_code = "H"
            end if
        else
            atom_code = "Z"
        end if

    end subroutine get_atom_code

    subroutine dump_surrounding_lattice(atom, lattice, comment, file_unit)
        !Modules
        use types, only : atom_type, atom_type_ext
        use variables, only : ilattice, simulation
        !Arguments
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: lattice(:)
        character(*), intent(in) :: comment
        integer, intent(in) :: file_unit
        !Variables
        integer :: atoms_per_layer
        integer :: ilat, ilat_min, ilat_max

        !Method
        !Determine bounds of lattice to output
        atoms_per_layer = simulation%Nx * simulation%Ny * 2
        ilat = ilattice(atom)
        if (ilat - (2 * atoms_per_layer) < 1) then
            ilat_min = 1
        else
            ilat_min = ilat - (2 * atoms_per_layer)
        end if
        if (ilat + (2 * atoms_per_layer) > size(lattice)) then
            ilat_max = size(lattice)
        else
            ilat_max = ilat + (2 * atoms_per_layer)
        end if

        call output_xyz(file_unit, lattice(ilat_min:ilat_max), comment, opt_is_lattice_dump=.true.)



    end subroutine dump_surrounding_lattice


    !Look down natoms basis values below an atom and return an atom list of these n atoms
    subroutine look_below(atom, natoms, atom_list)
        !Modules
        use types,      only : atom_type
        use variables,  only : simulation
        !Variables
        type(atom_type), intent(in) :: atom
        integer, intent(in) :: natoms
        type(atom_type), intent(out) :: atom_list(natoms)

        integer :: ib, i
        integer :: sbasis, ebasis    !Start and End Basis
        type(atom_type) :: t_atom           !Temp atom
        integer :: look_down

        !Initialise Variables
        look_down   = size(atom_list)                                   !Must be larger than 2 (due to loop?)

        t_atom      = atom
        sbasis      = atom%basis - 1
        ebasis      = atom%basis - look_down

        !Main Method
        i = 0
        do ib = sbasis,ebasis,-1
            i = i + 1
            atom_list(i) = atom_type(0,0,0,0)
            t_atom%basis = mod(simulation%Nbasis + ib -1,simulation%Nbasis) + 1

            if (ib <= 0) t_atom%k = atom%k - 1
            atom_list(i) = t_atom
        end do

    end subroutine look_below


    !----------------------------------------
    !Construct string for extended xyz format
    !----------------------------------------
    subroutine get_output_string(curr_step, &
                                 curr_time, &
                                 k_max, &
                                 k_scale_mode, &
                                 selection_on, &
                                 last_frame, &
                                 growth_height, &
                                 growth_rate_overall, &
                                 growth_rate_instant, &
                                 envs, &
                                 rms, &
                                 NN2_rms, &
                                 NN3_rms, &
                                 NN4_rms, &
                                 act_layers_occ, &
                                 imin_layer, &
                                 dimer_pc, &
                                 dimer_misalign, &
                                 text_out)
        !Modules
        use variables,  only : simulation,a0,surface
        use types,      only : minmax_k
        !Arguments
        integer, intent(in) :: curr_step
        real(dp),intent(in) :: curr_time
        type(minmax_k),intent(in) :: k_max
        logical, intent(in) :: selection_on,last_frame
        integer, intent(in) :: k_scale_mode
        real(dp), intent(in) :: growth_height !In Angstrom
        real(dp), intent(in) :: growth_rate_overall,growth_rate_instant !In nm/h
        integer, intent(in), dimension(0:4) :: envs
        real(dp), intent(in) :: rms, NN2_rms, NN3_rms, NN4_rms !In Angstrom
        real(dp), dimension(:), intent(in) :: act_layers_occ
        integer, intent(in) :: imin_layer
        real(dp), intent(in) :: dimer_pc, dimer_misalign
        character(1000),intent(out)  :: text_out
        !Variables
        real(dp), dimension(0:4) :: envs_pc
        character(200) :: lattice_txt,properties_txt,time_txt,frame_txt,roughness_txt,envs_txt,height_txt,gr_txt
        character(50) :: rms_trim,NN2_rms_trim,NN3_rms_trim,NN4_rms_trim
        character(50) :: env0_trim,env1_trim,env2_trim,env3_trim,env4_trim
        character(200) :: time_trim,gr_inst,gr_overall,frame_trim,height_trim
        character(200) :: dimer_prop, dimer_prop_trim, dimer_dev, dimer_dev_trim
        character(200) :: gr_overall_trim,gr_instant_trim
        character(200) :: dimer_txt
        character(500) :: act_layers_txt
        character(100) :: temp
        character(5) :: int_temp
        character(20) :: real_temp
        real(dp) :: cell_height          !In Angstrom
        integer :: il !Layer index
        !Method
        !Lattice Text
        if (k_scale_mode /= 0) then
            cell_height = (3 + k_max%val-surface%k_min%val) * a0
        else
            cell_height = simulation%max_height * a0
        end if
        write(lattice_txt,'(a9,2(f7.2,a13),f7.2,a1)') 'Lattice="',simulation%Nx * a0," 0.0 0.0 0.0 ",simulation%Ny * a0, " 0.0 0.0 0.0 ",cell_height,'"'

        !Column Text
        !Selection On (last frame must be off)
        if (selection_on) then
            write(properties_txt,'(a)') " Properties=species:S:1:pos:R:3:id:I:1:selection:I:1"
        !Last Frame on (selection must be off)
        elseif (last_frame) then
            write(properties_txt,'(a)') " Properties=species:S:1:pos:R:3:id:I:1:event:I:1:color:R:3:selection:I:1"
        !Neither on
        else
            write(properties_txt,'(a)') " Properties=species:S:1:pos:R:3:id:I:1"
        end if
        !Time Text
        write(time_trim,'(f10.5)') curr_time
        time_trim = adjustl(time_trim)
        write(time_txt,'(a7,a10,a1)') ' Time="',trim(time_trim),'"'
        !Roughness Text
        write(rms_trim,'(f10.5)') rms
        rms_trim = adjustl(rms_trim)
        write(NN2_rms_trim,'(f10.5)') NN2_rms
        NN2_rms_trim = adjustl(NN2_rms_trim)
        write(NN3_rms_trim,'(f10.5)') NN3_rms
        NN3_rms_trim = adjustl(NN3_rms_trim)
        write(NN4_rms_trim,'(f10.5)') NN4_rms
        NN4_rms_trim = adjustl(NN4_rms_trim)
        write(frame_trim,'(i10)') curr_step
        frame_trim = adjustl(frame_trim)
        write(roughness_txt,'(4(a,a,a1))') ' RMS(nm)="',trim(rms_trim),'"', &
                ' RMS[3A](nm)="',trim(NN2_rms_trim),'"', &
                ' RMS[8A](nm)="',trim(NN3_rms_trim),'"', &
                ' RMS[12A](nm)="',trim(NN4_rms_trim),'"'
        !Growth Rate
        write(gr_overall, '(f10.3)') growth_rate_overall
        gr_overall_trim = trim(adjustl(gr_overall))
        write(gr_inst, '(f10.3)') growth_rate_instant
        gr_instant_trim = trim(adjustl(gr_inst))
        write(gr_txt,'(2(a,a,a1))') ' GR[OR](nm/h)="',trim(gr_overall),'"', &
                ' GR[INST](nm/h)="',trim(gr_inst),'"'
        !Dimer Alignment/Proportion
        write(dimer_prop, '(f10.3)') 100 * dimer_pc
        dimer_prop_trim = trim(adjustl(dimer_prop))
        write(dimer_dev, '(f10.3)') dimer_misalign
        dimer_dev_trim = trim(adjustl(dimer_dev))
        write(dimer_txt,'(2(a,a,a1))') ' dimerpc="',trim(dimer_prop_trim),'"', &
                ' dimer_dev="',trim(dimer_dev_trim),'"'
    !Envs text
        if (sum(envs(:)) /= 0.0_dp) then
            envs_pc(:) = 100_dp*(real(envs(:))/sum(envs(:)))
        else
            envs_pc(:) = 0.0_dp
        end if
        write(env0_trim,'(f6.2)') envs_pc(0)
        env0_trim = adjustl(env0_trim)
        write(env1_trim,'(f6.2)') envs_pc(1)
        env1_trim = adjustl(env1_trim)
        write(env2_trim,'(f6.2)') envs_pc(2)
        env2_trim = adjustl(env2_trim)
        write(env3_trim,'(f6.2)') envs_pc(3)
        env3_trim = adjustl(env3_trim)
        write(env4_trim,'(f6.2)') envs_pc(4)
        env4_trim = adjustl(env4_trim)
        write(envs_txt,'(5(a,a,a1))') ' Iso="',trim(env0_trim),'"', &
                                        ' S_A="',trim(env1_trim),'"', &
                                        ' S_B="',trim(env2_trim),'"', &
                                        ' S_AB="',trim(env3_trim),'"', &
                                        ' Terr="',trim(env4_trim),'"'
        !Frame Text
        write(frame_txt,'(a,a,a1)') ' Frame="',trim(frame_trim),'"'
        write(height_trim,'(f6.3)') growth_height
        height_trim = adjustl(height_trim)
        write(height_txt,'(a,a,a1)') ' Height(nm)="',trim(height_trim),'"'
        !Activate Layers
        write(int_temp, '(i2)') size(act_layers_occ)
        write(act_layers_txt, '(a,a,a1)') ' active_layers="',trim(adjustl(int_temp)),'"'
        write(int_temp, '(i2)') imin_layer
        write(temp, '(a,a,a1)') ' imin="',trim(adjustl(int_temp)),'"'
        act_layers_txt = trim(act_layers_txt)//trim(temp)
        do il=1, size(act_layers_occ) + (15-size(act_layers_occ))
            write(int_temp, '(i2)') il
            if (il > size(act_layers_occ)) then
                write(real_temp, '(f6.2)') 0.00
            else
                write(real_temp, '(f6.2)') act_layers_occ(il)
            end if
            write(temp, '(a,a,a2,a,a2)') ' layer', trim(adjustl(int_temp)),'="',trim(adjustl(real_temp)),'%"'
            act_layers_txt = trim(act_layers_txt)//trim(temp)
        end do

        !Overall Text Out
        write(text_out,*) trim(lattice_txt), &
                          trim(properties_txt), &
                          trim(time_txt), &
                          trim(frame_txt), &
                          trim(height_txt), &
                          trim(roughness_txt), &
                          trim(gr_txt), &
                          trim(envs_txt), &
                          trim(act_layers_txt), &
                          trim(dimer_txt)

    end subroutine get_output_string

    !--------------------------------------------------------------
    !Return the RGB extended xyz colour output for an atom
    !Read the atom_type_ext for whether it needs standard colouring
    !or event based colouring
    !--------------------------------------------------------------
    subroutine colour_atom(atom, colour_text, last_frame)
        !Modules
        use types, only : atom_type_ext, rgb_type
        use input, only : colourise
        !Arguments
        type(atom_type_ext), intent(in) :: atom
        character(150), intent(out) :: colour_text
        logical, intent(out) :: last_frame
        !Variables
        type(rgb_type) :: rgb_colour

        last_frame = atom%last_frame

        !Method
        if (atom%last_frame) then
            if (colourise(atom%last_event)%colour_event) then
                rgb_colour = colourise(atom%last_event)%colour
                write(colour_text,'(f4.2,2(a,f4.2))') rgb_colour%R,"  ",rgb_colour%G,"  ",rgb_colour%B
            else
                last_frame = .false.
                write(colour_text,'(f4.2,2(a,f4.2))') 144.0/255,"  ", 144.0/255,"  ", 144.0/255
            end if

        else
            write(colour_text,'(f4.2,2(a,f4.2))') 144.0/255,"  ", 144.0/255,"  ", 144.0/255
        end if

        colour_text = trim(colour_text)

    end subroutine colour_atom

    subroutine write_to_layers_file(f_unit, s, kmc)
        !Modules
        use types, only : surface_type, kmc_type
        !Arguments
        integer, intent(in) :: f_unit
        type(surface_type), intent(in) :: s
        type(kmc_type), intent(in) :: kmc
        !Variables
        integer :: ilayer
        !Method
        write(f_unit, '(i10,a1,ES15.3,3(a1,i6))', advance='NO') kmc%step, ",", kmc%time ,",", &
                                                                s%active_layers, ",", s%min_layer-1, ",", s%max_layer-1
        do ilayer=1, size(s%layers)
            write(f_unit, '(a1,f6.3)', advance='NO') ",", s%layers(ilayer)
        end do
        write(f_unit, *) " "
    end subroutine write_to_layers_file

    !Write a array of integers to file opened at unit, separated by commas
    integer function int_array_as_csv(items, unit) result(complete)
        !Modules
        !Arguments
        integer, dimension(:), intent(in) :: items
        integer, intent(in) :: unit
        !Variables
        character(500) :: output_string
        character(10) :: item
        logical :: is_open, close_at_end
        integer :: i

        close_at_end = .false.
        output_string = ""
        complete = 1

        inquire(unit=unit, opened=is_open)
        if (.not. is_open) then
            open(unit=unit, file="tmp_int_array.csv", status='old')
            close_at_end = .true.
        end if

        do i=1, size(items)
            write(item, '(i10)') items(i)
            output_string = trim(output_string) // trim(item)
            if (i /= size(items)) then
                output_string = trim(output_string) // ","
            end if
        end do

        write(unit, '(a)') trim(output_string)
        complete = 0

        if (close_at_end) then
            close(unit)
        end if

    end function int_array_as_csv

    !-----------------------------------------------
    !Flush open units passed as an array of integers
    !-----------------------------------------------
    subroutine flush_units(units)
        !Arguments
        integer, dimension(:), intent(in) :: units
        !Variables
        integer :: i
        logical :: is_open
        !Method
        if (size(units) < 1) return

        do i=1, size(units)
            if (units(i) /= -1) then
                inquire(unit=units(i), opened=is_open)
                if (is_open) flush(units(i))
            end if
        end do

    end subroutine flush_units

    end module output
