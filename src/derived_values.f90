module derived_values
    use kinds,    only : dp
    use types,    only : acetylene_rates_type, fr_k_type

    implicit none

    real(dp)                :: k1,km1,k2,km2 ! RC H abstraction,H2 addition (k-1),H addition
    real(dp)                :: k3,k3_act ! Rate constant for CH3 dimer insertion and when dimer is activated
    real(dp)                :: k5,k6,k7 ! C insertion,CHinsertion,CH2insertion
    real(dp)                :: k14 ! RC for Butler defect formation
    real(dp)                :: kbetaScission
    real(dp)                :: Fsp3 ! Butler's fraction of sp3
    real(dp)                :: Fyuri ! yuri fraction of act monorad sites
    real(dp)                :: Fbutler ! Fraction act monorad sites: simplified Butler model [jap 99 104907]
    real(dp)                :: Fmr,Fbr ! chosen fraction of monoradical and biradical sites
    real(dp)                :: dcalc ! Yuri's estimate of crystallite size

    real(dp)                :: CH3impactrate
    real(dp), dimension(4)  :: CHXimpactrate
    real(dp)                :: activate_rate,deactivate_rate

    real(dp)                :: Gmono,Gbi,Gchx ! initialise.335-337 unknown (free energies?)

    real(dp), dimension(4)  :: rDimerInsertCHX, rTroughInsertCHX
    real(dp)                :: defectatomAdd ! rTroughInsertCH3 * defgrowth_param
    !-------
    !Etching
    !-------
    real(dp)                :: etchrate0,etchrate1,etchrate2,etchrate3,etchrate4
    real(dp),dimension(0:4) :: pref_etch_rates
    real(dp)                :: ketch_CHx

    real(dp)                :: k_dimer_mig  !Dimer migration rate
    !Gap Migration subreactions [A. Netto and M. Frenklach, Diam. Relat. Mater., 2005, 14, 1630–1646.]
    real(dp)                :: k6a_1f,k6a_2f,k6a_2r,k6a_1r
    real(dp)                :: kgmig_a,kgmig_b,kgmig_c,kgmig_d
    real(dp), dimension(4)  :: kgmig_all_rates
    real(dp)                :: k_down_mig   !Migration down a step
    !----
    !C2H2
    !----
    !C2H2 Adsorption
    real(dp)                :: C2H2_impactrate
    type(acetylene_rates_type) :: C2H2_ads_rates
    !C2H2 Migration
    type(fr_k_type), allocatable :: kC2H2p_mig_sub(:)
    real(dp) :: kC2H2p_mig(2) !(Without/With C2H2 pendant adjacent to final site)

    !----------------------------
    !Isolated Dimer Etching (ide)
    !----------------------------
    !Hold all forward and backward rates for each subreaction 12-n-*i*
    type(fr_k_type) :: ide_rates_all(5,7)

    real(dp), target    :: kide_ind(5) !Hold forward rate constants for enacting 12-i (i=1-5)
    real(dp), target    :: kide_comb(5) !Hold forward rate constants for enacting 12-1 + 12-i (i=2-5)
    real(dp), pointer   :: kide(:) !Associated with either individual or combined rate constants
    real(dp) :: kide_perms(16,4) !Rates for each ide etch for each permutation of the 4 etching reactions

    !Reform dimer from C2H2 pendant (c2h2p)
    real(dp) :: kc2h2p_dimer

    !Dimer breaking
    real(dp) :: kf_adj_dimer, kr_adj_dimer  !Forward and reverse rates of reactor for dimer formation (adjacent)
    real(dp) :: prob_adj_dimer_break        !Probability of adjacent dimer being broken


end module derived_values
