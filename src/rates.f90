module rates

    use kinds,                    only : dp


    implicit none

contains
    !------------------
    !Takes input of the rate_sum_vector and random number
    !returns index k such than rsv(k-1)< rnum <= rsv(k+1)
    ! ------------------
    subroutine locate(rsv,rnum, k)
        !-------
        !Modules
        !-------
        use variables,  only : kmc_info
        use enumerate,  only : file_units
        !---------
        !Variables
        !---------
        real(dp), intent(in) :: rsv(:)
        real(dp), intent(in) :: rnum
        integer, intent(out) :: k
        integer :: iRSV

        !-----------
        !Main Method
        !-----------

        !Start at beginning of rate sum vector (rsv)
        k = 1

        !Detect if random number input is 0.0 and error if so
        if (rnum == 0.0)  then
            write(*,*) "ERROR: LOCATE(RNUM) - RNUM = 0"
            write(file_units%error, '(i10, a, EN15.5)') kmc_info%step, "Input Random Number to locate rate in RSV = 0.0: ",rnum
            STOP
        end if

        do iRSV=1,size(rsv)
            if (rsv(iRSV) >= rnum) then
                !Found rate in rsv at index iRSV
                k = iRSV
                return
            end if
        end do

        write (*,'(a)') "ERROR: LOCATE() - RNUM NOT WITHIN RSV(:)"
        write (file_units%error,'(a,i6,a,i6,a,ES12.5)') "ERROR: LOCATE(): step: ",kmc_info%step, " size(rsv): ",size(rsv), " rnum: ",rnum
        STOP

    end subroutine locate


    !----------------------------------------------------------------------------
    !Call rate setting subroutines for surface atoms from input surface atom list
    !----------------------------------------------------------------------------
    subroutine call_rate_setting(atoms, rate_list, ilr)
        !todo refactor
        !Modules
        use types, only : atom_type, atom_type_ext, rate_type
        use variables, only : surface, return_ilattice_limits, &
                & read_atom, read_ipair, read_dimer, is_act, atom_from_index
        use enumerate, only : types_of_atom
        use periodic, only : get_neighbour_atoms, ret_neighbour
        use input, only : reactions
        use dimer, only : return_bridging_atom, isolated_dimer
        !Rate setting subroutines
        use etching, only : set_pref_etch_rate, set_etch_c2h2_rate, set_ide_rate, set_chx_etch_k11_rate
        use adsorb_atom, only : set_chx_insert_rate, set_c2h2_M1_rate, set_c2h2_M2_rate, &
                set_c2h2_M3_rate, biradical_site
        use migration, only : set_dimer_migration_rate, set_gap_mig_rate, set_C2H2_mig_biradical_rate, &
                set_mig_down_rate
        use c2h2, only : set_C2H2p_gap_migrate
        use ide, only : set_dimer_reformation_rate
        !Variables
        type(atom_type_ext), intent(in) :: atoms(:)
        type(rate_type), intent(inout) :: rate_list(:)
        integer, intent(inout) :: ilr
        integer, dimension(2) :: lattice_limits
        type(atom_type) :: atom
        type(atom_type) :: N2,N3,temp1,temp4 !Neighbours
        integer :: local_k_min
        integer :: iatom                !Surface atom index within lattice(:)
        logical :: activated_atom
        logical :: activated_neighbour  !True if either N2 or N3 is activated
        type(atom_type) :: N1_site, bridging, pair      !N1 of atom to be used as site, atom bridging a dimer pair, dimer_pair
        !Initialise
        activated_neighbour = .false.
        activated_atom      = .false.
        local_k_min = 1000
        !Main Method
        call return_ilattice_limits(surface%k_min%val-1,surface%k_max%val,lattice_limits)
        do iatom=lattice_limits(1),lattice_limits(2)
            if (atoms(iatom)%surface .eqv. .false.) cycle

            atom = atoms(iatom)%atom
            !Check if this surface atom is below current 'min_k' value
            if (atom%k < local_k_min) then
                local_k_min = atom%k
            end if
            !Get Neighbours
            call get_neighbour_atoms(atom, temp1,N2,N3,temp4)
            !Assign activated_neighbour (migration)
            if (is_act(N2, atoms) .or. is_act(N3, atoms)) then
                activated_neighbour = .true.
            endif
            !Assign activated_atom
            if (is_act(atom, atoms)) activated_atom = .true.

            !-----------------
            !CHx Incorporation
            !-----------------
            if (reactions%active%InsertDimerCHX_on .or. reactions%active%InsertTroughCHX_on) then
                N1_site = ret_neighbour(atom,1)
                if (read_atom(N1_site, atoms) == types_of_atom%empty_site) then
                    call set_chx_insert_rate(N1_site, atoms, rate_list, ilr)
                end if
            end if
            !---------------
            !C2H2 Adsorption
            !---------------
            if (reactions%active%acetylene%M1) then
                call set_c2h2_M1_rate(atom, atoms, rate_list, ilr)
            end if
            if (reactions%active%acetylene%M2) then
                if (activated_atom) then
                    if (read_dimer(atom, atoms)) then
                        call return_bridging_atom(atom,atom_from_index(read_ipair(atom, atoms), atoms), bridging)
                        !If biradical, only call the rate setting on the lowest basis atom of the dimer pair (prevent double rate setting)
                        if (biradical_site(bridging, atoms)) then
                            pair = atom_from_index(read_ipair(atom, atoms), atoms)
                            if (atom%basis < pair%basis) call set_c2h2_M2_rate(atom, atoms, rate_list, ilr)
                        else
                            call set_c2h2_M2_rate(atom, atoms, rate_list, ilr)
                        end if
                    end if
                end if
            end if
            if (reactions%active%acetylene%M3a) then
                if (activated_atom) then
                    call set_c2h2_M3_rate(atom, atoms, rate_list, ilr)
                end if
            end if
            !-------
            !Etching
            !-------
            !Carbon (pref)
            if (reactions%active%pref_etch_C_on) then ! only set etch rates if etching is on
                if (activated_neighbour) then
                    call set_pref_etch_rate(atom, atoms, rate_list, ilr)
                end if
            end if
            if (reactions%active%etch_CHx) then
                call set_chx_etch_k11_rate(atom, atoms, rate_list, ilr)
            end if
            !C2H2
            if (reactions%active%acetylene%etchCH3 .or. reactions%active%acetylene%etchC2H2) then
                call set_etch_c2h2_rate(atom, atoms, rate_list, ilr)
            end if
            !Isolated Dimer Etching (ide)
            if (any(reactions%active%ide_etch_on)) then
                call set_ide_rate(atom, atoms, rate_list, ilr)
            end if
            !Dimer Reformation
            if (reactions%active%C2H2p_dimer) then
                call set_dimer_reformation_rate(atom, atoms, rate_list, ilr)
            end if

            !---------
            !Migration
            !---------
            !Dimer
            if (reactions%active%dimer_migration_on) then
                if (activated_neighbour) then
                    call set_dimer_migration_rate(atom, atoms, rate_list, ilr)
                end if
            end if
            !Gap
            if (any(reactions%active%gap_mig_ind)) then
                if  (activated_neighbour) then
                    call set_gap_mig_rate(atom, atoms, rate_list, ilr)
                end if
            end if
            !Down
            if (reactions%active%gap_mig_down_on) then
                if (activated_neighbour) then
                    call set_mig_down_rate(atom, atoms, rate_list, ilr)
                end if
            end if

            !Migration of C2H2 into biradical site
            if (read_atom(atom, atoms) == types_of_atom%acetyl_c2) then
                if (is_act(atom, atoms)) then
                    if (reactions%active%acetylene%migC2H2_birad) then
                        call set_C2H2_mig_biradical_rate(atom, atoms, rate_list, ilr)
                    end if
                end if
            end if

            !Migrate C2H2 pendant (gap)
            if (any(reactions%active%C2H2p_mig)) then
                call set_C2H2p_gap_migrate(atom, atoms, rate_list, ilr)
            end if

        end do

        surface%k_min%val = local_k_min

    end subroutine call_rate_setting

    !-----------------------------------------------------------------------
    !Output rates within the model to visualise_rates.xyz with starting atom
    !final sites and any activation requirements
    !Calls bespoke visualisation routines for each rate
    !Used for rates which can be visualised on a clean surface
    !-----------------------------------------------------------------------
    subroutine visualise_init_rates(atom, atoms)
        use types, only : atom_type, atom_type_ext
        use variables, only : rates_visualised
        use surface_m, only : detect_site_env
        use enumerate, only : dimer_site
        use migration, only : visualise_dimer_migration

        type(atom_type), intent(in) :: atom !Initial atom position (starting point for visualisation)
        type(atom_type_ext), intent(in), dimension(:) :: atoms
        type(atom_type) :: e_atom !Atom to be changed depending on context
        e_atom = atom

        if (.not. rates_visualised%dimer_migrate) then
            !DIMER MIGRATION
            !Confirm that `atom' is above a dimer site
            do
                if (detect_site_env(e_atom, atoms) == dimer_site) then
                    exit
                end if
                e_atom%basis = e_atom%basis + 1
            end do

            call visualise_dimer_migration(e_atom, atoms)
            rates_visualised%dimer_migrate = .true.
        end if

    end subroutine visualise_init_rates

end module rates