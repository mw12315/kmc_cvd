! Created by  on 10/30/20.
! Generate surface structures for testing
module test_construct

    use kinds, only : dp

contains

    !-----------------------------------------------------------------------
    !Add dimer constructions onto the surface to set isolated dimer testing
    !-----------------------------------------------------------------------
    subroutine add_isolated_dimers(atoms, kmc_info, sites)
        !Modules
        use types, only : atom_type_ext, atom_type, rate_type, kmc_type
        use variables, only : set_act_state, read_atom, create_dimer_pair
        use adsorb_atom, only : execute_chx_insertion
        use enumerate, only : tRates, actstate_min, types_of_atom
        use dimer, only : dimer_assessment
        use surface_m, only : add_surface_atom
        use execute_rates, only : execute_event
        use periodic, only : get_site_from_N_path
        !Arguments
        type(kmc_type), intent(inout) :: kmc_info
        type(atom_type_ext), intent(inout) :: atoms(:)
        type(atom_type), allocatable, intent(out) :: sites(:)
        !Variables
        type(atom_type) :: atom, atom2
        integer :: i

        allocate(sites(7))

        !Create the isolated dimers
        do i=1, size(sites)
            sites(i) = atom_type(2+4*(i-1),2+4*(i-1),3,4)
            call execute_event(rate_type(sites(i), event=tRates%DinsertCH3), &
                                atoms, kmc_info, .false.)
            atom = get_site_from_N_path(sites(i), [1,2])
            call execute_event(rate_type(atom, event=tRates%DinsertCH3), &
                    atoms, kmc_info, .false.)
            call create_dimer_pair(sites(i), atom, atoms)
        end do


        ! + void + row dimer
        atom = get_site_from_N_path(sites(2), [2,1,2,1])
        call execute_event(rate_type(atom, &
                event=tRates%DinsertCH3), &
                atoms, kmc_info, .false.)
        atom2 = get_site_from_N_path(atom, [1,2])
        call execute_event(rate_type(atom2, event=tRates%DinsertCH3), &
                atoms, kmc_info, .false.)
        call create_dimer_pair(atom, atom2, atoms)


        ! + void + row carbon
        atom = get_site_from_N_path(sites(3), [2,1,2,1])
        call execute_event(rate_type(atom, &
                event=tRates%DinsertCH3), &
                atoms, kmc_info, .false.)


        ! + void + 2 row carbons
        atom = get_site_from_N_path(sites(4), [2,1,2,1])
        call execute_event(rate_type(atom, &
                event=tRates%DinsertCH3), &
                atoms, kmc_info, .false.)
        atom2 = get_site_from_N_path(atom, [1,2])
        call execute_event(rate_type(atom2, event=tRates%DinsertCH3), &
                atoms, kmc_info, .false.)


        ! + row carbon
        atom = get_site_from_N_path(sites(5), [2,1])
        call execute_event(rate_type(atom, &
                event=tRates%DinsertCH3), &
                atoms, kmc_info, .false.)

        ! + row 2 carbons
        atom = get_site_from_N_path(sites(6), [2,1])
        call execute_event(rate_type(atom, &
                event=tRates%DinsertCH3), &
                atoms, kmc_info, .false.)
        atom2 = get_site_from_N_path(atom, [1,2])
        call execute_event(rate_type(atom2, event=tRates%DinsertCH3), &
                atoms, kmc_info, .false.)


        ! + row dimer
        atom = get_site_from_N_path(sites(7), [2,1])
        call execute_event(rate_type(atom, &
                event=tRates%DinsertCH3), &
                atoms, kmc_info, .false.)
        atom2 = get_site_from_N_path(atom, [1,2])
        call execute_event(rate_type(atom2, event=tRates%DinsertCH3), &
                atoms, kmc_info, .false.)
        call create_dimer_pair(atom, atom2, atoms)

    end subroutine add_isolated_dimers

    !----------------------------------------------
    !Add dimers to the surface for etching with ide
    !----------------------------------------------
    subroutine add_dimers_for_etching(atoms, kmc_info, sites)
        !Modules
        use types, only : atom_type_ext, atom_type, kmc_type, rate_type
        use variables, only : set_act_state, create_dimer_pair
        use enumerate, only : tRates, actstate_max
        use adsorb_atom, only : execute_chx_insertion
        use dimer, only : dimer_assessment
        use surface_m, only : add_surface_atom
        use periodic, only : get_site_from_N_path
        use execute_rates, only : execute_event
        !Arguments
        type(atom_type_ext), intent(inout) :: atoms(:)
        type(kmc_type), intent(inout) :: kmc_info
        type(atom_type), intent(out) :: sites(5)
        !Variables
        integer :: i
        type(atom_type) :: atom


        !Create the isolated dimers
        do i=1, size(sites)
            sites(i) = atom_type(2+4*(i-1),2+4*(i-1),3,4)
            call execute_event(rate_type(sites(i), event=tRates%DinsertCH3), &
                    atoms, kmc_info, .false.)
            atom = get_site_from_N_path(sites(i), [1,2])
            call execute_event(rate_type(atom, event=tRates%DinsertCH3), &
                    atoms, kmc_info, .false.)
            call create_dimer_pair(sites(i), atom, atoms)
        end do


    end subroutine add_dimers_for_etching


    !--------------------------------------------------------------
    !Create initial atomic environment suitable for rate setting of
    !C2H2 pendant gap migration.
    !Returns the site which should have rate setting called on it
    !--------------------------------------------------------------
    subroutine create_c2h2p_migrate_env(atoms, kmc_info, site)
        !Modules
        use types, only : atom_type_ext, atom_type, kmc_type, rate_type
        use enumerate, only : tRates
        use adsorb_atom, only : execute_chx_insertion, execute_C2H2_adsorption
        use pendant, only : patom2_store
        use activate, only : activate_atom
        use periodic, only : get_site_from_N_path, ret_neighbour
        !Arguments
        type(atom_type_ext), intent(inout) :: atoms(:)
        type(kmc_type), intent(inout) :: kmc_info
        type(atom_type), intent(inout) :: site
        !Variables
        type(atom_type) :: incorp_site, act_target, adadatom
        type(rate_type) :: rate

        !Add C2H2 pendant to the surface
        incorp_site = atom_type(10,10, 3, 4)
        site = incorp_site
        rate = rate_type(incorp_site, &
                         0.0_dp, &
                         tRates%C2H2_M1a, &
                         patom2_store(incorp_site, atoms), &
                         0)
        call execute_C2H2_adsorption(rate, kmc_info, atoms)
        call activate_atom(ret_neighbour(rate%site, 2), 3, atoms) !Next to migrating atom
        call activate_atom(ret_neighbour(rate%site, 3), 3, atoms) !Next to migrating atom

        !1> With CH2 adjacent to final site
        adadatom = get_site_from_N_path(incorp_site, [2,1,2,1])
        rate = rate_type(adadatom, &
                0.0_dp, &
                tRates%DinsertCH3, &
                atom_type(), &
                0)
        call execute_chx_insertion(rate, kmc_info, atoms)
        act_target = get_site_from_N_path(rate%site, [2,1,2]) !Next to final site
        call activate_atom(act_target, 3, atoms)

        !2> With C2H2 adjacent to final site
        call activate_atom(ret_neighbour(rate%site, 3), 3, atoms) !Next to migrating atom
        act_target = get_site_from_N_path(rate%site, [3,4,3])
        call activate_atom(act_target, 2, atoms)
        adadatom = get_site_from_N_path(incorp_site, [3,4,3,4])
        rate = rate_type(adadatom, &
                0.0_dp, &
                tRates%C2H2_M1a, &
                patom2_store(adadatom, atoms), &
                0)

        call execute_C2H2_adsorption(rate, kmc_info, atoms)
        call activate_atom(act_target, 2, atoms)

    end subroutine create_c2h2p_migrate_env


    subroutine create_c2h2p_dimer_env(atoms, kmc_info, sites)
        use types, only : atom_type_ext, atom_type, kmc_type, rate_type
        use variables, only : set_act_state, undimer_atom, create_dimer_pair
        use enumerate, only : tRates
        use pendant, only : patom2_store
        use periodic, only : get_site_from_N_path
        use execute_rates, only : execute_event
        !Arguments
        type(atom_type_ext), intent(inout) :: atoms(:)
        type(kmc_type), intent(inout) :: kmc_info
        type(atom_type), allocatable, intent(out) :: sites(:)
        !Variables
        type(rate_type) :: rate
        type(atom_type) :: incorp_site
        type(atom_type) :: dimer(2)

        allocate(sites(1))

        !add C2H2p
        incorp_site = atom_type(10,10,4,4)
        sites(1) = incorp_site
        rate = rate_type(incorp_site, &
            event=tRates%C2H2_M1a, &
            site2=patom2_store(incorp_site, atoms))
        call execute_event(rate, atoms, kmc_info, .false.)
        call set_act_state(rate%site2, atoms, 1)
        !--------------
        !Isolated Dimer
        !--------------
        call undimer_atom(get_site_from_N_path(sites(1), [1,2,2]), atoms)
        call undimer_atom(get_site_from_N_path(sites(1), [1,2,3]), atoms)

        call set_act_state(get_site_from_N_path(sites(1), [1,2,3]), atoms, 1)
        !------------------
        !Non-Isolated Dimer
        !------------------
        call set_act_state(get_site_from_N_path(sites(1), [4,3,2]), atoms, 1)
        dimer(1) = get_site_from_N_path(sites(1), [2,1])
        dimer(2) = get_site_from_N_path(dimer(1), [4,3])
        rate = rate_type(dimer(1), &
                event=tRates%DinsertCH3)
        call execute_event(rate, atoms, kmc_info, .false.)
        rate = rate_type(dimer(2), &
                event=tRates%DinsertCH3)
        call execute_event(rate, atoms, kmc_info, .false.)
        call create_dimer_pair(dimer(1), dimer(2), atoms)

        call undimer_atom(get_site_from_N_path(sites(1), [4,3,3]), atoms)

    end subroutine create_c2h2p_dimer_env

end module test_construct