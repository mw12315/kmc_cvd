program test_main

    use kinds, only : dp
    use types, only : atom_type_ext, atom_type, &
                      sim_type, kmc_type, surface_type, &
                      counter_type, rate_type
    use initialise, only : read_input_files, count_unique_rates, generate_rates, &
                           zero_global_counters, init_types, initialise_lattice, init_surface_type, init_segments
    use variables, only : simulation, kmc_info, lattice, surface, counters, segments, segment_bounds
    use input, only : conditions, energies, etchingf, reactions, outputs, dimerf, colourise
    use output, only : output_surface, output_input_values, output_surface
    use json, only : json_output_input_values
    use randomise, only : activate_surface
    use random, only : init_random_seed
    !Tests
    use tests, only : test_ij_cell_from_dir, test_dimer_isolation, test_ide_rate_setting, test_locate, &
                      test_ide_execution, test_c2h2p_mig_rate_setting, test_c2h2p_mig_execution, &
                      test_dimer_reformation_rate_set, test_dimer_reformation_execution, &
                      test_ide_rate_scaling, test_ide_perms

    implicit none

    !Variables
    integer, parameter :: unit_surface = 30
    integer, parameter :: u_fail_out = 50
    integer :: failed_tests
    logical :: passed
    type(rate_type), allocatable :: rates_set(:)
    type(atom_type), allocatable :: sites(:)
    !System Initialisation
    open(unit=u_fail_out, file='test_diamond.log')
    call read_input_files(simulation, conditions, energies, etchingf, reactions, outputs, dimerf, colourise)
    write(6,*) "Input files sucessfully read"
    call count_unique_rates(simulation, reactions)
    call init_random_seed(simulation%seeding_clock)
    call generate_rates(conditions, reactions, energies, etchingf, dimerf) !Stored within derived values
    !sim_info, conditions, reactions, energies, etching_i
    call output_input_values(simulation, conditions, reactions, energies, etchingf, dimerf)
    call json_output_input_values(simulation, conditions, reactions, energies, etchingf, dimerf, flush=.true.)
    write(6,*) "Initial input values written to main output file"

    call zero_global_counters(counters, kmc_info)
    call init_types()

    simulation%Nx = 30
    simulation%Ny = 30
    simulation%Nz = 3
    simulation%fill_to_basis = 2


    !=============
    !BEGIN TESTING
    !=============
    open(unit=unit_surface, file='system.xyz')
    failed_tests = 0
    call set_up()
    write(6,'(a)') "BEGIN TESTING"
    write(6,'(a)') "PERIODIC"
    write(6,'(a)', advance='no') "ij_cell_frm_dir(): "
    call test_ij_cell_from_dir(u_fail_out, failed_tests, passed)
    call write_pass_fail(passed)
    write(6,'(a)') "ISOLATED DIMERS"
    call output_surface(lattice, surface, kmc_info, unit_surface)
    call tear_down()

    write(6,'(a)', advance='no') "test LOCATE"
    call test_locate(passed)
    call write_pass_fail(passed)

    write(6,'(a)', advance='no') "test dimer isolation"
    call set_up()
    !Set Rate Setting appropriately for the tests
    reactions%active%ide_etch_on(1:5) = [.false., .true., .true., .true., .true.]
    reactions%ide_individual_active = .false.
    call test_dimer_isolation(u_fail_out, lattice, sites, kmc_info, unit_surface, failed_tests, passed)
    call write_pass_fail(passed)
    write(6,'(a)', advance='no') "test IDE rate setting"
    call test_ide_rate_setting(u_fail_out, lattice, sites, failed_tests, passed)
    call write_pass_fail(passed)
    call tear_down()

    write(6,'(a)', advance='no') "test IDE execution"
    call set_up()
    call test_ide_execution(u_fail_out, lattice, kmc_info, failed_tests, unit_surface, passed)
    call tear_down()
    if (passed) then
        write(6,*) "PASS"
    else
        write(6,*) "FAIL"
    end if

    write(6, '(a)') "C2H2 Pendant Migration"
    write(6, '(a)', advance='no') "    rate setting"
    call set_up()
    allocate(rates_set(2))
    call test_c2h2p_mig_rate_setting(u_fail_out, lattice, kmc_info, rates_set, unit_surface, passed)
    call write_pass_fail(passed)
    if (passed) then
        write(6, '(a)', advance='no') "    rate execution"
        call test_c2h2p_mig_execution(u_fail_out, lattice, kmc_info, rates_set, unit_surface, passed)
        call write_pass_fail(passed)
    end if
    deallocate(rates_set)
    call tear_down()

    write(6, '(a)') "C2H2 Pendant Dimer Formation"
    write(6, '(a)', advance='no') "    rate setting"
    call set_up()
    reactions%active%C2H2p_dimer = .true.
    reactions%dimer_row_isolation_range = 1
    reactions%single_c_prevent_iso = .false.
    call test_dimer_reformation_rate_set(u_fail_out, lattice, kmc_info, rates_set, unit_surface, passed, failed_tests)
    call write_pass_fail(passed)

    write(6, '(a)', advance='no') "    rate execution"
    if (passed .eqv. .true.) then
        !Only successful rate setting should proceed execution
        call test_dimer_reformation_execution(u_fail_out, lattice, kmc_info, rates_set, unit_surface, passed, failed_tests)
        call write_pass_fail(passed)
    end if

    deallocate(rates_set)
    call tear_down()

    write(6, '(a)', advance='no') "ide rate scaling"
    call test_ide_rate_scaling(u_fail_out, passed, failed_tests)
    call write_pass_fail(passed)


    call test_ide_perms()


    if (failed_tests /= 0) then
        STOP 1
    end if


contains

    !-------------------------------------------------------------------
    !Initialise the lattice and surface according to the similation type
    !-------------------------------------------------------------------
    subroutine set_up()

        call init_surface_type(surface, outputs%values_at_x_s%num_values)
        call initialise_lattice(simulation, lattice, surface)
        call init_segments(simulation, surface, segments, segment_bounds)
        call activate_surface(lattice, kmc_info, surface, segments, 10000.0_dp)

    end subroutine set_up

    subroutine tear_down()
        !Modules
        use finalise, only : deallocate_surface_instance

        call deallocate_surface_instance(surface)
        if (allocated(lattice)) deallocate(lattice)

        if (allocated(segments)) deallocate(segments)

    end subroutine tear_down


    subroutine write_pass_fail(passed)
        logical, intent(in) :: passed
        if (passed) then
            write(6,*) "PASS"
        else
            write(6,*) "FAIL"
        end if
    end subroutine write_pass_fail

end program test_main
