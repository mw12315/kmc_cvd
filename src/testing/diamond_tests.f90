module tests

    use kinds, only : dp


contains

    !-------------------------------------------------------------------------
    !Check that an initial position (i,j,ib) is correctly mapped to (ni,nj,nb)
    !by the ij_cell_from_dir method - using pbc
    !-------------------------------------------------------------------------
    subroutine test_ij_cell_from_dir(f_unit, failed_tests, passed)
        !Modules
        use variables, only : simulation
        use periodic, only : ij_cell_from_dir
        !Arguments
        integer, intent(in) :: f_unit
        integer, intent(inout) :: failed_tests
        logical, intent(out) :: passed
        !Variables
        integer, parameter :: ndirs = 9
        integer, parameter :: nloc = 5
        integer, parameter :: ntests = ndirs*nloc
        integer :: itest
        integer :: idir, iloc
        !Input Data
        integer, dimension(nloc*ndirs) :: i_vals, j_vals, d_vals
        integer, dimension(nloc*ndirs) :: expect_ni = -1, expect_nj = -1
        integer, dimension(nloc) :: locs_i, locs_j
        !Subroutine outputs
        integer :: ni, nj

        passed = .true.
        !Locations on Surface
        !Central, edgeL, edgeR, cornerTL, cornerBR
        locs_i = [simulation%Nx / 2,  &
                  1, simulation%Nx, &
                  1, simulation%Nx]
        locs_j = [simulation%Ny / 2, &
                  simulation%Ny / 2, simulation%Ny / 2, &
                  simulation%Ny, 1]

        itest = 0
        !Initialise input data
        do idir=0, ndirs-1
            do iloc=1,nloc
                itest = itest + 1
                i_vals(itest) = locs_i(iloc)
                j_vals(itest) = locs_j(iloc)
                d_vals(itest) = idir
            end do
        end do

        !Expected Values
        expect_ni = [locs_i(1),locs_i(2),locs_i(3),locs_i(4),locs_i(5), & !Dir 0
                     locs_i(1),locs_i(2),locs_i(3),locs_i(4),locs_i(5), & !Dir 1 (N)
                     locs_i(1)+1,locs_i(2)+1,1,locs_i(4)+1,1, & !Dir 2 (NE)
                     locs_i(1)+1,locs_i(2)+1,1,locs_i(4)+1,1, & !Dir 3 (E)
                     locs_i(1)+1,locs_i(2)+1,1,locs_i(4)+1,1, & !Dir 4 (SE)
                     locs_i(1),locs_i(2),locs_i(3),locs_i(4),locs_i(5), & !Dir 5 (S)
                     locs_i(1)-1,simulation%Nx,locs_i(3)-1,simulation%Nx,locs_i(5)-1, & !Dir 6 (SW)
                     locs_i(1)-1,simulation%Nx,locs_i(3)-1,simulation%Nx,locs_i(5)-1, & !Dir 7 (W)
                     locs_i(1)-1,simulation%Nx,locs_i(3)-1,simulation%Nx,locs_i(5)-1 & !Dir 8 (NW)
                    ]

        expect_nj = [locs_j(1),locs_j(2),locs_j(3),locs_j(4),locs_j(5), & !Dir 0
                     locs_j(1)+1,locs_j(2)+1,locs_j(3)+1,1,locs_j(5)+1, & !Dir 1 (N)
                     locs_j(1)+1,locs_j(2)+1,locs_j(3)+1,1,locs_j(5)+1, & !Dir 2 (NE)
                     locs_j(1),locs_j(2),locs_j(3),locs_j(4),locs_j(5), & !Dir 3 (E)
                     locs_j(1)-1,locs_j(2)-1,locs_j(3)-1,locs_j(4)-1,simulation%Ny, & !Dir 4 (SE)
                     locs_j(1)-1,locs_j(2)-1,locs_j(3)-1,locs_j(4)-1,simulation%Ny, & !Dir 5 (S)
                     locs_j(1)-1,locs_j(2)-1,locs_j(3)-1,locs_j(4)-1,simulation%Ny, & !Dir 6 (SW)
                     locs_j(1),locs_j(2),locs_j(3),locs_j(4),locs_j(5), & !Dir 7 (W)
                     locs_j(1)+1,locs_j(2)+1,locs_j(3)+1,1,locs_j(5)+1 & !Dir 8 (NW)
                    ]

        do itest=1, ntests
            call ij_cell_from_dir(i_vals(itest), j_vals(itest), d_vals(itest), &
                                  ni, nj)
            if (expect_ni(itest) /= ni .or. expect_nj(itest) /= nj) then
                passed = .false.
                call failed_int_test(itest, "ij_cell_from_dir", &
                                     [expect_ni(itest), expect_nj(itest)], &
                                     [ni, nj], &
                                     f_unit)
                failed_tests = failed_tests + 1
            end if
        end do

    end subroutine test_ij_cell_from_dir


    !------------------------------------------------------------------------
    !Check that a value within the rate sum vector (rsv) is correctly located
    !using the lcoate function
    !------------------------------------------------------------------------
    subroutine test_locate(passed)
        !Modules
        use rates, only : locate
        !Arguments
        logical, intent(out) :: passed
        !Variables
        real(dp) :: rsv(10)
        real(dp) :: ran(10), ran2
        integer :: chosen(10), expected(10)
        integer :: i, rsv_max = 20
        passed = .true.


        rsv = (/ (I, I = 2,rsv_max,2) /)
        ran = (/ (((I)/10.0)**2, I = 1,10) /)
        expected = [1,1,1,2,3,4,5,7,9,10]

        do i=1,size(rsv)
            ran2 = ran(i) * rsv_max
            call locate(rsv, ran2, chosen(i))
            if (chosen(i) /= expected(i)) then
                if (passed) write(*,'(a6,4(a8))') " ", 'i', 'ran2', 'k', 'expect'
                write(*,'(a6, i8, f8.1, 2(i8))') "FAIL: ", i, ran2, chosen(i), expected(i)
                passed = .false.
            end if

        end do


    end subroutine test_locate

    !-------------------------------------------------------------------
    !Test the detection of dimer reconstructions isolated on the surface
    !-------------------------------------------------------------------
    subroutine test_dimer_isolation(fail_unit, atoms, sites, kmc_info, surface_unit, failed_tests, passed)
        !Modules
        use types, only : atom_type_ext, surface_type, sim_type, kmc_type, atom_type
        use variables, only : read_atom, surface
        use test_construct, only : add_isolated_dimers
        use output, only : output_surface
        use dimer, only : isolated_dimer
        use input, only : reactions
        use assert_m, only : assert
        !Arguments
        integer, intent(in) :: fail_unit
        type(atom_type_ext), intent(inout) :: atoms(:)
        type(atom_type), allocatable, intent(inout) :: sites(:)
        type(kmc_type), intent(inout) :: kmc_info
        integer, intent(in) :: surface_unit
        integer, intent(inout) :: failed_tests
        logical, intent(out) :: passed
        !Variables
        integer :: itest, isite, i
        logical :: expected_isolated_dimers(4,7)
        integer :: range(4)
        logical :: isoc(4)
        character(10) :: site_names(7)
        character(30) :: message
        logical, allocatable :: passes(:)
        logical :: yet_to_print = .false.
        passed = .true.

        write(fail_unit,'(a)') 'test_dimer_isolation'

        call add_isolated_dimers(atoms, kmc_info, sites)
        call output_surface(atoms, surface, kmc_info, surface_unit)
        !Dimer, Dimer + void + row, Dimer + void + row carbon, Dimer + void + 2 row carbons
        !Dimer + row carbon, Dimer + row dimer
        !Range: 1,2
        !isoc T
        !isoc F
        !4 combinations
        site_names = ["D   ", "DVD ", "DVC ", "DVCC", "DC  ", "DCC ", "DD  "]

        !Create option combinations
        range =     [1,1,2,2]
        isoc =      [.true.,.false.,.true.,.false.]
        ! any carbon, 2 carbons

        expected_isolated_dimers = reshape(&
            [.true.,.true.,.true.,.true., &
             .true.,.true.,.false.,.false., &
             .true.,.true.,.false.,.true., &
             .true.,.true.,.false.,.false., &
             .false.,.true.,.false.,.true., &
             .false.,.false.,.false.,.false., &
             .false.,.false.,.false.,.false.],&
             shape=[4,7], order=[1,2])

        allocate(passes(size(expected_isolated_dimers)))
        passes = .true.
        i = 0
        do isite=1, size(sites)
            do itest=1, size(range)
                i = i + 1
                write(message, '(i2,a2,i1,a1,i1,a2,a5,i1,l1,l1)') i," (",isite,",",itest,") ", site_names(isite),range(itest), isoc(itest)
                reactions%dimer_row_isolation_range = range(itest)
                reactions%single_c_prevent_iso = isoc(itest)
                passes(i) = assert(isolated_dimer(sites(isite), atoms), expected_isolated_dimers(itest, isite), &
                message, fail_unit, not_yet_printed=yet_to_print)
            end do
        end do

        passed = all(passes .eqv. .true.)

        if (passed) then
            write(fail_unit,'(a)') ' PASS'
        else
            write(fail_unit,'(a)') ' FAIL'
            failed_tests = failed_tests + 1
        end if

        deallocate(passes)

    end subroutine test_dimer_isolation

    !---------------------------------------------------------------------
    !Test that rate setting of isolated dimer etching reactions is correct
    !for a range of different dimer positions
    !---------------------------------------------------------------------
    subroutine test_ide_rate_setting(log_unit, atoms, sites, failed_tests, passed)
        !Modules
        use types, only : atom_type_ext, rate_type, atom_type, same_atom
        use variables, only : ilattice, atom_pair
        use etching, only : set_ide_rate
        use assert_m, only : assert
        use input, only : reactions
        !Arguments
        integer, intent(in) :: log_unit
        type(atom_type_ext), intent(in) :: atoms(:)
        type(atom_type), allocatable, intent(in) :: sites(:)
        integer, intent(inout) :: failed_tests
        logical, intent(out) :: passed
        !Variables
        logical, allocatable :: passes(:)
        type(rate_type), allocatable :: rate_list(:)
        integer :: isite, itest, ilr
        integer, allocatable :: expected_rates_set(:) !Per atom site rates set
        integer :: range(4)
        logical :: isoc(4)
        logical :: yet_to_print
        character(30) :: test_message
        character(4) :: site_names(7)
        write(log_unit, '(a)', advance='no') 'test_ide_rate_setting'
        passed = .true.
        yet_to_print = .true.
        !Allocate rate_list
        allocate(rate_list(size(sites)*2))
        allocate(passes(size(sites)))
        allocate(expected_rates_set(size(sites)))

        reactions%active%ide_etch_on = .false.
        reactions%active%ide_etch_on(1) = .true.
        reactions%ide_individual_active = .true.

        site_names = ["D   ", "DVD ", "DVC ", "DVCC", "DC  ", "DCC ", "DD  "]
        !Create option combinations
        !Create option combinations
        range =     [1,1,2,2]
        isoc =      [.true.,.false.,.true.,.false.]
        ! range(1): any carbon, 2 carbons
        ! range(2): any carbon, 2 carbons
        expected_rates_set(:) = [4, 2, 3, 2, 2, 0, 0]
        do isite=1, size(sites)
            rate_list(:) = rate_type()
            ilr = 0
            do itest=1, size(range)
                reactions%dimer_row_isolation_range = range(itest)
                reactions%single_c_prevent_iso = isoc(itest)
                call set_ide_rate(atom_pair(sites(isite), atoms), atoms, rate_list, ilr)
            end do
            write(test_message, '(a5,i2,L1,L1)') site_names(isite), &
                    reactions%dimer_row_isolation_range, &
                    reactions%single_c_prevent_iso
            passes(isite) = assert(ilr, expected_rates_set(isite), test_message, &
                    log_unit, not_yet_printed=yet_to_print)
        end do

        passed = all(passes .eqv. .true.)

        if (passed) then
            write(log_unit,'(a)') " PASS"
        else
            write(log_unit,'(a)') " FAIL"
            failed_tests = failed_tests + 1
        end if


    end subroutine test_ide_rate_setting

    !Test execution of isolated dimer etching
    subroutine test_ide_execution(log_unit, atoms, kmc_info, failed_tests, u_surface, passed)
        !Modules
        use types, only : atom_type, atom_type_ext, kmc_type, rate_type
        use enumerate, only : tRates, not_activated, actstate_min
        use test_construct, only : add_dimers_for_etching
        use variables, only : surface, read_atom, ilattice, read_dimer, read_act, atom_pair, linked_atom
        use periodic, only : get_neighbour_atoms, get_site_from_N_path
        use output, only : output_surface
        use etching, only : execute_ide_etch
        use assert_m, only : assert
        use pendant, only : is_pendant
        use input, only : reactions
        !Arguments
        integer, intent(in) :: log_unit
        type(atom_type_ext), intent(inout) :: atoms(:)
        type(kmc_type), intent(inout) :: kmc_info
        integer, intent(inout) :: failed_tests
        integer, intent(in) :: u_surface
        logical, intent(out) :: passed
        !Variables
        type(atom_type_ext), allocatable :: atoms2(:) !Local copy of atoms
        integer :: i
        type(atom_type) :: N1_2,N1_3, N2_2, N2_3!Neighbours 2 and 3 of atom1 and atom2
        type(atom_type) :: tmp1, tmp4
        type(atom_type) :: sites(5), pairs(5) !Pairs of atoms making up isolated dimers
        integer :: exp_states(5,2), exp_act(5,2)
        type(rate_type) :: rates(5)
        integer :: events(5)
        logical :: passed1, passed2, passed3, passed4, passed5
        logical :: passes(20) = .true.
        logical :: yet_to_print_to_log
        character(20) :: message

        write(log_unit, '(a)') 'test_ide_execution (single)'
        if (tRates%ide_12_2 /= 28) then !Ensure changes to ide_12_2 index are caught
            !SWITCH CASE BELOW NEEDS CHANGING
            write(6,*) "ide_etch_2 is not as event index 28 as expected!", tRates%ide_12_2
            STOP 2
        end if
        passed = .true.
        yet_to_print_to_log = .true.

        events(:) = (/ tRates%ide_12_1, tRates%ide_12_2, tRates%ide_12_3, tRates%ide_12_4,  tRates%ide_12_5 /)

        !Set up
        call add_dimers_for_etching(atoms, kmc_info, sites)
        call output_surface(atoms, surface, kmc_info, u_surface)
        allocate(atoms2(size(atoms)))
        atoms2(:) = atoms(:)

        !Copy initial states to expected pair list
        do i=1, size(sites)
            pairs(i) = atom_pair(sites(i), atoms)
        end do

        !IDE-1 followed by IDE 2,3,4,5
        !IDE_12_1
        write(log_unit, '(a)', advance='no') '    ide_12_1'
        do i=1, size(sites,1)
            !Pair is moved to being a acetyl carbon above sites(i)
            rates(i) = rate_type(site=pairs(i), site2=sites(i), &
                                 rate_val=0.0_dp, event=tRates%ide_12_1)
            call execute_ide_etch(rates(i), kmc_info, atoms)
            pairs(i) = linked_atom(sites(i), atoms) !Keep pair(i) pointing at a valid atom
        end do
        call output_surface(atoms, surface, kmc_info, u_surface)

        passed1 = assert((is_pendant(sites(1),atoms).and.is_pendant(pairs(1),atoms)), .true., &
        "IDE_12_1 created pendant", log_unit, not_yet_printed=yet_to_print_to_log)
        if (passed1 .eqv. .false.) then
            failed_tests = failed_tests + 1
            write(log_unit, '(a)') " FAIL"
        else
            write(log_unit, '(a)') " PASS"
        end if

        !Apply IDE_12_n (n=2,5)
        do i=2, size(sites)
            rates(i) = rate_type(site=pairs(i), site2=sites(i), &
                    rate_val=0.0_dp, event=events(i))
            call execute_ide_etch(rates(i), kmc_info, atoms)
        end do
        call output_surface(atoms, surface, kmc_info, u_surface)

        !Confirm 12_n correctly enacted
        passes = .true.
        passed2 = .true.
        yet_to_print_to_log = .true.
        exp_states(:,:) = reshape((/ 1,2, 1,0, 0,0, 0,0, 0,0 /), shape=(/ 5,2 /), order=(/ 2,1 /))
        exp_act(:,:) = reshape((/ 0,0, 1,0, 0,0, 0,0, 0,0 /), shape=(/ 5,2 /), order=(/ 2,1 /))

        write(log_unit, '(a)', advance='no') '    ide_12_n states,activation'
        do i=1, size(sites,1)
            write(message, '(i2,a)') i," a1 state"
            passes(4*i-3) = assert(read_atom(sites(i),atoms), exp_states(i,1), message, log_unit, &
                    not_yet_printed=yet_to_print_to_log)
            write(message, '(i2,a)') i," a2 state"
            passes(4*i-2) = assert(read_atom(pairs(i),atoms), exp_states(i,2), message, log_unit, &
                    not_yet_printed=yet_to_print_to_log)
            write(message, '(i2,a)') i," a1 act"
            passes(4*i-1) = assert(read_act(sites(i),atoms), exp_act(i,1), message, log_unit, &
                    not_yet_printed=yet_to_print_to_log)
            write(message, '(i2,a)') i," a2 act"
            passes(4*i) = assert(read_act(pairs(i),atoms), exp_act(i,2), message, log_unit, &
                    not_yet_printed=yet_to_print_to_log)
        end do
        passed2 = all(passes .eqv. .true.)

        if (passed2 .eqv. .false.) then
            failed_tests = failed_tests + 1
            write(log_unit, '(a)') " FAIL"
        else
            write(log_unit, '(a)') " PASS"
        end if

        !Check surface morphology of atoms left behind
        passed3 = .true.
        write(log_unit, '(a)', advance='no') '    ide_12_n surface structure'
        do i=1, size(rates)
            write(message, '(a,i1)') "ide_12_", i
            message = trim(message)
            call get_neighbour_atoms(sites(i), tmp1, N1_2, N1_3, tmp4)
            N2_2 = get_site_from_N_path(sites(i),[1,2,2])
            N2_3 = get_site_from_N_path(sites(i),[1,2,3])
            passes = .true.
            select case(events(i))
            case(27)
                !(C2H2 pendent)
                passes(1) = assert(read_dimer(N2_2, atoms), .true., &
                        message//" C2H2p dimer ", &
                        log_unit, not_yet_printed=yet_to_print_to_log)
            case(28)
                !(bridged CHx)
                passes(1) = assert(read_dimer(N1_2, atoms) .and. read_dimer(N1_3, atoms), .false., message//" A1_dimer ", &
                        log_unit, not_yet_printed=yet_to_print_to_log)
            case(29)
                !(no dimer, single radical)
                passes(1) = assert(read_dimer(N1_2, atoms) .or. read_dimer(N1_3, atoms), .false., &
                        message//" A1_dimer ", &
                        log_unit, not_yet_printed=yet_to_print_to_log)
                passes(3) = assert(read_act(N1_3, atoms),actstate_min, message//" act", &
                        log_unit, not_yet_printed=yet_to_print_to_log)
            case(30)
                !(dimer, single radical)
                passes(1) = assert(read_dimer(N1_2, atoms) .and. read_dimer(N1_3, atoms), .true., message//" A1_dimer ", &
                        log_unit, not_yet_printed=yet_to_print_to_log)
                passes(3) = assert(read_act(N1_3, atoms), actstate_min, message//" act", &
                        log_unit, not_yet_printed=yet_to_print_to_log)
            case(31)
                !(no dimer, single radical)
                passes(1) = assert(read_dimer(N1_2, atoms) .or. read_dimer(N1_3, atoms), .false., &
                        message//" A1_dimer ", &
                        log_unit, not_yet_printed=yet_to_print_to_log)
                passes(3) = assert(read_act(N1_3, atoms),actstate_min, message//" act", &
                        log_unit, not_yet_printed=yet_to_print_to_log)
            end select
            if (ANY(passes .eqv. .false.)) passed3 = .false.
        end do

        if (.not. passed3) then
            failed_tests = failed_tests + 1
            write(log_unit, '(a)') " FAIL"
        else
            write(log_unit, '(a)') " PASS"
        end if


        !==============================
        !Combined IDE_N = ide_1 + ide_n
        !==============================
        write(log_unit, '(a)') 'test_ide_execution (combined)'
        reactions%ide_individual_active = .false.
        call output_surface(atoms2, surface, kmc_info, u_surface)
        do i=1, size(sites)
            pairs(i) = atom_pair(sites(i), atoms2)
        end do
        !Apply IDE_12_n (n=2,5)
        do i=2, size(sites)
            rates(i) = rate_type(site=pairs(i), site2=sites(i), &
                    rate_val=0.0_dp, event=events(i))
            call execute_ide_etch(rates(i), kmc_info, atoms2)
        end do
        call output_surface(atoms2, surface, kmc_info, u_surface)

        !Confirm 12_n correctly enacted
        passes = .true.
        passed4 = .true.
        yet_to_print_to_log = .true.
        exp_states(:,:) = reshape((/ 1,2, 1,0, 0,0, 0,0, 0,0 /), shape=(/ 5,2 /), order=(/ 2,1 /))
        exp_act(:,:) = reshape((/ 0,0, 1,0, 0,0, 0,0, 0,0 /), shape=(/ 5,2 /), order=(/ 2,1 /))

        write(log_unit, '(a)', advance='no') '    ide_12_n states,activation'
        do i=2, size(sites,1)
            write(message, '(i2,a)') i," a1 state"
            passes(4*i-3) = assert(read_atom(sites(i),atoms2), exp_states(i,1), message, log_unit, &
                    not_yet_printed=yet_to_print_to_log)
            write(message, '(i2,a)') i," a2 state"
            passes(4*i-2) = assert(read_atom(pairs(i),atoms2), exp_states(i,2), message, log_unit, &
                    not_yet_printed=yet_to_print_to_log)
            write(message, '(i2,a)') i," a1 act"
            passes(4*i-1) = assert(read_act(sites(i),atoms2), exp_act(i,1), message, log_unit, &
                    not_yet_printed=yet_to_print_to_log)
            write(message, '(i2,a)') i," a2 act"
            passes(4*i) = assert(read_act(pairs(i),atoms2), exp_act(i,2), message, log_unit, &
                    not_yet_printed=yet_to_print_to_log)
        end do
        passed4 = all(passes .eqv. .true.)

        if (passed4 .eqv. .false.) then
            write(log_unit, '(a)') " FAIL"
            failed_tests = failed_tests + 1
        else
            write(log_unit, '(a)') " PASS"
        end if

        !Check surface morphology of atoms left behind (and their activation)
        passed5 = .true.
        write(log_unit, '(a)', advance='no') '    ide_12_n surface structure'
        do i=2, size(rates)
            write(message, '(a,i1)') "ide_12_", i
            message = trim(message)
            call get_neighbour_atoms(sites(i), tmp1, N1_2, N1_3, tmp4)
            N2_2 = get_site_from_N_path(sites(i),[2])
            N2_3 = get_site_from_N_path(sites(i),[3])
            passes = .true.
            select case(events(i))
            case(27)
                !(C2H2 pendent)
                passes(1) = assert(read_dimer(N2_2, atoms2), .true., &
                        message//" C2H2p dimer ", &
                        log_unit, not_yet_printed=yet_to_print_to_log)
            case(28)
                !(bridged CHx)
                passes(1) = assert(read_dimer(N1_2, atoms2) .and. read_dimer(N1_3, atoms2), .false., message//" A1_dimer ", &
                        log_unit, not_yet_printed=yet_to_print_to_log)
            case(29)
                !(no dimer, single radical)
                passes(1) = assert(read_dimer(N1_2, atoms2) .or. read_dimer(N1_3, atoms2), .false., &
                        message//" A1_dimer ", &
                        log_unit, not_yet_printed=yet_to_print_to_log)
                passes(2) = assert(read_act(N1_3, atoms2),actstate_min, message//" act", &
                        log_unit, not_yet_printed=yet_to_print_to_log)
            case(30)
                !(dimer, single radical)
                passes(1) = assert(read_dimer(N1_2, atoms2) .and. read_dimer(N1_3, atoms2), .true., message//" A1_dimer ", &
                        log_unit, not_yet_printed=yet_to_print_to_log)
                passes(2) = assert(read_act(N1_3, atoms2), actstate_min, message//" act", &
                        log_unit, not_yet_printed=yet_to_print_to_log)
            case(31)
                !(no dimer, single radical)
                passes(1) = assert(read_dimer(N1_2, atoms2) .or. read_dimer(N1_3, atoms2), .false., &
                        message//" A1_dimer ", &
                        log_unit, not_yet_printed=yet_to_print_to_log)
                passes(2) = assert(read_act(N1_3, atoms2),actstate_min, message//" act", &
                        log_unit, not_yet_printed=yet_to_print_to_log)
            end select
            if (ANY(passes .eqv. .false.)) passed5 = .false.
        end do

        if (.not. passed5) then
            failed_tests = failed_tests + 1
            write(log_unit, '(a)') " FAIL"
        else
            write(log_unit, '(a)') " PASS"
        end if

        passed = passed1 .and. passed2 .and. passed3 .and. passed4 .and. passed5

    end subroutine test_ide_execution


    !--------------------------------------------
    !Printing of a failed test involving integers
    !--------------------------------------------
    subroutine failed_int_test(ntest, ndescr, expected, got, file_unit)
        !Arguments
        integer, intent(in) :: ntest
        character(*), intent(in) :: ndescr
        integer, dimension(:), intent(in) :: expected, got
        integer, intent(in) :: file_unit
        !Variables
        logical :: is_open

        inquire(unit=file_unit, opened=is_open)

        if (.not. is_open) then
            open(unit=file_unit, file='failed_tests.txt')
            write(file_unit,*) "itest ", "descr ", "expected ", "actual "
        end if

        write(6,*) ntest, trim(ndescr), expected, got

    end subroutine failed_int_test

    subroutine test_c2h2p_mig_rate_setting(log_unit, atoms, kmc_info, rate_list, u_surface, passed)
        !Modules
        use types, only : atom_type, atom_type_ext, kmc_type, rate_type
        use enumerate, only : tRates, not_activated, actstate_min
        use test_construct, only : create_c2h2p_migrate_env
        use variables, only : surface, read_atom, ilattice, read_dimer, read_act
        use periodic, only : get_neighbour_atoms
        use output, only : output_surface
        use c2h2, only : set_C2H2p_gap_migrate
        use derived_values, only : kC2H2p_mig
        use assert_m, only : assert
        use input, only : reactions
        !Arguments
        integer, intent(in) :: log_unit
        type(atom_type_ext), intent(inout) :: atoms(:)
        type(kmc_type), intent(inout) :: kmc_info
        integer, intent(in) :: u_surface
        logical, intent(out) :: passed
        type(rate_type), intent(inout) :: rate_list(:)
        !Variables
        type(atom_type) :: site !Site to be passed to rate setting
        integer :: last_rate_set = 0
        logical :: passed_test(3)
        logical :: yet_to_print
        passed_test = .true.
        kC2H2p_mig(2) = 10.0_dp
        yet_to_print = .true. !Haven't printed a assert result to file


        write(log_unit, '(a)', advance='no') "C2H2 Pendant Migration Rate Setting"
        call create_c2h2p_migrate_env(atoms, kmc_info, site)
        call output_surface(atoms, surface, kmc_info, u_surface)

        reactions%active%C2H2p_mig = .true.

        call set_C2H2p_gap_migrate(site, atoms, rate_list, last_rate_set)

        passed_test(1) = assert(last_rate_set, 2, "C2H2p gmig rates set", log_unit)
        passed_test(2) = assert(rate_list(1)%event, tRates%gapMigrateC2H2_0, "C2H2p rate_list(1)", log_unit, &
            not_yet_printed=yet_to_print)
        passed_test(3) = assert(rate_list(2)%event, tRates%gapMigrateC2H2_1, "C2H2p rate_list(2)", log_unit, &
                not_yet_printed=yet_to_print)

        passed = all(passed_test)

        if (passed) then
            write(log_unit, '(a)') " PASS"
        else
            write(log_unit, '(a)') " FAIL"
        end if

    end subroutine test_c2h2p_mig_rate_setting


    subroutine test_c2h2p_mig_execution(log_unit, atoms, kmc_info, rate_list, u_surface, passed)
        !Modules
        use types, only : atom_type, atom_type_ext, kmc_type, rate_type
        use enumerate, only : types_of_atom, not_activated, actstate_min
        use variables, only : surface, read_atom, read_linked_atom, read_ilink, &
                ilattice, read_dimer, read_act, are_linked, linked_atom
        use periodic, only : ret_neighbour
        use output, only : output_surface
        use migration, only : execute_migration
        use assert_m, only : assert
        !Arguments
        integer, intent(in) :: log_unit
        type(atom_type_ext), intent(inout) :: atoms(:)
        type(kmc_type), intent(inout) :: kmc_info
        type(rate_type), intent(in) :: rate_list(:)
        integer, intent(in) :: u_surface
        logical, intent(out) :: passed
        !Variables
        logical :: single_passes(10)
        type(atom_type_ext), allocatable :: atoms_cp1(:), atoms_cp2(:)
        type(atom_type) :: linked_carbon
        logical :: yet_to_print
        allocate(atoms_cp1(size(atoms,1)))
        allocate(atoms_cp2(size(atoms,1)))
        atoms_cp1(:) = atoms
        atoms_cp2(:) = atoms   !Hold copies of atom array so that the same atom can be migrated twice
        single_passes = .true.
        yet_to_print = .true. !Haven't printed a assert result to file

        write(log_unit, '(a)', advance='no') "C2H2 Pendant Migration Execution"

        if (size(rate_list) /= 2) then
            write(6,*) "Invalid number of rates passed via rate_list argument"
            passed = .false.
            return
        end if

        linked_carbon = linked_atom(rate_list(1)%site, atoms)
        call execute_migration(rate_list(1), kmc_info, atoms_cp1)
        single_passes(1) = assert(read_atom(rate_list(1)%site, atoms_cp1), types_of_atom%empty_site, &
                "1: site1(1) empty", log_unit, not_yet_printed=yet_to_print)
        single_passes(2) = assert(read_atom(linked_carbon, atoms_cp1), types_of_atom%empty_site, &
                "1: site1(2) empty", log_unit, not_yet_printed=yet_to_print)
        single_passes(3) = assert(read_dimer(ret_neighbour(rate_list(1)%site,2), atoms_cp1), .true., &
                "1: dimer reformed", log_unit, not_yet_printed=yet_to_print)
        single_passes(4) = assert(read_atom(rate_list(1)%site2, atoms_cp1), types_of_atom%surf_carb, &
                "1: site2: carbon", log_unit, not_yet_printed=yet_to_print)
        single_passes(5) = assert(read_linked_atom(rate_list(1)%site2, atoms_cp1), types_of_atom%acetyl_c2, &
                "1: site2: acetyl_c2", log_unit, not_yet_printed=yet_to_print)

        call output_surface(atoms_cp1, surface, kmc_info, u_surface)


        call output_surface(atoms_cp2, surface, kmc_info, u_surface)
        call execute_migration(rate_list(2), kmc_info, atoms_cp2)
        single_passes(6) = assert(read_atom(rate_list(2)%site, atoms_cp2), types_of_atom%empty_site, &
                "1: site1(1) empty", log_unit, not_yet_printed=yet_to_print)
        single_passes(7) = assert(read_atom(linked_carbon, atoms_cp2), types_of_atom%empty_site, &
                "1: site1(2) empty", log_unit, not_yet_printed=yet_to_print)
        single_passes(8) = assert(read_dimer(ret_neighbour(rate_list(2)%site,2), atoms_cp2), .true., &
                "1: dimer reformed", log_unit, not_yet_printed=yet_to_print)
        single_passes(9) = assert(read_atom(rate_list(2)%site2, atoms_cp2), types_of_atom%surf_carb, &
                "1: site2: carbon", log_unit, not_yet_printed=yet_to_print)
        single_passes(10) = assert(read_linked_atom(rate_list(2)%site2, atoms_cp2), types_of_atom%acetyl_c2, &
                "1: site2: acetyle_c2", log_unit, not_yet_printed=yet_to_print)

        call output_surface(atoms_cp2, surface, kmc_info, u_surface)

        passed = all(single_passes)

        if (passed) then
            write(log_unit, '(a)') " PASS"
        else
            write(log_unit, '(a)') " FAIL"
        end if

    end subroutine test_c2h2p_mig_execution


    subroutine test_dimer_reformation_rate_set(log_unit, atoms, kmc_info, rate_list, u_surface, passed, failed_tests)
        !Modules
        use types, only : atom_type, atom_type_ext, kmc_type, rate_type
        use enumerate, only : not_activated, actstate_min
        use variables, only : surface, read_atom, read_linked_atom, read_ilink, &
                ilattice, read_dimer, read_act, are_linked
        use periodic, only : ret_neighbour
        use output, only : output_surface
        use test_construct, only : create_c2h2p_dimer_env
        use assert_m, only : assert
        use ide, only : set_dimer_reformation_rate
        !Arguments
        integer, intent(in) :: log_unit
        type(atom_type_ext), intent(inout) :: atoms(:)
        type(kmc_type), intent(inout) :: kmc_info
        type(rate_type), allocatable, intent(inout) :: rate_list(:)
        integer, intent(in) :: u_surface
        logical, intent(out) :: passed
        integer, intent(inout) :: failed_tests
        !Variables
        type(atom_type), allocatable :: sites(:)
        integer :: ilr
        integer :: isite
        logical, allocatable :: passes(:)
        integer, allocatable :: exp_ilr(:)
        type(rate_type), allocatable :: tmp_rate_list(:)
        logical :: yet_to_print = .true.
        passed = .false.

        !Create a surface environment to test rate setting
        call create_c2h2p_dimer_env(atoms, kmc_info, sites)
        call output_surface(atoms, surface, kmc_info, u_surface)
        allocate(passes(size(sites)))
        allocate(exp_ilr(size(sites)))
        allocate(tmp_rate_list(5*size(sites)))
        exp_ilr = 1
        passes = .true.

        ilr = 0
        do isite=1, size(sites)
            call set_dimer_reformation_rate(sites(isite), atoms, tmp_rate_list, ilr)
            passes(isite) = assert(ilr, exp_ilr(isite), "c2h2p_dimer ilr", log_unit, &
                    not_yet_printed=yet_to_print)
        end do

        !Return rates set for execution testing
        allocate(rate_list(ilr))
        rate_list(:) = tmp_rate_list(1:ilr)

        passed = all(passes .eqv. .true.)

        if (passed .eqv. .false.) then
            failed_tests = failed_tests + 1
        end if

    end subroutine test_dimer_reformation_rate_set


    subroutine test_dimer_reformation_execution(log_unit, atoms, kmc_info, rate_list, u_surface, passed, failed_tests)
        !Modules
        use types, only : atom_type, atom_type_ext, kmc_type, rate_type
        use enumerate, only : not_activated, actstate_min
        use variables, only : surface, read_atom, read_linked_atom, read_ilink, &
                ilattice, read_dimer, read_act, are_linked
        use periodic, only : ret_neighbour
        use output, only : output_surface
        use execute_rates, only : execute_event
        use assert_m, only : assert
        !Arguments
        integer, intent(in) :: log_unit
        type(atom_type_ext), intent(inout) :: atoms(:)
        type(kmc_type), intent(inout) :: kmc_info
        type(rate_type), allocatable, intent(in) :: rate_list(:)
        integer, intent(in) :: u_surface
        logical, intent(out) :: passed
        integer, intent(inout) :: failed_tests
        !Variables

        passed = .true.
        if (.not. allocated(rate_list)) then
            write(log_unit, *) "rate_list not allocated"
            passed = .false.
            return
        end if

        call execute_event(rate_list(1), atoms, kmc_info, .false.)
        call output_surface(atoms, surface, kmc_info, u_surface)



        if (passed .eqv. .false.) then
            failed_tests = failed_tests + 1
        end if

    end subroutine test_dimer_reformation_execution

    !Test the scaling of ide rates according to thise available
    subroutine test_ide_rate_scaling(log_unit, passed, failed_tests)
        use ide, only : scale_kide, ide_k_sum_sum_all, ide_factors
        use assert_m, only : assert
        !Arguments
        integer, intent(in) :: log_unit
        logical, intent(out) :: passed
        integer, intent(inout) :: failed_tests
        !Variables
        real(dp) :: k(5) = [100.0, 10.0, 20.0, 30.0, 40.0]
        logical :: avail_rates(4)
        real(dp) :: scaled_k(4)
        real(dp) :: exp_scaled_k(4)
        integer :: i
        logical :: single_pass(5)
        character(50) :: message = "", i_txt
        single_pass = .true.
        !All available, single step (dimer mode = .true.)
        message = "all k, dimer, scaled_k "
        avail_rates = .true.
        exp_scaled_k = [5.0, 1E1, 1.5E1, 2.0E1]
        scaled_k = scale_kide(avail_rates, k, .true.)

        do i=1, size(scaled_k)
            write(i_txt, '(i1)') i
            single_pass(i) = assert(scaled_k(i), exp_scaled_k(i), message // i_txt, message_unit=log_unit)
        end do
        if (any(single_pass .eqv. .false.)) then
            passed = .false.
            failed_tests = failed_tests + 1
        end if

        !2/4 available, single step (dimer mode = .true.)
        single_pass = .true.
        message = "2/4 k, dimer, scaled_k "
        avail_rates = [.true., .true., .false., .false.]
        exp_scaled_k = [7.69, 1.54E1, 0.0, 0.0]
        scaled_k = scale_kide(avail_rates, k, .true.)

        do i=1, size(scaled_k)
            write(i_txt, '(i1)') i
            single_pass(i) = assert(scaled_k(i), exp_scaled_k(i), message // i_txt, log_unit)
        end do
        if (any(single_pass .eqv. .false.)) then
            passed = .false.
            failed_tests = failed_tests + 1
        end if


        !All available, single step (dimer mode = .false.)
        single_pass = .true.
        message = "all k, pendant, scaled_k "
        avail_rates = .true.
        exp_scaled_k = [1E1, 2E1, 3E1, 4E1]
        scaled_k = scale_kide(avail_rates, k, .false.)

        do i=1, size(scaled_k)
            write(i_txt, '(i1)') i
            single_pass(i) = assert(scaled_k(i), exp_scaled_k(i), message // i_txt, log_unit)
        end do
        if (any(single_pass .eqv. .false.)) then
            passed = .false.
            failed_tests = failed_tests + 1
        end if


    end subroutine test_ide_rate_scaling


    subroutine test_ide_perms()
!        use ide, only : ide_permutations, i_kide
!        use derived_values, only : kide_perms

!        integer :: i, ik

!        do i=1, size(ide_permutations, 1)
!            ik = i_kide(ide_permutations(i,:))
!            write(6,'(4(L3), i3, 4(f8.1))') ide_permutations(i,:), ik, kide_perms_p(ik,:)
!        end do


    end subroutine test_ide_perms


end module tests
