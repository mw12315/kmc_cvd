module adsorb_atom

    use kinds,  only : dp
    use input, only : reactions
    use enumerate, only : tRates

    implicit none

    logical, dimension(4) :: chx_species

contains

    !--------------------------------------------
    !Set rate for inserting ch3 radical into site
    !Dimer site or Trough Site
    !   X   X
    !  C-C    C
    ! /  \  C   C
    !--------------------------------------------
    subroutine set_chx_insert_rate(site, atoms, rate_list, ilr)
        !Modules
        use types, only : atom_type_ext, atom_type, rate_type
        use periodic, only : ret_neighbour, count_nearest_atoms, get_site_from_N_path
        use variables, only : read_atom, set_rate, is_act
        use enumerate, only : dimer_site, trough_site_2, types_of_atom, &
                irate_trough_ins, irate_dimer_ins
        use  surface_m, only : detect_site_env, C2H2_pendant
        use derived_values, only : rDimerInsertCHX, rTroughInsertCHX
        use input, only : reactions, dimerf
        use dimer, only : saturated_dimer
        !Arguments
        type(atom_type), intent(in) :: site
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        type(rate_type), dimension(:), intent(inout)  :: rate_list
        integer, intent(inout) :: ilr
        !Variables
        integer         :: NN                   !Count of sites nearest neighbours
        integer         :: site_env             !Local environment of the site
        type(atom_type) :: N2, N3               !Neighbours of site
        logical :: biradical, hindered          !Site is above a biradical, steric hinderance due to dimer formation (or lack of incorporated atoms)
        integer :: ispecies
        logical :: is_saturated_dimer
        real(dp) :: rate_scale
        !Initialisation
        biradical = .false.
        hindered = .false.
        is_saturated_dimer = .false.
        rate_scale = 1.0_dp

        site_env = detect_site_env(site, atoms) ![Dimer, Trough0, Trough1, Trough2]
        if (site_env == dimer_site) then
            if (.not. reactions%active%InsertDimerCHX_on) return
        else
            if (.not. reactions%active%InsertTroughCHX_on) return
        end if
        call count_nearest_atoms(site, atoms,  NN)
        N2 = ret_neighbour(site, 2)
        N3 = ret_neighbour(site, 3)

        !Ensure no hindering pendants nearby
        if (NN > 2) return
        !Check no pendant carbon from C2H2 hindering absorption site
        if (C2H2_pendant(site, atoms)) return
        !N2 and N3 must exist
        if (read_atom(N2, atoms) == types_of_atom%empty_site) return
        if (read_atom(N3, atoms) == types_of_atom%empty_site) return

        if (site_env == dimer_site) then
            if (dimerf%incorp_CHx_sat_dimer) then
                is_saturated_dimer = saturated_dimer(N2, atoms)
            end if
        end if

        !Either of support atoms must be activated (or saturated_dimer present and active)
        if ((.not. is_act(N2, atoms) .and. .not. is_act(N3, atoms)) .and. .not. is_saturated_dimer) then
            return
        end if

        if (site_env == dimer_site) then
            if (is_saturated_dimer) rate_scale = dimerf%sat_dimer_incorp_scale
            do ispecies=1,size(chx_species)
                if (chx_species(ispecies)) then
                    call set_rate(site, irate_dimer_ins(ispecies), rDimerInsertCHX(ispecies) * rate_scale, &
                            rate_list, ilr)
                end if
            end do
        else !Trough Site
            if (can_trough_incorp(site, atoms)) then
                do ispecies=1,size(chx_species)
                    if (chx_species(ispecies)) then
                        call set_rate(site, irate_trough_ins(ispecies), rTroughInsertCHX(ispecies), &
                                rate_list, ilr)
                    end if
                end do
            end if
        end if

    end subroutine set_chx_insert_rate

    !--------------------------------------------------
    !Can incorporation into a trough site occur given
    !its local environment
    !ASSUME AT LEAST 1 OF ITS NEIGHBOURS IS ACTIVATED
    !--------------------------------------------------
    logical function can_trough_incorp(site, atoms)
        !Modules
        use types, only : atom_type_ext, atom_type
        use variables, only : read_atom
        use periodic, only : get_site_from_N_path
        use enumerate, only : types_of_atom, trough_site_0, trough_site_1, trough_site_2
        !Arguments
        type(atom_type), intent(in) :: site
        type(atom_type_ext), intent(in) :: atoms(:)
        !Variables
        logical :: hindered
        integer :: adjacent_states
        !Main Method
        can_trough_incorp = .false.

        adjacent_states = read_atom(get_site_from_N_path(site, (/ 2,1 /)), atoms) &
                         + read_atom(get_site_from_N_path(site, (/ 3,4 /)), atoms)

        select case(adjacent_states)
            case(1)
                can_trough_incorp = .true.
            case(2)
                if (biradical_site(site, atoms)) then
                    can_trough_incorp = .true.
                end if
            case default
                return
        end select

    end function can_trough_incorp

    !----------------------------------------------------------------------------
    !Set rate for absorption of C2H2 molecule via Mechanism 1(a/b) (mono-radical)
    !----------------------------------------------------------------------------
    subroutine set_c2h2_M1_rate(atom, atoms, rate_list, ilr)
        !Modules
        use types, only : atom_type_ext, atom_type, rate_type
        use variables, only : read_atom, read_dimer, set_rate, is_act
        use enumerate, only : types_of_atom, dimer_site, tRates
        use surface_m, only : detect_site_env,C2H2_pendant
        use derived_values, only : C2H2_ads_rates
        !Variables
        type(atom_type), intent(in) :: atom
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        type(rate_type), dimension(:), intent(inout) :: rate_list
        integer, intent(inout) :: ilr
        type(atom_type) :: c1_fsite, c2_fsite
        type(atom_type), dimension(2) :: c1_act_sites, c2_fsites2
        type(atom_type), dimension(2,2) :: c2_act_sites
        type(atom_type) :: chosen_c2fsite
        integer :: rate_flavour
        real(dp) :: ads_rate   !Chosen rate of absorption
        integer :: iSite
        !-----------
        !Main Method
        !-------------------------------------------------
        !Absorption Requirements
        ! - Monoradical dimer below potential site for C1
        ! - Potential for filling of adjacent site with C2
        !-------------------------------------------------
        rate_flavour = tRates%C2H2_M1a
        ads_rate = C2H2_ads_rates%M1a

        !Get sites (C1_fsite, C2_fsite, C1_act_sites, C2_fsites2, C2_act_sites2)
        call get_c2h2_aM1_sites(atom, atoms, c1_fsite, c2_fsite, c1_act_sites, c2_fsites2, c2_act_sites)
        chosen_c2fsite = c2_fsite
        !Ensure no C2H2 (carbon 2) pendants hindering final sites
        if (C2H2_pendant(c1_fsite, atoms)) return
        if (C2H2_pendant(c2_fsite, atoms)) return
        if (C2H2_pendant(c2_fsites2(1), atoms)) return
        if (C2H2_pendant(c2_fsites2(2), atoms)) return

            !C1_fsite empty
        if (read_atom(c1_fsite, atoms) == types_of_atom%empty_site) then
            !C2_fsite empty (should always be)
            if (read_atom(c2_fsite, atoms) == types_of_atom%empty_site) then
                !C1 is above dimer mono-radical
                if (detect_site_env(c1_fsite, atoms) == dimer_site) then
                    if (is_act(c1_act_sites(1), atoms) .or. is_act(c1_act_sites(2), atoms))  then
                        !Check whether additional final sites for C2 are available
                        do iSite=1, size(c2_fsites2)
                            if (read_atom(c2_fsites2(iSite), atoms) == types_of_atom%empty_site) then
                                if (is_act(c2_act_sites(iSite,1), atoms) .and. is_act(c2_act_sites(iSite,2), atoms)) then
                                    if (fully_supported(c2_fsites2(iSite), atoms)) then
                                        chosen_c2fsite = c2_fsites2(iSite)
                                        rate_flavour = tRates%C2H2_M1b
                                        ads_rate = C2H2_ads_rates%M1b
                                    end if
                                end if
                            end if
                        end do
                        call set_rate(c1_fsite,rate_flavour,ads_rate, rate_list, ilr, chosen_c2fsite)
                    end if
                end if
            end if
        end if

    end subroutine set_c2h2_M1_rate

    !--------------------------------------------------------------------------
    !Set rate for absorption of C2H2 molecule via Mechanism 2(a/b) (bi-radical)
    !--------------------------------------------------------------------------
    subroutine set_c2h2_M2_rate(atom, atoms, rate_list, ilr)
        !Modules
        use types, only : atom_type,atom_type_ext,rate_type
        use variables, only : read_atom,read_dimer,set_rate,is_act
        use enumerate, only : types_of_atom, dimer_site, tRates
        use surface_m, only : detect_site_env,C2H2_pendant
        use derived_values, only : C2H2_ads_rates
        !Variables
        type(atom_type), intent(in) :: atom
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        type(rate_type), dimension(:), intent(inout) :: rate_list
        integer, intent(inout) :: ilr
        type(atom_type) :: c1_fsite, c2_fsite
        type(atom_type), dimension(4) :: c1_act_sites   !These require either 1 and 3 or 2 and 4 to be activated
        !Main Method
        call get_c2h2_aM2_sites(atom, atoms, c1_fsite, c2_fsite, c1_act_sites)

        !Ensure no C2H2 (carbon 2) pendants hindering final sites
        if (C2H2_pendant(c1_fsite, atoms)) return
        if (C2H2_pendant(c2_fsite, atoms)) return

        !C1_fsite empty
        if (read_atom(c1_fsite, atoms) == types_of_atom%empty_site) then
            !C2_fsite empty (should always be)
            if (read_atom(c2_fsite, atoms) == types_of_atom%empty_site) then
                !C1 is above dimer mono-radical
                if (detect_site_env(c1_fsite, atoms) == dimer_site) then
                    !Matched activation
                    if ((is_act(c1_act_sites(1),atoms) .and. is_act(c1_act_sites(3),atoms)) .or. (is_act(c1_act_sites(2),atoms) .and. is_act(c1_act_sites(4),atoms))) then
                        !Check C1 site fully supported
                        if (fully_supported(c1_fsite, atoms)) then
                            call set_rate(c1_fsite, tRates%C2H2_M2, C2H2_ads_rates%M2, rate_list, ilr, c2_fsite)
                        end if
                    end if
                end if
            end if
        end if

    end subroutine set_c2h2_M2_rate

    !--------------------------------------------------------------------------
    !Set rate for absorption of C2H2 molecule via Mechanism 3 (bi-radical)
    !--------------------------------------------------------------------------
    subroutine set_c2h2_M3_rate(atom, atoms, rate_list, ilr)
        !Modules
        use types, only : atom_type, atom_type_ext, rate_type
        use variables, only : read_atom,read_dimer,set_rate,is_act
        use enumerate, only : types_of_atom, trough_site_0,trough_site_1, trough_site_2, tRates
        use surface_m, only : detect_site_env,C2H2_pendant
        use derived_values, only : C2H2_ads_rates
        !Variables
        type(atom_type), intent(in) :: atom
        type(atom_type_ext), intent(in) :: atoms(:)
        type(rate_type), dimension(:), intent(inout) :: rate_list
        integer, intent(inout) :: ilr
        type(atom_type) :: c1_fsite, c2_fsite
        type(atom_type), dimension(2) :: c1_act_sites
        integer :: site_env
        !Main Method
        call get_c2h2_aM3_sites(atom, c1_fsite, c2_fsite, c1_act_sites)

        !Ensure no C2H2 (carbon 2) pendants hindering final sites
        if (C2H2_pendant(c1_fsite, atoms)) return
        if (C2H2_pendant(c2_fsite, atoms)) return

        !C1 Site must be a trough site
        site_env = detect_site_env(c1_fsite, atoms)
        if (site_env >= trough_site_2 .and. site_env <= trough_site_0) then
            !C1 Site must be empty
            if (read_atom(c1_fsite, atoms) == types_of_atom%empty_site) then
                !C2 Site must be empty
                if (read_atom(c2_fsite, atoms) == types_of_atom%empty_site) then
                    !C1 site is above a bi_radical
                    if (is_act(c1_act_sites(1),atoms) .and. is_act(c1_act_sites(2),atoms)) then
                        if (fully_supported(c1_fsite, atoms)) then
                            call set_rate(c1_fsite, tRates%C2H2_M3, C2H2_ads_rates%M3a, rate_list, ilr, c2_fsite)
                        end if
                    end if
                end if
            end if
        end if

    end subroutine set_c2h2_M3_rate

    !------------------------------------------------------------------------------
    !Adsorb a CHX molecule into the target atom location. Set the lattice to filled
    !with the correct specie, set the activation state of the atom from its bonding
    !------------------------------------------------------------------------------
    subroutine execute_chx_insertion(rate_to_execute, kmc_info, atoms)
        !Modules
        use types, only : rate_type, kmc_type, atom_type_ext, atom_type
        use enumerate, only : file_units, types_of_atom
        use periodic, only : num_bonds, ret_neighbour
        use variables, only : read_atom, ads_event2species, set_act_state, NN_bulk, set_last_frame, undimer_atom
        use surface_m, only : add_surface_atom
        use dimer, only : dimer_assessment
        !Arguments
        type(rate_type), intent(in) :: rate_to_execute
        type(kmc_type), intent(in) :: kmc_info
        type(atom_type_ext),dimension(:), intent(inout) :: atoms
        !Variables
        integer :: NN
        integer :: ads_species, act_level
        if (read_atom(rate_to_execute%site, atoms) /= types_of_atom%empty_site) then
            write(file_units%error, *) kmc_info%step, "CHX insertion reaction into non-empty site!?: (site, event)", &
                    rate_to_execute%site, rate_to_execute%event
            write(6,*) "Error! Check errors.txt for information"
            STOP
        end if
        !Main Method
        NN = num_bonds(rate_to_execute%site, atoms)
        ads_species = ads_event2species(rate_to_execute%event)
        act_level = NN_bulk - ads_species - NN

        call add_surface_atom(rate_to_execute%site, types_of_atom%surf_carb, atoms)
        !Explicitly remove dimer below following a dimer insertion
        if (rate_to_execute%event >= tRates%DinsertCH3 .and. rate_to_execute%event <= tRates%DinsertC) then
            call undimer_atom(ret_neighbour(rate_to_execute%site,2), atoms)
        end if
        call set_act_state(rate_to_execute%site, atoms, act_level)
        call set_last_frame(rate_to_execute%site, atoms ,.true. , rate_to_execute%event)
        call dimer_assessment(rate_to_execute%site, atoms)

    end subroutine execute_chx_insertion

    !----------------------------------------------------------
    !Execute adsorption of a C2H2 molecule onto the surface
    !Pendant atom above will be linked with its supporting atom
    !----------------------------------------------------------
    subroutine execute_C2H2_adsorption(rate_to_execute, kmc_info, atoms)
        !Modules
        use types, only : rate_type, atom_type, atom_type_ext, kmc_type
        use variables, only : read_atom, set_act_state, link_atoms, set_last_frame
        use enumerate, only : types_of_atom, tRates, not_activated, file_units
        use surface_m, only : add_surface_atom
        use dimer, only : dimer_assessment
        !Arguments
        type(rate_type), intent(in) :: rate_to_execute
        type(kmc_type), intent(inout) :: kmc_info
        type(atom_type_ext),dimension(:), intent(inout) :: atoms
        !Error checking
        if (read_atom(rate_to_execute%site, atoms) /= types_of_atom%empty_site) then
            write(*,*) kmc_info%step, "execute_C2H2_adsorption: site1 not empty!:",rate_to_execute%site
            STOP
        end if
        if (read_atom(rate_to_execute%site2, atoms) /= types_of_atom%empty_site) then
            write(*,*) kmc_info%step, "execute_C2H2_adsorption: site2 not empty!:", rate_to_execute%site2
            STOP
        end if

        !Rate Execution
        if (rate_to_execute%event == tRates%C2H2_M1b) then
            call add_surface_atom(rate_to_execute%site, types_of_atom%surf_carb, atoms)
            call set_act_state(rate_to_execute%site, atoms, not_activated)
            call add_surface_atom(rate_to_execute%site2, types_of_atom%surf_carb, atoms)
            call set_act_state(rate_to_execute%site2, atoms, not_activated)
            !Check full support of position 2
            if (.not. fully_supported(rate_to_execute%site2, atoms)) then
                write(file_units%error,*) kmc_info%step, "execute_C2H2_adsorption:M1b: site 2 not fully supported", rate_to_execute%site2
            end if
        elseif (rate_to_execute%event >= tRates%C2H2_M1a .and. rate_to_execute%event <= tRates%C2H2_M3) then
            call add_surface_atom(rate_to_execute%site, types_of_atom%surf_carb, atoms)
            call set_act_state(rate_to_execute%site, atoms, not_activated)
            call add_surface_atom(rate_to_execute%site2, types_of_atom%acetyl_c2, atoms)
            call set_act_state(rate_to_execute%site2, atoms, not_activated)
            call link_atoms(rate_to_execute%site, rate_to_execute%site2, atoms)
        else
            write(*,*) kmc_info%step, "execute_C2H2_adsorption: invalid rate_to_execute%event: ",rate_to_execute%event
        end if


        !Post execution tasks
        call set_last_frame(rate_to_execute%site, atoms, .true., rate_to_execute%event)
        call set_last_frame(rate_to_execute%site2, atoms, .true., rate_to_execute%event)
        call dimer_assessment(rate_to_execute%site, atoms)
        call dimer_assessment(rate_to_execute%site2, atoms)

    end subroutine execute_C2H2_adsorption


    !============
    !SITE GETTING
    !============
    !----------------------------------------------------------------------------
    !Get sites for C2H2 absorption via the mono-radical M1 mechanism (Skokov1995)
    !Final sites for atoms C1 and C2 (N4 of C1)
    !Required sites (either) to be activated for C1
    !C2 sites if a valid `continuation' site is available
    !Required activation sites for such a C2 sites (2,2)
    !----------------------------------------------------------------------------
    subroutine get_c2h2_aM1_sites(atom, atoms, C1_fsite, C2_fsite, C1_act_sites, C2_fsites2, C2_act_sites2)
        !Modules
        use types, only : atom_type_ext, atom_type
        use periodic, only : ret_neighbour, get_site_from_N_path
        !Variables
        type(atom_type), intent(in) :: atom
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        type(atom_type), intent(out) :: C1_fsite, C2_fsite  !Final sites of Carbons 1 and 2 of the absorbing C2H2
        type(atom_type), intent(out), dimension(2) :: C1_act_sites, C2_fsites2
        type(atom_type), intent(out), dimension(2,2) :: C2_act_sites2
        integer :: iSite
        integer, dimension(2,2) :: C2fsites_path = reshape((/ 1,2, 4,3 /), shape = (/ 2, 2 /), order = (/ 2,1 /))
        !Main Method
        C1_fsite = ret_neighbour(atom, 1) !Always get N1 to avoid double rate setting
        C2_fsite = ret_neighbour(C1_fsite, 4)
        C1_act_sites(1:2) = (/ ret_neighbour(C1_fsite,2), ret_neighbour(C1_fsite,3) /)
        
        c2_fsites2(1:2) = (/ get_site_from_N_path(C1_fsite, C2fsites_path(1,:)), get_site_from_N_path(C1_fsite, C2fsites_path(2,:)) /)
        do iSite=1,2
            C2_act_sites2(iSite,1:2) = (/ ret_neighbour(C2_fsites2(iSite),2), ret_neighbour(C2_fsites2(iSite),2) /)
        end do

    end subroutine get_c2h2_aM1_sites

    !---------------------------------------------------------------------
    !From an input atom on the surface, return the final sites of an
    !adsorbed C2H2 molecule and any required atoms which must be activated
    !Follows C2H2 adsorption mechanism II (site B5) from Skokov1995
    !---------------------------------------------------------------------
    subroutine get_c2h2_aM2_sites(atom1, atoms, C1_fsite, C2_fsite, C1_act_sites)
        !Modules
        use types, only : atom_type_ext, atom_type
        use variables, only : read_atom,is_act,read_ipair,atom_from_index
        use periodic, only : ret_neighbour, get_site_from_N_path
        use enumerate, only : types_of_atom
        use dimer, only : return_bridging_atom
        !Variables
        type(atom_type), intent(in) :: atom1
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        type(atom_type), intent(out) :: C1_fsite, C2_fsite
        type(atom_type), dimension(4), intent(out) :: C1_act_sites  !Only 2/4 have to be activated but must be 1+3 or 2+4 indices
        type(atom_type) :: pair1                                    !Dimer pair of input atom
        type(atom_type), dimension(2) :: atom2, pair2 !Atom part of other dimer mono-radical, pair of other atom (either side)
        integer :: iatom
        !Main Method
        pair1 = atom_from_index(read_ipair(atom1, atoms), atoms)
        !Find site directly above dimer radical site
        call return_bridging_atom(atom1,pair1, C1_fsite)
        C2_fsite = ret_neighbour(C1_fsite, 4)
        !Get sites of other dimer mono-radical
        atom2(:) = (/ get_site_from_N_path(atom1, (/ 2,1 /)), get_site_from_N_path(atom1, (/ 3,4 /)) /)
        pair2(:) = (/ get_site_from_N_path(pair1, (/ 2,1 /)), get_site_from_N_path(pair1, (/ 3,4 /)) /)
        !Activation Sites on final site side
        C1_act_sites(1:4) = (/ atom1, pair1, atom2(1), pair2(1) /)
        !If both exist, choose only index 2 to be the other pair of the mechanism set (to prevent double rate setting)
        do iatom=1,2
            if ((read_atom(atom2(iatom), atoms) /= types_of_atom%empty_site .and. &
                    read_atom(pair2(iatom), atoms) /= types_of_atom%empty_site)) then
                C1_act_sites(3:4) = (/ atom2(iatom), pair2(iatom) /)
            end if
        end do

    end subroutine get_c2h2_aM2_sites


    subroutine get_c2h2_aM3_sites(atom, C1_fsite, C2_fsite, C1_act_sites)
        !Modules
        use types, only : atom_type
        use periodic, only : ret_neighbour, get_site_from_N_path
        !Variables
        type(atom_type), intent(in) :: atom
        type(atom_type), intent(out) :: C1_fsite, C2_fsite  !Final sites of Carbons 1 and 2 of the absorbing C2H2
        type(atom_type), intent(out), dimension(2) :: C1_act_sites
        !Main Method
        C1_fsite = ret_neighbour(atom, 1)
        C2_fsite = ret_neighbour(C1_fsite, 4)

        C1_act_sites(:) = (/ ret_neighbour(C1_fsite,2), ret_neighbour(C1_fsite,2)/)

    end subroutine get_c2h2_aM3_sites

    !Is a site above a biradical?
    logical function biradical_site(site, atoms) result(is_biradical_site)
        !Modules
        use types, only : atom_type,atom_type_ext
        use periodic, only : ret_neighbour
        use variables, only : is_act, read_atom
        use enumerate, only : types_of_atom, file_units
        !Variables
        type(atom_type), intent(in) :: site
        type(atom_type_ext),dimension(:),intent(in) :: atoms
        type(atom_type) :: N2,N3
        !Main Method
        N2 = ret_neighbour(site,2)
        N3 = ret_neighbour(site,3)

        if ( is_act(N2, atoms) .and. is_act(N3, atoms)) then
            is_biradical_site = .true.
        else
            is_biradical_site = .false.
        end if

        if (read_atom(N2, atoms) == types_of_atom%empty_site .or. read_atom(N3, atoms) == types_of_atom%empty_site) then
            is_biradical_site = .false.
        end if

    end function biradical_site

    !----------------------------------------------------
    !Check that an atom site (or atom) is fully supported
    !----------------------------------------------------
    logical function fully_supported(atom, atoms) result(supported)
        !Modules
        use types, only : atom_type_ext, atom_type
        use variables, only : read_atom, read_linked_atom
        use periodic, only : ret_neighbour
        use enumerate, only : types_of_atom
        !Variables
        type(atom_type), intent(in) :: atom
        type(atom_type_ext),dimension(:), intent(in) :: atoms
        type(atom_type) :: N2, N3

        N2 = ret_neighbour(atom, 2)
        N3 = ret_neighbour(atom, 3)
        supported = .false.
        !Method
        if (read_atom(atom, atoms) == types_of_atom%acetyl_c2) then
            if (read_linked_atom(atom, atoms) /= types_of_atom%empty_site) then
                supported = .true.
            end if
        else
            if (read_atom(N2, atoms) /= types_of_atom%empty_site .and. &
                    read_atom(N3, atoms) /= types_of_atom%empty_site) then
                supported = .true.
            end if
        end if

        return

    end function fully_supported


end module adsorb_atom
