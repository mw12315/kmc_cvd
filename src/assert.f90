module assert_m

    use kinds, only : dp

    implicit none

    private 

    interface assert
        module procedure assert_logical
        module procedure assert_integer
        module procedure assert_real8
    end interface assert

    public :: assert


contains

    logical function assert_logical(test, expected, test_message, message_unit, write_pass, not_yet_printed) result(pass)
        !Arguments
        logical, intent(in) :: test
        logical, intent(in) :: expected
        character(*), optional, intent(in) :: test_message
        integer, optional, intent(in) :: message_unit
        logical, optional, intent(in) :: write_pass
        logical, optional, intent(inout) :: not_yet_printed !fail failure of a series still to be reached
        !Variables
        logical :: not_yet_printed_
        character(100) :: message
        integer :: unit
        logical :: write_on_pass

        if (present(test_message)) then
            message = trim(test_message)
        else
            message = ""
        end if
        if (present(message_unit)) then
            unit = message_unit
        else
            unit = 6
        end if
        if (present(write_pass)) then
            write_on_pass = write_pass
        else
            write_on_pass = .false.
        end if
        if (present(not_yet_printed)) then
            not_yet_printed_ = not_yet_printed
        else
            not_yet_printed_ = .false.
        end if

        if (test .neqv. expected) then
            if (not_yet_printed_) write(unit,*)
            write(unit, '(a, a, L2, a, L2)') test_message, ' FAIL', test, " != ", expected
            pass = .false.
            if (present(not_yet_printed)) not_yet_printed = .false.
        else
            if (write_on_pass) then
                if (not_yet_printed_) write(unit,*)
                write(unit, '(a, a, L2, a, L2)') test_message, ' PASS', test, " == ", expected
                if (present(not_yet_printed)) not_yet_printed = .false.
            end if
            pass = .true.
        end if

    end function assert_logical

    logical function assert_integer(test, expected, test_message, message_unit, write_pass, not_yet_printed) result(pass)
        !Arguments
        integer, intent(in) :: test
        integer, intent(in) :: expected
        character(*), optional, intent(in) :: test_message
        integer, optional, intent(in) :: message_unit
        logical, optional, intent(in) :: write_pass
        logical, optional, intent(inout) :: not_yet_printed !fail failure of a series still to be reached
        !Variables
        logical :: not_yet_printed_
        character(100) :: message
        integer :: unit
        logical :: write_on_pass

        if (present(test_message)) then
            message = trim(test_message)
        else
            message = ""
        end if
        if (present(message_unit)) then
            unit = message_unit
        else
            unit = 6
        end if
        if (present(write_pass)) then
            write_on_pass = write_pass
        else
            write_on_pass = .false.
        end if
        if (present(not_yet_printed)) then
            not_yet_printed_ = not_yet_printed
        else
            not_yet_printed_ = .false.
        end if

        if (test /= expected) then
            if (not_yet_printed_) write(unit,*)
            write(unit, '(a, a, i10, a, i10)') test_message, ' FAIL', test, " != ", expected
            pass = .false.
            if (present(not_yet_printed)) not_yet_printed = .false.
        else
            if (write_on_pass) then
                if (not_yet_printed_) write(unit,*)
                write(unit, '(a, a, i10, a, i10)') test_message, ' PASS', test, " == ", expected
            end if
            pass = .true.
            if (present(not_yet_printed)) not_yet_printed = .false.
        end if

end function assert_integer


logical function assert_real8(test, expected, test_message, message_unit, &
                              write_pass, not_yet_printed, eps_val) result(pass)
    !Arguments
    real(dp), intent(in) :: test
    real(dp), intent(in) :: expected
    character(*), optional, intent(in) :: test_message
    integer, optional, intent(in) :: message_unit
    logical, optional, intent(in) :: write_pass
    logical, optional, intent(inout) :: not_yet_printed !fail failure of a series still to be reached
    real(dp), optional, intent(in) :: eps_val
    !Variables
    logical :: not_yet_printed_
    character(100) :: message
    integer :: unit
    logical :: write_on_pass
    real(dp) :: eps

    if (present(test_message)) then
        message = trim(test_message)
    else
        message = ""
    end if
    if (present(message_unit)) then
        unit = message_unit
    else
        unit = 6
    end if
    if (present(write_pass)) then
        write_on_pass = write_pass
    else
        write_on_pass = .false.
    end if
    if (present(not_yet_printed)) then
        not_yet_printed_ = not_yet_printed
    else
        not_yet_printed_ = .false.
    end if
    if (present(eps_val)) then
        eps = eps_val
    else
        eps = 1E-1
    end if

    if ((abs(test - expected)) > eps) then
        if (not_yet_printed_) write(unit,*)
        write(unit, '(a, a, ES15.5, a, ES15.5)') test_message, ' FAIL', test, " != ", expected
        pass = .false.
        if (present(not_yet_printed)) not_yet_printed = .false.
    else
        if (write_on_pass) then
            if (not_yet_printed_) write(unit,*)
            write(unit, '(a, a, ES15.5, a, ES15.5)') test_message, ' PASS', test, " == ", expected
        end if
        pass = .true.
        if (present(not_yet_printed)) not_yet_printed = .false.
    end if

end function assert_real8

end module assert_m
