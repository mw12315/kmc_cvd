module initialise

    use kinds,      only : dp
    use enumerate,  only : file_units

    implicit none

contains

    subroutine read_input_files(simulation, conditions, energies, etching_i, reactions, outputs, dimersf, colourise)
        !Modules
        use types, only : sim_type, conditions_ftype, energy_ftype, &
                outputs_ftype, reactions_ftype, etching_ftype, dimer_ftype, event_colour_type
        use input, only : read_simulation, read_conditions, read_energy, &
                read_etching, read_reactions, read_outputs, read_dimer_file, &
                          read_atom_colours
        use variables, only : add_unit_to_open_list
        !Arguments
        type(sim_type), intent(inout) :: simulation
        type(conditions_ftype), intent(inout) :: conditions
        type(energy_ftype), intent(inout) :: energies
        type(etching_ftype), intent(inout) :: etching_i
        type(reactions_ftype), intent(inout) :: reactions
        type(outputs_ftype), intent(inout) :: outputs
        type(dimer_ftype), intent(inout) :: dimersf
        type(event_colour_type), allocatable, intent(inout) :: colourise(:)
        !Variables
        logical :: file_found
        !Main Method

        open(unit=file_units%error, file="errors.txt", action='readwrite')
        call add_unit_to_open_list(file_units%error)
        open(unit=file_units%warning, file="warnings.txt", action='readwrite')
        call add_unit_to_open_list(file_units%warning)
        open(unit=file_units%log, file="diamond.log")
        call add_unit_to_open_list(file_units%log)

        call read_simulation(simulation)
        write(file_units%log, *) "read simulation.input"
        call read_conditions(conditions)
        write(file_units%log, *) "read conditions.input"
        call read_energy(energies)
        write(file_units%log, *) "read energy.input"
        call read_etching(etching_i)
        write(file_units%log, '(a, a13)') "read ", etching_i%input_fname
        call read_reactions(reactions, conditions)
        write(file_units%log, *) "read reactions.input"
        call read_outputs(simulation, outputs)
        write(file_units%log, *) "read outputs.input"
        call read_dimer_file(dimersf, file_found)
        if (file_found) write(file_units%log, *) "read", dimersf%fname
        call read_atom_colours(file_found, colourise)
        if (file_found) write(file_units%log, *) "read colours.input"

    end subroutine read_input_files

    subroutine zero_global_counters(counters, kmc_info)
        !Modules
        use types, only : counter_type, kmc_type, average_reset_rates_set
        !Arguments
        type(counter_type), intent(inout) :: counters
        type(kmc_type), intent(inout) :: kmc_info

        call allocate_global_counters(counters)
        counters%events(:,:) = 0    !Event Counters
        counters%d_align(:) = 0     !Dimer Aligned
        counters%d_break = 0     !Dimer broken
        counters%rates_set_per_it(:) = 0    !Rate setting per iteration

        if (.not. allocated(kmc_info%ave_rates_set%sum)) then
            allocate(kmc_info%ave_rates_set%sum(size(counters%rates_set_per_it))) !Average of per iteration rate setting
            allocate(kmc_info%ave_rate_set_since_last%sum(size(counters%rates_set_per_it))) !Average of per iteration rate setting
            call average_reset_rates_set(kmc_info%ave_rates_set)
            call average_reset_rates_set(kmc_info%ave_rate_set_since_last)
        end if

    end subroutine zero_global_counters

    subroutine allocate_global_counters(c)
        !Modules
        use types, only : counter_type
        use enumerate, only : tRates
        !Arguments
        type(counter_type), intent(out) :: c

        allocate(c%events(tRates%nrates,2), c%rates_set_per_it(tRates%nrates))

    end subroutine allocate_global_counters


    subroutine init_types()
        use periodic, only : init_neighbour_type

        !Main Method
        call init_rate_type_arrays()

        call init_neighbour_type()

    end subroutine init_types


    subroutine init_rate_type_arrays()
        use enumerate, only : irate_dimer_ins, irate_trough_ins, tRates
        use input, only : reactions
        use adsorb_atom, only : chx_species

        irate_dimer_ins =  (/ tRates%DinsertCH3, &
                                   tRates%DinsertCH2, &
                                   tRates%DinsertCH, &
                                   tRates%DinsertC /)

        irate_trough_ins = (/ tRates%TinsertCH3, &
                                   tRates%TinsertCH2, &
                                   tRates%TinsertCH, &
                                   tRates%TinsertC /)


        chx_species = (/ reactions%active_species%CH3, reactions%active_species%CH2, reactions%active_species%CH, reactions%active_species%C /)

    end subroutine init_rate_type_arrays

    !---------------------------------------------------------
    !Count the number of rates per site and total unique rates
    !---------------------------------------------------------
    subroutine count_unique_rates(simulation, reactions)
        !Modules
        use types, only : sim_type, reactions_ftype
        !Arguments
        type(sim_type), intent(inout) :: simulation
        type(reactions_ftype), intent(in) :: reactions
        !Variables
        integer :: i
        !Main Method
        !=========================================================
        !Count the number of rates per site and total unique rates
        !=========================================================
        simulation%rates_per_site = 0
        simulation%unique_rates = 0

        !Dimer Insertion of CHx
        if (reactions%active%InsertDimerCHX_on) then
            simulation%unique_rates =  simulation%unique_rates + 1
            if (reactions%active_species%CH3) simulation%rates_per_site = simulation%rates_per_site + 1
            if (reactions%active_species%CH2) simulation%rates_per_site = simulation%rates_per_site + 1
            if (reactions%active_species%CH) simulation%rates_per_site = simulation%rates_per_site + 1
            if (reactions%active_species%C) simulation%rates_per_site = simulation%rates_per_site + 1
        end if
        if (reactions%active%InsertTroughCHX_on) then
            simulation%unique_rates =  simulation%unique_rates + 1
            if (reactions%active_species%CH3) simulation%rates_per_site = simulation%rates_per_site + 1
            if (reactions%active_species%CH2) simulation%rates_per_site = simulation%rates_per_site + 1
            if (reactions%active_species%CH) simulation%rates_per_site = simulation%rates_per_site + 1
            if (reactions%active_species%C) simulation%rates_per_site = simulation%rates_per_site + 1
        end if
        !Etch CHx
        if (reactions%active%pref_etch_C_on) then
            simulation%rates_per_site = simulation%rates_per_site + 1
            simulation%unique_rates =  simulation%unique_rates + 1
        end if
        !Migration
        if (reactions%active%dimer_migration_on) then
            simulation%rates_per_site = simulation%rates_per_site + 2   !2 different dimer migration types (row, chain)
            simulation%unique_rates =  simulation%unique_rates + 1
        end if
        if (any(reactions%active%gap_mig_ind)) then
            do i=1, size(reactions%active%gap_mig_ind)
                simulation%rates_per_site = simulation%rates_per_site + 1
            end do
            simulation%unique_rates =  simulation%unique_rates + 1
        end if
        if (reactions%active%gap_mig_down_on) then
            simulation%rates_per_site = simulation%rates_per_site + 2
            simulation%unique_rates =  simulation%unique_rates + 1
        end if
        if (any(reactions%active%C2H2p_mig)) then
            simulation%unique_rates =  simulation%unique_rates + 1
            do i=1,size(reactions%active%C2H2p_mig)
                if (reactions%active%C2H2p_mig(i)) simulation%rates_per_site = simulation%rates_per_site + 1
            end do
        end if
        !---------
        !Acetylene
        !---------
        if (reactions%active_species%C2H2) then
            !Adsorption
            if (reactions%active%acetylene%M1) then
                if (reactions%active_species%C2H2) simulation%rates_per_site = simulation%rates_per_site + 1
                if (reactions%active_species%C2H2) simulation%unique_rates =  simulation%unique_rates + 1
            end if
            if (reactions%active%acetylene%M2) then
                if (reactions%active_species%C2H2) simulation%rates_per_site = simulation%rates_per_site + 1
                if (reactions%active_species%C2H2) simulation%unique_rates =  simulation%unique_rates + 1
            end if
            if (reactions%active%acetylene%M3a) then
                if (reactions%active_species%C2H2) simulation%rates_per_site = simulation%rates_per_site + 1
                if (reactions%active_species%C2H2) simulation%unique_rates =  simulation%unique_rates + 1
            end if
            if (reactions%active%acetylene%M3b) then
                if (reactions%active_species%C2H2) simulation%rates_per_site = simulation%rates_per_site + 1
                if (reactions%active_species%C2H2) simulation%unique_rates =  simulation%unique_rates + 1
            end if
            !Etch (CH3/C2H2 etched) + 2
            if (reactions%active%acetylene%etchC2H2) then
                simulation%rates_per_site = simulation%rates_per_site + 1
                simulation%unique_rates =  simulation%unique_rates + 1
            end if
            if (reactions%active%acetylene%etchCH3 .and. reactions%active_species%C2H2) then
                simulation%rates_per_site = simulation%rates_per_site + 1
                simulation%unique_rates =  simulation%unique_rates + 1
            end if
            !Isolated Dimer Etch
            if (any(reactions%active%ide_etch_on)) then
                simulation%unique_rates =  simulation%unique_rates + 1
                do i=1,size(reactions%active%ide_etch_on)
                    if (reactions%active%ide_etch_on(i)) simulation%rates_per_site = simulation%rates_per_site + 1
                end do
            end if
            !Dimer Reformation
            if (reactions%active%C2H2p_dimer) then
                simulation%rates_per_site = simulation%rates_per_site + 1
                simulation%unique_rates =  simulation%unique_rates + 1
            end if

        end if

    end subroutine count_unique_rates

    !------------------------------
    !Initialise the lattice and surface arrays
    !------------------------------
    subroutine initialise_lattice(simulation, lattice, surface)
        !Modules
        use types,          only : atom_type_ext, atom_type, sim_type, surface_type
        use variables,      only : ilat_index
        use enumerate,      only : types_of_atom
        use  surface_m,     only : add_surface_atom
        !Arguments
        type(sim_type), intent(in) :: simulation
        type(atom_type_ext), allocatable, dimension(:), intent(inout) :: lattice
        type(surface_type), intent(inout) :: surface
        !Variables
        integer         :: i, j, k, ibasis, iatom
        integer         :: max_atoms
        integer         :: ilattice_val
        type(atom_type) :: atom
        !Main Method
        max_atoms = simulation%Nx * simulation%Ny * simulation%Nbasis * (simulation%max_height+1)
        allocate(lattice(max_atoms))

        !Initialise entire lattice (all empty site, no dimers)
        surface%total_atoms = 0
        surface%total_dimers = 0
        iatom = 0
        do k=1, simulation%max_height
            do j=1,simulation%Ny
                do i=1,simulation%Nx
                    do ibasis=1,simulation%Nbasis
                        iatom = iatom + 1
                        ilattice_val = ilat_index(i,j,ibasis,k)
                        lattice(ilattice_val)%atom%i        = i
                        lattice(ilattice_val)%atom%j        = j
                        lattice(ilattice_val)%atom%basis    = ibasis
                        lattice(ilattice_val)%atom%k        = k

                        lattice(ilattice_val)%state         = types_of_atom%empty_site
                        lattice(ilattice_val)%act_state     = 0
                        lattice(ilattice_val)%dimer         = .false.
                        lattice(ilattice_val)%ipair         = 0
                        lattice(ilattice_val)%iLink         = 0
                        lattice(ilattice_val)%surface       = .false.
                        lattice(ilattice_val)%last_frame    = .false.
                        lattice(ilattice_val)%mig           = 0

                        !Check for errors within ilattice_val
                        if (ilattice_val /= iatom) write(file_units%error,*) "Lattice(:) index error:",ilattice_val,iatom
                    end do
                end do
            end do
        end do

        !Initialise bulk as specified from input file
        do k = 1, simulation%Nz
            do j = 1, simulation%Ny
                do i = 1, simulation%Nx
                    do ibasis = 1, simulation%Nbasis
                        ilattice_val = ilat_index(i,j,ibasis,k)
                        lattice(ilattice_val)%state         = types_of_atom%surf_carb
                        lattice(ilattice_val)%dimer         = .false.
                        lattice(ilattice_val)%surface       = .false.
                        lattice(ilattice_val)%last_frame    = .false.
                    end do
                end do
            end do
        end do

        !Initialise surface as specified from input file
        iatom = 0
        k = simulation%Nz+1
        do j = 1, simulation%Ny
            do i = 1, simulation%Nx
                do ibasis = 1, 2
                    iatom = iatom + 1
                    ilattice_val = ilat_index(i,j,ibasis,k)
                    lattice(ilattice_val)%dimer         = .false.
                    lattice(ilattice_val)%last_frame    = .false.

                    atom = atom_type(i,j,ibasis,k)
                    call add_surface_atom(atom, types_of_atom%surf_carb, lattice)
                end do
            end do
        end do

        !Set Highest and Lowest Heights
        surface%k_max%val = simulation%Nz+1
        surface%k_max%loc_i = 1
        surface%k_max%loc_j = 1
        surface%k_min%val = simulation%Nz + 1
        surface%k_min%loc_i = 1
        surface%k_min%loc_j = 1

        call initialise_dimers(lattice, surface)

    end subroutine initialise_lattice

    subroutine initialise_dimers(lattice, surface)
        !Modules
        use types, only : atom_type_ext, atom_type, surface_type
        use variables, only : return_ilattice_limits
        use periodic, only : ij_cell_from_dir
        use dimer, only : create_dimer
        !Arguments
        type(atom_type_ext), allocatable, dimension(:), intent(inout) :: lattice
        type(surface_type), intent(inout) :: surface
        !Variables
        integer, dimension(2) :: surf_iatom_bounds
        integer :: iatom
        integer :: total_dimers
        type(atom_type) :: atom, pair !atom, aother atom (pair) to form dimer with atom
        integer :: ni, nj !cell indices from looking in a direction (periodic)

        call return_ilattice_limits(surface%k_min%val, surface%k_max%val, surf_iatom_bounds)

        do iatom=surf_iatom_bounds(1), surf_iatom_bounds(2)
            if (lattice(iatom)%surface) then
                atom = lattice(iatom)%atom
            else
                cycle
            end if

            if (atom%basis == 1) then
                !When in a 'even' cell - form a dimer between 1 and 2 basis atoms in the SAME cell
                if (mod((atom%i+atom%j),2) == 0) then
                    pair = atom; pair%basis = 2
                    call create_dimer(atom, pair, lattice)
                end if
            else if (atom%basis == 2) then
                if (mod((atom%i+atom%j),2) /= 0) then
                    call ij_cell_from_dir(i=atom%i, j=atom%j, direction=2, ni=ni, nj=nj)
                    pair = atom_type(ni, nj, 1, atom%k)
                    call create_dimer(atom, pair, lattice)
                end if
            end if
        end do

        !Count the dimers
        total_dimers = 0
        do iatom=surf_iatom_bounds(1), surf_iatom_bounds(2)
            if (.not. lattice(iatom)%surface) cycle

            if (lattice(iatom)%dimer) then
               total_dimers = total_dimers + 1
            end if
        end do

        if (mod(total_dimers, 2) == 0) then
            surface%total_dimers = total_dimers/2
        else
            write(6,*) "Non-even number of surface dimers!", total_dimers
            write(6,*) "Quitting"
            STOP
        end if

    end subroutine initialise_dimers

    subroutine init_surface_type(s, n_xvals)
        !Modules
        use types, only : surface_type, average_reset
        use variables, only : simulation
        use enumerate, only : edge_state, tRates
        !Arguments
        type(surface_type), intent(inout) :: s
        integer, intent(in) :: n_xvals
        !Variables
        integer :: i
        !Method
        s%start_height = 0.0_dp
        s%init_height = 0.0_dp
        s%total_atoms = 0
        s%total_dimers = 0
        s%average_height = 0.0_dp
        s%ave_growth_height = 0.0_dp
        s%instant_growth_rate%rate = 0.0_dp
        s%instant_growth_rate%last_height = 0.0_dp
        s%instant_growth_rate%last_time = 0.0_dp
        s%instant_growth_rate%time_diff = 0.0_dp
        s%instant_growth_rate%height_diff = 0.0_dp
        s%ave_growth_rate = 0.0_dp
        s%rms_roughness = 0.0_dp
        s%d_rms_roughness = 0.0_dp
        s%rms_2NN = 0.0_dp
        s%rms_3NN = 0.0_dp
        s%rms_4NN = 0.0_dp
        s%width = 0.0_dp
        s%envs(:) = 0
        do i=edge_state%isolated, edge_state%terrace
            call average_reset(s%ave_envs(i))
        end do
        if (.not. allocated(s%grate_at_x)) then
            allocate(s%grate_at_x(n_xvals))
        end if
        s%grate_at_x(:) = 0.0_dp
        if (.not. allocated(s%inst_grate_x)) then
            allocate(s%inst_grate_x(n_xvals))
        end if
        s%inst_grate_x(:) = 0.0_dp
        if (.not. allocated(s%rms_at_x)) then
            allocate(s%rms_at_x(n_xvals))
        end if
        s%rms_at_x(:) = 0.0_dp
        if (.not. allocated(s%rms2N_at_x)) then
            allocate(s%rms2N_at_x(n_xvals))
        end if
        s%rms2N_at_x(:) = 0.0_dp
        if (.not. allocated(s%rms3N_at_x)) then
            allocate(s%rms3N_at_x(n_xvals))
        end if
        s%rms3N_at_x(:) = 0.0_dp
        if (.not. allocated(s%rms4N_at_x)) then
            allocate(s%rms4N_at_x(n_xvals))
        end if
        s%rms4N_at_x(:) = 0.0_dp
        if (.not. allocated(s%reg_at_x)) then
            allocate(s%reg_at_x(n_xvals, tRates%nrates))
        end if
        s%reg_at_x(:,:) = 0.0_dp
        if (.not. allocated(s%events_at_x)) then
            allocate(s%events_at_x(n_xvals, tRates%nrates))
        end if
        s%events_at_x(:,:) = 0.0_dp
        if (.not. allocated(s%layers)) then
            allocate(s%layers(simulation%max_height*4))
        end if
        s%layers(:) = 0.0_dp
        s%active_layers = 0
        call average_reset(s%ave_active_layers)
        call average_reset(s%ave_rms)
        call average_reset(s%ave_2Nrms)
        call average_reset(s%ave_3Nrms)
        call average_reset(s%ave_4Nrms)
        s%min_layer = 0
        s%max_layer = 0
    end subroutine init_surface_type

    !Initialise lattice for a bookmarked lattice
    subroutine init_bookmarked_lattice(atoms, simulation, surface, ilats, lattice)
        use types,      only : atom_type_ext, atom_type, sim_type, surface_type, kmc_type
        use variables,  only : return_ilattice_limits,ilat_index
        use enumerate,  only : types_of_atom,no_dimer
        !Arguments
        type(atom_type_ext),intent(in) :: atoms(:)      !List of atoms on the surface layers to initialise
        type(sim_type), intent(inout) :: simulation
        type(surface_type), intent(inout) :: surface
        integer,intent(out) :: ilats(:)                 !lists of lattice indicices of the surface atoms read in
        type(atom_type_ext), allocatable, intent(out) :: lattice(:)
        !Variables
        integer                                                     :: max_atoms
        integer                                                     :: iatom,ilattice_val
        integer                                                     :: i,j,ibasis,k
        !Redefine max_height of the surface
        if (.not. simulation%continue_unfinished) then
            simulation%max_height = simulation%max_height + surface%k_min%val - 2
            write(file_units%bookmark_log,'(a,i5)') "New lattice max z:", simulation%max_height
        end if

        max_atoms = simulation%Nx * simulation%Ny * simulation%Nbasis * (simulation%max_height+1)
        !Allocate lattice so run from bottom of initial surface (-2).
        allocate(lattice(max_atoms))

        write(file_units%bookmark_log,'(a,i5,a,i4)') "Read in", size(atoms)," atoms starting at k=",surface%k_min%val
        !Initialise bulk part of the lattice
        do k=1, surface%k_min%val - 1
            do j = 1, simulation%Ny
                do i = 1, simulation%Nx
                    do ibasis = 1, simulation%Nbasis
                        ilattice_val = ilat_index(i,j,ibasis,k)
                        lattice(ilattice_val)%atom = atom_type(i,j,ibasis,k)
                        lattice(ilattice_val)%state         = types_of_atom%surf_carb
                        lattice(ilattice_val)%act_state     = 0
                        lattice(ilattice_val)%dimer         = .false.
                        lattice(ilattice_val)%ipair         = 0
                        lattice(ilattice_val)%surface       = .false.
                        lattice(ilattice_val)%last_frame    = .false.
                        lattice(ilattice_val)%surface       = .false.
                        lattice(ilattice_val)%mig           = 0
                    end do
                end do
            end do
        end do
        write(file_units%bookmark_log,'(a,i1,i5,a3,i9,a)') "Initialisd bulk part of lattice: k range: ", 1,surface%k_min%val - 1,"(1:",ilattice_val,")"

        !Initialise surface part from bookmarked atoms
        surface%total_atoms = 0
        do iatom=1,size(atoms)
            ilattice_val = ilats(iatom)
            lattice(ilattice_val) = atoms(iatom)
            lattice(ilattice_val)%last_frame    = .false.
            if (.not. simulation%continue_unfinished) then
                lattice(ilattice_val)%mig = 0
            end if
            if (atoms(iatom)%surface) surface%total_atoms = surface%total_atoms + 1
        end do

        write(file_units%bookmark_log,'(a,i9,a,i9,a,i5,a,i5)') "Initialised surface from atom_list :",ilats(1)," : ",ilats(size(ilats))," with ",surface%total_atoms," surface atoms of ",size(ilats)

        !Initialise the rest of the lattice
        do k =surface%k_max%val+1,simulation%max_height
            do j = 1, simulation%Ny
                do i = 1, simulation%Nx
                    do ibasis = 1, simulation%Nbasis
                        ilattice_val = ilat_index(i,j,ibasis,k)
                        lattice(ilattice_val)%atom = atom_type(i,j,ibasis,k)
                        lattice(ilattice_val)%state         = types_of_atom%empty_site
                        lattice(ilattice_val)%dimer         = .false.
                        lattice(ilattice_val)%surface       = .false.
                        lattice(ilattice_val)%last_frame    = .false.
                    end do
                end do
            end do
        end do

        write(file_units%bookmark_log,'(a,i9,a,i9)') "Initialised rest of lattice:",ilat_index(1,1,1,surface%k_max%val+1),":",ilat_index(simulation%Nx,simulation%Ny,simulation%Nbasis,simulation%max_height)

    end subroutine init_bookmarked_lattice

    !Initialise segment bounds
    subroutine init_segments(sim_info, surface, segments, segment_bounds)
        !Modules
        use types, only : sim_type, surface_type, xy_segment_type
        use segment, only : generate_segment_bounds
        use enumerate, only : file_units
        !Arguments
        type(sim_type), intent(inout) :: sim_info
        type(surface_type), intent(in) :: surface
        type(surface_type), allocatable, intent(out) :: segments(:)
        type(xy_segment_type), allocatable, intent(out) :: segment_bounds(:)

        write(file_units%log,'(a, L1)') "Segments?: ", sim_info%use_segments
        allocate(segments(sim_info%xsegments * sim_info%ysegments))
        call generate_segment_bounds(sim_info%Nx, sim_info%Ny, sim_info%xsegments, sim_info%ysegments,  segment_bounds)
        segments(:) = surface
        if (sim_info%use_segments) write(file_units%log,*) sim_info%xsegments,"x",sim_info%ysegments

    end subroutine init_segments

    !-----------------
    !Generate all rates
    !-----------------
    subroutine generate_rates(conditions, reactions, energies, etching_i, dimerf)
        !Modules
        use kinds, only : dp
        use types, only : conditions_ftype, reactions_ftype, energy_ftype, etching_ftype, dimer_ftype
        use derived_values
        use enumerate, only : edge_state
        use variables, only : Rgas, h, kb, NA
        use arrhenius, only : k_arrhenius
        !Arguments
        type(conditions_ftype), intent(in) :: conditions
        type(reactions_ftype), intent(in) :: reactions
        type(energy_ftype), intent(in) :: energies
        type(etching_ftype), intent(in) :: etching_i
        type(dimer_ftype), intent(in) :: dimerf
        integer     :: mass !Mass of atoms (g)
        real(dp)    :: Par, v
        !Gap Migration temporary values
        integer     :: i

        !-------------------------------------------------------------------
        ! RATES CALCULATED
        !-------------------------------------------------------------------
        ! k1            H abstraction
        ! km1           H2 addition (reverse k1)
        ! k2            H addition to radical site
        ! km2           H desorption from surface carbon
        ! k3            CH3 'insertion' (barrierless direction insertion
        ! k3_act        CH3 dimer insertion
        ! k5            C 'insertion'
        ! k6            CH 'insertion'
        ! k7            CH2 'insertion'
        ! k14           Bulter defect formation
        ! Fsp3          Bulter sp3 fraction
        ! kmig          CH2 migration
        ! ksmig         Radical Site Migration
        ! kbetaScission beta-scission rate
        ! k6_1f         gap migration subreactions
        ! kgmig_a-d     Migration into a trough
        ! kC2H2M1a      Adsorption of C2H2 into dimer mono-radical (Skokov1995)
        ! kC2H2M1b      Adsorption of C2H2 around dimer mono-radical (Skokov1995)
        ! kC2H2M2/3     Adsorption of C2H2 into a bi-radical (Skokov1995) B5, C sites
        ! kC2H2p_mig    Migration of C2H2 pendants
        ! ketch_CHx     Etching via reverse of incorporation (Netto)
        ! kc2h2p_dimer  C2H2 pendant forming a dimer reconstruction
        ! kide         Isolated Dimer Etching
        ! kf_adj_dimer_form formation of dimer with an adjacent CHx
        ! kf_adj_dimer_form Breaking of dimer with an adjacent CHx
        !-------------------------------------------------------------------
        !----------------------------------------------------
        ! Calculate gas molecule impact rates (CH3,CH2,CH,C)
        !----------------------------------------------------
        !Construct arrays to access g and s values via index within a loop
        mass = 15
        do i=1,4
            Par = conditions%CHX_s(i) * conditions%CHX_g(i)                         !Sticking coefficient for CH3 addition
            v = 100 * (8e3 * 8.314_dp * conditions%Tns / (3.142_dp * mass)) ** .5   !Velocity term of CH3 in cm/s
            CHXimpactrate(i) = (Par * conditions%CHxconc(i) * v * .25) / 1.56E+15
            mass = mass - 1
        end do
        !C2H2
        mass = 26
        v = 100 * (8e3 * 8.314_dp * conditions%Tns / (3.142_dp * mass)) ** .5
        C2H2_impactrate = (conditions%C2H2_conc * v * conditions%C2h2_s) / 1.56E+15

        !==============
        !Reaction Rates
        !==============
        !------------------
        !H abstraction (k1)
        ![cm^3 s^-1] (J. Appl. Phys. 101, 053115 (2007))
        !------------------
        k1 = (energies%Ak1 * (conditions%Tns ** .5_dp) * EXP(-energies%Ek1 / conditions%Ts)) * (conditions%Hconc)

        !-----------------
        !H2 addition (km1)
        ![cm^3 s^-1] (J. Appl. Phys. 101, 053115 (2007))
        !-----------------
        km1 = energies%Akm1 * (conditions%Tns ** .5_dp) * EXP(-energies%Ekm1 / conditions%Ts) * (conditions%H2conc)

        !---------------
        !H addition (k2)
        ![cm^3 s^-1] (J. Appl. Phys. 101, 053115 (2007))
        !---------------
        k2 = energies%Ak2 * (conditions%Tns ** .5_dp) * (conditions%Hconc)

        !------------------
        !H Desorption (km2)
        !------------------
        !Assuming CH bond energy barrier is 413 kJ/mol and pre-exp factor is 1e13 s-1
        !and that it is Arrhenius.  ### km2 ~ 0 for all Ts ###
        km2 = 1E+13_dp * EXP(-413000 / (8.314 * conditions%Ts))

        !----------------------------------------
        !CH3 insertion into surface dimer radical
        !----------------------------------------
        k3 = 2.4E-13_dp * (conditions%Tns**0.5)                                        ![cm^3 s^-1] (J. Appl. Phys. 101, 053115 (2007))
!       k3_act = (CHXconc(1) / NA) * 1E+13_dp ! dimer insertion from Frenlach 2005

        !--------------------------------------------
        ! Rate constant for defect formation (Butler)
        !--------------------------------------------
        k14 = 1.12E+16_dp * EXP(-201600 / (8.314 * conditions%Ts)) / conditions%Ts

        ! Butler's fraction of sp3
        Fsp3 = k1 * conditions%Hconc / (k1 * conditions%Hconc + k3 * (sum(conditions%CHxconc)) + k14)

        ! fraction of open (monorad) sites
        Fyuri = 1 / (1 + .3 * EXP(3430 / real(conditions%Ts)) + .1 * EXP(-4420 / real(conditions%Ts)) * conditions%H2conc / conditions%Hconc) !10.1021/jp803735a
        Fbutler = k1 / (k1 + k2)     ! simplified version of Butler's model, from JAP 99 104907 10.1063/1.2195347

        IF (trim(reactions%RadSiteDensity) == "yuri") THEN      !Yuri!s model
            Fmr = Fyuri
        ELSE                         !Butler!s model
            Fmr = Fbutler
        END IF
        Fbr = Fmr * Fmr              ! no. biradical sites

        ! Yuri's estimate of crystallite size in nm
        dcalc = (2.0_dp + 0.6_dp * EXP(3430.0_dp / conditions%Ts)) * (conditions%Hconc / (sum(conditions%CHxconc))**2)

       !surface CH2 activation rate
        activate_rate = k1 * conditions%Hconc + km2

        !surface CH2* deactivation rate
        deactivate_rate = km1 * conditions%H2conc + k2 * conditions%Hconc

        !---------------
        !DIMER Migration
        !---------------
        k_dimer_mig = energies%A_dmig * EXP(-energies%E_dmig / (Rgas * conditions%Ts))
        !----------------------
        !Migration into Gap
        !Values from Netto20015
        !----------------------
        k6a_1f = energies%A_gmig1f * exp(real(-energies%E_gmig1f/(conditions%Ts*Rgas)))
        k6a_1r = energies%A_gmig1r * exp(real(-energies%E_gmig1r/(conditions%Ts*Rgas)))
        k6a_2f = energies%A_gmig2f * exp(real(-energies%E_gmig2f/(conditions%Ts*Rgas)))
        k6a_2r = energies%A_gmig2r * exp(real(-energies%E_gmig2r/(conditions%Ts*Rgas)))
        !Overall reactions for envs A-D (1-4)
        kgmig_a = (k6a_1f * k6a_2f)/(k6a_2f + k6a_1r)
        kgmig_b = (k6a_2f * k6a_2r)/(k6a_2f + k6a_2f)
        kgmig_c = (k6a_1f * k6a_1r)/(k6a_1r + k6a_1r)
        kgmig_d = (k6a_1r * k6a_2r)/(k6a_2f + k6a_1r)
        kgmig_all_rates(1:4) = (/ kgmig_a,kgmig_b,kgmig_c,kgmig_d /)
        !Down
        k_down_mig = energies%A_down_mig * EXP(-energies%E_down_mig/(Rgas * conditions%Ts))

        !Rate constants from the A3 and A4 sites [Skokov et al, JPC 98 (1994) 7073] of 5100 s-1 and 150000 s-1.
!        Gmono = 3.8E-14 * conditions%Ts**0.5 * conditions%CHxconc(1) * Fmr * 0.5 * k1 * conditions%Hconc * (1 / (k1 * conditions%Hconc + kdes))
!        Gbi = 3.8E-14 * conditions%Ts**0.5 * conditions%CHxconc(1) * Fmr**2
!        Gchx = 3.9E-14 * conditions%Ts**0.5 * sum(conditions%CHxconc(2:4)) * Fmr

        !Beta-scission rate constant (Jeremy)
        kbetaScission = (kb * conditions%Ts / h) * EXP(-energies%EbetaScission / (Rgas * conditions%Ts))

        !Finalise incorporation rates (CHx)
        rDimerInsertCHX = CHXimpactrate * reactions%dimer_insertion_scale
        rTroughInsertCHX = CHXimpactrate * reactions%trough_insertion_scale

        !=======
        !ETCHING
        !=======
        !--------------------
        !PREFERENTIAL ETCHING
        !Battaile1999
        !--------------------
        pref_etch_rates(:) = 0.0_dp
        do i=edge_state%isolated, edge_state%terrace
            pref_etch_rates(i) = k_arrhenius(A=etching_i%flex_etch(i)%A, &
                                             E=etching_i%flex_etch(i)%E, &
                                             T=conditions%Ts) * (conditions%Hconc/NA)
        end do

        !----------------------
        !Isolated Dimer Etching
        !Skokov1995, Netto2005
        !----------------------
        call isolated_dimer_etch_rates(etching_i, conditions, ide_rates_all, kide_ind, kide_comb, &
                kide_perms)
        allocate(kide(size(kide_ind)))
        if (reactions%ide_individual_active) then
            kide => kide_ind
        else
            kide => kide_comb
        end if

        !---------
        !CHx Etch
        !Netto2005
        !---------
        call chx_etch_rate(real((conditions%Hconc/NA),dp), conditions%Ts, ketch_CHx)

        !==========
        !C2H2
        !Skokov1995
        !==========
        !----------
        !Absorption
        !----------
        C2H2_ads_rates%M1a = C2H2_impactrate * exp(-energies%acetylene_ads%M1%E/(Rgas * conditions%Ts))
        C2H2_ads_rates%M1b = C2H2_impactrate * exp(-energies%acetylene_ads%M1%E/(Rgas * conditions%Ts))
        C2H2_ads_rates%M2 = C2H2_impactrate * exp(-energies%acetylene_ads%M2%E/(Rgas * conditions%Ts))
        C2H2_ads_rates%M3a = C2H2_impactrate * exp(-energies%acetylene_ads%M3a%E/(Rgas * conditions%Ts))
        C2H2_ads_rates%M3b = C2H2_impactrate * exp(-energies%acetylene_ads%M3b%E/(Rgas * conditions%Ts))
        !-------
        !Etching
        !-------
        C2H2_ads_rates%etchCH3 = energies%acetylene_ads%etchCH3%A * exp(-energies%acetylene_ads%etchCH3%E/(Rgas * conditions%Ts))
        C2H2_ads_rates%etchC2H2 = energies%acetylene_ads%etchC2H2%A * exp(-energies%acetylene_ads%etchC2H2%E/(Rgas * conditions%Ts)) * (conditions%Hconc/NA)

        !---------
        !Migration
        !---------
        allocate(kC2H2p_mig_sub(4))
        call c2H2p_mig_subreaction_rates(conditions%Ts, energies, kC2H2p_mig_sub)
        call c2h2p_mig_rate_overall(kC2H2p_mig_sub, kC2H2p_mig)

        !Dimer Formation from C2H2 pendant
        kc2h2p_dimer = k_arrhenius(energies%c2h2p_dimer%A, energies%c2h2p_dimer%E, conditions%Ts)

        !Dimer formation/breaking
        !Energy barrier from S. Skokov et al., J. Phys. Chem., 1994, 98, 7073–7082.
        !Prefactors calculated from reactions rates at 1200K
        kf_adj_dimer = k_arrhenius(1E12_dp, 2.93E3_dp, conditions%Ts)
        if (dimerf%dimer_break_model == 'skokov') then
            kr_adj_dimer = k_arrhenius(1.6E11_dp, 2.93E3_dp, conditions%Ts)
        else
            kr_adj_dimer = k_arrhenius(4.79E13_dp, 59.8E3_dp, conditions%Ts)
        end if

        if (dimerf%prob_adj_dimer_break < 0) then
            prob_adj_dimer_break = kr_adj_dimer / (kf_adj_dimer + kr_adj_dimer)
        else
            prob_adj_dimer_break = dimerf%prob_adj_dimer_break
        end if

    end subroutine generate_rates

    !----------------------------------------------------------------------
    !Generate rate constants (f,r) and overall rate constants for ide
    !5 reactions, i subreactions within each reaction
    !Each reaction 12-i (i=2-5) requires 12-1 to first occur
    !k_individ(:): rates of all 5 reactions
    !k_overall(2:5): rates of all 4 etching reactions chained with ide-1
    !k_perms(16,4): overall etching reaction rates chain with ide-1 for all
    !16 combinations of etching reactions
    !----------------------------------------------------------------------
    subroutine isolated_dimer_etch_rates(etching_i, conditions_i, k_all_subr, &
            k_individ, k_overall, k_perms)
        !Modules
        use kinds, only : dp
        use input, only : reactions
        use types, only : etching_ftype, conditions_ftype, fr_k_type
        use ide, only : ide_k_all_fr, ide_k_12_n, ide_k_n_comb, scale_kide, ide_permutations, i_kide, scale_kide
        !Arguments
        type(etching_ftype), intent(in) :: etching_i
        type(conditions_ftype), intent(in) :: conditions_i
        type(fr_k_type), intent(out) :: k_all_subr(:,:) !k vals for 12-(n)-(i)-[f,r]
        real(dp), intent(out) :: k_individ(:)
        real(dp), intent(out) :: k_overall(:)
        real(dp), intent(out) :: k_perms(16,4)
        !Variables
        integer :: n, ik

        !Method
        if (.not. etching_i%explicit_rate_input) then
            !1> Calculate all subreactions (12-1-1-f, ..., 12-n-i-f)
            !2> Use subreactions to calculate forward reactions (12-1, ..., 12-n
            !3> Calculate forward rate coefficients for 12-1 + 12-n (chained reaction)
            do n=1,etching_i%n_reactions
                call ide_k_all_fr(etching_i, conditions_i, n, etching_i%ide_ahhr(n,:), k_all_subr(n,:))
                call ide_k_12_n(k_all_subr(n,:), n, k_individ(n))
            end do
        else !Explicit rates for 12-i (1-5) provided
            do n=1, etching_i%n_reactions
                k_individ(n) = etching_i%explicit_rates(n)
            end do
        end if


        do n=1, size(k_perms, 1)
            ik = i_kide(ide_permutations(n,:))
            k_perms(ik, :) = scale_kide(ide_permutations(n,:), k_individ, .true.)
        end do

        k_overall(1) = k_individ(1)
        k_overall(2:5) = k_perms(16,:)
    end subroutine isolated_dimer_etch_rates

    !-------------------------------------------------------------------------
    !Calcalate rate of CHx etching as a reverse of CH2 incorporation
    !From A. Netto and M. Frenklach, Diam. Relat. Mater., 2005, 14, 1630–1646.
    !DOI: 10.1016/j.diamond.2005.05.009
    !Typo in paper, / T should apply to the entire equation rather than just
    !the 2nd summation. Without it, rate of etching is circa. 1E15 / s
    !-------------------------------------------------------------------------
    subroutine chx_etch_rate(hconc_mol_per_cm3, temperature_in_K, k)
        !Modules
        !Arguments
        real(dp), intent(in) :: hconc_mol_per_cm3
        integer, intent(in) :: temperature_in_K
        real(dp), intent(out) :: k
        !Variables
        real(dp) :: lnk

        !Method
        lnk = ((35.345_dp - (7.2873E8_dp * hconc_mol_per_cm3) + (3.4063E16_dp * (hconc_mol_per_cm3**2))) &
            - (38.931_dp - (1.2038E9_dp * hconc_mol_per_cm3) + (5.9123E16_dp * (hconc_mol_per_cm3**2)))) / temperature_in_K

        k = exp(lnk)

    end subroutine chx_etch_rate

    !---------------------------------------------------------------------------
    !Reaction Rates of C2H2 pendant migration
    !A <--> B <--> C <--> D
    !   k1     k2     k3
    !Reactions and Rates from:
    !S. Skokov, B. Weiner and M. Frenklach, J. Phys. Chem., 1995, 99, 5616–5625.
    !---------------------------------------------------------------------------
    subroutine c2H2p_mig_subreaction_rates(temperature_in_kelvin, energies, k)
        !Modules
        use types, only : fr_k_type, energy_ftype
        use arrhenius, only : k_arrhenius
        !Arguments
        integer, intent(in) :: temperature_in_kelvin
        type(energy_ftype), intent(in) :: energies
        type(fr_k_type), intent(out) :: k(:)
        !Variables
        integer :: i

        !Method
        do i=1,energies%C2H2p_nreactions
            k(i)%f = k_arrhenius(energies%C2H2p_mig(i)%f%A, energies%C2H2p_mig(i)%f%E, temperature_in_kelvin)
            k(i)%r = k_arrhenius(energies%C2H2p_mig(i)%r%A, energies%C2H2p_mig(i)%r%E, temperature_in_kelvin)
        end do

    end subroutine c2H2p_mig_subreaction_rates

    !-------------------------------------------------------
    !Overall rate of C2H2 pendant migration, using 2nd order
    !steady-state approximation.
    !           1
    ![ A ] <-> [ B <-> C <-> D ]
    !  k1             k2
    !           2
    ! A <-> B -> D
    !
    !-------------------------------------------------------
    subroutine c2h2p_mig_rate_overall(ksub , k)
        !Modules
        use types, only : fr_k_type
        !Arguments
        type(fr_k_type), intent(in) :: ksub(:) !Subreaction (fr) rates
        real(dp), intent(out) :: k(2) !(No C2H2, C2H2) adjacent to final site
        real(dp) :: k2

        k2 = (ksub(3)%f * ksub(2)%f * ksub(1)%f) / ksub(3)%r

        !No C2H2 Adjacent to C2H2
        k(1) = (k2 * ksub(1)%f) / (ksub(1)%r + k2)

        k(2) = ((ksub(4)%f * ksub(1)%f) / (ksub(1)%r + ksub(4)%f))

    end subroutine c2h2p_mig_rate_overall

end module initialise
