#!/usr/bin/python
import sys
byte = sys.argv[1]
if byte.isdigit():
    kbyte = float(byte) / 1024.0
    mbyte = float(kbyte) / 1024.0
    gbyte = float(mbyte) / 1024.0
    print "%s b" % byte
    print "%.2f kb" % (kbyte)
    print "%.4f mb" % (mbyte)
    print "%.6f gb" % (gbyte)
