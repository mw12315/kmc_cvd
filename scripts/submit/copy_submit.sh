#!/bin/sh
if [ -n "$1" ]; then
  submit_script=$1
else
  submit_script="go_diamond.sh"
fi

for f in */
do
  echo "${f}.sh"
  cp $HOME/scripts/submit/$submit_script "${f}${f%/}.sh"
done
rm -f ".sh"


