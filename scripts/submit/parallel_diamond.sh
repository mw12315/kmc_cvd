#!/bin/tcsh -f
cd $PBS_O_WORKDIR
setenv OMP_NUM_THREADS $PBS_NP
setenv OMP_WAIT_POLICY active
setenv OMP_DYNAMIC false
setenv OMP_PROC_BIND true
qstat -f $PBS_JOBID > jobinfo.out
$HOME/local/src/diamond_lattice/Parallel-Release/diamond
