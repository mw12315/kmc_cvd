#!/bin/sh
for var in */
do
  cd "$var"
  for script in ./*.sh
  do
    qsub "$script"
  done
  cd - > /dev/null
done
