#!/bin/sh
# submit a single directory to the queue
if [ -z "$2" ]; then
  t="05:00:00"
else
  t=$2
fi

cd "$1"
for script in ./*.sh
do
  eval "qsub ${script} -l walltime=${t}"
done
cd - > /dev/null
