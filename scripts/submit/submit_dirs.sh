#!/bin/sh

if [[ -z $1 ]]; then
  echo "Usage: hh:mm:ss dir1 dir2 .. dirN"
  exit 0
fi

wt=$1
if echo "$wt" | grep -vq ":"; then
  echo "arg1 should be a walltime"
  exit 1
fi

skip_wait=$2
if echo "$skip_wait" | grep -iwEv "[yn]"; then
  echo "arg2 should be [y,n]"
  exit 1
else
  if [[ "$skip_wait" == "y" ]]; then
    skip_wait=true
    echo "skipping 1.1s wait time"
  else
    skip_wait=false
  fi
fi

for var in "${@:3}"
do
  cd "$var"
  for script in ./*.sh
  do
    eval "qsub ${script} -l walltime=$wt"
  done
  if [[ ! $skip_wait ]]; then
    sleep 1.1
  fi
  cd - > /dev/null
done
