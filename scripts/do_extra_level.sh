#!/bin/sh

for f in */
do
  cd $f
  echo $f
  eval $1
  cd - > /dev/null
done
