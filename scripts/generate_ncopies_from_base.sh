#! /bin/bash

base_name=$1
start=$2
end=$3
ncopies=$4

if [[ $# -ne 4 ]]
then
  echo "requires 4 arguments"
  exit 1
fi

for ((n=$start;n<=$end;n++));
do
 eval "create_numbered_dirs.sh ${base_name}_${n} ${ncopies}"
done

