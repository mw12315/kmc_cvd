#!/bin/sh

for f in */
do
cd $f
python3 ~/local/src/diamond_lattice/python_scripts/read_all_outputs.py
cd - > /dev/null
done
