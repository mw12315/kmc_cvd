#!/bin/sh
x=""
START=1
END=$2
if [ -z $1 ]; then echo "no directory entered"; exit 1; fi
if [ -z $2 ]; then echo "no number entered"; exit 1; fi
for (( n=$START; n<=$END; n++))
do
  eval "cp -r $1 $1_r$n"
  echo $1_r$n
done

eval "rm -r $1"
