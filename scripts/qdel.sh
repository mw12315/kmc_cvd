#!/bin/sh

START=$1
END=$2

for (( n=$START; n<=$END; n++))
do
  eval "qdel $n"
done
