#!/bin/sh

for dir in "$@"
do
  cd $dir
  python3 $HOME/local/src/diamond_lattice/python_scripts/tools/copy_seed.py
  cd - > /dev/null
done
