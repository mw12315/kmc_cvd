#! /bin/bash
rm -f $1/diamond.out
rm -f $1/diamond.log
rm -f $1/diamond.json $1/diamond_cont.json
rm -f $1/diamond.log
rm -f $1/bookmark.log
rm -f $1/bookmark.geom $1/bookmark.json
rm -f $1/surface_connectivity.csv
rm -f $1/events*.csv
rm -f $1/nuc_events.csv rm -f $1/events.out
rm -f $1/surface_height.csv
rm -f $1/activated_fraction.csv
rm -f $1/rate_reg_tot.csv
rm -f $1/rate_vals.csv
rm -f $1/dimer_count.csv
rm -f $1/dt.csv
rm -f $1/fort.*
rm -f $1/non0rates.txt
rm -f $1/errors.txt
rm -f $1/warnings.txt
rm -f $1/final_surface.xyz $1/final_surface.xyz.gz
rm -f $1/frames_surface*.xyz
rm -f $1/frames_*.xyz
rm -f $1/vis_errors.txt
rm -f $1/vis_warnings.txt
rm -f $1/kmc_vis.xyz $1/kmc_vis.xyz.gz
rm -f $1/*sh.e*
rm -f $1/*sh.o*
rm -f $1/*.xyz
rm -f $1/*.xyz.gz
rm -f $1/growth_rate.csv
rm -f $1/height*.csv
rm -f $1/layers.csv
rm -f $1/roughness.csv
rm -f $1/atoms_envs.csv
rm -f $1/bookmark.input
rm -f $1/randomise.log
# BC4 related files
rm -f $1/slurm*
rm -f $1/a.out
rm -f $1/e.out
rm -f $1/LOG
rm -f $1/std.out
