#!/bin/sh
COPIES=$1
for f in */
do
  cd $f
  echo $f
  # Make new directory and move contents to that directory
  sub_name="${f%/}_sub"
  eval "mkdir ${sub_name}"
  eval "mv * ${sub_name} > /dev/null"
  eval "sh $HOME/scripts/create_numbered_dirs.sh" $sub_name $COPIES
  # Remove original folder
  eval "rm -r ${sub_name}"
  cd ..
done
