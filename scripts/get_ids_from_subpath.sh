#!/bin/sh
start=$1
end=$2
search=$3

if [[ $# -ne 3 ]]; then
  echo "requires start id, end id and search pattern"
  exit 1
fi


for ((n=$start; n<=$end; n++))
do
  eval "qstat -f ${n} | grep -A1 -e 'init_work_dir\|Job Id'" | grep -B 4 -e "${search}" | grep -e "Job Id:" |  grep -o "[0-9]*"
done

# PASS THIS LIST OF JOB IDS to QDEL:
# cat file_of_jobs | xargs qdel
