# Input is a final location (including final slash) - input files within the subdirectories within the root folder are then copied to this location within the folder they came in
loc=$1
for f in */
do
  echo "make ${loc}${f}"
  eval "mkdir ${loc}${f}"
  echo "final location: ${loc}${f}"
  for d in ${f}*.input
  do
    echo $d
    eval "cp ${d} ${loc}${f}"
  done
done

