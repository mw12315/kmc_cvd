#!/usr/bin/python
###############################################################################
#
#	This little python code takes a file 'visualise' which is
#	is output from the main KMC program rearanges it and builds
#	an xyz-movie file to be read in OVITO
#
###############################################################################

from collections import namedtuple
import argparse
import os.path

parser = argparse.ArgumentParser(description='outputs .xyz files from a "visualise" kmccvd output file')
parser.add_argument('-r','--framerate', default='1', nargs='?', type=int, help='muliple of frames rendered')
parser.add_argument('-s','--framestart',default='1', nargs='?', type=int, help='starting frame to render')
parser.add_argument('-e', '--frameend', default='-1', nargs='?', type=int, help='final frame to render. Entering "-1" will select the end of the sumulation')
parser.add_argument('-i','--input', default='visualise', help='path/to/input/file - files in $PWD require only name')
parser.add_argument('-o','--output',default='out', help='choose output file name e.g. "scd_32"')
parser.add_argument('-1','--single',action='store_true',default='False',help='flag to print a single frame chosen by "--framestart"')
args = parser.parse_args()


def xyz_of_populated_sites(sites_tensor, total_sites, frame_id, atom_label):
  frame_xyz = [] 
  counter = 1
  frame_xyz.append("%d" % total_sites)
  frame_xyz.append("Frame %d" % frame_id)
  for x_index, yz_plane in enumerate(sites_tensor):
    for y_index, z_vector in enumerate(yz_plane):
      for z_index, site in enumerate(z_vector):
        print "(%i,%i,%i)" % (x_index,y_index,z_index)
        if (site):
          if (z_index == 0) and (x_index%2 != 0) and (x_index != 0): #z=0 x odd but not zero
            log_file.write(str(counter) + "\t" + str(x_index) + "\t" + str(y_index) + "\t" + str(z_index) + "\t" + "|\t") #printing initial counter values
            z_index += 0.5
            log_file.write(str(x_index) + "\t" + str(y_index) + "\t" + str(z_index) + "\n") #finalvalues
            counter += 1
          if (z_index == 1) and (x_index%2 != 0) and (x_index != 0): #z == 1 and x is odd but not zero
            log_file.write(str(counter) + "\t" + str(x_index) + "\t" + str(y_index) + "\t" + str(z_index) + "\t" + "|\t") #printing initial counter values
            y_index += 0.5
            log_file.write(str(x_index) + "\t" + str(y_index) + "\t" + str(z_index) + "\n") #finalvalues
            counter += 1 
          if (z_index == 1) and (x_index%2 == 0): #z == 1 and x is even
            log_file.write(str(counter) + "\t" + str(x_index) + "\t" + str(y_index) + "\t" + str(z_index) + "\t" + "|\t") #printing initial counter values 
            z_index += 0.5
            y_index += 0.5
            log_file.write(str(x_index) + "\t" + str(y_index) + "\t" + str(z_index) + "\n") #finalvalues
            counter += 1 
          frame_xyz.append(("%s\t%5.1f\t%5.1f\t%5.1f" % (atom_label, x_index,y_index,z_index)))
        else:
          break
  return frame_xyz

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]
        
Action = namedtuple("Action", "type x y z")

#fname = raw_input('Enter filename: ')
fname1 = args.output + '.xyz'
print_frame_rate = args.framerate
print_frame_start = args.framestart
print_frame_end = args.frameend

print "Frame Rate (%s), Frame Start (%s), Frame End (%s), Single Frame (%s)" % (print_frame_rate, print_frame_start, print_frame_end, args.single)

log_file_name = args.output + '.log'
log_file = open(log_file_name, "w")
#Read lines of output from visual.out parsing them into 
#a multi-dim array and taking the final line off
oldlines = open(args.input, "r").readlines()
visualise_lines = []
for line in oldlines:
  visualise_lines.append([
    Action(action[0], int(action[1]), int(action[2]), int(action[3]))
    for action in chunks(line.strip().split("\t"),4)
  ])
  
if(print_frame_end == -1):
  print_frame_end = len(visualise_lines)

#Error Messages Start
if(not os.path.isfile(args.input)):
  parser.error("Input file not found")  

if(print_frame_rate <= 0):
  parser.error("Frame rate cannot be less than 1") 

if(print_frame_start < 0):
  parser.error("Starting frame cannot be less than 0")

if(print_frame_start >= len(visualise_lines)):
  parser.error("Starting frame cannot be beyond total number of frames") 

if(print_frame_end <= print_frame_start):
  parser.error("End frame cannot be less than or equal to the starting frame. Print a single frame using the --single flag")

if (print_frame_end > len(visualise_lines)):
  parser.error("End frame cannot be greater than total number of frames")

#Error Messages End
if args.single:
    print_frame_end = print_frame_start

FILE = open(fname1, "w")
#This bit of code loops the input to find the maximum values
#and starting height
x_cord = -1;y_cord = -1;z_cord = -1

h_start = visualise_lines[0][0].z
h_start = h_start + 1
z_cord = max(z_cord, h_start)

for line in visualise_lines:
  for action in line:
    x_cord = max(x_cord, action.x)
    y_cord = max(y_cord, action.y)
    z_cord = max(z_cord, action.z)

total = x_cord*y_cord*(h_start)

#*_place keeps track of the (x,y) coordinate on the linear atom list
#surf_height tells at which height (x,y) is currently
occupation_grid = [ [ [ False for z in xrange(z_cord)] for y in xrange(y_cord)] for x in xrange(x_cord)]

#orig array holds xyz atom list. This loop initially fills it.
for i in xrange(y_cord):
  for j in xrange(x_cord):
    for k in xrange(h_start):
       occupation_grid[j][i][k] = True

frame = 0

FILE = open(fname1, "a")

xyz_of_frame = xyz_of_populated_sites(occupation_grid, total, frame, "B")
for line_of_frame in xyz_of_frame:
  FILE.write(line_of_frame+"\n")

for line in visualise_lines:
  frame = frame + 1
  #construction phase
  for action in line:
    if (action.type == "Add"):
      occupation_grid[action.x-1][action.y-1][action.z-1] = True
      total += 1
      
    if (action.type == "Take"):
      occupation_grid[action.x-1][action.y-1][action.z-1] = False
      total -= 1
        
  # Print phase
  print_frame = frame % print_frame_rate == 0 and \
                frame >= print_frame_start and \
                frame <= print_frame_end
  if(print_frame):
    xyz_of_frame = xyz_of_populated_sites(occupation_grid, total, frame, "C")
    for line_in_frame in xyz_of_frame:
      FILE.write(line_in_frame+"\n")

print "Output %s frames" % str(print_frame_end - print_frame_start + 1)
FILE.close()
log_file.close()
