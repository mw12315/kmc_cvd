#!/bin/sh
# This script generates a script '$1_movie.sh' which can be submitted to the batch queue
# qsub. This script in turn called movie_new.py within the local/src/kmccvd/ directory,
# while also parsing any arguments required, either during generation "$1 - $5" or by
# editing the script following generation and prior to submission.
#
# Arguments should be parsed such "-e 500" "-r 2" "-i visualise_addition"
#
echo "#!/bin/tcsh -f" > $PWD/sub_$1_movie.py.sh
chmod +x $PWD/sub_$1_movie.py.sh
echo "cd \$PBS_O_WORKDIR" >> $PWD/sub_$1_movie.py.sh
echo "# add arguments for movie_new.py here: eg '-e 500' '-s 250'" >> $PWD/sub_$1_movie.py.sh
echo "\$HOME/local/src/kmccvd/movie_new.py -o $1 $2 $3 $4 $5" >> $PWD/sub_$1_movie.py.sh
echo "$1_movie.sh output to $PWD"
mv sub_$1_movie.py.sh $1_movie.sh
