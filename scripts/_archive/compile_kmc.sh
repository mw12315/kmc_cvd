#!/bin/sh
#------------------------------------------------------------
# This script is used to compile all .f90 files and then
# links the compiled files (.o) into a 
# single binary, labelled with the date.
# The created binary is then copied to the bin/kmc/cur dir
# to be run as the most up to date script.
# 
# $1 output binary version
# $2 comment - eg. changes to .f90 being compiled
#
# EXAMPLE INPUT
# comp_kmc 0.11 "Compiled main.f90 with additional QOL"
#
#
# *.f90 modules should first be checked for syntax by using
#  the command "gfortran -fsyntax-only" <file>
#------------------------------------------------------------

moment=$(date +%F)
#
#
gfortran -o kmc_{$1}.exe *.f90
echo -e "${moment} | $1  | ${2}\r" >> ${HOME}/bin/kmc/version_archive/changelog.txt
#
#echo "compiled"
#
cp kmc_{$1}.exe $HOME/bin/kmc/cur/kmc.exe
echo "----------- Copied kmc_{$1}.exe to $HOME/bin/kmc/cur/ -----------"
