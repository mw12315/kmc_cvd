#!/usr/bin/python
###############################################################################
#
#	This little python code takes a file 'visual.out' which is
#	is output from the main KMC program rearanges it and builds
#	and xyz-movie file to be read in VMD
#
###############################################################################


FILE = open('mcd_surface.xyz', "w")
VIS = open('mcd_surf.out', "r")

#Read lines of output from visual.out parsing them into 
#a multi-dim array and taking the final line off

oldlines = VIS.readlines()
newlines = []                   #newlines is the striped down lines
tmp = []
words = []                      #words is the split array fo newlines

#loop strips and splits lines
for i in xrange(len(oldlines)):
  newlines.append(oldlines[i].rstrip())
  tmp = newlines[i].split("\t")
  words.append(tmp)

#This bit of code loops the input to find the maximum values
x_cord = -1;y_cord = -1;z_cord = -1

for i in xrange(len(words)):
  if int(words[i][1]) > x_cord:
    x_cord = int(words[i][1])
  if int(words[i][2]) > y_cord:
    y_cord = int(words[i][2])
  if int(words[i][3]) > z_cord:
    z_cord = int(words[i][3])
  if (len(words[i][:]) > 4):
    if int(words[i][5]) > x_cord:
      x_cord = int(words[i][5])
    if int(words[i][6]) > y_cord:
      y_cord = int(words[i][6])
    if int(words[i][7]) > z_cord:
      z_cord = int(words[i][7])

total = x_cord*y_cord
print x_cord,y_cord

#*_place keeps track of the (x,y) coordinate on the linear atom list
#surf_height tells at which height (x,y) is currently
orig_place = [ [ [ 0 for z in range(z_cord)] for y in range(y_cord)] for x in range(x_cord)]

#orig array holds xyz atom list. This loop initially fills it.

for i in xrange(y_cord):
  for j in xrange(x_cord):
    orig_place[j][i][0] = 1

orig = [] 
orig.append("%d" % total)
orig.append(" comment line")
n=2
for i in xrange(x_cord):
  for j in xrange(y_cord):
    for k in xrange(z_cord):
      if (orig_place[i][j][k] == 1):
          orig.append(("A %5.1f\t%5.1f\t%5.1f" % (j,i,k)))
          n+=1

FILE = open('output.xyz', "a")
#print array to output.xyz
for i in xrange(len(orig)):
 print >> FILE, orig[i]

for k in xrange(len(words)):
 if (words[k][0] == "Add"):
  orig_place[int(words[k][1])-1][int(words[k][2])-1][int(words[k][3])-1] = 1
  total = total + 1
  if ( len(words[k][:]) > 4): 
   if ( words[k][4] == "Take"):
    total = total - 1
    orig_place[int(words[k][5])-1][int(words[k][6])-1][int(words[k][7])-1] = 0
 if (words[k][0] == "Take"):
  total = total - 1
  orig_place[int(words[k][1])-1][int(words[k][2])-1][int(words[k][3])-1] = 0
  if ( len(words[k][:]) > 4): 
   if ( words[k][4] == "Take"):
    total = total - 1
    orig_place[int(words[k][5])-1][int(words[k][6])-1][int(words[k][7])-1] = 0

 orig = [] 
 orig.append("%d" % total)
 orig.append(" comment line")
 for i in xrange(x_cord):
   for j in xrange(y_cord):
     for k in xrange(z_cord):
       if (orig_place[i][j][k] == 1):
           orig.append(("A %5.1f\t%5.1f\t%5.1f" % (j,i,k)))

 for i in xrange(len(orig)):
  print >> FILE, orig[i]

FILE.close()
VIS.close()

