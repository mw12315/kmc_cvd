#!/bin/sh
#----------------------------------------------
# $1 name of input file exluding 'input' suffix
# $2 wall time hh:mm:ss
#
# -Copies awk script to folder
# -Makes a script named go_$1.sh and gives it write privilages
#----------------------------------------------
cp $HOME/scripts/vis.awk ./
echo "#!/bin/tcsh -f" > $PWD/go_$1.sh
chmod +x go_$1.sh
echo "cd \$PBS_O_WORKDIR" >> go_$1.sh
echo "\$HOME/local/bin/kmccvd<<EOF" >> go_$1.sh
echo "$1" >> go_$1.sh
echo "EOF" >> go_$1.sh
echo "output: $PWD/go_$1.sh"
