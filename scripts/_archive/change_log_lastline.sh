#!/bin/bash
#
lines=$(wc -l $HOME/bin/kmc/version_archive/changelog.txt | awk '{print $1}')
lines=$(($lines -2))
touch changelog.temp
cat $HOME/bin/kmc/version_archive/changelog.txt | head -qn${lines} >> $HOME/bin/kmc/version_archive/changelog.temp
mv $HOME/bin/kmc/version_archive/changelog.temp $HOME/bin/kmc/version_archive/changelog.txt
