#! /bin/bash
rm -f diamond.out diamond.json
rm -f bookmark.log
rm -f surface_connectivity.csv
rm -f events*.csv events.out nuc_events.csv
rm -f surface_height.csv
rm -f activated_fraction.csv
rm -f rate_reg_tot.csv
rm -f rate_vals.csv
rm -f dimer_count.csv
rm -f dt.csv
rm -f fort.*
rm -f non0rates.txt
rm -f errors.txt
rm -f warnings.txt
rm -f final_surface.xyz
rm -f frames_surface*.xyz
rm -f frames_*.xyz
rm -f vis_errors.txt
rm -f vis_warnings.txt
rm -f kmc_vis.xyz
rm -f *sh.e*
rm -f *sh.o*
rm -f *.xyz
rm -f growthrate*.csv
rm -f height*.csv
rm -f atoms_envs.csv
