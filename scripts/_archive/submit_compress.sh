#!/bin/sh
echo "#!/bin/tcsh -f" > $PWD/compress_$1.sh
chmod +x $PWD/compress_$1.sh
echo "#PBS -l walltime=15:00:00,nodes=1:ppn=1" >> $PWD/compress_$1.sh
echo "cd \$PBS_O_WORKDIR" >> $PWD/compress_$1.sh
echo "gzip -5 $1" >> $PWD/compress_$1.sh
echo  "$PWD/compress_$1.sh output to $PWD"
if [ "$2" == "sub" ]
then
	qsub -mabe $PWD/compress_$1.sh
	rm  $PWD/compress_$1.sh
	echo "submitted and deleted $PWD/compress_$1.sh"
else
	echo "$PWD/compress_$1.sh ready to submit"
fi
