#!/bin/sh
for f in */
do
  cd $f
  pwd
  python3 $DL_SCRIPTS/../python_scripts/tools/copy_seed.py
  cd - > /dev/null
done
