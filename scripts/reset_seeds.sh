#!/bin/sh

for f in "$@"
do
  cd $f
  python3 $HOME/local/src/diamond_lattice/python_scripts/tools/reset_seed.py
  cd - > /dev/null
done
