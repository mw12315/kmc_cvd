#!/bin/bash
set -e

if [[ -z $1 ]]; then
    echo "Require at least one directory to inflate .xyz.gz files within"
    exit 1
fi

dirs="$@"
echo "inflate all kmc_vis.xyz files within ${dirs}"

for f in $dirs
do
    cd $f &> /dev/null
    find -path "*kmc_vis.xyz.gz" | xargs gzip -d
    cd - &> /dev/null
done
