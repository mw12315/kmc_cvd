#!/bin/bash
set -e

if [[ -z $1 ]]; then
    echo "Require at least one directory to inflate .xyz.gz files within"
    exit 1
fi

dirs="$@"
echo "inflate all final_surface.xyz files within ${dirs}"

for f in $dirs
do
    cd $f &> /dev/null
    find -path "*final_surface.xyz.gz" | xargs gzip -d
    cd - &> /dev/null
done
