#!/bin/bash
set -e

if [[ -z $1 ]]; then
    echo "Require at least one directory to compress .xyz files within"
    exit 1
fi

dirs="$@"
echo "compressing all .xyz files within ${dirs}"

for f in $dirs
do
    cd $f &> /dev/null
    echo -n "$f size: `(du -sh .)`"
    find -path "*.xyz" | xargs -n 1 gzip --best
    echo -n " -> `(du -sh .)`"
    echo ""
    cd - &> /dev/null
done
