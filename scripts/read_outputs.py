#!/users/mw12315/local/usr/bin/python3.6
import sys
import os
import datetime
from datetime import datetime

cwd = os.getcwd()

Ts=0
Tns=0
CH3conc=0.0E0
CH3s=0.0
CH3g=0.0
Hconc=0.0E0
H2conc=0.0E0
EtchRateFactor=0.0
TroughInsertFactor=0.0
Nrates=0
Seed=0
simTime=0.0
Niterations=0.0
CH3Dinsert=0.0
CH3Tinsert=0.0
CH3Etch=0.0
CH2Etch=0.0
percentMig=0.0
growthRate=0.0E0
Roughness=0.0E0
CPUtime=0.0

now = datetime.now()

date = now.strftime("%y%m%d")

with open('all_sims_'+date+'.csv','w') as dat:
    dat.write("Location,Nx, Ny, Nz, S_bas, Ts, Tns, [CH3], sCH3, gCH3, [H], [H2], Etch Factor, Trough Factor, Rates, Seed, Sim Time \ s, Iterations, Dimer Insert %, Trough Insert %, Etch %, Migrate %,Growth Rate m/h, Roughness /m, CPU Time\n")
    for f_tuple in os.walk(cwd):
        if "diamond.out" in f_tuple[2]:
            print("{}\\diamond.out".format(f_tuple[0]))
            with open(os.path.join(f_tuple[0],"diamond.out") ,'r') as f:
                lines = f.readlines()
                for index,line in enumerate(lines):
                    if "N(x)" in line:
                        split_line = lines[index+1].split()
                        Nx = split_line[0]
                        Ny = split_line[1]
                        Nz = split_line[2]
                        Nbasis = split_line[3]
                    if "Temperature" in line:
                        split_line = line.split()
                        Ts = split_line[-2].strip(",")
                        Tns = split_line[-1].strip(",")
                    if "[CH3]" in line:
                        split_line = line.split()
                        CH3conc = split_line[-3].strip(",")
                        CH3s = split_line[-2].strip(",")
                        CH3g = split_line[-1].strip(",")
                    if "[H]" in line:
                        split_line = line.split()
                        H2conc = split_line[-1].strip(",")
                        Hconc = split_line[-2].strip(",")
                    if "Etch Rate Factor" in line:
                        split_line = line.split()
                        EtchRateFactor = split_line[-1]
                    if "Trough Insertion rate" in line:
                        split_line = line.split()
                        TroughInsertFactor = split_line[-1]
                    if "Rates per Site: [detected]" in line:
                        split_line = line.split()
                        Nrates = split_line[-1]
                    if "Seeding Clock Value" in line:
                        split_line = line.split()
                        Seed = split_line[-1]
                    if "total time" in line:
                        split_line = line.split()
                        simTime = split_line[3]
                    if "iterations:" in line:
                        split_line = line.split()
                        Niterations = split_line[1]
                    if "Rate Counters" in line:
                        CH3_split = lines[index+2].split()
                        CH3Dinsert = CH3_split[2]
                        CH3Tinsert = CH3_split[4]
                        CH3Etch = CH3_split[6]
                    if "CH2:" in line:
                        split_line = line.split()
                        CH2Etch = split_line[-1]
                    if "Migration N:" in line:
                        split_line = lines[index + 7].strip()
                        percentMig = split_line
                    if "Ave Growth Rate" in line:
                        split_line = line.split()
                        growthRate = split_line[-1]
                    if "RMS Roughness" in line:
                        split_line = line.split()
                        Roughness = split_line[-1]
                    if "Total CPU" in line:
                        split_line = line.split()
                        CPUtime = split_line[-1]

            dat.write("{}/diamond.out,{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format(f_tuple[0],Nx,Ny,Nz,Nbasis,Ts,Tns,CH3conc,CH3s,CH3g,Hconc,H2conc,EtchRateFactor,TroughInsertFactor,Nrates,Seed,simTime,Niterations,CH3Dinsert,CH3Tinsert,CH3Etch+CH2Etch,percentMig,growthRate,Roughness,CPUtime))

