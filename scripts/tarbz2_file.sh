#!/bin/bash
# Pass a directory name and it will be archived and then compressed
cd $PBS_O_WORKDIR

if [[ -z $1 ]]
then
  echo "no input dir passed"
  exit 1
fi

tar -zcf "$1.tar.bz2" $1 --remove-files
echo "created $1.tar.bz2 and deleted $1" 
