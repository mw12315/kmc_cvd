from glob import glob
import argparse
import os
import random
import sys


def instruction(cwd: str, n: int, np: int, binary, pre_seeded=False):
    if pre_seeded:
        sleep = 0
    elif n > np:
        sleep = random.randint(0, 100)
    else:
        sleep = n

    return "(cd {:s}; sleep {}; {} &> std.out)".format(cwd, sleep, binary)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('n', type=int, help="number of jobs to run in parallel")
    parser.add_argument('-f', '--file', type=str, default="tasks.txt", help="file to store tasks", dest='f')
    parser.add_argument('-e', '--exe', type=str, default="~/local/bin/diamond", help="Path to executable", dest='e')
    parser.add_argument('--force', action="store_true", help="Force creation of 'started' simulations", dest="force")
    args = parser.parse_args()

    files = glob('**/simulation.input', recursive=True)
    targets = [os.path.split(f)[0] for f in files]

    np = args.n
    fname = args.f
    exe = args.e

    ntasks = 0
    with open(fname, 'w') as tasks:
        for n, t in enumerate(targets):
            if not os.path.exists(os.path.join(t, 'diamond.out')) or args.force:
                ntasks += 1
                print(instruction(t, n, np, os.path.abspath(os.path.expanduser(exe)), False), file=tasks)


print("Created {:s} containing {:d} tasks".format(fname, ntasks))
