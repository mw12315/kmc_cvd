#!/bin/sh
echo $1
rm -f $1/bookmark.log
rm -f $1/surface_connectivity.csv
rm -f $1/events*.csv 
rm -f $1/events.out
rm -f $1/nuc_events.csv
rm -f $1/surface_height.csv
rm -f $1/activated_fraction.csv
rm -f $1/rate_reg_tot.csv
rm -f $1/rate_vals.csv
rm -f $1/dimer_count.csv
rm -f $1/dt.csv
rm -f $1/fort.*
rm -f $1/non0rates.txt
rm -f $1/frames_surface*.xyz
rm -f $1/frames_*.xyz
rm -f $1/vis_errors.txt
rm -f $1/vis_warnings.txt
rm -f $1/kmc_vis.xyz
rm -f $1/*sh.e*
rm -f $1/*sh.o*
rm -f $1/growthrate*.csv
rm -f $1/height*.csv
rm -f $1/errors.txt
rm -f $1/warnings.txt
rm -f $1/atoms_envs.csv
