"""
Copy a slurm 3.0.5 submission script (default binary: ~/local/bin/diamond) to all directories containing the simulation.input
input file, naming the script according to the containing directory name and a prefix (if passed)
"""
from glob import glob
from shutil import copyfile
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-p", help="script name prefix", dest="prefix", default="")
parser.add_argument("-L", help="path to submission script to copy", dest="loc",
                    default=os.path.join(os.environ.get("DL_SCRIPTS"), "submit", "go_diamond.sh"))

args = parser.parse_args()

files = glob('**/simulation.input', recursive=True)
targets = [os.path.split(f)[0] for f in files]

with open("submit_scripts.log", "w") as f:
    for n, target in enumerate(targets):
        f.write(f'{n} {target}\n')
        script_name = args.prefix + os.path.split(target)[1] + ".sh"
        copyfile(os.path.abspath(args.loc), os.path.join(target, script_name))

print(f'copied {args.loc} to {n + 1} directories')
