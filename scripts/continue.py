"""
Copy input files from dir to dir/dir_cont
"""
import os
from sys import argv
from glob import glob
from shutil import copy, copyfile

def dirs():
  if len(argv) == 1:
    print("Usage: {} sim_dir1 sim_dir2 ..".format(argv[0]))
    exit(1)

  dir_list = argv[1:]
  for f in dir_list:
    if not os.path.isdir(f):
      print(f,"not a directory")
      exit(1)

  return dir_list


def create_continuation_dirs(directories):
  for d in directories:
    
    cont_dir_name = os.path.join(d, os.path.basename(d)+"_cont")

    os.mkdir(cont_dir_name)
    print(cont_dir_name)

    # Copy key files into subdirectory
    files = glob(os.path.join(d, "*.input")) + glob(os.path.join(d, "bookmark.*")) + glob(os.path.join(d, "*.sh"))
    for f in files:
      if '.sh' not in f:
        copy(f, cont_dir_name)
      else:
        split_sub_sh = os.path.splitext(os.path.split(f)[1])
        copyfile(f, os.path.join(cont_dir_name, split_sub_sh[0]+"_cont"+split_sub_sh[1]))
    
    

if __name__ == "__main__":
  dir_list = dirs()

  create_continuation_dirs(dir_list)
