"""Plot the dimer surface counts for 2 separate files, named dimer_count.csv and dimer_count2.csv"""
import matplotlib.pyplot as plt
import numpy as np

from numpy import genfromtxt

data = genfromtxt('dimer_count.csv',delimiter=',',skip_header=1)
data2 = genfromtxt('dimer_count2.csv',delimiter=',',skip_header=1)

fig = plt.figure()
fig.suptitle(input("Title: "))
axes = fig.add_subplot(1,1,1)
axes.set_ylabel("Surface Dimers")
axes.set_xlabel("Simulation Time /s")


if input("'step' or 'time'?: ") == "step":
    x = data[:,0]
    x2= data2[:,0]
else:
    x = data[:,1]
    x2= data2[:,1]

dimers = data[:,-1]
dimers2=data2[:,-1]


axes.plot(x,dimers,label="Reconstructed Surface")
axes.plot(x,dimers2,label="Unreconstructed Surface")

axes.legend(loc='upper right')
axes.set_ylim(0,25)

plt.show()
