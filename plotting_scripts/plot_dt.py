import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import sys

if len(sys.argv) != 2:
  error = "Invalid number of arguments: {}, required: 1".format(len(sys.argv)-1)
  sys.exit(error)
file = sys.argv[1]

data = np.genfromtxt(file,skip_header=1,delimiter=",")

while True:
  try:
    data_skip = int(input('Data Skip: '))
    window_size = int(input('Moving Average Window Size: '))
    break
  except ValueError:
    print("Invalid")


# Moving Average(ignore edges)
half_window = int(round(window_size/2,0))
total_values = len(data[:,0]) - half_window*2 - 1
m_ave = np.zeros((total_values,3))
i = -1

for point in range(half_window,len(data[:,0])-half_window-1):
     i +=1
     m_ave[i,0] = data[point,0]
     m_ave[i,1] = data[point,2]
     # average of window
     window = np.append(data[point-half_window:point+half_window,1],data[point,1])
     m_ave[i,2] = np.average(window)

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.plot(data[::data_skip,0],data[::data_skip,1])
ax.plot(m_ave[::data_skip,0],m_ave[::data_skip,2],label='Moving Average - window size: {}'.format(window_size))
ax.set_xlabel("Iteration")
ax.set_ylabel("Time Step (s)")
fig.legend()

fig2 = plt.figure()
ax2 = fig2.add_subplot(1,1,1)
ax2.plot(data[::data_skip,2],data[::data_skip,1])
ax2.plot(m_ave[::data_skip,1],m_ave[::data_skip,2],label='Moving Average - window size: {}'.format(window_size))
ax2.set_xlabel("Simulation Time (s)")
ax2.set_ylabel("Time Step (s)")
fig2.legend()

ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
ax.set_yscale('log')
ax2.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
ax2.set_yscale('log')

plt.show()
