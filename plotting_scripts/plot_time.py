"""plot the time of the simulation as a function of time, which gives an overall impression of the size of the timestep throughout a simulation"""
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

data = np.genfromtxt('events.csv',skip_header=1,delimiter=",",usecols=(0,1))

steps = data[:,0]
time  = data[:,1]

figure = plt.figure()
axes = figure.add_subplot(1,1,1)
axes.set_ylabel('Steps')
axes.set_xlabel('Simulated Time / s')
axes.ticklabel_format(style='sci',scilimits=(0,2),axis='y')

axes.plot(time,steps,label='Simulated Time wrt. Simulation Steps')

plt.show()