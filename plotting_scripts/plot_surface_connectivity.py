#!/usr/bin/python3
"""Plot surface connectivity data output from diamond_lattice, reading .csv files named surface_connectivity.csv"""

import matplotlib.pyplot as plt
import numpy as np

data = np.genfromtxt('surface_connectivity.csv',delimiter=',',skip_header=1)

fig = plt.figure()
fig.suptitle(input("Title: "))
axes1 = fig.add_subplot(1,1,1)
axes1.set_ylabel("Number of Carbon Environments")

if input("'time' or 'step': ") == "step":
    x = data[:,0] # step
    axes1.set_xlabel("Step")
else:
    x = data[:,1] # time
    axes1.set_xlabel("Simulation Time /s")

N1 = data[:,2]
N2 = data[:,3]
N3 = data[:,4]
total = data[:,-1]

axes1.plot(x,N1,label="1 Nearest Neighbour")
axes1.plot(x,N2,label="2 Nearest Neighbours")
axes1.plot(x,N3,label="3 Nearest Neighbours")
axes1.plot(x,total,label="Total Surface Atoms")

axes1.legend(loc='upper right')

plt.show()
