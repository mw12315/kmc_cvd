import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import sys

# create axis

f = plt.figure()
ax = f.add_subplot(1,1,1)

arg_list = sys.argv
arg_list.pop(0)

print('2:adsch\n3:insch\n4:etch\n5:mig\n6:migG\n7:migD')
column = int(input('Column: '))

if len(sys.argv) > 1:
  for i,arg in enumerate(arg_list):
    print(i+1,arg)
    data = np.genfromtxt(arg,skip_header=1,delimiter=",",usecols=(1,column))
    label = arg[10:14]
    print(label)
    ax.plot(data[:,0],data[:,1],label=label+"_"+str(column))



plt.legend()
plt.show()

