from matplotlib import pyplot as plt
import numpy as np
from sys import argv
import re


def y_n(message, default):
    """
    :param message: input() message (str)
    :param default: default value if no value entered (y/n)
    :return: valid y or n return value
    """

    return_dict = {'y': True, 'n': False}

    while True:
        value = input("{} ({}): ".format(message, default))
        if not value:
            return return_dict[default]
        if value.lower() in ["y", "n"]:
            return return_dict[value.lower()]
        else:
            print("Please enter 'y','n' or nothing for (default) value")


def moving_average(array, window):
    """
    :param array: 2D list/numpy array
    :param window: size of moving average window
    :return: 2D array of moving average data
    """
    # Ensure that window is an even integer
    if window % 2 != 0: window += 1

    # Set up returned y list
    y_out = []
    # Split input data into two 1D arrays
    x, y = np.hsplit(array, 2)
    x = x[window // 2:-(window // 2)]
    for i in range(len(y)):
        if window // 2 <= i <= len(y) - window // 2 - 1:
            ave = []
            for j in range(i - window // 2, i + 1 + (window // 2)):
                ave.append(y[j])
            y_out.append(np.average(ave))

    return np.column_stack((x, y_out))


pat = re.compile("\w*/(\w*).\w+")
# Create plot environment
fig, ax = plt.subplots()
if y_n("Time on x axis", "y"):
    x_type = "time"
    ax.set_xlabel("Simulation Time / s")
else:
    x_type = "step"
    ax.set_xlabel("Simulation Iteration")

if y_n("Plot Dimer Insertion", "y"):
    dimer_insert_plot = True
else:
    dimer_insert_plot = False

if y_n("Plot Trough Insertion", "y"):
    trough_insert_plot = True
else:
    trough_insert_plot = False

if y_n("Plot Etch", "y"):
    etch_plot = True
else:
    etch_plot = False

if y_n("Plot Migration", "y"):
    migration_plot = True
else:
    migration_plot = False

if y_n("Plot Total", "y"):
    total_plot = True
else:
    total_plot = False

if y_n("Plot non moving average graphs?","n"):
    all_graphs = True
else:
    all_graphs = False

# Loop over files within arguments list
for file in argv[1:]:
    data = np.genfromtxt(fname=file, skip_header=2, delimiter=",")
    f_name = pat.search(file).group(1)
    print(f_name)
    while True:
        try:
            d_skip = int(input('Iteration Skip: '))
            ma_window = int(input('Moving Average Window Size (even int): '))
            break
        except ValueError:
            print('Invalid integer entered')

    if x_type == "time":
        x = data[:, 1]
    else:
        x = data[:, 0]

    # Copy y data into 1D numpy lists
    trough = data[:, 2]
    dimer = data[:, 3]
    etch = data[:, 4]
    migration = data[:, 5]
    total = data[:, 6]

    # Plot required columns for current file
    if dimer_insert_plot:
        if all_graphs: ax.plot(x[::d_skip], dimer[::d_skip], label="D Ins {}".format(f_name))
        m_ave = moving_average(np.column_stack((x, dimer)), ma_window)
        ax.plot(m_ave[:, 0], m_ave[:, 1], label="D Ins {}_ma {}".format(ma_window, f_name), linestyle='--')
    if trough_insert_plot:
        if all_graphs: x.plot(x[::d_skip], trough[::d_skip], label="T Ins {}".format(f_name))
        m_ave = moving_average(np.column_stack((x, trough)), ma_window)
        ax.plot(m_ave[:, 0], m_ave[:, 1], label="T Ins {}_ma {}".format(ma_window, f_name), linestyle='--')
    if etch_plot:
        if all_graphs: ax.plot(x[::d_skip], etch[::d_skip], label="Etch {}".format(f_name))
        m_ave = moving_average(np.column_stack((x, etch)), ma_window)
        ax.plot(m_ave[:, 0], m_ave[:, 1], label="Etch {}_ma {}".format(ma_window, f_name), linestyle='--')
    if migration_plot:
        if all_graphs: ax.plot(x[::d_skip], migration[::d_skip], label="Migration {}".format(f_name))
        m_ave = moving_average(np.column_stack((x, migration)), ma_window)
        ax.plot(m_ave[:, 0], m_ave[:, 1], label="Migration {}_ma {}".format(ma_window, f_name), linestyle='--')
    if total_plot:
        if all_graphs: ax.plot(x[::d_skip], total[::d_skip], label="Total {}".format(f_name))
        m_ave = moving_average(np.column_stack((x, migration)), ma_window)
        ax.plot(m_ave[:, 0], m_ave[:, 1], label="Total {}_ma {}".format(ma_window, f_name), linestyle='--')


ax.set_ylabel("Rate Sum / $s^{-1}$")
plt.legend()
plt.show()
