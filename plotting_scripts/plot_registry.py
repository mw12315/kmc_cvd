#!/usr/bin/python3
import matplotlib.pyplot as plt
import numpy as np
import sys

if len(sys.argv) != 2:
    print("Usage: {} path/to/reg_file.csv".format(sys.argv[0]))
    sys.exit(1)

file_name = sys.argv[1]
registerData = np.genfromtxt(file_name, delimiter=',', skip_header=1)

x = registerData[:, 1]

total = registerData[:, -1]

data_skip = int(input("Data Point Skip: (default = 1) "))
if not data_skip:
    data_skip = 1

x = x[::data_skip]
total = total[::data_skip]

percentage = input("Percentage Output? (y/N): ")
if not percentage: percentage = "N"

ch_DInsert = input("Plot Dimer Insertion? (Y/n)? ")
if not ch_DInsert: ch_DInsert = "Y"
ch_TInsert = input("Plot Trough Insertion? (Y/n)? ")
if not ch_TInsert: ch_TInsert = "Y"
ch_Etch = input("Plot Etch? (Y/n)? ")
if not ch_Etch: ch_Etch = "Y"
ch_Migrate = input("Plot Migration? (Y/n)? ")
if not ch_Migrate: ch_Migrate = "Y"
ch_MigGap = input("Plot Migration in Gap? (Y/n)? ")
if not ch_MigGap: ch_MigGap = "Y"
ch_MigDown = input("Plot Migration Down? (Y/n)? ")
if not ch_MigDown: ch_MigDown = "Y"

if ch_DInsert == "Y":
    DInsert = registerData[:, 2]
    DInsert = DInsert[::data_skip]
    if percentage == "Y":
        DInsert = (DInsert / total) * 100
if ch_TInsert == "Y":
    TInsert = registerData[:, 3]
    TInsert = TInsert[::data_skip]
    if percentage == "Y":
        TInsert = (TInsert / total) * 100
if ch_Etch == "Y":
    Etch = registerData[:, 8]
    Etch = Etch[::data_skip]
    if percentage == "Y":
        Etch = (Etch / total) * 100
if ch_Migrate == "Y":
    Migrate = registerData[:, 5]
    Migrate = Migrate[::data_skip]
    if percentage == "Y":
        Migrate = (Migrate / total) * 100
if ch_MigGap == "Y":
    MigG = registerData[:, 6]
    MigG = MigG[::data_skip]
    if percentage == "Y":
        MigG = (MigG / total) * 100
if ch_MigDown == "Y":
    MigD = registerData[:, 7]
    MigD = MigD[::data_skip]
    if percentage == "Y":
        MigD = (MigD / total) * 100

print("done")

title = input("Graph Title: ")

plt.figure()
axes = plt.subplot(1, 1, 1)
axes.set_title(title)
axes.set_xlabel("Time / s")

if percentage == "Y":
    axes.set_ylabel("Percentage of Rate in Registry")
else:
    axes.set_ylabel("Rates in Rate Registry")
print("Created Axes")

if ch_Etch == "Y":
    axes.plot(x, Etch, label='Etch Rates')
    print("Plot Etching")
if ch_TInsert == "Y":
    axes.plot(x, TInsert, label='Trough Insert Rates')
    print("Plot Trough Insertion")
if ch_DInsert == "Y":
    axes.plot(x, DInsert, label='Dimer Insert Rates')
    print("Plot Dimer Insertion")
if ch_Migrate == "Y":
    axes.plot(x, Migrate, label='Migration Rates')
    print("Plot Plot Migration")
if ch_MigGap == "Y":
    axes.plot(x, MigG, label='Gap Migration Rates')
if ch_MigDown == "Y":
    axes.plot(x, MigD, label='Migration Down Rates')

# axes.set_xlim(0,2)
# axes.set_ylim(0,60)

axes.legend(loc="upper right")
print("Display Plots:")
plt.show()
