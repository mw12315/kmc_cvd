import numpy as np
import glob
import matplotlib
from matplotlib import pyplot as plt

files = glob.glob("roughness/*roughness*.csv")

figure = plt.figure()
ax = figure.add_subplot(2, 1, 1)


values =  []
gvalues = []
for file in files:
    data = np.genfromtxt(file, delimiter=",", usecols=(1, 2),skip_header=1)
    label = file[10:15]
    gvalues.append(int(file[10:12]))
    ax.plot(data[:, 0], data[:, 1], label=label)
    std_error = data[:,1].std()
    print("{}: Roughness std: {:10.3e} nm".format(label, std_error/1E-9))
    values.append(std_error/1E-9)
plt.legend()
ax2 = figure.add_subplot(2, 1, 2)
print(gvalues)
print(values)
ax2.scatter(gvalues, values, label="Roughness std")


# plt.legend(loc="upper left", bbox_to_anchor=(1,1))
plt.legend()
plt.show()



