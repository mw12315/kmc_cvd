import numpy as np
import matplotlib
import matplotlib.pyplot as plt

data1 = np.genfromtxt("activated_fraction1.csv",delimiter=",",skip_header=1,autostrip='on',usecols=(0,1,3))
data2 = np.genfromtxt("activated_fraction2.csv",delimiter=",",skip_header=1,autostrip='on',usecols=(0,1,3))
data3 = np.genfromtxt("activated_fraction3.csv",delimiter=",",skip_header=1,autostrip='on',usecols=(0,1,3))

figure = plt.figure()
axis = figure.add_subplot(1,1,1)

axis.set_xlabel("Simulation Step")
axis.set_ylabel("Percentage of Surface Atoms Activated")

axis.scatter(data1[0:1000,0],data1[0:1000,2],label="Old",s=0.5)
axis.scatter(data3[0:1000,0],data3[0:1000,2],label="New2",s=5)
axis.scatter(data2[0:1000,0],data2[0:1000,2],label="New1",s=2.5)


plt.legend()
plt.show()
