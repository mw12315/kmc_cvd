import matplotlib.pyplot as plt
import numpy as np

from numpy import genfromtxt

data = genfromtxt('dimer_count.csv',delimiter=',',skip_header=1)

fig = plt.figure()
fig.suptitle(input("Title: "))
axes = fig.add_subplot(1,1,1)
axes.set_ylabel("Surface Dimers")


if input("'step' or 'time'?: ") == "step":
    x = data[:,0]
else:
    x = data[:,1]

dimers = data[:,-1]


axes.plot(x,dimers,label="Surface Dimers")

axes.legend(loc='upper right')

plt.show()