"""Plot roughness data output from diamond_lattice, reading .csv files named roughness<frame_range>.csv"""

import matplotlib.pyplot as plt
import numpy as np
import csv
import glob

path = "./roughness*.csv"
file_name = glob.glob(path)

choice = input("'time' or 'step' based plotting: ")
if not choice:
    choice =  "time"

if choice == 'time':
    data = np.genfromtxt(file_name[0],delimiter=',',skip_header=1, usecols=(1,2))
else:
    data = np.genfromtxt(file_name[0], delimiter=',', skip_header=1,usecols=(0,2))

plt.rc('font', size=14)          # controls default text sizes
plt.rc('axes', titlesize=12)     # fontsize of the axes title
plt.rc('axes', labelsize=16)     # fontsize of the x and y labels
plt.rc('xtick', labelsize=14)    # fontsize of the tick labels
plt.rc('ytick', labelsize=14)    # fontsize of the tick labels
plt.rc('legend', fontsize=14)    # legend fontsize
plt.rc('figure', titlesize=14)   # fontsize of the figure title

fig = plt.figure()
#fig.suptitle(input("Title: "))
axes1 = fig.add_subplot(1,1,1)
axes1.set_ylabel("RMS Roughness / nm")



x = data[:,0]
if choice == 'time':
    axes1.set_xlabel("Simulation Time / s")
else:
    axes1.set_xlabel("Simulation Steps / s")

#Set xticks
#xtic = np.arange(0,1300,100)
#axes1.set_xticks(xtic)
#Set Axis Limits
#axes1.set_xlim(35,1250)
#axes1.set_ylim(0.2,0.5  )

roughness = data[:,-1]/(1E-9)

axes1.plot(x,roughness,label="RMS Roughness")

axes1.legend(loc='upper left')

plt.show()
