import matplotlib.pyplot as plt
import numpy as np
from sys import argv, exit

if len(argv) != 3:
    print("Usage: {} cumulative_event_counts.csv rate_to_plot".format(argv[0]))
    exit(1)
else:
    f = argv[1]
    r = int(argv[2])

# Rate labels
rate_labs = ['D_insert_CH3',
             'D_insert_CH2',
             'D_insert_CH',
             'D_insert_C',
             'T_insert_CH3',
             'T_insert_CH2',
             'T_insert_CH',
             'T_insert_C',
             'C2H2 M1a',
             'C2H2 M1b',
             'C2H2 M2',
             'C2H2 M3',
             'D migration r',
             'D migration c',
             'G migration A',
             'G migration B',
             'G migration C',
             'G migration D',
             'Down migration',
             'C2H2 birad. migration',
             'C2H2 migration',
             'C2H2-CH3 etch',
             'C2H2 etch'
             ]
print('Load in data')
data = np.genfromtxt(f, skip_header=1)
print('loaded {} lines'.format(len(data[:, 1])))

fig, ax = plt.subplots()

ax.set_xlabel("Simulation Time /s")

ax.plot(data[:, 1], data[:, r+1], label=rate_labs[r])

ax.legend()
plt.show()
