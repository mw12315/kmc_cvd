"""Plot height data output from diamond_lattice, reading .csv files named height<frame_range>.csv"""

import matplotlib.pyplot as plt
import numpy as np
import csv
import glob

path = "./height*.csv"
file_name = glob.glob(path)

data = np.genfromtxt(file_name[0],delimiter=',',skip_header=1)

fig = plt.figure()
fig.suptitle(input("Title: "))
axes1 = fig.add_subplot(1,1,1)
axes1.set_ylabel("Height /m")

if input("'time' or 'step': ") == "step":
    x = data[:,0] # step
    axes1.set_xlabel("Step")
else:
    x = data[:,1] # time
    axes1.set_xlabel("Simulation Time /s")

height = data[:,-1]

axes1.plot(x,height,label="Average Surface Height")

axes1.legend(loc='upper right')

plt.show()