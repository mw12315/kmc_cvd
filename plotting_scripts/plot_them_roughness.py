import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import sys

"""plot roughness traces (time) for all 4 types of roughness collected by diamond_lattice"""

files = sys.argv[1:]
# files = ['r1_down_roughness.csv', 'r1_none_roughness.csv']

# Create Figure
figure1 = plt.figure(1, figsize=(6.4, 4.8), dpi=200)
figure2 = plt.figure(2, figsize=(6.4, 4.8), dpi=200)
figure3 = plt.figure(3, figsize=(6.4, 4.8), dpi=200)
figure4 = plt.figure(4, figsize=(6.4, 4.8), dpi=200)

glob_rms = figure1.add_subplot(1, 1, 1)
NN2_rms  = figure2.add_subplot(1, 1, 1)
NN3_rms  = figure3.add_subplot(1, 1, 1)
NN4_rms  = figure4.add_subplot(1, 1, 1)

glob_rms.set_title('Global RMS Roughness')
NN2_rms.set_title('NN2 RMS Roughness')
NN3_rms.set_title('NN3 RMS Roughness')
NN4_rms.set_title('NN4 RMS Roughness')

glob_rms.set_xlabel("Simulation Time /s")
NN2_rms.set_xlabel("Simulation Time /s")
# NN2_rms.set_ylim((0.02, 0.06))
NN3_rms.set_xlabel("Simulation Time /s")
NN4_rms.set_xlabel("Simulation Time /s")

glob_rms.set_ylabel("RMS Roughness /nm")
NN2_rms.set_ylabel("RMS Roughness /nm")
NN3_rms.set_ylabel("RMS Roughness /nm")
NN4_rms.set_ylabel("RMS Roughness /nm")

for index, file in enumerate(files):
    data = np.genfromtxt(file, skip_header=1, delimiter=",", usecols=(1, 2, 4, 5, 6))

    glob_rms.plot(data[:, 0], data[:, 1]/1E-9, label="{}_g".format(index))
    NN2_rms.plot(data[1:len(data[:,0]),0], data[1:len(data[:,0]), 2]/1E-9, label="{}_NN2".format(index))
    NN3_rms.plot(data[1:len(data[:,0]), 0], data[1:len(data[:,0]), 3]/1E-9, label="{}_NN3".format(index))
    NN4_rms.plot(data[1:len(data[:,0]), 0], data[1:len(data[:,0]), 4]/1E-9, label="{}_NN4".format(index))

glob_rms.legend()
NN2_rms.legend()
NN3_rms.legend()
NN4_rms.legend()

plt.show()
