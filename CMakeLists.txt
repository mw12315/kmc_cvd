project(diamond_lattice LANGUAGES Fortran)
cmake_minimum_required(VERSION 3.10)

set( CMAKE_Fortran_MODULE_DIRECTORY ${PROJECT_BINARY_DIR} )
set( PROFILE "" CACHE BOOL "compile using -pg (gprof)" )
set( JSONFORTRAN_PATH "" CACHE PATH "path/to/json-fortran.cmake" )

set(SOURCES
        src/randomise.f90
        src/random.f90
        src/derived_values.f90
        src/migration.f90
        src/types.f90
        src/etching.f90
        src/initialise.f90
        src/output.f90
        src/variables.f90
        src/basis.f90
        src/periodic.f90
        src/fileIO.f90
        src/enumerate.f90
        src/input.f90
        src/kinds.f90
        src/surface.f90
        src/dimer.f90
        src/rates.f90
        src/execute_rates.f90
        src/adsorb_atom.f90
        src/kmc.f90
        src/json.f90
	    src/segment.f90
        src/activate.f90
        src/printing.f90
        src/ide.f90
        src/bookmark.f90
        src/arrhenius.f90
        src/c2h2.f90
        src/pendant.f90
        src/finalise.f90
        src/assert.f90)

set (TEST_SRC
        src/testing/diamond_tests.f90
		src/testing/test_construct.f90)

# json-fortran library
set(CMAKE_MODULE_PATH ${JSONFORTRAN_PATH})
find_package( jsonfortran-${CMAKE_Fortran_COMPILER_ID} 7.1.0 REQUIRED PATHS
  ${CMAKE_MODULE_PATH})
include_directories( "${jsonfortran_INCLUDE_DIRS}" )

#set(CMAKE_Fortran_FLAGS "-ffree-form -ffree-line-length-none -std=f2008")
#if(${CMAKE_Fortran_COMPILER_ID} MATCHES "GNU")
#    message("GNU Fortran Compiler")
#    set(CMAKE_Fortran_FLAGS_DEBUG "-g -fbounds-check -fbacktrace \
#    -fdump-core")
#    set(CMAKE_Fortran_FLAGS_RELEASE "-O3")
#else()
#    message("Non-GNU Fortran Compiler?")
#    message("${CMAKE_COMPILER_ID}")
#endif()

# Compiler flags
include(cmake/CompilerFlags.cmake)
# Compiler warnings
include(cmake/CompilerWarnings.cmake)

if(PROFILE)
    message("Profiling with gprof on")
    if(${CMAKE_Fortran_COMPILER_ID} MATCHES "GNU")
        set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -pg -g -fno-omit-frame-pointer -O2 -DNDEBUG -fno-inline-functions -fno-inline-functions-called-once -fno-optimize-sibling-calls")
        set(CMAKE_BUILD_TYPE  "Debug")
    endif()
endif()

message(${CMAKE_BUILD_TYPE})
if(CMAKE_BUILD_TYPE STREQUAL Release)
    message("Active Fortran Flags: ${CMAKE_Fortran_FLAGS} ${CMAKE_Fortran_FLAGS_RELEASE}")
else()
     message("Active Fortran Flags: ${CMAKE_Fortran_FLAGS} ${CMAKE_Fortran_FLAGS_DEBUG}")
endif()

add_executable(diamond ${SOURCES} src/main.f90)
add_executable(test_diamond ${SOURCES} ${TEST_SRC} src/testing/test_main.f90)
target_link_libraries(diamond jsonfortran-static)
target_link_libraries(test_diamond jsonfortran-static)
